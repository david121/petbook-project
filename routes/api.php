<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API ROUTES
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register','UserController@register');
// Auth::routes(["verify"=>true])
//Route::post('/register', 'UserController@register');

//followers
//Route::post('/followers', 'UserController@followers');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->post('/friends/request', 'FriendsController@request');
Route::middleware('auth:api')->post('/friends/confirm', 'FriendsController@confirm');
Route::middleware('auth:api')->post('/friends/cancelRequest', 'FriendsController@cancel');
Route::middleware('auth:api')->get('/friends/requests', 'FriendsController@getRequests');
Route::middleware('auth:api')->get('/friends', 'FriendsController@getFriends');

Route::middleware('auth:api')->group(function () {
Route::resources([
        '/posts'=>'PostsController',
        '/comments'=>'CommentsController',
        '/likes' => 'LikesController',
        '/hobbies'=>'HobbiesController',
        '/activities' => 'UserActivitiesController',
        '/friendship' => 'Friendship',
]);

    Route::get('/chat/check', 'ChatsController@checkChatExists');
    Route::get('/chat/fetch', 'ChatsController@fetchChats');
    Route::post('/chat/update', 'ChatsController@updateChat');
    Route::post('/chat/store', 'ChatsController@storeChat');
    Route::post('/chat/messageUnread', 'ChatsController@messageUnread');
    Route::post('/chat/fromSearch', 'ChatsController@fromSearch');

    Route::post('/testMail', 'ChatsController@test');


    Route::get('/notifications', 'UserController@Notifications');
    Route::post('/notification/checked', 'UserController@NotificationChecked');
    Route::post('/notifications/allRead', 'UserController@NotificationsRead');


    Route::get('/fact', 'FunFactsController@getFact');
//    Route::get('/david', 'UserController@David');
//    Route::get('/search', 'SearchController@search');
    Route::get('/haystack', 'SearchController@search');
    Route::get('/suggestPets', 'PetController@petSuggestion');
    Route::get('/listings', 'ListingController@listings');
    Route::get('/my-listings', 'ListingController@myListings');
    Route::post('/listings/list', 'ListingController@listPet');
    Route::post('/listings/delete', 'ListingController@deleteListing');
    Route::post('/listings/view', 'ListingController@listingView');
    Route::post('/listing/filter', 'ListingController@filtered_listing');

    Route::post('/commentLike', 'CommentLikesController@like');
    Route::post('/comments/reply', 'CommentsController@reply');
    Route::post('/admire', 'AdmirersController@admire');
    Route::post('/admire/cancel', 'AdmirersController@cancelAdmiration');
    Route::post('/admire/unadmire', 'AdmirersController@unAdmireFromVisitor');

    Route::post('/archive/add', 'PostArchiveController@addPostToArchive');
    Route::post('/archive/hide', 'PostArchiveController@hidePostInArchive');
    Route::get('/archive', 'PostArchiveController@getArchivedPosts');
    Route::get('/archive/hidden', 'PostArchiveController@getHiddenPosts');
    Route::post('/archive/restore', 'PostArchiveController@restoreHiddenPost');
    Route::post('/archive/unsave', 'PostArchiveController@unSavePost');
    Route::post('/user_cover', 'UserController@uploadCover');
    Route::post('/pet', 'PetController@create');
    Route::get('/pet/admirers', 'PetController@admirers');
    Route::post('/petBio', 'PetController@bio');
    Route::post('/pet/changeOwnership', 'PetController@changeOwnership');
    Route::get('/fetchQuestions', 'PetQuestionsController@questions');
    Route::get('/fetchType', 'PetQuestionsController@types');
    Route::get('/fetchBreed', 'PetQuestionsController@breeds');
    Route::get('/fetchGender', 'PetQuestionsController@gender');
    Route::get('/fetchPets', 'PetController@fetch');
    Route::get('/pets/similarPets', 'PetController@similarPets');
    Route::get('/findBreed', 'PetController@findBreed');
    Route::post('/pet/update', 'PetController@updatePet');
    Route::post('/pet/destroy', 'PetController@deleteProfile');
    Route::get('/activePets', 'PetController@activePets');
    Route::post('/user/bio', 'UserController@userBio');
    Route::post('/user/city', 'UserController@userCity');
    Route::get('/fetchPet', 'PetController@fetchPet');
    Route::get('/visitPetPosts', 'PetController@visitPetPosts');
    Route::get('/petPosts', 'PetController@petPosts');
    Route::get('/fetchProfile', 'UserController@fetchVisitingProfile');
    Route::get('/admiredPets', 'UserController@admiredPets');
    Route::get('/nearestUsers', 'UserController@nearestUsers');
    Route::get('/getUser', 'UserController@getUser');
    Route::get('/userPosts', 'UserController@userPosts');
    Route::get('/aboutBreed', 'BreedController@aboutBreed');
    Route::post('/mobilePost', 'PetController@mobilePost');
    Route::post('/resendCode', 'UserController@ReSendEmailVerificationCode');
    Route::post('/verifyUser', 'UserController@verifyUser');
    Route::post('/location', 'UserController@locationToggle');
    Route::get('/sentRequests', 'FriendsController@sentRequests');
});

Route::get('/browse-pets', 'PetController@browse');
Route::post('/checkEmailExists', 'UserController@checkEmailExists');
Route::post('/recoveryTestPassed', 'UserController@SendPasswordRecoveryMail');
Route::post('/verifyRecoveryCode', 'UserController@verifyRecoveryCode');
Route::post('/changePassword', 'UserController@changePassword');
Route::get('/xyz000726', 'UserController@Existing');

//TEST ROUTES

Route::post('/test_comment', 'SampleController@store_comments');
Route::get('/test_comment', 'SampleController@retrieve_comments');


Route::get('/test_fetch_types', 'PetTypeController@index');
Route::post('/test_insert_breed', 'PetBreedController@store');
Route::post('/test_find_breed', 'PetTypeController@store');

