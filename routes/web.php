<?php

/*
|--------------------------------------------------------------------------
| Web ROUTES
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/{path?}', 'react')->where('path', '.*');

/*
Route::get('mailable', function () {
    $user = \App\User::find(10);

    return new App\Mail\ValidateRegistration($user);

});
*/

/*
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
*/
