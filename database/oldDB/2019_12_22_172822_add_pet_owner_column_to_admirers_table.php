<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPetOwnerColumnToAdmirersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admirers', function (Blueprint $table) {
            $table->bigInteger("pet_owner")->after("pet_id")->unsigned();
            $table->foreign("pet_owner")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admirers', function (Blueprint $table) {
            $table->dropColumn('pet_owner');
        });
    }
}
