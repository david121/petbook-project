<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('followers')->after('email')->default(0);
            $table->integer('liked_pets')->after('followers')->nullable()->default(0);
            $table->string('city')->after('liked_pets')->nullable()->default(null);
            $table->string('country')->after('city')->nullable()->default(null);
            $table->string('bio')->after('country')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['followers','liked_pets','city','country','bio']);
        });
    }
}
