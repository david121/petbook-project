<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmirersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admirers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("pet_id")->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete("RESTRICT")->onUpdate("RESTRICT");
            $table->foreign('pet_id')->references('id')->on('pets')->onDelete("RESTRICT")->onUpdate("RESTRICT");
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admirers');
    }
}
