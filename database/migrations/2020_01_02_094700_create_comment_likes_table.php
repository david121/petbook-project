<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table-> bigInteger('comment_id')->unsigned();
            $table-> bigInteger('user_id')->unsigned();
            $table->unique(['comment_id','user_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("RESTRICT");
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete("CASCADE")->onUpdate("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_likes');
    }
}
