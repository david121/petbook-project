<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->date('date_of_birth')->nullable();
            $table->tinyInteger('gender');
            $table->string('username', 255)->unique();
            $table->string('profile_photo', 255)->nullable();
            $table->text('bio')->nullable();
            //Removed pet type because its already stored in breed
            $table->bigInteger('breed_id')->unsigned();
            $table->foreign('breed_id')->references('id')->on('pet_breeds')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
