<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('email', 255)->unique();
            $table->string('username', 255)->unique()->nullable();
            $table->tinyInteger('profile_type');//It should be number. Always store numbers type, status .... 0 - pet owner, 1 - pet store
            //I removed followers, pets liked_pets. You should count them in select dynamically. Its bad practise to store count numbers in database.

            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->tinyInteger('location')->default(1);
            $table->text('bio')->nullable();
            $table->string('photo_path', 255)->nullable();
            $table->integer('coins')->default(5000);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
//            $table->string('verificationCode');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
