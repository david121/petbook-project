<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table-> bigInteger("user_id")->unsigned();
            $table-> bigInteger("pet_id")->unsigned();
            $table-> tinyInteger("type");
            $table-> tinyInteger("status")->default(1);
            $table-> bigInteger("mate_breed")->unsigned();
            $table-> integer("price")->unsigned()->default(0);
            $table-> string("note");
            $table-> string("suburb");
            $table-> string("city");
            $table-> string("country");
            $table-> foreign('user_id')->references('id')->on('users')->onUpdate("RESTRICT")->onDelete("CASCADE");
            $table-> foreign('pet_id')->references('id')->on('pets')->onUpdate("RESTRICT")->onDelete("CASCADE");
            $table-> foreign('mate_breed')->references('id')->on('pets')->onUpdate("RESTRICT")->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
