<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactOfTheDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_of_the_day', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table-> bigInteger('pet_type')->unsigned();
            $table-> text('fact');
            $table-> foreign('pet_type')->references('id')->on('pet_types')->onDelete("RESTRICT")->onUpdate("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_of_the_day');
    }
}
