  <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('user_id_1')->unsigned();
            $table->bigInteger('user_id_2')->unsigned();
            $table->unique(['user_id_1','user_id_2']);
            $table->foreign('user_id_1')->references('id')->on('users')->onUpdate("RESTRICT")->onDelete("CASCADE");
            $table->foreign('user_id_2')->references('id')->on('users')->onUpdate("RESTRICT")->onDelete("CASCADE");
            $table->text('last_message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
