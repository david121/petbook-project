<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmirersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admirers', function (Blueprint $table) {
            //modify unique key constraints
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->bigInteger('pet_id')->unsigned();
            $table->unique(['user_id','pet_id']);
            $table->foreign('pet_id')->references('id')->on('pets')->onDelete('CASCADE')->onUpdate('RESTRICT');
            //I removed pet_owner. It will cause problems when transferring pets
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admirers');
    }
}
