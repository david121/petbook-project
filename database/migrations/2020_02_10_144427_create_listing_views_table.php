<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('listing_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->unique( array('user_id','listing_id') );
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete("RESTRICT")->onUpdate("RESTRICT");
            $table->foreign('user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_views');
    }
}
