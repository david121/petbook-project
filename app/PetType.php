<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PetType extends Model
{
    protected $table = 'pet_types';

    public function breeds(){
      return $this->hasMany('App\PetBreed','pet_type');
    }
}
