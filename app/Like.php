<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Like extends Model
{
    use HasApiTokens, Notifiable;

    protected $table = 'post_likes';

    public function post(){
        return $this->belongsTo('App\Post');
    }
    public function archive(){
        return $this->belongsTo('App\PostArchive');
    }

}
