<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentFriendsRequest extends Model
{
    protected $table = "sent_friends_request";

    public function sent_request(){
        return $this->belongsTo("App\User");
    }
    public function received_request(){
        return $this->hasOne("App\ReceivedFriendsRequest");
    }
}
