<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admirer extends Model
{
    protected $table = "admirers";

    public function users (){
        return $this->belongsTo('App\User');
    }

    public function pets (){
        return $this->belongsTo('App\Pet');
    }

}
