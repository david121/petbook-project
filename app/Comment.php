<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    public function post() {
        return $this->belongsTo('App\Post');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function comments() {
        return $this->hasMany('App\CommentsComment');
    }
    public function likes() {
        return $this->hasMany('App\CommentLike');
    }

}
