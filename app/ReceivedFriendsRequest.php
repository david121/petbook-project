<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivedFriendsRequest extends Model
{
    protected $table = "received_friends_request";

    public function user (){
        return $this->belongsTo("App\User");
    }

    public function sender (){
        return $this->belongsTo("App\User");
    }
    public function sent_request(){
        return $this->hasOne("App\SentFriendsRequest");
    }
}
