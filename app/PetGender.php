<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetGender extends Model
{
    protected $table = "pet_gender";

    public function pet(){
        return $this->hasMany('App\Pet');
    }
}
