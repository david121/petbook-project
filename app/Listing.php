<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $table = "listings";

    function user (){
        return $this->belongsTo('App\User');
    }
}
