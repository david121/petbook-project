<?php

namespace App\Console\Commands;

use App\Http\Controllers\FunFactsController;
use Illuminate\Console\Command;
use Illuminate\Console\Sche\Schedule;
use Illuminate\Support\Facades\Log;

class TodaysFact extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Fact:Today';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $fact = new FunFactsController;
       $fact->factOfTheDay();
    }
}
