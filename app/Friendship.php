<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    protected $table = "friendship";

    public function users () {
        return $this->belongsTo('App\User');
    }

    public function sent_request () {
        return $this->belongsTo('App\SentFriendsRequest');
    }

    public function received_request () {
        return $this->belongsTo('App\ReceivedFriendsRequest');
    }

    public function is_friends () {
        return $this->hasMany('App\User','user_id');
    }

}
