<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    protected $table = 'hobbies';

    public function pet(){
       return $this->belongsTo('App\Pet');
    }
}
