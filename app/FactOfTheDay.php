<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactOfTheDay extends Model
{
    protected $table = "fact_of_the_day";
}
