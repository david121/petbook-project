<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
    protected $guarded = ['status'];
    protected $table = "friends";

    public function receiver_user(){
        return $this->belongsTo('App\User');
    }

    public function sender_user(){
        return $this->belongsTo('App\User');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }



}
