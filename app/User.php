<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'followers','liked_pets','city','country','bio','photo_path','username','profile_type','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function activities () {
        return $this->hasMany('App\UserActivity');
    }

    public function pets (){
        return $this->hasMany('App\Pet');
    }

    public function posts (){
        return $this->hasMany('App\Post');
    }

    public function comments (){
        return $this->hasMany('App\Comment');
    }
    public function comments_comments (){
        return $this->hasMany('App\CommentsComment');
    }

    public function sentFriendsRequest (){
        return $this->hasMany('App\SentFriendsRequest');
    }
    public function receivedFriendsRequest (){
        return $this->hasMany('App\ReceivedFriendsRequest');
    }

    public function friends (){
        return $this->hasMany('App\Friends','sender_user_id');
    }

    public function archive (){
        return $this->hasMany('App\PostArchive');
    }

    public function is_sender(){
        return $this->hasMany('App\Friends','sender_user_id', 'id');
    }

    public function is_receiver(){
        return $this->hasMany('App\Friends','receiver_user_id', 'id');
    }
    public function listing(){
        return $this->hasMany('App\Listing','user_id', 'id');
    }


}
