<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunFact extends Model
{
    protected $table = "fun_facts";
}
