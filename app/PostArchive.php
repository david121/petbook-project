<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostArchive extends Model
{
    protected $fillable = [
        'user_id','post_id', 'type'
    ];

    protected  $table = "post_archives";

    public function user(){
        return $this->belongsTo('App\User');
    }
//    public function post(){
//        return $this->hasMany('App\Post');
//    }
    public function comments(){
        return $this->belongsTo('App\Comment');
    }
    public function likes(){
        return $this->hasMany('App\Like');
    }
    public function pet(){
        return $this->belongsTo('App\Pet');
    }
    public function photo(){
        return $this->hasMany('App\PostPhoto');
    }
}
