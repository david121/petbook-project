<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class FriendsRequest extends Notification
{
    use Queueable;

//    private $receiver ;

    /**
     * Create a new notification instance.
     *
     * @param $receiver
     */
    public function __construct()
    {
//        $this->receiver = $receiver;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hi')
                    ->line('You received a new friends request from ')
                    ->action('Respond to request', url('https://pawrry.com'))
                    ->line('Thank you for using Pawrry!');
    }

    public function toDatabase(){
        return [
            "type"=>"Friendship",
            "sender"=> Auth::user()->id,
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
