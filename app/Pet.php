<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
        'user_id', 'pet_name', 'pet_type', 'pet_breed','pet_age','username','profile_photo','gender','admirers'
    ];

    protected $table = 'pets';

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function type(){
        return $this->belongsTo('App\PetType');
    }

    public function breed(){
        return $this->belongsTo('App\PetBreed');
    }

    public function hobby(){
       return $this->hasMany('App\Hobby');
    }

    public function posts () {
        return $this->hasMany('App\Post');
    }
    public function admirers(){
        return $this->hasMany("App\Admirer");
    }
}
