<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentsComment extends Model
{
    protected $table = 'comments_comments';

    public function comment() {
        return $this->belongsTo('App\Comment');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
}
