<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ListingType extends Enum
{
    const Adoption =   0;
    const Mating =   1;
    const Sale = 2;
}
