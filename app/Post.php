<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function user () {
        return $this->belongsTo('App\User');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likes () {
        return $this->hasMany('App\Like');
    }

    public function post_photo () {
        return $this->hasMany('App\PostPhoto');
    }

    public function is_liked(){
        return $this->hasMany('App\Like');
    }

    public function pet(){
        return $this->belongsTo('App\Pet');
    }

}
