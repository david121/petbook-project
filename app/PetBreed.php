<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetBreed extends Model
{
    protected $table = 'pet_breeds';

    public function type (){
       return $this->belongsTo('App\PetType');
    }
}
