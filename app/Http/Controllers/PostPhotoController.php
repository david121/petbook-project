<?php

namespace App\Http\Controllers;

use App\PostPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostPhotoController extends Controller
{
    /**
     * @param $id
     * @param $petId
     */
    public static function destroy ($id, $petId) {
        $photo = PostPhoto::where('post_id',$id)->get();
        $toDel = PostPhoto::where('post_id',$id);
        foreach ($photo as $p){
            $toDel->delete();
            Storage::disk('public')->delete(Auth::user()->id.'/'.'pets'.'/'.$petId.'/'.$p->photo);
        }
    }
}
