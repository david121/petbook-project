<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Like;
use App\Post;
use App\PostArchive;
use App\PostPhoto;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
//use Carbon\Carbon;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     *
     *
     */
    public function index()
    {
        try{
            $time = [];
            $id = Auth::user()->id;
            $posts = Post::with(['likes', 'post_photo', 'pet','user'])->withCount(['is_liked'  => function (Builder $query) {
                $query->where('user_id', '=', Auth::user()->id);
            }])->leftJoin('admirers', function ($join) {
                $join->on('admirers.pet_id', '=', 'posts.pet_id');
            })->where([['admirers.user_id', $id], ['admirers.status', 1],['posts.status',1]])
                ->orWhere([['posts.user_id',"=",$id],['posts.status',1]])->distinct()->orderBy('posts.created_at', 'desc')->get();
            foreach ($posts as $post){
                $post->comments = count(Comment::where("post_id",$post->id)->get());
                if ($post->user_id !== $id){
                    $arch = PostArchive::where([["user_id",$id],["type",1],["post_id",$post->id],["status",1]])->get();
                     $post->saved = count($arch);
                     $post->saved_id = count($arch)? $arch[0]->id : null;
                }
            }
            if (count($posts)>0){
                foreach ($posts as $post){
                    if ($post->user_id !== $post->owner_user_id){
                        $postOwner = User::find($post->owner_user_id);
                        $post->postOwner = $postOwner->username;
                    }
                    array_push($time, Carbon::parse( $post->created_at)->diffForHumans());
                }
            }
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success"=>true, "posts"=>$posts,"postTime" => $time],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $numberOfFiles = 0;
           if(isset($request->photo)){
               $numberOfFiles = count($request->photo);
           };
            $files = $request->photo;
            $post = new Post;
            $post-> user_id = Auth::user()->id;
            $post-> caption = $request->caption;
            $post->pet_id = $request->id;
            $post->owner_user_id = Auth::user()->id;
            $post->save();
            if ($numberOfFiles > 0){
                foreach ($files as $pic){
                    Storage::disk('public')->put("pets"."/".$request->id."/".$pic->getFilename().'.'.$pic->getClientOriginalExtension(),File::get($pic));
                    $postPhoto = new PostPhoto;
                    $postPhoto->post_id = $post->id;
                    $postPhoto->path = $pic->getFilename().'.'.$pic->getClientOriginalExtension();
                    $post->post_photo()->save($postPhoto);
                }
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'status'=>'posted'], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        dd($request->caption);
         $request->validate([
            'caption' => 'required',
            'id' => 'required'
         ]);
         try{
             $post = Post::find($request->id);
             $post->caption = $request->caption;
             $post -> save();
         }catch (\Exception $e){
             return response()->json(['success'=>false, 'message'=>$e->getMessage()], 200);
         }
        return response()->json(['success'=>true, 'status'=>'updated'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request )
    {
        try{
            if (PostPhoto::where('post_id', $id)->count() > 0) {
                    $photos = PostPhoto::where('post_id', $id)->pluck('path');
                    for ($i = 0; $i < count($photos); $i++) {
                        Storage::disk('public')->delete('pets' . '/' . $request->petId . '/' . $photos[$i]);
                    }
                };
            $post = Post::find($id);
            $post->delete();

            /*
            if (Like::where('post_id',$id)->count() > 0){Like::where('post_id',$id)->delete();}
            elseif (Comment::where('post_id',$id)->count()>0){Comment::where('post_id',$id)->delete();}
            elseif (PostArchive::where('post_id',$id)->count()>0){PostArchive::where('post_id',$id)->delete();}
            elseif (PostPhoto::where('post_id',$id)->count()>0){
                $photos = PostPhoto::where('post_id',$id)->pluck('path');
                for ($i=0; $i<count($photos); $i++){
                    Storage::disk('public')->delete('pets'.'/'.$request->petId.'/'.$photos[$i]);
                }
                PostPhoto::where('post_id',$id)->delete();
            };
            $post = Post::find($id);
            $post->delete();*/

        }catch (\Exception $e) {
            return response()->json(["success"=>false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success"=>true], 200);
    }
}
