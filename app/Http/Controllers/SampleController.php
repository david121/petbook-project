<?php

namespace App\Http\Controllers;

use App\Recursion;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    public function store_comments(Request $request){

        try{
            if (!$request->has("parent")){
                $comment = new Recursion;
                $comment -> comment= $request->comment;
                $comment -> save();
            }
        }catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }
    public function retrieve_comments(){

        try{
            $comments = Recursion::all();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true, "comments"=>$comments],200);
    }
}
