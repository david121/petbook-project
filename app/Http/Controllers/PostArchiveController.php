<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Enums\ArchiveType;
use App\Post;
use App\PostArchive;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Post archive Table.
 *
 * type 1 = archived / saved posts.
 * type 2 = hidden posts.
 *
*/


class PostArchiveController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPostToArchive (Request $request){ //save post
        try{
            $request->validate(["postId"=>"required"]);
            DB::table("post_archives")
                ->updateOrInsert(
                    ['user_id'=>Auth::user()->id,
                        'post_id'=>$request->postId,
                        'type' => ArchiveType::Save,
                        'status' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function hidePostInArchive (Request $request){
        try{
            $request->validate(["postId"=>"required"]);
            $post = Post::find($request->postId);
            $post->status=0;
            $post->save();
            DB::table("post_archives")
                ->updateOrInsert(
                    [
                        'user_id'=>Auth::user()->id,
                        'post_id'=>$request->postId,
                        'type' => ArchiveType::Hide,
                        'status'=>1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArchivedPosts (Request $request){//get saved Posts
        try{
                $arch = PostArchive::where([['user_id',Auth::user()->id],['type',1],["status",1]])->get();
            if (count($arch)){
                foreach ($arch as $a){
                    $a->post = Post::with(["likes","pet","user","post_photo"])->withCount(['is_liked'  => function (Builder $query) {
                        $query->where('user_id', '=', Auth::user()->id);
                    }])->where([["id",$a->post_id]])->get();
                    $a->time = Carbon::parse($a->post[0]->created_at)->diffForHumans();
                    $a->comments = count(Comment::where("post_id",$a->post_id)->get());
                }
            }else $arch = null;

        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true, "posts"=>$arch],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHiddenPosts (Request $request){
        try{

            $arch = DB::table("post_archives")->where([["user_id",Auth::user()->id],["type",2],["status",1]])->get();
            if (count($arch)){
                    foreach ($arch as $a){
                    $a->post = Post::with(["likes","pet","user","post_photo"])->withCount(['is_liked'  => function (Builder $query) {
                        $query->where('user_id', '=', Auth::user()->id);
                    }])->where([["id",$a->post_id],["status",0]])->get();
                    $a->time = Carbon::parse($a->post[0]->created_at)->diffForHumans();
                    $a->comments = count(Comment::where('post_id',$a->post[0]->id)->get());
                }
            }else $arch = null;
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true, "posts"=>$arch],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreHiddenPost (Request $request){
        try{
            $post = Post::find($request->post_id);
            $post->status= 1;
            if ($post->save()){
                $archive = PostArchive::find($request->archive_id);
                $archive->status=0;
                $archive->save();
            }
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

    public function unSavePost (Request $request){
        try{
            $request->validate(["archive_id"=>"required"]);
            $archive = PostArchive::find($request->archive_id);
                $archive->status=0;
                $archive->save();
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

}
