<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Hobby;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HobbiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $hobbies = Hobby::where('pet_id', $_GET['pet_id'])->get();
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'hobbies'=>$hobbies], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $hobby = new Hobby;
            $hobby-> pet_id = $request->id;
            $hobby-> hobby = $request->hobby;
            $hobby->pet()->where('id', $request->id)
                ->update(['hobbies'=> DB::raw('hobbies + 1')]);
            $hobby-> save();
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'hobby'=>'saved'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
