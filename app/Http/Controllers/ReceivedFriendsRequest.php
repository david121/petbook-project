<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ReceivedFriendsRequest extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $recipient
     * @return void
     */
    public static function store($recipient)
    {
        try{
            $receivedRequest = new \App\ReceivedFriendsRequest;
            $receivedRequest->user_id = $recipient;
            $receivedRequest->sender = Auth::user()->id;
            $receivedRequest->save();
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success"=>true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $receivedRequests =  DB::table('received_friends_request')
                ->leftJoin('users','received_friends_request.sender','=', 'users.id')
                ->select('received_friends_request.*','users.username AS sender')->where('user_id',Auth::user()->id)
                ->get();
            dd($receivedRequests);
        }
        catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success"=>true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @param bool $sender
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$sender)
    {
        try {
            if ($sender){
            \App\ReceivedFriendsRequest::where('sender', $id)->delete();
            }else
            \App\ReceivedFriendsRequest::where('user_id', $id)->delete();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()], 200);
        }
            return response()->json(["success"=>true], 200);
        }
}
