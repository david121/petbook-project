<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){
        try{

            $users = DB::table('users')->select('id','username','firstname','lastname')->get();
            $pets = DB::table('pets')->select('id','username','name','user_id as owner')->get();
            $all = [];
            foreach ($users as $user){ array_push($all,$user);}
            foreach ($pets as $pet){ array_push($all,$pet);}

        }catch (\Exception $e){
            return response()->json(["success"=>false,"message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true,"result"=>$all],200);
    }
}
