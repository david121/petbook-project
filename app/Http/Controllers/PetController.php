<?php

namespace App\Http\Controllers;

use App\Admirer;
use App\Comment;
use App\CommentLike;
use App\Enums\PetGenderType;
use App\Like;
use App\Listing;
use App\Notifications\NewPet;
use App\Pet;
use App\PetBreed;
use App\PetType;
use App\Post;
use App\PostArchive;
use App\PostPhoto;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;

class PetController extends Controller
{

    public $successStatus = 200;

    function create(Request $request)
    {
        try{
             $g = $request->input('gender');
             $gender= '';
             switch ($g){
                 case 'Male' : $gender = PetGenderType::Male;break;
                 case 'Female' : $gender = PetGenderType::Female;break;
             }

            $params = $request->all();
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $pet = new Pet;
            $pet->name = ucfirst(strtolower($params['name']));
            $pet->user_id = Auth::user()->id;
            $pet->gender = $gender;
            $pet->breed_id = $params['breed'];
            $pet->date_of_birth = $params['age'];
            $pet->username = strtolower( $params['username']);
            $pet->profile_photo = $file->getFilename().'.'.$extension;

            if ($pet->save()){
                Storage::disk('public')->put("pets"."/".$pet->id."/".$file->getFilename().'.'.$extension,File::get($file));
                //send New pet Notification to friends
                $friendsId = [];
                $id = Auth::user()->id;
                $userFriends = DB::table('friends')
                    ->where([['sender_user_id','=',$id],['status','=',1]])
                    ->orWhere([['receiver_user_id','=',$id],['status','=',1]])
                    ->get();
                foreach ($userFriends as $userFriend) {
                    $userFriend->sender_user_id == Auth::user()->id?
                        array_push($friendsId,$userFriend->receiver_user_id) : array_push($friendsId,$userFriend->sender_user_id);
                }

                for ($i = 0; $i < count($friendsId); $i++ ){
                    $user = \App\User::find($friendsId[$i]);
                    $user->notify(new NewPet($pet->id));
                }

            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg' => $e->getMessage()],$this->successStatus);
        }

        return response()->json(['success' => true], $this->successStatus);
    }

    function deleteProfile (Request $request){
        try{
            $id = $request->petId;
            $pet = Pet::find($id);
            $not_dat = DB::table('notifications')->orderBy("created_at")->get();
            $ids = array();
            foreach ($not_dat as $n){
                $data = json_decode($n->data);
                if ($data->type === "Post Like" || $data->type === "New Pet" || $data->type === "Admirer"){
                    if ($data->pet == $pet->id){
                        array_push($ids,$n->id);
                    }
                }
            }
            if ($pet->delete()){
                Storage::disk('public')->deleteDirectory("pets/".$id);
                DB::table('notifications')->whereIn('id',$ids)->delete();
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg' => $e->getMessage()],$this->successStatus);
        }
        return response()->json(['success' => true], $this->successStatus);
    }

    function fetch(Request $request){
        //Fetch Pets belonging to a user
        $pets = null;
        $src = [];
        try{
            if ($request->has('profileId')){
                    $pets = Pet::where('user_id',$request->profileId)->get();
                    foreach ($pets as $pet){
                        array_push($src, Storage::url('pets'.'/'.$pet->id.'/'.$pet->profile_photo));
                    }
            }else{
//                $user = $request->user();
                $pets = Pet::where('user_id',Auth::user()->id)->get();
                foreach ($pets as $pet){
                    $pet->listed = Listing::where([['pet_id',$pet->id],['status', 1]])->count() ? Listing::where([['pet_id',$pet->id],['status', 1]])->get() : null;
                    array_push($src, Storage::url('pets'.'/'.$pet->id.'/'.$pet->profile_photo));
                }
            }

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }

        return response()->json(['success'=>true, 'pet'=>$pets, 'src' => $src ],$this->successStatus);
    }

    function similarPets (Request $request){
        try{
            $pets = DB::table('pets')
                ->select('username','profile_photo','id')
                ->where('breed_id',$request->breedId)
                ->where('user_id','<>', Auth::user()->id)->get();
        }
        catch(\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true, 'pets'=>$pets],$this->successStatus);
    }

    function fetchPet(Request $request){
        //Fetch Single Pet profile
        try{
            $pet = null;
            $src = '';
            if ($request->has('pet')){//pet_id
                $pet = Pet::with(["breed","user"])->withCount(["admirers"=>function (Builder $query){
                    $query->where([["user_id",Auth::user()->id],["status","=",1]]);
                }])->where("id",$request->pet)->get();
                $admirers = count(Admirer::where([["pet_id",$request->pet],["status",1]])->get());
                $pet[0]->admirers = $admirers;
                foreach ($pet as $p) {
                    $src = Storage::url('pets'.'/'.$p->id.'/'.$p->profile_photo);
                }
            }else{
//                dd("Hello");
                $id = $request->input('pet_id');
                $src = '';
                $pet = Pet::with(["breed"])->where('id',$id)->get();
                $admirers = count(Admirer::where([["pet_id",$id],["status",1]])->get());
                $pet[0]->admirers = $admirers;
                foreach ($pet as $p){
                    $src = Storage::url('pets'.'/'.$p->id.'/'.$p->profile_photo);
                }
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true, 'pet' => $pet, 'src' => $src],$this->successStatus);
    }

    public function admirers (Request $request){
        try{

            $admirers = DB::table('admirers')->join('users','users.id','=','admirers.user_id')
                ->select("users.username","users.photo_path","users.id as user_id","users.profile_type")
                ->where([['admirers.pet_id',$request->id],['admirers.status', 1]])->get();
            foreach ($admirers as $admirer) {
                $status = DB::table('friends')->where([['sender_user_id',Auth::user()->id],['receiver_user_id',$admirer->user_id],['status','<>',2]])
                    ->orWhere([['sender_user_id',$admirer->user_id],['receiver_user_id',Auth::user()->id],['status','<>',2]])
                    ->select('status','id')->get();
                if (count($status)>0){
                    $admirer->friend_status = $status[0];
                }else $admirer->friend_status = null;
            }
        }catch(\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true, 'admirers' => $admirers],200);
    }

    public function bio (Request $request){
        try{

            $pet = Pet::find($request->id);
            $pet->bio = htmlspecialchars(trim($request->bio));
            $pet->save();
        }catch(\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }



    public function findBreed(Request $request){
        try{
            $breed = PetBreed::find($request->breedId);
            $breeds = PetType::find($breed->pet_type)->breeds()->orderBy('name')->get();
        }
        catch(\Exception $e){
            return response()->json(['success'=>false, 'msg' => $e->getMessage()],$this->successStatus);
        }
        return response()->json(['success' => true, 'breed'=>$breed,'breeds'=>$breeds], $this->successStatus);
    }

    function updatePet(Request $request){
        try{
            $file = null;$extension = null;
            $removePic = false;
            if ($request->input('ext') !== null){
                $file = $request->file('photo');
                $extension = $request->ext;
            }elseif($request->input('photo') === 'avatar'){
                    $removePic = true;
            }
            $params = $request->all();
            $pet = Pet::find($params["petId"]);
            $pet->name = $params["name"];
            $pet->username=$params["username"];
            $pet->breed_id=$params["breed"];
            $pet->date_of_birth=$params["d_o_b"];
            if ($file){
                $pet->profile_photo = $file->getFilename().'.'.$extension ;
            }if ($removePic){
                $pet->profile_photo = null;
            }
            if ($pet->save() && $file){
                Storage::disk('public')->put("pets"."/".$params["petId"]."/".$file->getFilename().'.'.$extension,File::get($file));
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg' => $e->getMessage()],$this->successStatus);
        }
       return response()->json(['success' => true], $this->successStatus);
    }

    public function petPosts(Request $request){
        try{
            $response = null;
            $posts = [];
            if ($request->has("pet")){
                $posts = Post::with(['likes', 'post_photo', 'pet'])->withCount(['is_liked'  => function (Builder $query) {
                    $query->where('user_id', '=', Auth::user()->id);
                }])->where([['pet_id', $request->pet],["posts.status",1]])->orderBy('posts.created_at','desc')->get();
            }else{
            $pet = $request->id;
                $posts = Post::with(['likes', 'post_photo','pet'])->withCount(['is_liked'  => function (Builder $query) {
                    $query->where('user_id', '=', Auth::user()->id);
                }])->where([['pet_id', $pet],["posts.status",1]])->orderBy('posts.created_at','desc')->get();
            }
            foreach ($posts as $post){
                if ($post->user_id !== $post->owner_user_id){
                    $postOwner = User::find($post->owner_user_id);
                    $post->postOwner = $postOwner->username;
                }
                $post->comments = count(Comment::where("post_id",$post->id)->get());
            }
             $response = count($posts) > 0 ? $posts : "None";
            if ($response !== "None"){
                for($i=0; $i<count($posts); $i++){
                    $posts[$i]->time = Carbon::parse( $posts[$i]->created_at)->diffForHumans();
                }
            }
        }catch(\Exception $e){
            return response()->json(["success"=>false, "msg"=> $e->getMessage()],200);
        }
        return response()->json(["success"=>true, "posts"=>$response],200);
    }

    public function changeOwnership (Request $request){
        try{
            $pet = Pet::find($request->petId);
            $pet ->user_id = $request->newOwner;
            if ($pet->save()){
                DB::table('pet_owners')->insert([
                    ['user_id'=>Auth::user()->id,'pet_id'=>$request->petId],
                    ['user_id'=>Auth::user()->id,'pet_id'=>$request->petId],
                ]);
                DB::table('posts')->where('pet_id',$request->petId)->update(["user_id"=>$request->newOwner]);
                DB::table('admirers')
                    ->updateOrInsert(
                        ['user_id'=>Auth::user()->id,'pet_id'=>$request->petId],
                        ['status' => 1]
                    );
                if(Admirer::where([["user_id",$request->newOwner],["pet_id",$request->petId]])->exists()){
                    Admirer::where([["user_id",$request->newOwner],["pet_id",$request->petId]])->update(["status"=>2]);
                }
            }
        }catch(\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

    public function petSuggestion (){
        try{
            $id = Auth::id();
            $admPetsId = array();
            $admired = DB::table('admirers')
                ->select("pet_id")
                ->where([["user_id",Auth::user()->id], ["status", "=", 1]])->pluck('pet_id');
                foreach ($admired as $a){ array_push($admPetsId, $a);}

            $userFriends = DB::table('friends')->leftJoin('users',function ($join){
                $join -> on("friends.sender_user_id","=","users.id")->orOn("friends.receiver_user_id","=","users.id");
            })->select("friends.id AS friendship","friends.*", "users.id as friendId")
                ->where([['sender_user_id','=',$id],['status','=',1]])
                ->whereNotIn('users.id',[$id])
                ->orWhere([['receiver_user_id','=',$id],['status','=',1]])
                ->whereNotIn('users.id',[$id])
                ->inRandomOrder()->limit('3')
                ->get();

            $friendsId = [];
            foreach ($userFriends as $friend){array_push($friendsId,$friend->friendId);}

            $suggestByFriends = DB::table('pets')
                ->leftJoin('admirers', function ($join){
                    $join->on('pets.id', '=', 'admirers.pet_id')->where([['admirers.user_id', '!=', Auth::user()->id]]);
                })
                ->leftJoin('users', 'users.id', '=', 'admirers.user_id')
                ->select('pets.id','pets.username','pets.user_id as owner','pets.profile_photo','admirers.user_id as admirer', 'users.username as admirer_usn')
                ->whereIn('admirers.user_id',$friendsId)
                ->where('admirers.status','=',1)
                ->where([['admirers.user_id','<>',Auth::user()->id],['admirers.status',1]])
                ->whereNotIn('admirers.pet_id', $admPetsId)
                ->where('pets.user_id','<>',Auth::user()->id)
                ->distinct()->inRandomOrder()
                ->limit('4')
                ->get();

//            Suggest by user's pet(s) breed(s)
            $userPetsBreeds = DB::table('pets')
                ->where('user_id',Auth::user()->id)
                ->pluck('breed_id');
            $breedsId = [];
            foreach ($userPetsBreeds as $id){array_push($breedsId,$id);}

            $suggestByBreeds = DB::table('pets')->join('admirers', function ($join){
                $join->on('pets.id', '=', 'admirers.pet_id')->where([['admirers.user_id', '!=', Auth::user()->id]]);
            })
                ->select('pets.id','username','pets.user_id as owner','pets.profile_photo')
                ->whereIn('pets.breed_id',$breedsId)
                ->where('pets.user_id','!=', Auth::user()->id)
                ->whereNotIn('admirers.pet_id', $admPetsId)
                ->distinct()->inRandomOrder()
                ->limit('4')
                ->get();

            $random = DB::table('pets')->leftJoin('admirers','pets.id', '=', 'admirers.pet_id')
                ->select('pets.name','pets.username','pets.id','pets.user_id as owner','pets.profile_photo')
                ->whereNotIn('admirers.pet_id', $admPetsId)
                ->where('pets.user_id' , '<>', Auth::user()->id)
                ->limit('6')
                ->inRandomOrder()
                ->distinct()
                ->get();



            $fr = [];
            foreach ($suggestByFriends as $suggestByFriend) {array_push($fr, $suggestByFriend);}
            $frIDs=[];
            foreach ($fr as $item) {array_push($frIDs,$item->id);}
            foreach ($suggestByBreeds as $suggestByBreed) {
                if (!in_array($suggestByBreed->id,$frIDs)){
                    array_push($fr, $suggestByBreed);
                }
            }
            $allIds=array();
            foreach ($fr as $item) {array_push($allIds,$item->id);}
            foreach ($random as $rand) {
                if (!in_array($rand->id,$allIds)){
                    array_push($fr, $rand);
                }
            }
        }catch(\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true, "pets"=>$fr, 'likedPets' =>$admPetsId],200);
    }


    public function activePets (Request $request) {
        try{
            $pets = Pet::where('user_id',Auth::user()->id)->pluck('id');
            $likedPets = DB::table('admirers')->leftJoin('pets', function($join){
                $join -> on("admirers.pet_id", "=", "pets.id");
            })->select("pets.id")
                ->where([["admirers.user_id",Auth::user()->id], ["admirers.status", "=", 1]])->pluck("id");
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(["success"=>true, "pets"=>$pets, "likedPets"=>$likedPets],$this->successStatus);
    }

    public function browse () {
        try{
            $pets = DB::table('pets')->leftJoin('users','pets.user_id','=','users.id')
                ->leftJoin('pet_breeds','pets.breed_id','=', 'pet_breeds.id')
                ->select('pets.id','pets.profile_photo','pets.name','pets.username','pets.gender','pet_breeds.name as breedName','pet_breeds.description')
                ->where('pet_breeds.name', '<>', 'Other Breed')
                ->inRandomOrder()
                ->distinct()
                ->limit('6')
                ->get();

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(["success"=>true,'pets'=>$pets],$this->successStatus);
    }

    function mobilePost (){
        try{
            $pets = Pet::where('user_id',Auth::user()->id)->get();
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(["success"=>true,'pets'=>$pets],$this->successStatus);

    }


}
