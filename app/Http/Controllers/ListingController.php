<?php

namespace App\Http\Controllers;

use App\Enums\ListingType;
use App\Enums\PetGenderType;
use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use const http\Client\Curl\AUTH_ANY;

class ListingController extends Controller
{
    private $successStatus = 200;

    public function listPet(Request $request){
        try{
            //dd($request->location["suburb"]);
            $request->validate([
              "petId" => "required",
              "type" => "required"
            ]);
            $type = $request->type;
            $listingType ='';
            switch ($type){
                case "Adoption": $listingType = ListingType::Adoption; break;
                case "Mating" : $listingType = ListingType::Mating;break;
                case "Sale" : $listingType = ListingType::Sale;
            }
             $coins = DB::table('users')->where('id',Auth::user()->id)->pluck('coins');
            if ($coins[0] < 250){
                return response()->json(["success"=>true, "list_error" => true, "message"=>"INSUFFICIENT COINS"],$this->successStatus);
            }
                $listing = new Listing;
                if ($listingType == 1){
                    $listing->mate_breed = $request->mateBreed;
                }else if ($listingType == 2)$listing->price = $request->price;
                $listing -> user_id = Auth::user()->id;
                $listing -> pet_id = $request->petId;
                $listing -> type = $listingType;
                $listing->note = $request->note;
                $listing->suburb = $request->location["suburb"];
                $listing->city = $request->location["city"];
                $listing->country = $request->location["country"];
                if ($listing->save()){
                    DB::table('users')->where('id', Auth::user()->id)->update(["coins"=> $coins[0] - 250]);
                }else
                return response()->json(["success"=>true, "list-error" => true],$this->successStatus);
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true],$this->successStatus);
    }

    public function listings(){
        try{
            $user_loc = DB::table('temp_user_loc')->where('user_id', Auth::user()->id)->pluck('city');
            $listings = DB::table('listings')
                ->join('pets','listings.pet_id','=', 'pets.id')
                ->select('listings.*','pets.username','pets.profile_photo','pets.id AS pet_id','pets.gender')
                ->where('listings.city',$user_loc[0])->get();
            foreach ($listings as $listing){
                if ($listing->type == 1 ){
                    $listing->mate_breed = DB::table('pet_breeds')->select('name')->where('id',$listing->mate_breed)->pluck("name");
                }
                $listing->views = DB::table('listing_views')->where('listing_id',$listing->id)->count();
            }
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true,"listings"=>$listings],$this->successStatus);
    }

    public function listingView(Request $request){
        try{
//            dd(DB::table('listing_views')->find(47));
            if (!DB::table('listing_views')->find($request->listing_id)){
                DB::table('listing_views')->insert([
                    "listing_id" => $request->listing_id,
                    "user_id" => Auth::user()->id
                ]);
            }
//            DB::table('listing_views')->insertOrIgnore([
//                ["listing_id" => $request->listing_id,"user_id" => Auth::user()->id]
//            ]);
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true],$this->successStatus);
    }

    public function myListings(){
        try{
            $listings = DB::table('listings')->join('pets','listings.pet_id','=', 'pets.id')
                ->select('listings.*','pets.username','pets.profile_photo','pets.id AS pet_id')
                ->where('listings.user_id',Auth::user()->id)->get();
            foreach ($listings as $listing){
                if ($listing->type == 1 ){
                    $listing->mate_breed = DB::table('pet_breeds')->select('name')->where('id',$listing->mate_breed)->pluck("name");
                }
                $listing->views = DB::table('listing_views')->where('listing_id',$listing->id)->count();
            }
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true,"listings"=>$listings],$this->successStatus);
    }
    public function deleteListing(Request $request){
        try{
//            dd($request->input("id"));
            if (DB::table('listing_views')->where('listing_id','=', $request->input("id"))->count() > 0){
                DB::table('listing_views')->where('listing_id','=', $request->input("id"))->delete();
            }
            DB::table('listings')->where('id','=', $request->input("id"))->delete();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true],$this->successStatus);
    }

    public function filtered_listing(Request $request){
        try{
            $user_loc = DB::table('temp_user_loc')->where('user_id', Auth::user()->id)->get();
            $params = $request->all();
            $cond = array();
            if($params["country"]["value"]){
                $cond[] = ['listings.country','=',$user_loc[0]->country];
            }else $cond[] = ['listings.city','=',$user_loc[0]->city];
            foreach ($params as $k=>$v) {
                if (!$v["value"]){
                    if ($v["type"] == "pet-type"){
                        $pet = $k == "Cat" ? 1 : 2;
                        $cond[]=["pet_breeds.pet_type",'<>',$pet];
                    }if ($v["type"] == "list-type"){
                        $type = null;
                        switch ($k){
                            case "Adoption":$type=ListingType::Adoption;break;
                            case "Mating":$type=ListingType::Mating;break;
                            case "Sale":$type=ListingType::Sale;break;
                        }
                        $cond[]=["listings.type",'<>',$type];
                    }
                }
            }
          //  $location = '';
            $listings = DB::table('listings')
                ->join('pets','listings.pet_id','=', 'pets.id')
                ->join('pet_breeds','pets.breed_id','=', 'pet_breeds.id')
                ->select('listings.*','pets.username','pets.profile_photo','pets.id AS pet_id')
                ->where( $cond )
                ->get();
            if($params["country"]["value"]){
                $location = count($listings)? $listings[0]->country : null;
            }else $location = count($listings)? $listings[0]->city : null;
            foreach ($listings as $listing){
                if ($listing->type == 1 ){
                    $listing->mate_breed = DB::table('pet_breeds')->select('name')->where('id',$listing->mate_breed)->pluck("name");
                }
                $listing->views = DB::table('listing_views')->where('listing_id',$listing->id)->count();
            }
//            dd($listings);
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(["success"=>true,"listings"=>$listings,'location'=>$location],$this->successStatus);
    }


}
