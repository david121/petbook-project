<?php
namespace App\Http\Controllers;

use App\Chat;
use App\Mail\ValidateRegistration;
use App\Notifications\NewMessage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatsController extends Controller
{
    private $successStatus = 200;


    function checkChatExists (Request $request){
        try{
            $chat = Chat::where([['user_id_1',Auth::user()->id], ['user_id_2', $request->id]])
                ->orWhere([['user_id_1', $request->id,['user_id_2',Auth::user()->id]]])->pluck('id');
            $chat_id = count($chat) > 0 ? $chat : null;
        }catch (\Exception $e){
            return response()->json(['success'=>false,"msg"=>$e->getMessage()]);
        }
        return response()->json(['success'=>true, 'chatId'=>$chat_id[0]], $this->successStatus );
    }

    function storeChat (Request $request){
        try{
//            $chatId = '';
//            dd($request->message);
            if (
                    Chat::where([['user_id_1',Auth::user()->id], ['user_id_2', $request->id]])
                    ->orWhere([['user_id_1', $request->id,['user_id_2',Auth::user()->id]]])
                    ->count()
                ){
                $chatId = Chat::where([['user_id_1',Auth::user()->id], ['user_id_2', $request->id]])
                    ->orWhere([['user_id_1', $request->id,['user_id_2',Auth::user()->id]]])
                    ->pluck('id');
//                dd($chatId[0]);
                $chat_Id = $chatId[0];
                DB::table('chats')->where('id',$chat_Id)->update([
                    "last_message"=>$request->message,
                    "updated_at"=>Carbon::now(),
                ]);
                $user = \App\User::find($request->input("id"));
                $user->notify(new NewMessage());
            }else{
            $chat_Id = DB::table('chats')->insertGetId(
                [
                    'user_id_1' => Auth::user()->id,
                    'user_id_2'=> $request->id,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now(),
                    'last_message'=>$request->input('message')
                ]
            );
            $user = \App\User::find($request->input("id"));
            $user->notify(new NewMessage());
        }
        }catch (\Exception $e){
            return response()->json(['success'=>false,'message'=>$e->getMessage()]);
        }
        return response()->json(['success'=>true, 'chatId'=>$chat_Id], $this->successStatus );
    }

    function fetchChats (){
        try{
            $chats = DB::table('chats')->join('users', function ($join){
                $join->on('users.id','=','chats.user_id_1')->orOn('users.id','=','chats.user_id_2');
            })->select('users.id','users.username','users.photo_path','chats.id as chatId', 'chats.last_message as message','chats.updated_at')
                ->whereNotIn('users.id',[Auth::user()->id])
                ->where('chats.user_id_1', Auth::user()->id)->orWhere('chats.user_id_2', Auth::user()->id)
                ->whereNotIn('users.id',[Auth::user()->id])
                ->orderByDesc('chats.updated_at')
                ->get();

            $u = User::find(Auth::user()->id);
            foreach ($u->unreadNotifications as $n){
                if ($n->data['type'] === "Message"){
                   foreach ($chats as $chat){
                       if ($chat->id === $n->data['sender']){
                          $chat->unread = true;
                          $chat->unread_id = $n->id;
                       }else $chat->unread = 0;
                   }
                }
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()]);
        }
        return response()->json(['success'=>true, 'list'=>$chats], $this->successStatus );
    }

    function updateChat (Request $request){
        try{
            $chat = Chat::find($request->input('chatId'));
            $chat->last_message = $request->input('message');
            $chat->save();
        }catch (\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()]);
        }
        return response()->json(['success'=>true,], $this->successStatus );
    }

    function messageUnread(Request $request){
        $chat = Chat::find($request->input('chatId'));
        $rec=$chat->user_id_1 === Auth::user()->id ? $chat->user_id_2 : $chat->user_id_1 ;
        $user = User::find($rec);
        $user->notify(new NewMessage());
       // dd($rec);
    }

    function test (){
        \Mail::to('olurebidavid@gmail.com')->send(new ValidateRegistration());
    }

}
