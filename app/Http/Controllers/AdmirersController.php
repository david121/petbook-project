<?php

namespace App\Http\Controllers;

use App\Admirer;
use App\Notifications\PetAdmirer;
use App\Pet;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AdmirersController extends Controller
{
    public static function friendshipAdmire($senderId){
        try{
            $senderPets = Pet::where("user_id",$senderId)->pluck("id");
            $userPets = Pet::where("user_id",Auth::user()->id)->pluck("id");
            if (count($senderPets)>0){

                Log::info("Number of Sender Pets: " . $userPets);
                for ($i = 0 ; $i < count($senderPets); $i++){
                    $data1= array(
                        array("user_id" => Auth::user()->id,
                            "pet_id" => $senderPets[$i],
                            "status" => 1
                            )
                        );
                    Admirer::insert($data1);
                }
               // DB::table('admirers')->insert($data1);
            }
            if (count($userPets)>0){
                for ($i = 0 ; $i < count($userPets); $i++){
                    $data= [
                        "user_id" => $senderId,
                        "pet_id" => $userPets[$i],
                        "status" => 1
                    ];
                    DB::table('admirers')->insert($data);
                }
            }

        }catch (\Exception $e){
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public static function friendshipUnAdmire($friendId){
        try{
            $friendPets = Pet::where('user_id',$friendId)->pluck('id');
            $petsId = []; foreach ($friendPets as $pet){array_push($petsId, $pet);}
            for ($i = 0 ; $i < count($petsId); $i++){
                    DB::table('admirers')->where([['user_id',Auth::user()->id],['status', 1]])
                        ->whereIn('pet_id',[$petsId][$i])->update(['status'=>2]);
            }
        }catch(\Exception $e){
            return response()->json(["success" => false,"message"=>$e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public function admire (Request $request) {
        try{
                $request->validate([
                    'pet_id' => 'required'
                ]);
            DB::table('admirers')
                ->updateOrInsert(
                    ['user_id'=>Auth::user()->id,'pet_id'=>$request->pet_id],
                    ['status' => 1]
                );
            //send Admire Notification
            $admirer = User::find(Auth::id());//admirer
            $pet = Pet::where('id',$request->pet_id)->get();//admired pet
            $user = User::find($pet[0]->user_id);//pet owner
            $admirer->petId=$pet[0]->id;
            $admirer->petName=$pet[0]->name;
            $admirer->receiver=$user->firstname;
            Mail::to($user)->send(new \App\Mail\PetAdmirer($admirer));
            $user->notify(new PetAdmirer($request->pet_id));

        }catch(\Exception $e){
            return response()->json(["success" => false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public function cancelAdmiration (Request $request) {
        try{

            if($request->admire_id === null){
//                dd($request->pet_id);
                DB::table('admirers')->where([['pet_id',$request->pet_id],['user_id',Auth::user()->id]])->update(["status"=>2]);
            }else{
                $request->validate([
                    'admire_id' => 'required'
                ]);
                $adm = Admirer::find($request->admire_id);
                if (!$adm){
                    throw new \Exception("Initial admiration does not exist");
                }
                $adm->status = 2;
                $adm->save();
            }
        }catch(\Exception $e){
            return response()->json(["success" => false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public function unAdmireFromVisitor(Request $request){
        try{
            Admirer::where([['user_id',Auth::user()->id],['pet_id',$request->pet_id],['status',1]])->update(['status'=>2]);
        }catch (\Exception $e){
            return response()->json(["success" => false, "message"=>$e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

}
