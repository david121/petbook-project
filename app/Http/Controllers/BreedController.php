<?php

namespace App\Http\Controllers;

use App\PetBreed;
use Illuminate\Http\Request;

class BreedController extends Controller
{
    public function aboutBreed (Request $request){
        try{
            $aboutBreed = PetBreed::where('breed', $request->breed)->get();
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], 200);
        }
        return response()->json(["success"=>true, 'about'=>$aboutBreed],200);
    }
}
