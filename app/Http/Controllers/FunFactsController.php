<?php

namespace App\Http\Controllers;

use App\FactOfTheDay;
use App\FunFact;
use Illuminate\Support\Facades\DB;


class FunFactsController extends Controller
{
    protected $factId;
    public function getFact(){
        try{//use whereDate
            $today =date('Y-m-d');
//            dd($today);
            $fact = DB::table('fact_of_the_day')->whereDate('created_at',$today)->get();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true,'fact' => $fact],200);
    }

    public function factOfTheDay () {
        try{
            $facts = FunFact::all();
            $factsArray = array();
            foreach ($facts as  $fact){
                array_push($factsArray,$fact);
            }
            $dayFact = array_rand($factsArray,1);
            $today = new FactOfTheDay;
            $today->pet_type =$factsArray[$dayFact]->pet_type;
            $today->fact =$factsArray[$dayFact]->fact;
            if ($today->save()){
              $this->factId=$today->id;
            }
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }
}
