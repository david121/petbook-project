<?php

namespace App\Http\Controllers;

use App\PetBreed;
use App\PetGender;
use App\PetQuestion;
use App\PetType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetQuestionsController extends Controller
{
    protected $successStatus = 200;

    public function questions (){

        try{
                $questions = PetQuestion::all();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "response"=>$e->getMessage()],$this->successStatus);
        }

        return response()->json(["success"=>true, "questions"=>$questions],$this->successStatus);
    }

    public function types (){
        try{
            $types = PetType::all();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "response"=>$e->getMessage()],$this->successStatus);
        }

        return response()->json(["success"=>true, "types"=>$types],$this->successStatus);
    }

    public function breeds (Request $request){
        try{
            $type = $request->input('type');
            $breeds = PetType::find($type)->breeds()->orderBy('name')->get();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "response"=>$e->getMessage()],$this->successStatus);
        }

        return response()->json(["success"=>true, "breeds" => $breeds],$this->successStatus);
    }

    public function gender (){
        try{
            $gender = PetGender::all();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "response"=>$e->getMessage()],$this->successStatus);
        }

        return response()->json(["success"=>true, "gender" => $gender],$this->successStatus);
    }
}
