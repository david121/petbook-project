<?php

namespace App\Http\Controllers;

use App\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
        $activities = UserActivity::where('user', Auth::user()->id)
            ->orderBy("created_at", "DESC")
            ->get();
        }catch(\Exception $e){
            return  response()->json(["success"=>false, "message"=>$e->getMessage()]);
        }
        return  response()->json(["success"=>true, "activities"=>$activities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $activity
     * @param $pet
     * @return \Illuminate\Http\Response
     */
    public function store($activity,$pet)
    {
        try{
            $newActivity = new UserActivity;
            $newActivity->user = Auth::user()->id;
            $newActivity->activity = $activity;
            $newActivity->pet = $pet;
            $newActivity->save();
        }catch (\Exception $e){
            return response()->json(["success"=>false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success"=>true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
