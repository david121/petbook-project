<?php

namespace App\Http\Controllers;

use App\Like;
use App\Notifications\PostLike;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try{
           $liked = Like::where('user_id', Auth::user()->id)
               ->where('post_id',$request->post)
               ->first();
            $like = new Like;
           if ($liked !== null){
               Like::where('user_id',Auth::user()->id)
                   ->where('post_id',$request->post )
                   ->delete();
               return response()->json(['success'=>true],200);
           }
            $like->post_id = $request->input('post');
            $like->user_id = Auth::user()->id;
            $like->save();
            if ($request->input('postOwner') !== Auth::user()->id){
                $user = \App\User::find($request->input('postOwner'));
                $user->notify(new PostLike($request->input('post'),$request->input('pet')));
            }
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()],200);
        }
        return response()->json(['success'=>true, 'liked'=>true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Fetch Likes for a Post
        try{
            $likers = DB::table('posts')
            ->where('posts.id','=', $id)
            ->leftJoin('post_likes', 'post_likes.post_id', '=','posts.id')
            ->leftJoin('users','post_likes.user_id', '=','users.id')
        ->select('users.username', 'users.id','users.photo_path')->get();
            $response = $likers[0]->username === null ? "None" : $likers;
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(['success'=>true, 'likers'=>$response],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function destroy($id)
    {
        $likes = Like::where('post_id',$id);
        $likes->delete();
    }
}
