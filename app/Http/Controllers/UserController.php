<?php
namespace App\Http\Controllers;
use App\Comment;
use App\Enums\UserType;
use App\Like;
use App\Mail\PasswordRecovery;
use App\Mail\ValidateRegistration;
use App\Pet;
use App\Post;
use App\Friends;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Torann\GeoIP\GeoIP;

class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * Register api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'username' => 'required',
            'profileType' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["success"=>false, 'msg'=>$validator->errors()], $this->successStatus);
        }
        $params = $request->all();
        try{
            $usrError = false;
            $emailError = false;
//            dd($params['email']);
            if (DB::table('users')->where('username',$params['username'])->count()){
                $usrError = true;
            }
            if (DB::table('users')->where('email',$params['email'])->count()){
                $emailError = true;
            }
            if ($usrError || $emailError){
                return response()->json(['success'=>false,"userExists"=>true, "username"=>$usrError,"email"=>$emailError], $this->successStatus);
            }else{
               $user = User::create([
                   'firstname' => ucfirst(strtolower($params['firstName'])),
                   'lastname' => ucfirst(strtolower($params['lastName'])),
                   'username'=>strtolower($params['username']),
                   'profile_type'=>UserType::PetOwner,
                   'email' => strtolower($params['email']),
                   'password' => Hash::make($params['password']),
               ]);
               if ($user){
                   $code = rand(10000 , 99999);
                   if (DB::table('user_verification')->insert(["user_id"=>$user["id"],"code"=>$code])){
                       $user["code"] = $code;
                       Mail::to($user["email"])->send(new ValidateRegistration($user));
                   }
               }
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,'user_id'=> $user->id], $this->successStatus );
    }

    public function ReSendEmailVerificationCode(){
        try{
            $code = rand (10000,99999);
            if (
                    DB::table('user_verification')->where('user_id',Auth::id())
                    ->updateOrInsert(["user_id"=>Auth::id()],
                        ["code"=>$code]
                    )
                )
            {
                $user = User::find(Auth::id());
                $user["code"]= $code;
                Mail::to($user)->send(new ValidateRegistration($user));
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus );
    }

    public function SendPasswordRecoveryMail(Request $request){
        try{
            $code = rand (10000,99999);
            if (
            DB::table('password_recovery')->where('email',$request->input('email'))
                ->updateOrInsert(["email"=>$request->input('email')],
                    ["code"=>$code]
                )
            )
            {
                $userId = User::where('email',$request->input('email'))->pluck('id');
                $user = User::find($userId[0]);
                $user["code"]= $code;
                Mail::to($user)->send(new PasswordRecovery($user));
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus );
    }

    public function checkEmailExists(Request $request){
        try{
            $email = $request->input("email");
            if (DB::table('users')->where('email',$email)->count()){
                $user_id=DB::table('users')->where('email',$email)->pluck('id');
                $q = [];
                if (DB::table('pets')->where('user_id',$user_id)->count()){
                    $pet = DB::table('pets')
                        ->select('profile_photo','name','username','breed_id','id')
                        ->where('user_id',$user_id)
                        ->inRandomOrder()
                        ->limit('1')->get();
                    $petNames = DB::table('pets')
                        ->select("username")
                        ->where('username', '<>', $pet[0]->username)
                        ->inRandomOrder()->limit('4')->get();
                    $usernames=['a'=>$pet[0]->username];
                    foreach ($petNames as $petName) {
                        array_push($usernames, $petName->username);
                    }
                    shuffle($usernames);
                    $q["type"] = "pet_name";
                    $q["question"] = "What's your Pet's username ?";
                    $q["photo"] = $pet[0]->profile_photo;
                    $q["petId"] = $pet[0]->id;
                    $q["options"] = $usernames;
                    $q["answer"] = $pet[0]->username;
                }else{
                    $lastName = DB::table('users')->select('lastname')->where('id',$user_id)->get();
                    $lastNames = DB::table('users')
                        ->select('lastname')
                        ->where('lastname', '<>', $lastName[0]->lastname)->inRandomOrder()->limit('5')->get();
                    $list = [0=>$lastName[0]->lastname];
                    foreach ($lastNames as $last) {
                        array_push($list, $last->lastname);
                    }
                    shuffle($list);
                    $q["type"] = "lastName";
                    $q["question"] = "What's your Last Name ?";
                    $q["options"] = $list;
                    $q["answer"] = $lastName[0]->lastname;
                }

            }else{
                return response()->json(['success'=>true,'message'=>'NONE'], $this->successStatus );
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,"q"=>$q], $this->successStatus );
    }

    public function verifyUser (Request $request){
        try{
            $input = $request->input('code');
            $code = DB::table('user_verification')->where('user_id',Auth::id())->pluck('code');
            if ($input==$code[0]){
                User::where("id",Auth::id())->update(["email_verified_at"=>Carbon::now()]);
            }else{
              return response()->json(['success'=>true, 'msg'=>"Wrong Code"], $this->successStatus);
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,'msg'=>"User Verified"], $this->successStatus );
    }

    public function verifyRecoveryCode (Request $request){
        try{
            $input = $request->input('code');
            $code = DB::table('password_recovery')->where('email',$request->input('email'))->pluck('code');
            if ($input !==$code[0]){
                return response()->json(['success'=>true, 'msg'=>"Wrong Code"], $this->successStatus);
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,'msg'=>"Passed"], $this->successStatus );
    }

    public function changePassword (Request $request){
        try{
            $pass = $request->input('password');
            $newPassword = Hash::make($pass);
            if (DB::table('users')->where('email',$request->input('email'))->update(['password'=>$newPassword])){
                DB::table('password_recovery')->where('email',$request->input('email'))->delete();
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false, "msg"=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,'msg'=>"Password Changed"], $this->successStatus );
    }


    function getUser (Request $request) {
        try{
            $user = $request->user();
            $id = $user->id;
            $src = array();
            $user->photo_path == null || $user->photo_path == '' ? array_push($src, 'None'):array_push($src, Storage::url("users"."/".$id."/".$user->photo_path));

            $u = User::find(Auth::user()->id);
            $chatNotif = [];
            $otherNotif = [];
            foreach ($u->unreadNotifications as $n){
                if ($n->data['type'] === "Message"){
                    array_push($chatNotif,$n);
                }elseif ($n->data['type'] !== "Message"){
                    array_push($otherNotif,$n);
                }
            }
            $user->messages = count($chatNotif);
            $user->notify = count($otherNotif);
            $userFriends = DB::table('friends')->leftJoin('users',function ($join){
                $join -> on("friends.sender_user_id","=","users.id")->orOn("friends.receiver_user_id","=","users.id");
            })->select("friends.id AS friendship","friends.*", "users.*")
                ->where([['sender_user_id','=',$id],['status','=',1]])
                ->orWhere([['receiver_user_id','=',$id],['status','=',1]])->get();
            $likedPets = DB::table('admirers')->leftJoin('pets', function($join){
                $join -> on("admirers.pet_id", "=", "pets.id");
            })->select("admirers.id AS admire_id", "admirers.*", "pets.*")
                ->where([["admirers.user_id",Auth::user()->id], ["admirers.status", "=", 1]])->get();
            $loc = DB::table('temp_user_loc')->select('city','country')->where('user_id',Auth::user()->id)->get();
            $user->city = count($loc) ? $loc[0]->city : null;
            $user->country = count($loc) ? $loc[0]->country : null;
            if(count($likedPets)>0){
                $user->admired_pets = $likedPets;
            }else{
                $user->admired_pets = null;
            }

            if(count($userFriends)){
                $user->friends = $userFriends;
            }else{
                $user->friends = null;
            }

        }catch (\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()],$this->successStatus);
        }
        return response()->json(['success'=>true, 'user'=>$user, 'src'=>$src],$this->successStatus);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function userPosts (Request $request){
        try{
            $posts = null;
            $likedPosts= null;

            if ($request->has('profileId')){
                $petsId = [];
                $pets = DB::table('pets') ->where('user_id', '=', $request->profileId)->get(['id']);
                foreach ($pets as $pet){array_push($petsId,$pet->id);}
                $posts = Post::with(['likes', 'post_photo', 'pet'])->withCount(['is_liked' => function (Builder $query) {
                    $query->where('user_id', '=', Auth::user()->id);
                }])->whereIn('pet_id',$petsId)->where("posts.status","=",1)->orderBy('posts.created_at', 'desc')->get();
                foreach ($posts as $post){
                    if ($post->user_id !== $post->owner_user_id){
                        $postOwner = User::find($post->owner_user_id);
                        $post->postOwner = $postOwner->username;
                    }
                    $post->comments = count(Comment::where("post_id",$post->id)->get());
                }
                $response = count($posts) > 0 ? $posts : 'None';
                if ($response !== 'None'){
                    for($i=0; $i<count($posts); $i++){
                        $posts[$i]->time = Carbon::parse( $posts[$i]->created_at)->diffForHumans();
                    }
                }
            }else{
                $petsId = [];
                $pets = DB::table('pets') ->where('user_id', '=', Auth::user()->id)->get(['id']);
                foreach ($pets as $pet){array_push($petsId,$pet->id);}
                $posts = Post::with(['likes', 'post_photo', 'pet'])->withCount(['is_liked'  => function (Builder $query) {
                    $query->where('user_id', '=', Auth::user()->id);
                }])->whereIn('pet_id',$petsId)->where("posts.status","=",1)->orderBy('posts.created_at', 'desc')->get();
                foreach ($posts as $post){
                    if ($post->user_id !== $post->owner_user_id){
                        $postOwner = User::find($post->owner_user_id);
                        $post->postOwner = $postOwner->username;
                    }
                    $post->comments = count(Comment::where("post_id",$post->id)->get());
                }
                $response = count($posts) > 0 ? $posts : 'None';
                if ($response !== 'None'){
                    for($i=0; $i<count($posts); $i++){
                        $posts[$i]->time = Carbon::parse( $posts[$i]->created_at)->diffForHumans();
                    }
                }
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'posts'=>$response], 200);

    }


    /**
     * assign a temporary location to user
     * base on IP address.
     * @param $params
     * @return \Illuminate\Http\JsonResponse
     */
    function temp_location ($params){
        try{
            if (DB::table('temp_user_loc')->where('user_id',Auth::user()->id)->count()>0){
                $region = DB::table('temp_user_loc')->where('user_id',Auth::user()->id)->pluck('region');
                if ($params['region'] !== $region[0]){
                    DB::table('temp_user_loc')->where("user_id",Auth::user()->id)->update(
                        ["region"=>$params['region'],"city"=>$params['city'],"country"=>$params['country']]
                    );
                }
            }else
                DB::table('temp_user_loc')->insert([
                   ["user_id"=>Auth::user()->id,"region"=>$params['region'],"city"=>$params['city'],
                   "country"=>$params['country']
                   ]//,"long"=>$request->long,"lat"=>$request->lat
                ]);
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus);
    }

    function nearestUsers (Request $request) {
        try{
            $params = $request->all();
            $this->temp_location($params);
            $id = Auth::user()->id;
            $friendsId = [];
            $userFriends = DB::table('friends')
                ->where([['sender_user_id','=',$id],['status','=',1]])
                ->orWhere([['receiver_user_id','=',$id],['status','=',1]])
                ->get();
            foreach ($userFriends as $userFriend) {
                $userFriend->sender_user_id == Auth::user()->id?
                    array_push($friendsId,$userFriend->receiver_user_id) : array_push($friendsId,$userFriend->sender_user_id);
            }

            //suggestion by location
            $loc = DB::table('temp_user_loc')->join('users', 'users.id', '=', 'temp_user_loc.user_id')
            ->select('temp_user_loc.city','users.id','users.username','users.photo_path','users.profile_type')
            ->where([['temp_user_loc.city',$params['city']],['temp_user_loc.user_id','<>',Auth::user()->id]])
            ->whereNotIn('temp_user_loc.user_id',$friendsId)->distinct()
            ->inRandomOrder()
            ->limit('5')
            ->get();

            foreach ($loc as $m){
                $status = DB::table('friends')->where([['sender_user_id',Auth::user()->id],['receiver_user_id',$m->id],['status','<>',2]])
                    ->orWhere([['sender_user_id',$m->id],['receiver_user_id',Auth::user()->id],['status','<>',2]])->pluck('status');
                if (count($status)>0){
                    $m->status = $status[0];
                }else $m->status = null;
            }


            //suggesting friend's friends
            $mut = DB::table('friends')
                ->join('users', function ($join){
                    $join->on('users.id', '=', 'friends.sender_user_id')->orOn('users.id', '=', 'friends.receiver_user_id');
                })->select('users.username', 'users.id', 'users.photo_path','users.profile_type')
                ->where('users.id','<>', $id)
                ->whereIn('sender_user_id',$friendsId)
                ->where("status","=",1)
                ->whereNotIn('users.id',$friendsId)
                ->orWhereIn('receiver_user_id',$friendsId)
                ->where("status","=",1)
                ->whereNotIn('users.id',$friendsId)
                ->whereNotIn('sender_user_id',[$id])
                ->whereNotIn('receiver_user_id',[$id])
                ->distinct()->inRandomOrder()
                ->limit('5')
                ->get();

            foreach ($mut as $m){
                $status = DB::table('friends')->where([['sender_user_id',Auth::user()->id],['receiver_user_id',$m->id],['status','<>',2]])
                    ->orWhere([['sender_user_id',$m->id],['receiver_user_id',Auth::user()->id],['status','<>',2]])->pluck('status');
                if (count($status)>0){
                    $m->status = $status[0];
                }else $m->status = null;
            }

            $fr=[];
            foreach ($loc as $user) { array_push($fr,$user);}
            $frIDs = [];
            foreach ($fr as $user){ array_push($frIDs, $user->id);}
            foreach ($mut as $user){
                if (!in_array($user->id, $frIDs)){
                    array_push($fr,$user);
                }
            }

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true, 'suggestions' => $fr],$this->successStatus);
    }


    //Fetch profile a user is visiting
    function fetchVisitingProfile (Request $request) {
        try{
            $id = $request->input('profileId');
            $profile = DB::table('users')->select('id','username','photo_path','firstname','lastname','city','country','location','bio','verified','coins','profile_type')
                ->where('id',$id)->get();
            $src = [];
            //active user's friends
                $f = DB::table('friends')->leftJoin('users',function ($join){
                    $join -> on("friends.sender_user_id","=","users.id")->orOn("friends.receiver_user_id","=","users.id");
                }) ->select( "users.id")->where([['sender_user_id', Auth::user()->id],['status',1]])->whereNotIn('users.id',[Auth::user()->id])
                    ->orWhere([['receiver_user_id', Auth::user()->id],['status',1]])->whereNotIn('users.id',[Auth::user()->id])
                    ->get();
                    $fId = [];
                foreach ($f as $item) {
                    array_push($fId, $item->id);
                }
            //profile's friends
            $userFriends = DB::table('friends')->leftJoin('users',function ($join){
                $join -> on("friends.sender_user_id","=","users.id")->orOn("friends.receiver_user_id","=","users.id");
            }) ->select("friends.id AS friendship", "friends.*", "users.id", "users.photo_path", "users.username")
                ->where([['friends.sender_user_id','=',$id],['status','=',1]])
                ->whereNotIn('users.id',[$id])
                ->orWhere([['friends.receiver_user_id','=',$id],['status','=',1]])
                ->whereNotIn('users.id',[$id])
                ->get();
            foreach ($userFriends as $userFriend) {
                    $status = DB::table('friends')->where([['sender_user_id',Auth::user()->id],['receiver_user_id',$userFriend->id],['status','<>',2]])
                        ->orWhere([['sender_user_id',$userFriend->id],['receiver_user_id',Auth::user()->id],['status','<>',2]])
                        ->select('status','id')->get();
                    if (count($status)>0){
                        $userFriend->friend_status = $status[0];
                    }else $userFriend->friend_status = null;
                }

            $likedPets = DB::table('admirers')
                ->where([["admirers.user_id",$id], ["admirers.status", "=", 1]])->count();


            if($likedPets>0){
                $profile[0]->admired_pets = $likedPets;
            }else{
                $profile[0]->admired_pets = null;
            }
            $loc = DB::table('temp_user_loc')->select('city','country')->where('user_id',$id)->get();
            $profile[0]->city = count($loc) ? $loc[0]->city : null;
            $profile[0]->country = count($loc) ? $loc[0]->country : null;

            //check if profile is friends with active user
            $friend = Friends::where([['sender_user_id','=',Auth::user()->id], ['receiver_user_id','=',$id], ['status','<>',2]])
                ->orWhere([['sender_user_id','=',$id], ['receiver_user_id','=',Auth::user()->id], ['status','<>',2]])->get();
            if(count($friend)){
                $profile[0]->is_friend = $friend[0];
            }else{
                $profile[0]->is_friend = null;
            }
            foreach ($profile as $p) {
                    $p->photo_path == null || $p->photo_path == ''? array_push($src, 'None'):array_push($src, Storage::url("users"."/".$p->id . "/" . $p->photo_path));
            }

        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true, 'profile' => $profile,'src'=>$src, "friends"=>$userFriends],$this->successStatus);
    }

    function admiredPets (Request $request){
        try{
            $likedPets = DB::table('admirers')->leftJoin('pets', function($join){
                $join -> on("admirers.pet_id", "=", "pets.id");
            })->select("admirers.id AS admire_id", "admirers.*", "pets.*")
                ->where([["admirers.user_id",$request->input('profileId')], ["admirers.status", "=", 1]])
                ->get();
        }catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true, 'pets' => $likedPets],$this->successStatus);

    }

    function userBio(Request $request){
        try {
            $user = User::find(Auth::user()->id);
            $user->bio = $request->bio;
            $user->save();
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true],$this->successStatus);
    }

    function userCity(Request $request){
        try {
            $user = User::find(Auth::user()->id);
            if ($request->city == ''){
                $user->city = null;
                $user -> country= null;
                $user->save();
            }else{
                $input = explode(',',$request->city);
                $user->city = htmlspecialchars(trim($input[0]));
                $user->country =htmlspecialchars(trim($input[1]));
                $user->save();
            }
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true],$this->successStatus);
    }

    public function uploadCover (Request $request) {
        try{
            $user = $request->user();
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('public')->put("users"."/".$user->id. "/" . $file->getFilename().'.'.$extension,  File::get($file));

            $owner = User::find($user->id);
            $owner->photo_path = $file->getFilename().'.'.$extension;
            $owner->save();
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(["success"=>true],$this->successStatus);
    }

    public function Notifications()
    {
        try{
            $user = User::find(Auth::user()->id);
            $notif = $user->notifications;
            foreach ($notif as $n) {
                if ($n->data['type'] == "Post Like"){
                    if ($n->data['pet'] !== null){
                    if (Pet::find($n->data['pet'])){
                        $pet = Pet::find($n->data['pet']);

                            $user = DB::table('users')->select('id','username','photo_path')->where('id',$n->data['user'])->get();
                            $n->userId=$user[0]->id;
                            $n->username=$user[0]->username;
                            $n->photo=$user[0]->photo_path;
                            $n->petId = $pet->id;
                            $n->petName = $pet->name;
                            $n->time = Carbon::parse($n->created_at)->diffForHumans();
                        }
                    }
                }elseif($n->data['type'] == 'Friendship'){
                    $req = $friend = Friends::where([['sender_user_id','=',$n->data['sender']],['receiver_user_id', '=', Auth::user()->id]])->get();
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['sender'])->get();
                    $n->sender = $user;
                    $n->status = $req[0]->status;
                    $n->resTime = Carbon::parse($req[0]->updated_at)->diffForHumans();
                    $n->requestId = $req[0]->id;
                    $n->time = Carbon::parse($n->created_at)->diffForHumans();
                }elseif($n->data['type'] == 'Admirer'){
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['sender'])->get();
                    if (Pet::find($n->data['pet'])){
                        $pet = Pet::find($n->data['pet']);
                        $n->sender = $user;
                        $n->petName = $pet->name;
                        $n->petId = $pet->id;
                        $n->time = Carbon::parse($n->created_at)->diffForHumans();
                    }else null;
                }elseif($n->data['type'] == 'Comment Like'){
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['user'])->get();
                    $comment = Comment::where('id',$n->data['commentId'])->get();
                    $pet = DB::table('posts')->join('pets','pets.id','=','posts.pet_id')->select('pets.*')->where('posts.id',$comment[0]->post_id)->get();
                    if ($pet){
                        //dd("here");
                        $n->sender = $user;
                        $n->petName = $pet[0]->name;
                        $n->petId = $pet[0]->id;
                        $n->postId = $comment[0]->post_id;
                        $n->comment = $comment[0]->comment;
                        $n->time = Carbon::parse($n->created_at)->diffForHumans();
                    }else null;
                }elseif($n->data['type'] == 'New Pet'){
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['sender'])->get();
                    if (Pet::find($n->data['pet'])){
                        $pet = Pet::find($n->data['pet']);
                        $n->sender = $user;
                        $n->petName = $pet->name;
                        $n->petId = $pet->id;
                        $n->time = Carbon::parse($n->created_at)->diffForHumans();
                    }
                }
                elseif($n->data['type'] == 'Comment'){
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['sender'])->get();
                    $post = Post::find($n->data['post']);
                    if (Pet::find($post->pet_id)) {dd("here");
                        $pet = Pet::find($post->pet_id);
                        $n->sender = $user;
                        $n->petName = $pet->name;
                        $n->petId = $pet->id;
                        $n->postId = $post->id;
                        $n->time = Carbon::parse($n->created_at)->diffForHumans();
                    }
                }
                elseif($n->data['type'] == 'Comment Reply'){
                    $user = DB::table('users')->select('id','username','photo_path as photo')->where('id',$n->data['sender'])->get();
                    $n->comment = $n->data['comment'];
                    $n->reply = $n->data['reply'];
                    $n->sender = $user;
                    $n->time = Carbon::parse($n->created_at)->diffForHumans();
                }elseif($n->data['type'] == 'Friendship Accepted'){
                    $user = DB::table('users')->select('username','photo_path as photo','id')->where('id',$n->data['sender'])->get();
                    $n->sender = $user;
                    $n->time = Carbon::parse($n->created_at)->diffForHumans();
                }
            }
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(["success"=>true,"alert"=>$notif],$this->successStatus);
    }

    public function NotificationChecked(Request $request){
        try{
            $user = User::find(Auth::user()->id);
            $user->notifications->find($request->input('id'))->markAsRead();
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus);
    }

    public function NotificationsRead(){
        try{
            $user = User::find(Auth::user()->id);
            foreach ($user->unreadNotifications as $notification) {
                $notification->markAsRead();
            }
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus);
    }

    public function locationToggle(){
        try{
            $user = User::find(Auth::user()->id);
            if ($user->location == 1){
                User::where('id',Auth::id())->update(["location"=>0]);
            }else User::where('id',Auth::id())->update(["location"=>1]);;

        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true], $this->successStatus);
    }
    public function David(){
        try{
            $data = DB::table('users')->where('profile_type','=',2)->get();
        }
        catch (\Exception $e){
            return response()->json(['success'=>false, 'msg'=>$e->getMessage()], $this->successStatus);
        }
        return response()->json(['success'=>true,"data"=>$data], $this->successStatus);
    }

}
