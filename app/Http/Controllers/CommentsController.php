<?php

namespace App\Http\Controllers;

use App\Comment;
use App\CommentLike;
use App\CommentsComment;
use App\Post;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd("Hello");

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if ($request->has('parentId')){

                $parentComment=Comment::where('id',$request->input('parentId'))->get();
                $parentUser = $parentComment[0]->user_id;
                $initialComment= $parentComment[0]->comment;
                $reply = $request->input('comment');
//                dd($initialComment);
                $comment = new Comment;
                $comment-> user_id = Auth::user()->id;
                $comment-> comment = $reply;
                $comment-> post_id = $request->input('postId');
                $comment-> parent_id = $request->input('parentId');
                if ($comment-> save()){
                    if ($parentUser !== Auth::id()){
                        $userReply = \App\User::find($parentUser);
                        $userReply->notify(new \App\Notifications\CommentReply($initialComment,$reply));
                    }
                }
                $userId = Post::where('id',$request->input('postId'))->pluck('user_id');
                   if ($userId[0] !== Auth::id() && $parentUser !== Auth::id()) {
                       $user = \App\User::find($userId[0]);
                       $user->notify(new \App\Notifications\Comment($request->input('postId')));
                   }
            }else{
                $comment = new Comment;
                $comment-> user_id = Auth::user()->id;
                $comment-> comment = $request->input('comment');
                $comment-> post_id = $request->input('postId');
                $comment-> save();
                $userId = Post::where('id',$request->input('postId'))->pluck('user_id');
//                dd($userId);
                if ($userId[0] !== Auth::user()->id){
                    $user = \App\User::find($userId[0]);
                    $user->notify(new \App\Notifications\Comment($request->input('postId')));
                }
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'status'=>'posted'], 200);
    }

    function get_comments($parent, $post_id) {

        $comments = DB::table('comments')
            ->join('users','users.id','=','comments.user_id')
            ->join('posts', 'comments.post_id' ,'=', 'posts.id')
            ->select('users.username','users.photo_path','comments.*', 'comments.id as comment_id')
            ->where('post_id', $post_id)
            ->where('parent_id', $parent)
            ->orderBy('comments.created_at','DESC')
            ->get()->all();

        if($comments && count($comments) > 0){
            foreach ($comments as $k => $v)  {
                $comments[$k]->children = $this->get_comments($v->id, $post_id);
            }
        }
        return $comments;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
//            $src = [];
            $comments = $this->get_comments(0, $id);

            /*$comments = DB::table('users')
                ->join('comments','users.id','=','comments.user_id')
                ->join('posts', 'comments.post_id' ,'=', 'posts.id')
                ->select('users.username','users.photo_path','comments.*')
                ->where('post_id',$id)
                ->orderBy('comments.created_at','DESC')
                ->get();*/
//            $comments = Comment::with([])->withCount(['likes'=>function(Builder $builder){
//                    $builder->where('user_id',Auth::user()->id);
//            }])->where('post_id',$id)
//                ->orderBy('comments.created_at','DESC')
//                ->get();
            $results = count($comments) > 0 ? $comments : 'None';
            if ($results !== "None"){
                for($i=0; $i<count($comments); $i++){
                    $comments[$i]->time = Carbon::parse( $comments[$i]->created_at)->diffForHumans();
                }

                foreach ($results as $r){
                   $like = CommentLike::where([["comment_id",$r->comment_id],["user_id",Auth::user()->id]])->count();
                   $likes = CommentLike::where("comment_id",$r->comment_id)->count();
                   $r->likes = $likes;
                    if ($like > 0){
                        $r->liked = 1;
                    }
                }
//                foreach ($results as $comment){
//                    $comment->replies = CommentsComment::with(["user"])
//                        ->where("comment_id",$comment->id)->get();
//                }
//                foreach ($results as $res){
//                    $res->photo_path == null || $res->photo_path == '' ? array_push($src, 'No Cover'):array_push($src, Storage::url("users"."/".$res->user_id . "/" . $res->photo_path));
//                }
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'comments' => $results], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function destroy($id)
    {
        try{
            if (Comment::where('parent_id',$id)->count() > 0){
                $children = Comment::where('parent_id',$id)->pluck('id');
                $chIds =[];
                foreach ($children as $child) {
                    array_push($chIds,$child);
                }
                if (Comment::whereIn('id',$chIds)->delete()){
                    Comment::where('id',$id)->delete();
                }
            }else
//            $child = Comment::where('parent_id',$id)->get();
            Comment::where('id',$id)->delete();
        }catch (\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'status'=>'deleted'], 200);
    }

    public function reply (Request $request){
        try{
            $comment = new CommentsComment;
            $comment-> user_id = Auth::user()->id;
            $comment-> comment = $request->input('comment');
            $comment-> comment_id = $request->input('commentId');
            $comment-> save();

        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'status'=>'posted'], 200);
    }
}
