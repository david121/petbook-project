<?php

namespace App\Http\Controllers;

use App\Admirer;
use App\Friends;
use App\Notifications\FriendRequestAccepted;
use App\Notifications\FriendsRequest;
use App\Pet;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FriendsController extends Controller
{


    public function request(Request $request)
    {
        try {
            $request->validate([
                'user_id' => 'required'
            ]);

            if(Auth::user()->id === $request->user_id){
                throw new \Exception("You can't follow yourself");
            }

            DB::table('friends')
                ->updateOrInsert(
                    ['sender_user_id'=>Auth::user()->id,'receiver_user_id'=>$request->user_id],
                    ['status' => '0','created_at'=>Carbon::now()]
                );
            $sender = \App\User::find(Auth::id());
            $user = \App\User::find($request->user_id);
            $sender->receiver = $user->firstname;
            Mail::to($user)->send(new \App\Mail\FriendsRequest($sender));
            $user->notify(new FriendsRequest());
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public function confirm(Request $request)
    {
        try {

            $request->validate([
                'friendship_id' => 'required'
            ]);

            $friend = Friends::find($request->friendship_id);
          //  dd(["sender"=>$friend->sender_user_id, "receiver"=>$friend->receiver_user_id]);
            if(!$friend){
                throw new \Exception("Friendship not found");
            }
            if($friend->receiver_user_id !== Auth::user()->id){
                throw new \Exception("You can't confirm this friendship");
            }
            $friend->status = 1;

            if ($friend->save()){
                AdmirersController::friendshipAdmire($friend->sender_user_id);
                $user = \App\User::find($friend->sender_user_id);
                $user->notify(new FriendRequestAccepted());

            }


        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }

    public function cancel(Request $request)
    {
        try {
            $request->validate([
                'friendshipId' => 'required'
            ]);
            $friend = Friends::find($request->friendshipId);
            $initialStatus = $friend->status;
            $friendId = $friend->sender_user_id == Auth::id() ?$friend->receiver_user_id : $friend->sender_user_id;
//            dd($friendId);
            if(!$friend){
                throw new \Exception("Friendship not found");
            }

            if($friend->receiver_user_id !== Auth::id() && $friend->sender_user_id !== Auth::id() ){
                throw new \Exception("You can't cancel this friendship");
            }
            $friend->status = 2;
            $friend->save();
            if ($initialStatus === 1)AdmirersController::friendshipUnAdmire($friendId);
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true], 200);
    }


    public function getRequests()
    {
        try {
            $friend = Friends::with(['receiver_user', 'sender_user'])->where([['receiver_user_id', '=', Auth::id()], ['status', '=', 0]])->get();
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true, "list"=>$friend], 200);
    }

    public function getFriends()
    {
        try {
           // $friends = Friends::with(['receiver_user', 'sender_user'])->where([['receiver_user_id', '=', Auth::user()->id], ['status', '=', 1]])->orWhere([['sender_user_id', '=', Auth::user()->id], ['status', '=', 1]])->get();
            $friends = DB::table('friends')->leftJoin('users',function ($join){
                $join -> on("friends.sender_user_id","=","users.id")->orOn("friends.receiver_user_id","=","users.id");
            })->select("friends.id AS friendship","friends.*", "users.username","users.firstname","users.lastname", "users.id", "users.photo_path")
                ->where('users.id','<>', [Auth::id()])
                ->where([['sender_user_id','=',Auth::id()],['status','=',1]])
                ->orWhere([['receiver_user_id','=',Auth::id()],['status','=',1]])->get();
        } catch (\Exception $e) {
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true, "list"=>$friends], 200);
    }

    public function sentRequests(){
        try{
            $requests = DB::table('friends')->leftJoin('users','friends.receiver_user_id', 'users.id')
                ->select('friends.id','friends.created_at','friends.receiver_user_id as user_id','users.username','users.photo_path')
                ->where([['friends.sender_user_id',Auth::id()],['friends.status',0]])
                ->get();
        }catch (\Exception $e){
            return response()->json(["success" => false, "message" => $e->getMessage()], 200);
        }
        return response()->json(["success" => true,"requests"=>$requests], 200);
    }
}
