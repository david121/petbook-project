<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class Friendship extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $friendshipData = array(
                array('user_id'=>Auth::user()->id, 'friend_id'=> $request->friendId),
                array('user_id'=>$request->friendId, 'friend_id'=> Auth::user()->id),
            );
            if (\App\Friendship::insert($friendshipData)){
                //update related friend requests from pending to accepted
            }
        }catch(\Exception $e) {
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
            $username = $request->user;
            $user = User::where('username', $username)->pluck('id');

           if ($request->has('user')){
               $friends = new \App\Friendship;
               $friends= $friends->users()->find($user[0]);
            dd($friends);
           }else{
               // do something
           }
        }catch(\Exception $e) {
            return response()->json(["success"=>false, "message"=>$e->getMessage()],200);
        }
        return response()->json(["success"=>true],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
