<?php

namespace App\Http\Controllers;

use App\Comment;
use App\CommentLike;
use App\Like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentLikesController extends Controller
{
    public function like (Request $request){
        try{

            $liked = CommentLike::where([
                ['user_id', Auth::user()->id],
                ['comment_id',$request->input('commentId')]
            ])->first();

            if ($liked !== null){
                CommentLike::where([
                    ['user_id', Auth::user()->id],
                    ['comment_id',$request->input('commentId')]] )
                    ->delete();
                return response()->json(['success'=>true],200);
            }
            $like = new CommentLike;
            $like->user_id = Auth::user()->id;
            $like->comment_id = $request->input('commentId');
            $like->save();


            // send Notification
            $userId = Comment::where('id',$request->input('commentId'))->pluck('user_id');
            if ($userId[0] !== Auth::user()->id){
                $user = \App\User::find($userId[0]);
                $user->notify(new \App\Notifications\CommentLike($request->input('commentId')));
            }
        }catch(\Exception $e){
            return response()->json(['success'=>false,'msg'=>$e->getMessage()], 200);
        }
        return response()->json(['success'=>true, 'status'=>'liked'], 200);
    }
}
