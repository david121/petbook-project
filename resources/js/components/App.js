import React, {Component} from 'react';
import Index from './AUTHENTICATION/Display';
import {Switch, Route, Redirect, withRouter, NavLink} from 'react-router-dom';
import RegisterUser from './AUTHENTICATION/RegisterUser';
import PetRegistration from './AUTHENTICATION/REGISTER PET/Pet';
import Profile from './USER PROFILE/Profile';
import UserType from "./AUTHENTICATION/UserType";
import UserPhoto from "./AUTHENTICATION/UserPhoto";
import Home from "./HOME/Home";
import {connect} from "react-redux";
import Chats from './MESSAGING/ChatList';
import './App.css';
// import ListPet from "./LISTING/Listing";
// import FindPet from "./LISTING/FindPet";
import PetForm from "./AUTHENTICATION/REGISTER PET/PetForm";
// import AddPet from "./AUTHENTICATION/REGISTER PET/AddPet";
import PetProfile from "./USER PROFILE/PetProfile";
import Tests from "./TESTS/Layout";
import Search from "./SEARCH/Search";
import Visitor from "./VISITING PROFILE/Visitor";
import PetVisitor from "./VISITING PROFILE/Pet";
import Message from "./MESSAGING/Chat";
// import UserActivity from "./ACTIVITIES/UserActivity";
import Activity from "./ACTIVITIES/Activity";
import ArchivedPosts from "./ARCHIVE/Archive";
import {postOptions, selectPet, showAddPet} from "./POSTS/STORE/Actions";
import {fetchUser, toggleAboutPet, toggleProfilePreview} from "./USER PROFILE/STORE/Actions";
import {addSuggestedUser, admSuggestedPet, showFriends} from "./USER_FRIENDS/STORE/Actions";
import AllListing from "./LISTING/AllListing";
import VerifyEmail from "./AUTHENTICATION/VerifyEmail";
import PasswordReset from "./AUTHENTICATION/PasswordReset";
import NoMatch from "./404";
import Process from "./LAYOUT/Process";
import ExploreMobile from "./HOME/ExploreMobile";
import Post from "./POSTS/Post";
import Preview from "./POSTS/Preview";
import ProfilePreview from "./USER PROFILE/ProfilePreview";
import {explore, makePostMobile} from "./HOME/STORE/Actions";
import MyProfileVisit from "./DAVID/MyProfileVisit";

class App extends Component {

    componentDidMount() {
        if(localStorage.getItem('token')){
            this.props.user();
        }
    }

    render() {


        let suggestPets;
        let nearbyUsers;
        let img ;
        let admBtn;
        let frdBtn;
        if(Object.keys(this.props.suggestedPets).length){
            suggestPets= this.props.suggestedPets.map((item,index)=>{
                // console.log("Sugg",item);
                if (this.props.admireClick && this.props.admiredItem === item.id){
                    admBtn = <img src={'/img/paws.svg'} alt="admire" height={'25'} width={'25'} className={'float-right mr-3'}/>
                }else if (!this.props.userAdmiredPets.includes(item.id)){
                    admBtn = <img src={'/img/paw.svg'} alt="admire" height={'25'} width={'25'} className={'float-right mr-3'} onClick={()=>this.props.admire(item.id)}/>
                }
                return <div key={index} className='suggestions' >
                    {item.profile_photo === null ?
                        <img src={`/img/rodent.svg`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                        :
                        <img src={`/storage/pets/${item.id}/${item.profile_photo}`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                    }
                    <NavLink to={`/pet-view/${item.id}`} onClick={this.props.explore}>{item.username}</NavLink>
                    {admBtn}
                </div>
            })
        }
        if(Object.keys(this.props.usersNearby).length) {
            nearbyUsers = this.props.usersNearby.map((item, index) => {
                if(item.photo_path !==null){
                    img = <img src={`/storage/users/${item.id}/${item.photo_path}`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                }else img=<img src={`/img/avatar.png`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>;

                if (this.props.addUserClick && this.props.addedItem === item.id) {
                    frdBtn = <i className={'float-right mr-3'}>sending request</i>
                } else if (item.status === 0) {//!this.props.userFriends.includes(item.id)
                    frdBtn = <img src={'/img/hourglass.svg'} alt={"sent"} className={'float-right mr-3'} width={'23'} height={'23'}/>;
                }else frdBtn = <img src={'/img/add-user.svg'} alt={"add"} className={'float-right mr-3'}
                                    onClick={() => this.props.friendsRequest(item.id)} width={'25'} height={'25'}/>;
                return <span key={index} className='text-info suggestions'>
                    {img}
                    <NavLink to={`pet-owner/${item.id}`} onClick={this.props.explore}>{item.username}</NavLink>
                    {frdBtn}
                    </span>
            });
        }
        let dayFact;

        if (this.props.factOfTheDay){
            // console.log(this.props.factOfTheDay)
            dayFact = <div className="text-center p-2">
                <p style={{fontFamily:"Cambria"}}>{this.props.factOfTheDay.fact}</p>
                <i className="far fa-thumbs-up"/>
            </div>
        }else dayFact = <div className="text-center">
            <p style={{fontFamily:"Cambria"}}>Unavailable</p>
        </div>;




        const style = {
            marginRight : this.props.margin,
            transition: "margin-right .5s",
            overflow: "hidden",
            backgroundColor: "#f1f1f1",//#f1f1f1
            minHeight : "100vh"
        };
        return (
            <div style={style} onClick={() => {
                this.props.closeAddPet();
                this.props.showSelect();
                this.props.options();
                this.props.aboutPet();
            }} className="parent-app">
                {this.props.fullLoader? <Process/> : null}
                <Switch>
                    <Route path="/password-recovery" component={PasswordReset}/>
                    <Route path="/form" component={PetForm}/>
                    <Route path="/messages" component={Message}/>
                    <Route path="/chats" component={Chats}/>
                    <Route path="/pet-listing" component={AllListing}/>
                    <Route path="/archive" component={ArchivedPosts}/>
                    <Route path="/activities" component={Activity}/>
                    <Route path="/home" component={Home}/>
                    <Route path="/test-route" component={Tests}/>
                    <Route path="/search" component={Search}/>
                    <Route path="/itssme_david" component={MyProfileVisit}/>
                    <Route path="/pet/:petId" component={PetProfile}/>
                    <Route path="/pet-view/:petId" component={PetVisitor}/>
                    <Route path="/pet-owner/:id" render={(props) => {
                        return <Visitor/>
                    }}/>
                    <Route path="/profile" component={Profile}/>

                    <Route path="/verify" component={VerifyEmail}/>
                    <Route path="/register-pet" component={PetRegistration}/>
                    <Route path="/photo" component={UserPhoto}/>
                    <Route path="/register-pet-store" component={null}/>
                    <Route path="/register-pet-owner" component={RegisterUser}/>
                    <Route path="/registering-user-type" component={UserType}/>
                    <Route exact path="/" component={Index}/>
                    <Route>
                        <NoMatch/>
                    </Route>
                    </Switch>

                <div className="col-sm-12 col-lg-8 sm-home-posts">   {/*mt-4*/}

                    {this.props.showXplore?
                        <ExploreMobile
                            close={this.props.explore}
                            dayFact={dayFact}
                            nearbyUsers={nearbyUsers}
                            suggestPets={suggestPets}
                        />
                        :null}
                    {this.props.postMobile?
                        <div className="postMobile">
                            <Post/>
                            <Preview
                                poster = 'user'
                                page = "HOME"
                                user = {this.props.user_info.username}
                                user_ = {this.props.user_info}
                                userId = {this.props.user_info.id}
                                cover = {this.props.photo}
                                numOfpets = {this.props.user_pets.length}
                                pets = {this.props.user_pets}
                                petPhoto = {this.props.pet_cover}
                            />
                        </div>
                        :''
                    }
                    {localStorage.getItem('token') && !localStorage.getItem('authenticate') && this.props.mobileControl?
                    <div className="explore-sm">
                        <div className="col-sm-4 nav-sm-btn nav-sm-btn-1">
                            <NavLink to={"/home"}> <img src={"/img/home.svg"} alt="home" width={"30"} height={"30"} onClick={()=>{this.props.showXplore?this.props.explore():null}}/> </NavLink>
                        </div>
                        {/*<div className="col-sm-4 nav-sm-btn nav-sm-btn-2 text-center">*/}
                        {/*    <img src={"/img/camera.svg"} alt="camera" width={"30"} height={"30"} onClick={this.props.makePostMobile}/>*/}
                        {/*</div>*/}
                        <div className="col-sm-4 nav-sm-btn nav-sm-btn-2 text-center">
                            <img src={"/img/compass.svg"} alt="explore" width={"30"} height={"30"} onClick={this.props.explore}/>
                        </div>
                        <div className="col-sm-4 nav-sm-btn nav-sm-btn-3 text-right">
                            <NavLink to={'/profile'}> <img src={"/img/user2.svg"} onClick={()=>{this.props.showXplore?this.props.explore():null}} alt="user" width={"30"} height={"30"}/> </NavLink>
                        </div>
                    </div>:
                null}
                </div>
            </div>
        );
    }
}

const MapState = (state)=>{
    return{
        margin : state.Layout.contentMargin,
        user_info : state.User.user_info,
        user_verified : state.User.userVerified,
        fullLoader:state.Layout.fullLoader,
        mobileControl:state.Layout.mobileControl,

        photo : state.User.user_photo,
        user_pets : state.Home.user_pets,
        pet_cover : state.User.user_pet_photos,

        factOfTheDay : state.Layout.fact,
        suggestedPets : state.Search.pets,
        userAdmiredPets : state.Search.likedPets,
        usersNearby : state.Search.nearestUsers,
        showXplore : state.Home.explore,
        postMobile : state.Home.post,

    }
};

const MapDispatch = (dispatch) => {
    return {
        user : () =>{dispatch(fetchUser())},
        closeAddPet : ()=>dispatch(showAddPet(true)),
        showSelect : ()=>dispatch(selectPet(true)),
        options : (postId) => {dispatch(postOptions(postId, true))},
        showFriends : () =>{dispatch(showFriends(true))},
        aboutPet : ()=>{dispatch(toggleAboutPet(true, true))},
        explore : () =>{dispatch(explore())},
        makePostMobile : () =>{dispatch(makePostMobile())},
        admire : (petId) =>{dispatch(admSuggestedPet(petId))},
        friendsRequest : (id) => {dispatch(addSuggestedUser(id))},
    }
};

export default connect(MapState,MapDispatch)( withRouter(App));
