
import axios from 'axios'


export const commentHandler=(comment)=>{
    return dispatch =>{
        dispatch({
            type : "COMMENTING",
            value : comment
        })
    }
};

export const submitComment = (comment) =>{
    return dispatch =>{
        axios.post('/api/test_comment',{comment:comment})
            .then(response=>{
                if (response.data.success){
                    fetchComments();
                    dispatch({
                        type: 'POSTED'
                    });
                }else{
                    console.log(response.msg)
                }
            })
            .catch(error=>{
                alert(error);
            });
    }
};

export const fetchComments = () =>{
    return dispatch =>{
        axios.get('/api/test_comment')
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type: 'TEST_COMMENTS',
                        comments : response.data.comments
                    });
                }else{
                    console.log(response.msg)
                }
            })
            .catch(error=>{
                alert(error);
            });
    }
};
