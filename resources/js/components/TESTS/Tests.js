import React, {Component} from 'react';
import {connect} from "react-redux";
import {commentHandler, fetchComments, submitComment} from "./actions";
class Tests extends Component {
    componentDidMount() {
        this.props.fetchComments()
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        // this.props.fetchComments()
    }

    render() {
        let commentNodes;
        let childComment;
        let comments = this.props.comments.length > 0 ? this.props.comments.map((comment,index)=>{
           if (comment.parent_comment === 0){
               commentNodes = <div className="card-body">
                                    <span className="text-primary mr-1">{comment.comment}</span>
                                   <span>
                                       <input type="text" placeholder="write comment" value={this.props.comment} onChange={(e)=>this.props.commentHandler(e,"")}/>
                                        <button>submit</button>
                                   </span>
                                   <div className="ml-5">{comment.id === comment.parent_comment? comment.comment : ''}</div> {/* if comment.id === comment.parent_comment render child comment*/}
                              </div>
           }
           return <div key={index}>
                    <div>{commentNodes}</div>
             </div>
        }) : '';

        return (
            <div>
                <h2 className="text-center">RECURSION</h2>
                <div className="ml-2">
                    <b>Hello World</b><br/>
                    <input type="text" placeholder="write comment" value={this.props.comment} onChange={this.props.commentHandler}/>
                    <button onClick={()=>this.props.submitComment(this.props.comment)}>submit</button>
                </div>
                <div>{comments}</div>
            </div>
        );
    }
}

const MapState = (state) => {
    return {
        comment : state.Tests.comment,
        comments :state.Tests.comments,
    }
};

const MapDispatch = (dispatch)=>{
        return{
            fetchComments : () => {dispatch(fetchComments())},
            commentHandler : (e) => {dispatch(commentHandler(e.target.value))},
            submitComment : (comment) => {dispatch(submitComment(comment))},
        }
};

export default connect(MapState, MapDispatch)(Tests);
