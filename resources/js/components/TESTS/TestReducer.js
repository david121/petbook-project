

const initialState = {
    comment: '',
    comments : [],
};

const TestReducer = (state=initialState, actions)=>{
        switch (actions.type) {
            case "POSTED" :
                return{
                    ...state,
                    comment: ''
                };
            case "COMMENTING" :
                return {
                    ...state,
                    comment: actions.value
                };
            case "TEST_COMMENTS" :
                console.log("here")
                return {
                    ...state,
                    comments: actions.comments
                };
            default : return state;
        }
};

export default TestReducer;
