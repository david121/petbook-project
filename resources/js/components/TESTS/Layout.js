import React, {useState} from 'react';
import './CSS/LayoutLargeScreen.css';
import './CSS/SmallScreen.css';
import './CSS/LayoutGeneral.css'
import {Nav} from "react-bootstrap";
import NavComp from "../LAYOUT/Nav";
import { useSpring, animated as a } from 'react-spring'

const Layout = () => {


        // switch (this.state.comp) {
        //     case "A":
        //         render = (
        //                 <div className="compo comp-A d-flex justify-content-center align-items-center">
        //                     Component A
        //                     <button onClick={()=>{this.setState({flipped:!this.state.flipped})}} className="btn btn-warning ml-4">switch</button>
        //                 </div>
        //         );break;
        //     case "B" : render = (
        //         <div className="compo comp-B d-flex justify-content-center align-items-center">
        //             Component B
        //             <button onClick={()=>{this.setState({flipped:!this.state.flipped})}} className="btn btn-success ml-4">switch</button>
        //         </div>
        //     )
        // }

        const [flipped, set] = useState(false);
        const { transform, opacity } = useSpring({
                opacity: flipped ? 1 : 0,
                transform: `perspective(600px) rotateX(${flipped ? 180 : 0}deg)`,
                config: { mass: 5, tension: 500, friction: 80 }
        });


        return <div>
                <NavComp/>
                <div className="container below-nav">
                        <div>
                                <div onClick={() => set(state => !state)}>
                                        <a.div className="c compo comp-A" style={{opacity: opacity.interpolate(o => 1 - o), transform}}/>
                                        <a.div className="c compo comp-B" style={{opacity,transform: transform.interpolate(t => `${t} rotateX(180deg)`)}}/>
                                        {/*hasas*/}
                                </div>
                                <span style={{position:"absolute",right:"100px"}}>
                                        asdsd
                                </span>
                        </div>
                </div>
        </div>


}

export default Layout;
