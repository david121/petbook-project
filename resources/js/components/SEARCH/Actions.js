import axios from 'axios'
import {factOfTheDay} from "../HOME/STORE/Actions";
export const nearestUsers = (data) =>{
    return dispatch =>{
        axios.get('/api/nearestUsers',{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },params:data})
            .then(response => {
                if (response.data.success){
                    dispatch({
                        type : "USER_SUGGESTIONS",
                        value : response.data.suggestions,
                    })
                }else{
                    console.log(response.data.msg)
                    // alert (response.data.msg)
                }
            })
            .catch(error=>{

            });
    }
};

export const suggestPet =  () =>{
    return dispatch=>{
        axios.get('/api/suggestPets',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        }).then(response=>{
            if (response.data.success){
                dispatch({
                    type : "PETS_SUGGESTION",
                    pets : response.data.pets,
                    likedPets : response.data.likedPets,
                })
            }
        })
            .catch(error=>{
                console.log(error)
            })
    }
};


export const getClient = () =>{
    return dispatch =>{
        let data;
        axios.get('https://ipapi.co/json/').then(response=>{
            if (response.status === 200){
                const res = response.data;
                data = {
                    region: res.region_code,
                    country: res.country_name,
                    city: res.city,
                    long: res.longitude,
                    lat: res.latitude,
                };
                dispatch(nearestUsers(data));
            }else dispatch(nearestUsers({}));
            //console.log(response.data.city + ", " +response.data.country);
        });
    }

    /*
    * http://ip-api.com/json?callback=?
    *
    * https://ipapi.co/json/
    *
    * https://ipinfo.io/json
    *https://ip-api.com/json?callback=
    * */
};
