
const initialState = {
    nearestUsers : [],
    pets:[],
    likedPets : [],
};

const Search = (state = initialState, actions)=>{
    switch (actions.type) {
        case "USER_SUGGESTIONS":
            return{
                ...state,
                nearestUsers : actions.value,
            };
        case 'PETS_SUGGESTION':
            // console.log("REDUCER",actions.pets);
            return{
                ...state,
                pets : actions.pets,
                likedPets: actions.likedPets
            };

        default : return state;
    }
};

export default Search;
