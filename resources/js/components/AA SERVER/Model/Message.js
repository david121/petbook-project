const mongoose = require('mongoose');

const chatSchema = mongoose.Schema({
     chatId:{type:Number,required:true},
     data:[{
          id:{
               type:Number,required:true
          },
          sender_name:{
               type:String,required:true
          },
          sender_photo:{
               type:String,required:true
          },
          message:{type:String,required:true},
          Date:{type:Date,required:true,default: Date.now()}
     }],
     lastMessage:{type:String,required:true}
},{timestamps:true});



module.exports = mongoose.model('Chats', chatSchema);
