const express = require('express');
const http = require('https');
const socketIo = require('socket.io');
const db = require('mongoose');
require('dotenv/config');
const port = process.env.PORT || 5000;
const app = express();
const server = http.createServer(app);
const io = socketIo(server);
const bodyParser = require('body-parser');
const router = express.Router();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const {getMessages,setRoom, getUser} = require('./actions');
const Message = require('./Model/Message');

app.get('/getMessages',(req,res)=>{
    res.header('Access-Control-Allow-Origin', 'https://pawrry.com');
    Message.find({chatId:JSON.parse(req.query.chatId)}).exec().then(chat=>{
        if (chat)
        res.send(chat[0].data)
    })
});

/*
app.get('/chats',(req,res)=>{
    res.header('Access-Control-Allow-Origin', '*');
    db.connect(process.env.DB_CONNECTION,
        { useUnifiedTopology: true,useNewUrlParser: true },()=>{
            console.log(req.query.Ids);
            Message.find()
                .where('chatId')
                .in(req.query.Ids)
                .exec(function (err, records) {
                    if (err){
                        console.log(err);
                        return
                    }
                    res.send(records)
                });
        });
});
*/

let num = 0;
io.on('connection',(socket)=>{
    console.log("connected", num++);
    const conn = db.connect(process.env.DB_CONNECTION,
        { useUnifiedTopology: true,useNewUrlParser: true },()=>{
    //        console.log("connected to mongoDB")
        });
    socket.on('join',({room,friend,user})=>{
        const {users} = setRoom(room,friend,user);
        socket.join(users.roomId);
    });

    socket.on('sendMessage',({chatId,sender,message})=>{

       // console.log("ROOM LOG---: ",io.nsps['/'].adapter.rooms[chatId]);
        const photo = sender.sender_photo === null?"None" : sender.sender_photo;
        const data = {
            Date:Date.now(),
            id:sender.id,
            sender_name:sender.sender_name,
            sender_photo: photo,
            message:message,
        };

        Message.countDocuments({chatId:chatId},(err,count)=>{
            if (count>0){
                Message.updateOne(
                    {chatId:chatId},
                    { $push: {data:{id:sender.id,sender_name:sender.sender_name,sender_photo:photo,message}},lastMessage:message},
                    (err,raw)=>{
                        if (err){console.log(err)}
                        else {
                            if (io.nsps['/'].adapter.rooms[chatId] !== undefined){
                                    data.receipt = io.nsps['/'].adapter.rooms[chatId].length < 2 ? 'unread' : 'read';
                                }
                                data.chatId = chatId;
                                    io.to(chatId).emit('message', {data});//{sender,text:message}
                        }
                    }
                );
            } else{
                const msg = new Message({
                    chatId:chatId,
                    data:[{
                        id:sender.id,
                        sender_name:sender.sender_name,
                        sender_photo: photo,
                        message:message,
                    }],
                    lastMessage:message
                });
            msg.save().then((response)=>{
                socket.join(chatId);
                 io.to(chatId).emit('message', {data})
                // console.log(response);
            }).catch((error)=>{
                console.log(error)
            });
            }
        });

        //sender.message = message;

      //  io.to(chatId).emit('message', {data});//{sender,text:message}
    });


    socket.on('disc',()=>{
        socket.close();
        console.log('user disconnected',num++)
    })
});


server.listen(port,()=>{
    console.log("server up on port " + port)
});
