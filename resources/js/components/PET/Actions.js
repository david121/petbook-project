import axios from 'axios'
import {fetchPets} from "../USER PROFILE/STORE/Actions";
import {fetchUserPosts} from "../POSTS/STORE/Actions";

export const fetchFriends = () =>{ // fetch friends for transfer purpose
    return dispatch=>{
        dispatch({type:"FETCHING_FRIENDS"});
        axios.get(`/api/friends`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({type:"FETCHING_FRIENDS"});
                    // console.log("response", response.data.friends)
                    dispatch({
                        type : 'FRIENDS',
                        friends: response.data.list
                    })
                } else{
                    console.log(response.data.msg)
                }

            })
            .catch(error=>{
                console.log(error)
            });
    }
};

export const petReceiver = (receiver,friends) =>{
    let friend = friends.filter(friend=>{
        return receiver === friend.firstname + " " + friend.lastname
    });
    return dispatch =>{
            dispatch({
                type: "PET_RECEIVER",
                value : receiver,
                friend : friend
            })
    }
};
export  const consentCheck = () =>{
    return dispatch=>{
        dispatch({type:"CONSENT_CHECKED"})
    }
};

export const changeOwnership = (receiverId,petId) =>{

    return dispatch =>{
        dispatch({type:"TRANSFERRING_PET"});
        axios.post('/api/pet/changeOwnership',{
            petId : petId,
            newOwner : receiverId
        },{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }
        }).then(response=>{
            if (response.data.success){
                dispatch({type:"TRANSFERRING_PET"});
                dispatch(fetchPets());
                dispatch(fetchUserPosts());
                dispatch({
                    type : "PET_TRANSFERRED"
                })
            }
        }).catch(error=>{
            console.log(error)
        })
    }
};

export const petAdmirers = (petId) =>{
    return dispatch=>{
        axios.get(`/api/pet/admirers`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{id : petId}
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({type:"FETCHING_FRIENDS"});
                    dispatch({
                        type : 'ADMIRERS',
                        admirers: response.data.admirers
                    })
                } else{
                    // console.log(response.data.msg)
                }
            })
            .catch(error=>{
                console.log(error)
            });
    }
};