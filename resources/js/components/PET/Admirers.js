import React, {Component} from 'react';
import {connect} from "react-redux";
import {petAdmirers} from "./Actions";
import {NavLink} from "react-router-dom";
import './MDQPet.css'
import {cancelRequest, friendsRequest, toggleRequest} from "../VISITING PROFILE/STORE/Actions";
import Visitor from "../VISITING PROFILE/Visitor";

class Admirers extends Component {

    componentDidMount() {
        this.props.getAdmirers(this.props.petId)
    }

    render() {
        let friendStatusBtn;
        let admirers;
        let linkTo ;
        if (Object.keys(this.props.admirers).length){
            admirers = this.props.admirers.map((item,index)=>{

                linkTo = item.user_id === this.props.activeUser.id ? '/profile' : `/pet-owner/${item.user_id}`;

                if (item.friend_status !== null){
                    if (item.friend_status.status === 1){
                        friendStatusBtn = <button className="friend" onClick={()=>this.props.unfriend(item.friend_status.id, this.props.petId,"USER_PET")}>unfriend</button>;
                    }else
                    if (item.friend_status.status === 0){
                        if (this.props.toggleRequestBtn  && this.props.toggledItem === item.user_id){
                            friendStatusBtn=<button className="friend" onMouseLeave={()=>this.props.toggleRequest()} onClick={()=>this.props.unfriend(item.friend_status.id, this.props.petId,"USER_PET")}>cancel</button>;
                        }else friendStatusBtn=<button className="friend" onMouseEnter={()=>this.props.toggleRequest(item.user_id)}>request sent</button>;
                    }else friendStatusBtn = <button className="friend" onClick={()=>this.props.friendsRequest(item.user_id,this.props.petId,"USER_PET")}>add friend</button>;
                }else friendStatusBtn = <button className="friend" onClick={()=>this.props.friendsRequest(item.user_id,this.props.petId,"USER_PET")}>add friend</button>;

                return <li key={index} className="friend-item">
                        <div className="row">
                            <div className="col-sm-7">
                               {item.photo_path === null?
                                   <img src={`/img/avatar.png`} alt="user" width="30" height="30" className="rounded-circle"/>
                                :
                                   <img src={`/storage/users/${item.user_id}/${item.photo_path}`} alt="user" width="30" height="30" className="rounded-circle"/>
                                   }
                                <NavLink to={linkTo} className="ml-4">{item.username}</NavLink>
                            </div>

                            <div className="col-sm-4 text-right">
                                {item.user_id === this.props.activeUser.id ? '' : friendStatusBtn}
                            </div>
                        </div>

                </li>
            })
        }else admirers = <div style={{width:"100%",fontFamily:"Cambria",fontWeight:"Bold"}} className="text-center">No admirers</div>;

        return (
            <div className='friends-wrapper'>
                <ul>
                    {admirers}
                </ul>

            </div>
        );
    }
}

const mapState = (state) =>{
 return{
     admirers : state.AboutPet.admirers,
     activeUser : state.User.user_info,
     toggleRequestBtn : state.Visitor.toggleRequest,
     toggledItem : state.Visitor.toggledItem,

 }
};

const mapDispatch = (dispatch) =>{
    return {
        unfriend : (friendshipId,userId,page) => {dispatch(cancelRequest(friendshipId,userId,page))},
        getAdmirers : (id) =>{ dispatch(petAdmirers(id))},
        toggleRequest : (id) =>{dispatch(toggleRequest(id))},
        friendsRequest : (id,petId,page) => {dispatch(friendsRequest(id,petId,page))},
    }
};

export default connect(mapState,mapDispatch)(Admirers);
