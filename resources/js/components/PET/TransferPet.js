import React, {Component} from 'react';
import {navigateManagePet} from "../USER PROFILE/STORE/Actions";
import {connect} from "react-redux";
import {changeOwnership, consentCheck, fetchFriends, petReceiver} from "./Actions";

class TransferPet extends Component {
    componentDidMount() {
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log("checked",this.props.friends)
    }

    render() {
        let friendsList;
        let transferForm;
        let disableSubmit = this.props.receiver.length === 0 || this.props.checked === false;
        let receiver;
        let transferBtn = this.props.transferring ?<button className="btn btn-warning" disabled>
                                                          Transferring {this.props.petName} to {this.props.receiverName}
                                                        <span className="spinner-grow spinner-grow-sm"/>
                                                    </button> :
            <input type="submit" value="process transfer" className="btn btn-primary" disabled={disableSubmit}/>;
        if (this.props.receiver.length === 0){
            receiver = <b>___________</b>;
        }else{
            if (Object.keys(this.props.receiver).length){
                receiver = <b>{this.props.receiver[0].firstname + " " + this.props.receiver[0].lastname} <small>({this.props.receiver[0].username})</small></b>;
            }
        }
        let requestFriendsButton = <button className="btn btn-warning text-light " onClick={this.props.requestFriends}>Continue with profile transfer</button>;
        if (this.props.fetching){
            requestFriendsButton= <button className="btn btn-warning" disabled>
                    setting up transfer
                <span className="spinner-grow spinner-grow-sm"/>
            </button>
        }
        if (this.props.friendsLoaded){
            requestFriendsButton = <i className="text-success">Please select a friend you want to transfer your pet to and check the corresponding box</i>;
            friendsList = this.props.friends.map((friend,index)=>{
                // console.log(friend.firstname);
                    if(friend.id !== this.props.activeUser){
                        return <option key={index}>{friend.firstname + " " + friend.lastname}</option>
                    }
            });

             transferForm = <form onSubmit={(e)=>this.props.submit(e,this.props.receiver[0].id,this.props.petId)}>
                 <div style={{width:"100%"}} className="text-center">
                    <input style={{width:"50%"}} placeholder="search a friend" list="friends" required={true} value={this.props.receiverName} onChange={(e)=>this.props.petReceiver(e,this.props.friends)}/>
                 </div>
                 <datalist id="friends">
                     {friendsList}
                 </datalist>
                 <label style={{fontFamily:"Cambria", color:"darkslategrey"}}>
                     <input type="checkbox" className="m-3" checked={this.props.checked} required={true} onChange={this.props.consent}/>
                      I have read, and I agree to the terms and conditions stated above. Therefore, I want to continue with
                     transferring <b>{this.props.petName}</b> to <b>{receiver}</b>.
                 </label>

                 {transferBtn}

             </form>

        }
        return (
            <div className="">
                <div className="col-sm-12">
                   <button className="btn btn-info" onClick={this.props.editPage}>back</button>
                </div>
                <div className="col-sm-12 mt-2" style={{fontFamily:"Cambria", color:"darkslategrey"}}>
                    <b>Profile Transfer (Terms & Conditions)</b>
                    <p>
                        Pawrry also offer users with the functionality to transfer their pet's profile to another user (Change of Pet Ownership).
                       This functionality, however, is not without restrictions:
                    </p>
                    <ol>
                        <li>Transfer of pets can be established ONLY between two users with friendship status.</li>
                        <li>Transfer of a pet can be done ONCE in 6 months</li>
                    </ol>
                    {requestFriendsButton}
                </div>

                <div className="mt-2">
                    {transferForm}
                </div>
                {/*<div>*/}
                {/*    {this.props.transferred ? <h5 className="text-success">Transfer of {this.props.petName} to {this.props.receiverName} completed</h5> : ''}*/}
                {/*</div>*/}
            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        petName : state.EditPet.name,
        petId : state.EditPet.petId,
        fetching: state.ManagePet.fetching,
        friendsLoaded : state.ManagePet.loadedFriends,
        friends : state.ManagePet.friends,
        receiver : state.ManagePet.receiver,// Array of receiver data
        receiverName: state.ManagePet.receiverName,//input on change
        checked : state.ManagePet.consentChecked,
        transferring : state.ManagePet.transferring,
        transferred : state.ManagePet.transferred,
    };
};

const mapDispatch = (dispatch) =>{
    return {
        editPage: () =>{dispatch(navigateManagePet('edit'))},
        requestFriends :() =>{dispatch(fetchFriends())},
        petReceiver : (e,friends) => {dispatch(petReceiver(e.target.value,friends))},
        consent : () => {dispatch(consentCheck())},
        submit : (e,receiverId,petId) => {e.preventDefault();dispatch(changeOwnership(receiverId,petId))}
    };
};

export default connect(mapState,mapDispatch)(TransferPet);
