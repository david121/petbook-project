import React, {Component} from 'react';
import {
    deletePetProfile,
    editPetHandler,
    findPetBreed,
    navigateManagePet, removePetPic,
    saveEditedPet
} from "../USER PROFILE/STORE/Actions";
import {connect} from "react-redux";
import Process from "../LAYOUT/Process";

class EditPet extends Component {

    componentDidMount() {
        this.props.petBreed(this.props.breedId);
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.breed.name);
    }

    render() {
        let breeds = this.props.otherBreeds.map((breed,index)=>{
            return breed.name === this.props.breed.name ? '' : <option className="select-options" key={index} value={breed.id}>{breed.name}</option>
        });

        let currentPhoto = this.props.newPhoto === null ?
            this.props.photo === null ?
            <img  src={`/img/rodent.svg`} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
            :
            <img  src={`/storage/pets/${this.props.pet.id}/${this.props.photo}`} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
            : this.props.newPhoto === 'avatar'
            ?
            <img  src={'/img/rodent.svg'} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
            :
            <img  src={this.props.newPhoto} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>;


        let breed = Object.keys(this.props.breed) ? this.props.breed.name : 'breed';

        /*const formData = new FormData();
        formData.append("name", this.props.name);
        formData.append("petId", this.props.petId);
        formData.append("username",this.props.username);
        formData.append("d_o_b",this.props.birthDate);
        formData.append("breed",this.props.breedId);
        formData.append("photo", this.props.photo);*/

        const data = {
            name:this.props.name,
            petId:this.props.petId,
            username:this.props.username,
            d_o_b:this.props.birthDate,
            breed:this.props.breedId,
            photo:this.props.photo,
            content_type:this.props.content_type,
            ext : this.props.ext
        };

        return (
            <div className="text-center" style={{width:'100%',height:'100vh'}}>
                {this.props.fullLoader? <Process/> : null}
                        <form onSubmit={(e)=>this.props.controlSubmit(e,data)}>
                            <div className='text-center p-2'>
                                <div className="d-flex justify-content-center">
                                    {currentPhoto}
                                </div>
                                <div className="p-2">
                                    <label className = "change-pet-pic"> <input type="file" hidden={true} onChange={(e)=>this.props.inputHandler("photo",e)} />
                                        Change Picture
                                    </label>
                                    <span className="ml-2 remove-pet-pic" onClick={this.props.removePicture}>Remove picture</span>
                                </div>
                                {
                                    this.props.file_invalid ? <div className="mb-2 text-center"> <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                                        Warning: Invalid File{/* - Persistence may lead to a permanent deactivation of your account!*/}
                                    </b> </div> :this.props.pet_file_large ? <div className="mb-2 text-center"> <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                                        File too large{/* - Persistence may lead to a permanent deactivation of your account!*/}
                                    </b> </div> :null
                                }
                            </div>

                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">name</span>
                                </div>
                                <input type="text" className="form-control edit-pet-input" value={this.props.name} onChange={(e)=>this.props.inputHandler("name",e)}/>
                            </div>

                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">username</span>
                                </div>
                                <input type="text" className="form-control edit-pet-input" readOnly={true} value={this.props.username} onChange={(e)=>this.props.inputHandler("username",e)}/>
                            </div>

                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">breed</span>
                                </div>
                                <select onChange={(e)=>this.props.inputHandler("breed",e)}>
                                    <option value={this.props.breed.name}>{this.props.breed.name}</option>
                                    {breeds}
                                </select>
                            </div>

                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">birth date</span>
                                </div>
                                <input type="date" className="form-control edit-pet-input" value={this.props.birthDate} onChange={(e)=>this.props.inputHandler("birthDate",e)}/>
                            </div>
                            <input type="submit" value="save"/>
                        </form>
                        <div className="text-left mb-2">
                            {/*<span className="transfer-pet-link" onClick = {()=>this.props.transferPage('lineage')}>Lineage</span>*/}
                        </div>
                        <div className="text-left mb-2">
                            <span className="transfer-pet-link" onClick = {()=>this.props.transferPage('transfer')}>Transfer Profile</span>
                        </div>
                        <button className="btn btn-danger mt-1 mb-1"
                            onClick={()=>this.props.promptWarning(this.props.name,this.props.petId)}>
                            Delete Profile
                        </button>
                    </div>
        );
    }
}

const mapState = (state) =>{
    return {
        petId : state.EditPet.petId,
        photo: state.EditPet.photo,
        ext : state.EditPet.ext,
        newPhoto : state.EditPet.photoPreview, // Preview
        name : state.EditPet.name,
        breedId: state.EditPet.breed,
        birthDate: state.EditPet.birthDate,
        username: state.EditPet.username,
        content_type: state.EditPet.content_type,
        file_invalid: state.EditPet.invalidFile,
        pet_file_large: state.EditPet.pet_file_large,
        breed : state.User.petBreed,
        otherBreeds : state.User.typeBreeds,
        fullLoader:state.Layout.fullLoader
    }
};
const mapDispatch = (dispatch) =>{
    return {
        petBreed : (breedId) => {dispatch(findPetBreed(breedId))},
        inputHandler : (name,e) => {dispatch(editPetHandler(name,e))},
        transferPage : (page) => {dispatch(navigateManagePet(page))},
        controlSubmit : (e,data) => {e.preventDefault();dispatch(saveEditedPet(data))},
        removePicture : () => {dispatch(removePetPic())},
        promptWarning : (name,id) => {
           if (confirm(`Deleting ${name}'s profile will also remove affiliated properties such as Posts, Admirers etc.`)){
               dispatch(deletePetProfile(id))
           }
        },
    }
};

export default connect(mapState,mapDispatch)(EditPet);
