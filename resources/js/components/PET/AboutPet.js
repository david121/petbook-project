import React, {Component} from 'react';
// import Tab from "./Tab";
import './CSS/AboutPet.css';
import {connect} from "react-redux";
import {similarPets, toggleAboutPet} from "../USER PROFILE/STORE/Actions";
import {aboutBreed} from "../AUTHENTICATION/REGISTER PET/STORE/Actions";
import {NavLink} from "react-router-dom";
class AboutPet extends Component {
    componentDidMount() {
        this.props.similarPets(this.props.breed_id)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.similarPets)
    }

    render() {
            let similar ;
            let pic;
            if(Object.keys(this.props.pets).length){
                 similar = this.props.pets.map((pet,index)=>{
                     // console.log(pet)
                         if (pet.profile_photo === null ){
                            pic= <img src={'/img/avatar.png'} className="rounded-circle m-3" width="150" height="130" alt={"pet"}/>
                         }else pic = <NavLink to={`/pet-view/${pet.id}`}>
                                         <img style={{cursor:'pointer'}} src={`/storage/pets/${pet.id}/${pet.profile_photo}`} className="rounded-circle m-3" width="100" height="80" alt={"pet"}/>
                                         <h6 style={{cursor:'pointer'}} className="text-center"> {pet.username}</h6>
                                     </NavLink>;
                          if (this.props.petId !== pet.id){
                             return <div key={index} className="col-sm-6 col-lg-4 col-xl-4 text-center">
                                    {pic}
                                </div>
                      }
                });
            }else similar = this.props.breed === "Other Breed"? null:<div className ="text-center p-3" >
                <small><b> Oops {this.props.petName} is the only {this.props.breed} on Pawrry</b></small>
            </div>;

        return (
                <div>
                    <p className="breedInfo">{this.props.about}</p>
                    {
                        this.props.breed === "Other Breed" ?
                        <div style={{width:"100%",fontFamily:"Cambria",fontWeight:"Bold"}} className="text-center">Breed undetermined</div> :
                        <p className="mt-4">
                            <small className="text-info explore">other <span className="xploreBreed">{this.props.breed.toLowerCase()}s</span> <i className="fas fa-caret-down text-info"/></small>
                        </p>
                    }
                    <div className="row">
                        {similar}
                    </div>
                </div>

        );
    }
}

const mapState = (state) =>{
  return {
      toggleAbout : state.AboutPet.aboutPet,
      aboutBreed : state.AboutPet.breedInfo,
      pets : state.AboutPet.similarPets,
  }
};

const mapDispatch = (dispatch) =>{
    return {
        similarPets : (breedId)=>{dispatch(similarPets(breedId))},
    }
};

export default connect(mapState,mapDispatch)(AboutPet);
