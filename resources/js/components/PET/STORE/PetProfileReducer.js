

const initialState = {
    aboutPet : false,
    breedInfo : '',
    admirers : [],
    similarPets:[],
};

const PetProfileReducer = (state=initialState, actions)=>{ //AboutPet in Index
    switch (actions.type) {
        case "ABOUT_PET" :
            return{
                ...state,
                aboutPet: !state.aboutPet
            };
        case "ABOUT_BREED" :
            return {
                ...state,
                breedInfo : actions.value
            };
        case "SIMILAR_PETS" :
            return {
                ...state,
                similarPets : actions.pets
            };
        case "ADMIRERS" :
            return {
                ...state,
                admirers : actions.admirers
            };
        default : return state;
    }
};

export default PetProfileReducer;
