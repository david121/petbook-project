
const initialState = {
    activePage : 'EDIT_PET',
    fetching: false,
    loadedFriends: false,
    friends : [],
    receiver : [],
    receiverName : '',
    consentChecked : false,
    transferring: false,
    transferred : false
};

const ManagePet = (state = initialState, actions) => {
    switch (actions.type) {
        case "NAVIGATE_PAGE":
            return {
                ...state,
                activePage: actions.value
            };
        case "FETCHING_FRIENDS":
            return {
                ...state,
                fetching: !state.fetching
            };
        case "FRIENDS":
            return {
                ...state,
                loadedFriends: true,
                friends: actions.friends
            };
        case "PET_RECEIVER":
            return {
                ...state,
                receiver: actions.friend,
                receiverName: actions.value
            };
        case "CONSENT_CHECKED":
            return {
                ...state,
                consentChecked: !state.consentChecked
            };
        case "TRANSFERRING_PET":
            return {
                ...state,
                transferring: !state.transferring
            };
        case "PET_TRANSFERRED":
            return {
                transferred: true
            };

        default: return state;
    }
};

export default ManagePet;
