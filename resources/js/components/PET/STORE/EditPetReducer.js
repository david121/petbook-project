
const initialState = {
    petId : '',
    name : '',
    breed:'',
    birthDate:'',
    photo : null,
    ext: '',
    photoPreview : null,
    username : '',
    testPhoto:null,
    content_type:'',
    transferPage : false,
    invalidFile: false,
    pet_file_large: false,
};


const EditPet = (state = initialState, actions) => {
    switch (actions.type) {
        case "EDIT_PET_MODAL":
            // console.log(actions.pet.profile_photo)
            return {
                ...state,
                name:actions.pet.name,
                breed:actions.pet.breed_id,
                birthDate :actions.pet.date_of_birth,
                photo:actions.pet.profile_photo,
                username : actions.pet.username,
                petId : actions.pet.id
            };
        case 'EDIT_PET_INPUT_HANDLER':

            if (actions.preview){
                return {
                    ...state,
                    [actions.name] : actions.value,
                    content_type:actions.content_type,
                    photoPreview:  actions.preview,
                    invalidFile:false,
                    ext : actions.ext
                };
            }
            return {
                ...state,
                [actions.name] : actions.value,
            };
        case 'REMOVE_PIC' : return {
            ...state,
            photoPreview:'avatar',
            photo:'avatar'
        } ;
        case 'INVALID_FORMAT' : return {
            ...state,
            invalidFile: true,
        };
        case 'FILE_LARGE_PET' : return {
            ...state,
            pet_file_large: true,
        } ;
        case "TRANSFER_PAGE":
            return {
                ...state,
                transferPage: true
            };
        default: return state;
    }
};

export default EditPet;
