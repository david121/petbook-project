import React, {Component} from 'react';
import {connect} from "react-redux";
import EditPet from "./EditPet";
import TransferPet from "./TransferPet";
import Lineage from "./Lineage";

class ManagePet extends Component {


    render() {
        return (

            <div className="manage-pet-modal">
                <div className="managePet-content">
                    <div className="managePet-header">
                        <span className="close" onClick={this.props.close}>&times;</span>
                        <h5>Manage Pet Profile</h5>
                    </div>
                    <div className="managePet-body">
                        {this.props.active === 'EDIT_PET'? <EditPet
                                    userId = {this.props.userId}
                                    pet = {this.props.pet}
                                    close={this.props.close}
                                />
                                : this.props.active === "TRANSFER_PET"?
                            <TransferPet activeUser={this.props.userId}/>
                            : <Lineage/>
                        }

                    </div>
                </div>
            </div>
        );
    }
}


const mapState = (state) =>{
    return {
        active : state.ManagePet.activePage,
    }
};

const mapDispatch = (dispatch)=>{
    return {
        // petEdit : (pet) => {dispatch(petEdit(pet))},
    }
};

export default connect(mapState, mapDispatch)(ManagePet);
