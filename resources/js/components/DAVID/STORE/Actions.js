import axios from 'axios'
export const getProfile = () =>{
    return dispatch =>{
        axios.get('/api/david',{headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }}).then(res=>{
            if (res.data.success){
                dispatch({type:"MY_PROFILE",data:res.data.data})
            }
        })
    }
};