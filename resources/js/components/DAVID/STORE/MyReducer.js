

const initialState = {
    profile : [],
};

const MyReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'MY_PROFILE':
            return{
                ...state,
                profile: action.data
            };

        default : return state
    }
};

export default MyReducer;