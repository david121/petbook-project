import React,{Component} from 'react';
import NavComp from "../LAYOUT/Nav";
import ScrollContainer from "react-indiana-drag-scroll";
import {Nav} from "react-bootstrap";
import {connect} from 'react-redux'
import {getProfile} from "./STORE/Actions";
import {navigateComponent} from "../USER PROFILE/STORE/Actions";
import Friends from "../USER_FRIENDS/Friends";
import LikedPets from "../USER_FRIENDS/LikedPets";
import Loader from "react-loader-spinner";

class MyProfileVisit extends Component{

    componentDidMount() {
        this.props.getProfile()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.props.profile)
    }

    render(){

    let activeComp;
    let tmlStyle;
    let admStyle;
    let brdStyle;
    let profStyle;

        let data =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;

        if (Object.keys(this.props.profile).length){

        if(this.props.activePage === 'timeline'){
            tmlStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div>
                {null}
            </div>
        }else if (this.props.activePage === 'friends'){
            admStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="admirer-component">
                <Friends type="VisitorPage" profile={this.props.profile.id} friends={this.props.friends} />
            </div>
        }else if (this.props.activePage === 'likedPets'){
            brdStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="breed-component">
                <LikedPets profile="Visitor" pets={this.props.profile.admired_pets} profileId={this.props.profile.id} />
            </div>;
        }else if (this.props.activePage === 'profile'){
            profStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="breed-component">
                <div className="col-sm-12 col-lg-4 mb-3 profile-sm-component">

                    <div className="text-center col-sm-12 bg-light">
                        <b className="text-center profile-intro">Profile Intro</b>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                        {
                            this.props.profile.city === null ? "No city indicated" :
                                this.props.profile.location === 1 ?
                                    <div>
                                        <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                        <span className="ml-3 user-location"> {this.props.profile.city + " "+ this.props.profile.country}</span>
                                    </div>
                                    : <div>
                                        <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                        <span className="ml-3 user-location text-center"> void </span>
                                    </div>
                        }
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                        {
                            this.props.profile.bio === null || this.props.profile.bio === ''
                                ?
                                <div className="text-left">
                                    <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-3'}/>
                                    <b style={{fontFamily:"Cambria", color:"darkslategrey"}}>N/A</b>
                                </div>
                                :
                                <div className="pet-bio-wrapper p-2">
                                    <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                                    <span className="ml-4 user-bio">{this.props.profile.bio}</span>
                                </div>
                        }
                    </div>
                </div>
            </div>;
        }

    data = <div>
            <NavComp/>
            <div className="container mt-1">
                <div className="row">

                    <div className="col-sm-12 mb-1">
                        <div className="user-page-info">
                            <button onClick={()=>this.this.props.history.goBack()} className="btn btn-light ml-1 mt-1 text-info">&#8592;</button>

                            <div className="row">
                                {/*User photo*/}
                                <div className="user-image-holder text-center col-sm-12 col-lg-6 p-3">
                                    {this.props.profile.photo_path === null ?
                                        <img src={"/img/avatar.png"} alt="profile pic" width="150" height="130" className="rounded-circle"/>
                                        :
                                        <img src={`/storage/users/${this.props.profile.id}/${this.props.profile.photo_path}`} alt="profile pic" width="150" height="130" className="rounded-circle"/>
                                    }
                                </div>

                                <div className="col-sm-12 col-lg-6">
                                    <div className="">
                                        <div className="mt-lg-5 mt-sm-1 user-name-holder">
                                            <span className="pet-name-profile mr-1">
                                                {this.props.profile.firstname + " " + this.props.profile.lastname}
                                                <img src={'/img/verified.svg'} alt="mark" width={20} height={20} className="ml-3"/>
                                            </span><br/>
                                            <span className="pet-name-profile user-username-profile">( {this.props.profile.username} )</span>
                                        </div>
                                        <div className="mt-4 text-center" style={{height:"100%"}}>
                                            <div className="row">
                                                <div className="col-sm-12 col-lg-6">
                                                    add friend
                                                </div>
                                                <div className="col-sm-12 col-lg-6 text-right">
                                                    <span style={{cursor:'pointer'}} onClick={this.props.messageModal} className="p-4">
                                                        <img src={'/img/message.svg'} alt={'chats'} width={'35'} height={'35'}/>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    { this.props._messageModal?
                        <div className="send-message-modal">
                            <div className="message-content-wrapper">
                                <div className="send-message-header">
                                    <span className="close" onClick={()=>this.props.messageModal('')}>&times;</span>
                                    <h5>Message</h5>
                                </div>
                                <div className="send-message-body">
                                    <form>
                                        <textarea rows="5" onChange={this.props.messageHandler} value={this.props.composedMessage} className="message-input-field" style={{width:'100%',resize:'none',border:'1px solid darkslategrey'}} placeholder="write a message ..."/>
                                        <div className="text-right">
                                            <button onClick={(e)=>this.props.sendVisitorMessage(e,this.props.profile.id,sender,this.props.composedMessage)} style={{backgroundColor:'darkslategrey', color:'white'}}>send message</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> : ''
                    }


                    {/*Pets*/}
                    <div className="col-sm-12">
                        <div className="pets pet-panel">
                            <ScrollContainer className="scroll-container ml-3">
                                {null}
                            </ScrollContainer>
                        </div>
                    </div>

                </div>


                <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                    <Nav.Item>
                        <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("friends")} className='tab-text' style={admStyle}>Friends ({null})</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("likedPets")} className='tab-text' style={brdStyle}>Admired Pets ({this.props.profile.admired_pets !== null ? this.props.profile.admired_pets : 0})</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="sm-profile-tab">
                        <Nav.Link eventKey="link-3" onClick={()=>this.props.navigateComponent("profile")} className='tab-text' style={profStyle}>Profile</Nav.Link>
                    </Nav.Item>
                </Nav>

                <div className="mt-3">
                    <div className="row">

                        <div className="col-sm-12 col-lg-4 mb-3 lg-profile-intro">

                            <div className="text-center col-sm-12 bg-light">
                                <b className="text-center profile-intro">Profile Intro</b>
                            </div>

                            <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                                {
                                    this.props.profile.city === null ? "No city indicated" :
                                        this.props.profile.location === 1 ?
                                            <div>
                                                <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                                <span className="ml-3 user-location"> {this.props.profile.city + " "+ this.props.profile.country}</span>
                                            </div>
                                            : <div>
                                                <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                                <span className="ml-3 user-location text-center"> void </span>
                                            </div>
                                }
                            </div>

                            <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                                {
                                    this.props.profile.bio === null || this.props.profile.bio === ''
                                        ?
                                        <div className="text-left">
                                            <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-3'}/>
                                            <b style={{fontFamily:"Cambria", color:"darkslategrey"}}>N/A</b>
                                        </div>
                                        :
                                        <div className="pet-bio-wrapper p-2">
                                            <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                                            <span className="ml-4 user-bio">{this.props.profile.bio}</span>
                                        </div>
                                }
                            </div>
                        </div>

                        <div className="col-sm-12 col-lg-8">
                            {activeComp}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    }
        return data;
    }
}

const MapState = (state) =>{
    return{
        profile : state.MyProfile.profile,
        activePage : state.User.activeComponent,
    };
};
const MapDispatch = (dispatch) =>{
    return {
        getProfile : ()=>{dispatch(getProfile())},
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},

    }
};

export default connect(MapState,MapDispatch)(MyProfileVisit);