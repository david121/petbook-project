import React, {Component} from 'react';
import Nav from "../LAYOUT/Nav";
import {connect} from "react-redux";
import css from './CSS/Archive.module.css';
import "./SavedPosts";
import  "./HiddenPosts";
import { renderArchivedPosts} from "./STORE/Actions";
import HiddenPosts from "./HiddenPosts";
import ArchivedPosts from "./SavedPosts";

class Archive extends Component {

    componentDidMount() {
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Archive";
    }

    render() {
        let hiddenClass = ["archive-links"]; let archiveClass = ["archive-links"];
        if (this.props.active==="ARCHIVED"){
            archiveClass.push("archived-active")
        }else
            if(this.props.active==="HIDDEN"){
            hiddenClass.push("hidden-active")
        }

        return (
            <div>
                <Nav username={this}/>
                <div className="container below-nav">

                    <div className="left-pane">
                        <div>
                            <span className={archiveClass.join(" ")} onClick={()=>this.props.renderArchive(false)}>Archived Posts</span>
                        </div>
                        <div>
                            <span className={hiddenClass.join(" ")} onClick={()=>this.props.renderArchive(true)}>Hidden Posts</span>
                        </div>
                    </div>

                    <div className="main">
                        {this.props.hidden?<HiddenPosts/> : <ArchivedPosts/>}
                    </div>

                    <div className="right-pane">

                    </div>
                </div>
            </div>
        );
    }
}

export const mapState = (state) =>{
    return {
        active: state.Archive.activeComponent,
        archived : state.Archive.archived,
        hidden : state.Archive.hidden,
    }
};

export const mapDispatch = (dispatch) =>{
    return {
        renderArchive : (hiddenPosts)=>{dispatch(renderArchivedPosts(hiddenPosts))}
    }
};

export default connect(mapState,mapDispatch)(Archive);
