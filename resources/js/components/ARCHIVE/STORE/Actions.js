
import axios from 'axios';
import {fetchAllPosts} from "../../HOME/STORE/Actions";
import {fetchUserPosts} from "../../POSTS/STORE/Actions";
import {Api_Process} from "../../LAYOUT/STORE/Actions";

export const renderArchivedPosts = (hiddenPosts) =>{
   return dispatch =>{
       if (hiddenPosts){
           dispatch({
               type : "RENDER_HIDDEN"
           })
       }else
       dispatch({
        type : "RENDER_ARCHIVED"
       })
   }
};

export const archivedPosts=()=>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.get('/api/archive',{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    // dispatch(fetchAllPosts());
                    dispatch({
                        type : "ARCHIVED_POSTS",
                        posts : response.data.posts
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};

export const hiddenPosts=()=>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.get('/api/archive/hidden',{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    dispatch({
                        type : "HIDDEN_POSTS",
                        posts : response.data.posts
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};

export const restorePost=(postId,archiveId)=>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.post('/api/archive/restore',{post_id:postId,archive_id:archiveId},{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    dispatch(hiddenPosts());
                    dispatch({
                        type : "HIDDEN_POSTS",
                        posts : response.data.posts
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};

export const unsavePost=(archiveId,page)=>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.post('/api/archive/unsave',{archive_id:archiveId},{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    switch (page) {
                        case "HOME": dispatch(fetchAllPosts());break;
                        case "ARCHIVE": dispatch(archivedPosts());break;
                    }
                    dispatch({
                        type : "SAVED_POST_REMOVED",
                        posts : response.data.posts
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};
