
const initialState = {
    archived : false,
    hidden : false,
    activeComponent: 'ARCHIVED',
    hiddenPosts : [],
    savedPosts : [],
};

const Archive = (state= initialState, actions) =>{
        switch (actions.type) {
            case "RENDER_ARCHIVED":
                return{
                    ...state,
                    archived: !state.archived,
                    hidden: false,
                    activeComponent: "ARCHIVED"
                };
            case "RENDER_HIDDEN":
                return{
                    ...state,
                    archived: false,
                    hidden: !state.hidden,
                    activeComponent: "HIDDEN"
                };
            case "HIDDEN_POSTS":
                return{
                    ...state,
                    hiddenPosts: actions.posts
                };
            case "ARCHIVED_POSTS":
                return{
                    ...state,
                    savedPosts: actions.posts
                };
            default: return state;
        }
};
export default  Archive;
