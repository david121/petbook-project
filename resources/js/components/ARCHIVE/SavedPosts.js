import React, {Component} from 'react';
import {archivedPosts, unsavePost} from "./STORE/Actions";
import {connect} from "react-redux";
import ShowPosts from "../POSTS/ShowPosts";
import {likePost, postComment, toggleComments, writeComment} from "../POSTS/STORE/Actions";
import AllPosts from "../POSTS/AllPosts";

class SavedPosts extends Component {
    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        let posts;
        if (this.props.posts === null){
            posts = <div className="card" id='post'>
                <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>No saved Posts!</h2>
                    </span>
                </div>
            </div>
        }else {
            if (this.props.posts){
                posts = this.props.posts.map((archive,index)=>{
                    // console.log("LOOP",archive.post[0]);
                    return <ShowPosts
                        key = {index}
                        page = "SAVED_POSTS"
                        archive = {true}
                        savedPost ={true}
                        post_owner = {archive.post[0].owner_user_id}
                        post_owner_username = {archive.postOwner}
                        remove = {()=>this.props.unsavePost(archive.id)}
                        pet = {archive.post[0].pet.name}
                        petId = {archive.post[0].pet_id}
                        // index = {index}
                        user = "petOwner"
                        postPic = {archive.post[0].post_photo}
                        numOfPics = {archive.post[0].post_photo.length}
                        petPhoto = {archive.post[0].pet.profile_photo}
                        userCover = {archive.post[0].user.photo_path}
                        userId = {archive.post[0].user.id}
                        userName = {archive.post[0].user.username}
                        postCaption = {archive.post[0].caption}
                        time = {archive.time}
                         clicked = {this.props.showComments}
                         clickedPost = {this.props.clickedPost}
                        postId = {archive.post_id}
                        totalComments = {archive.comments}
                        totalLikes = {archive.post[0].likes.length}
                        toggleComments = {()=>this.props.comments(archive.post[0].id,this.props.showComments)}
                        commentHandler = {this.props.writeComment}
                        liked = {archive.post[0].is_liked_count}
                        like = {this.props.likePost}
                        comment = {this.props.writtenComment}
                        commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,archive.post[0].id)}
                    />
                })
            }
        }

        return (
            <div>
                {posts}
            </div>
        );
    }
}
export const mapState = (state) =>{
    return {
        posts: state.Archive.savedPosts,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,
    }
};

export const mapDispatch = (dispatch) =>{
    return {
        fetchPosts : () => {dispatch(archivedPosts())},
        writeComment : (e) => {dispatch(writeComment(e.target.value))},
        comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId))},
        likePost : (postId,user,pet) =>{dispatch(likePost(postId,user,pet,"SAVED"))},
        unsavePost : (id) => {dispatch(unsavePost(id,'ARCHIVE'))}
    }
};
export default connect(mapState,mapDispatch)(SavedPosts);
