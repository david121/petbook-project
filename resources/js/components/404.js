
import React,{useEffect} from 'react';
import {NavLink} from "react-router-dom";

const NoMatch =() =>{

    useEffect(()=>{

    });

    return(
        <div>
            <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                <span className="navbar-brand"><NavLink to={'/'} style={{color:"white",textDecoration:"none"}}>Pawrry</NavLink></span>
            </nav>
            <div style={{width:"100%",height:"90vh"}} className="text-center">
                {/*<span>OOPS! YOU HIT THE WRONG ROUTE</span>*/}
                <img src={"/img/sad.jpg"} alt="not found" style={{width:"40%", height:"40%"}}/>
            </div>
        </div>
    )
};

export default NoMatch;