import axios from 'axios';
import {fetchUser, fetchVisitingProfile} from "../../USER PROFILE/STORE/Actions";
import {axiosHeader} from "../../index";
import {cancelRequest} from "../../VISITING PROFILE/STORE/Actions";
import {fetchChats} from "../../MESSAGING/STORE/ChatActions.Js";
export const openNav=()=>{

    return dispatch => {
        dispatch({
            type: 'OPEN_NAV',
            sidenavWidth: "250px", // sidenav width
            marginRight: '250px' // content margin right
        })
    }
};

export const closeNav=()=>{

    return dispatch => {
        dispatch({
            type: 'CLOSE_NAV',
            sidenavWidth: "0px", // sidenav width
            marginRight: '0px' // content margin right
        })
    }
};

export const toggleNotifier = () =>{
    return dispatch =>{
            dispatch({
                type: "TOGGLE_NOTIFY",
            });
    }
};
export const sentRequests = (notifier) =>{
    return dispatch =>{
        if (notifier){
            return dispatch({type:"TOGGLE_SENT_REQ"})
        }else {
            dispatch({type:"TOGGLE_SENT_REQ"});
            dispatch({type:"FETCHING_REQUESTS"});
            axios.get('/api/sentRequests',{
                headers:{
                    "Content-type" : "application/json",
                    "Accept" : "application/json",
                    "Authorization" : "Bearer " + localStorage.getItem('token')
                }
            }).then(response=>{
                dispatch({type:"FETCHING_REQUESTS"});
                if (response.data.success){
                    dispatch({type:"SENT_REQUESTS",requests:response.data.requests})
                }
            })

        }
    }
};

export const toggleSearch = () =>{
    return dispatch =>{
        dispatch({
            type: "TOGGLE_SEARCH",
        })
    }
};

export const Notifications = () =>{
    return dispatch =>{
        axios.get('/api/notifications',{headers:{
            "Content-type" : "application/json",
            "Accept" : "application/json",
            "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(response=>{
                if(response.data.success){
                    dispatch({
                        type: "NOTIFICATIONS",
                        notifications:response.data.alert
                    })
                }
            })
            .catch(err=>{

            });

    }
};

export const NotificationChecked = (id) =>{
    return dispatch =>{
        axios.post('/api/notification/checked',{id},{headers:{
            "Content-type" : "application/json",
            "Accept" : "application/json",
            "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(response=>{
                if(response.data.success){
                    // dispatch(fetchChats());
                    dispatch(fetchUser());
                    dispatch(Notifications());
                }
            })
            .catch(err=>{

            });

    }
};

export const markAllAsRead = () =>{
    return dispatch =>{
        axios.post('/api/notifications/allRead',null,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token'),
            }
        }).then(res=>{
            if (res.data.success){
                dispatch(fetchUser())
            }
        }).catch()
    }
};

export const searchHandler = (needle,haystack) =>{
    return dispatch =>{
        let res=[];
        if (needle !== '')
         res = haystack.filter((item)=>{
            return  item.username.includes(needle)
        });
        dispatch({
            type: "SEARCH_INPUT",
            value: needle,
            results:res
        })
    }
};

export const receivedRequest = () =>{
    return dispatch =>{
        axios.get(`/api/friends/requests`,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            },
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type: "RECEIVED_REQUEST",
                        withRequest : true,
                        value : response.data.list
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            });
    }
};

export const acceptRequest = (friendId, refresh,res) =>{
    return dispatch => {
        if (res === 2){
            dispatch(cancelRequest(friendId,null,"NOTIFY"));
            return false;
        }
        const data = {
             friendship_id : friendId,
         };
        axios.post('/api/friends/confirm',data,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    refresh ? dispatch(fetchVisitingProfile(refresh)):
                    dispatch(receivedRequest());
                    if (res)dispatch(Notifications());
                    dispatch({
                        type : "FRIENDSHIP_ESTABLISHED"
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const Search = (data) =>{
    return dispatch =>{
        // console.log(data)
        if (data !== ''){
            axios.get('/api/search',{
                headers:{
                    "Content-Type" : "application/json",
                    "Accept" : "application/json",
                    "Authorization" : "Bearer " + localStorage.getItem("token"),
                },
                params:{
                    needle : String(data)
                }
            })
            .then(response=>{
                if (response.data.success){
                        dispatch({
                            type: "SEARCH",
                        })
                }
            })
            .catch(error=>{
                console.log(error)
            });
        }
    }
    };

export const HayStack = () =>{
    return dispatch =>{
        // dispatch(Api_Process());
        axios.get('/api/haystack',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            },
        })
        .then(res=>{
            // dispatch(Api_Process());
            if (res.data.success){
                dispatch({type:"HAYSTACK_",value:res.data.result})
            }
        })
    }
};
export const Api_Process = () =>{
  return dispatch=>{dispatch({type:"API_PROCESS"})}
};
