
const initialState = {
    contentMargin : '',
    sidenavWidth : '',
    toggleSearch : false,
    toggleNotify : false,
    needle : '',
    search: false,
    fullLoader:false,
    fact : {},
    stacks:[],
    showSearchLog:false,
    queryResults:[],
    mobileControl:true
};

const Layout = (state = initialState, action) =>{
    switch (action.type) {
        case 'TOGGLE_SEARCH':
            return{
                ...state,
                toggleSearch: !state.toggleSearch
            };
        case 'TOGGLE_CONTROL':
            return{
                ...state,
                mobileControl: !state.mobileControl
            };

        case 'DAY_FACT':
            return{
                ...state,
                fact : action.value
            };
        case 'HAYSTACK_':
            return{
                ...state,
                stacks : action.value
            };
        case 'SEARCH_LOG':
            return{
                ...state,
                showSearchLog: !state.showSearchLog
            };

        case 'TOGGLE_NOTIFY':
            return{
                ...state,
                toggleNotify: !state.toggleNotify
            };
        case 'SEARCH_INPUT':
            return{
                ...state,
                needle : action.value,
                queryResults: action.results
            };
        case 'SEARCH':
            return{
                ...state,
                search : true
            };
        case 'OPEN_NAV':
            return{
                ...state,
                contentMargin: action.marginRight,
                sidenavWidth : action.sidenavWidth
            };
        case 'CLOSE_NAV':
            return{
                ...state,
                contentMargin:action.marginRight,
                sidenavWidth:action.sidenavWidth
            };
        case 'API_PROCESS':
            return{
                ...state,
                fullLoader : !state.fullLoader
            };
        default : return state
    }
};

export default Layout;