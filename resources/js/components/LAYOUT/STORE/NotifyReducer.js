
const initialState = {
    friendRequests : [],
    sentRequests : [],
    alerts:[],
    notify_collapse:false,
    open_requests:false,
    replyView:false,
    notifComment:null,
};

const Notifications = (state = initialState, action) =>{
    switch (action.type) {
        case 'RECEIVED_REQUEST':
            return{
                ...state,
                friendRequests: action.value
            };
        case 'SENT_REQUESTS':
            return{
                ...state,
                sentRequests: action.requests
            };
        case 'TOGGLE_SENT_REQ':
            return{
                ...state,
                open_requests: !state.open_requests
            };
        case 'FETCHING_REQUESTS':
            return{
                ...state,
                open_requests: true
            };
        case 'NOTIFICATIONS':
            return{
                ...state,
                alerts: action.notifications
            };
        case 'COLLAPSED_ALERT':
            return{
                ...state,
                notify_collapse: !state.notify_collapse
            };
        case 'VIEW_NOTIF_REPLY':
            return{
                ...state,
                replyView: !state.replyView,
                notifComment: action.id,
            };
        default : return state
    }
};

export default Notifications;
