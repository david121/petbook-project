import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './CSS/Layout.css';
import './CSS/Notify.css';
import './CSS/MQNav.css';
import './CSS/Search.css';
import {signOut} from './../AUTHENTICATION/STORE/Actions'
import {connect} from "react-redux";
import {
    openNav,
    closeNav,
    toggleSearch,
    searchHandler,
    Search,
    toggleNotifier,
    HayStack,
    sentRequests
} from './STORE/Actions'
import Notify from "./Notify";
import {fetchUser} from "../USER PROFILE/STORE/Actions";
import {Button, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {cancelRequest} from "../VISITING PROFILE/STORE/Actions";
class NavComponent extends Component {

    componentWillUnmount() {
        if (this.props.open_requests){
            this.props.sentRequests(this.props.open_requests)
        }if (this.props.openNotify){
            this.props.toggleNotify()
        }
    }

    render() {
        const style = {
            width : this.props.sidenavWidth
        };
        let userImage = this.props.user_info.photo_path === null ? <img src={`/img/avatar.png`} className="rounded-circle" alt="user" width="100" height="100"/>
         : <img src={`/storage/users/${this.props.user_info.id}/${this.props.user_info.photo_path}`} className="rounded-circle" alt="user" width="100" height="100"/>;

        // let searchBtn =;
        let searchBar = this.props.toggled ? <li className="nav-item centered-item" >
        <form onSubmit={(e)=>this.props.search(e,this.props.needle)}>
            <div className="input-group mb-3">
                <input type="text" className="form-control" value={this.props.needle} autoFocus={true} onChange={this.props.searchHandler} placeholder="Search" />
                    <div className="input-group-append">
                        <button className="btn btn-info" type="submit" onBlur={this.props.toggleSearch}><i className="fas fa-search"> </i></button>
                    </div>
            </div>
        </form>
        </li> : '';

        let notifier = this.props.openNotify ?<div className="notifier"> <Notify notify={this.props.toggleNotify} unread={this.props.user_info.notify}/></div> : '';

        let results = this.props.queryResults.length > 0 ?
            this.props.queryResults.map((item,index)=>{
                let link;
                if (!item.firstname){
                    link = this.props.user_info.id === item.owner ?`/pet/${item.id}` :`/pet-view/${item.id}`
                }else link = `/pet-owner/${item.id}`;
                return <li key={index} className="search-item">
                    <NavLink to={link} style={{textDecoration:"none"}}>
                        {!item.firstname?item.name:item.firstname + " " + item.lastname}
                        <small className="ml-2">({item.username})</small>
                    </NavLink>
                </li>
            }): <div className="text-center no-search-result">search by username</div>;

        let searchDisplay = this.props.showSearchLog ? (
            <div className="search-res">
                <div className="search-wrapper">
                    <div className="close-search-mobile text-right">
                        <b style={{fontSize:"25px",color:"darkslategrey",cursor:"pointer"}} onClick={this.props.searchLog}>&times;</b>
                    </div>
                   <ul>{results}</ul>
                </div>
            </div>
        ) : null;

       let badge = null;
       if (!this.props.notify_collapse && $(window).width() < 990){
           let messages = parseInt(this.props.user_info.messages);
           let other = parseInt(this.props.user_info.notify);
           if(messages> 0 || other > 0)
           {
               badge= messages+other;
           }
       }

       let req = "loading";
       let sentReq;
        if (Object.keys(this.props.myRequests).length){
            req = this.props.myRequests.map((item,index)=>{
                let photo = item.photo_path === null ? <img src={'/img/avatar.png'} alt="user" width={25} height={25} className="mr-2 rounded-circle"/> :
                    <img src={`/storage/users/${item.user_id}/${item.photo_path}`} alt="user" width={25} height={25} className="mr-2 rounded-circle"/>;
                return <li key={index} className="mb-3 d-flex">
                        <NavLink to ={`/pet-owner/${item.user_id}`}>
                            {photo} <span>{item.username}</span>
                        </NavLink>
                        <span onClick={()=>this.props.cancelRequest(item.id)} className="ml-5"
                              style={{backgroundColor:"darkslategrey",color:"white",fontFamily:"Cambria",padding:"3px",width:"50px",cursor:"pointer"}}>
                            cancel
                        </span>
                </li>
            });
        } else req = <span style={{fontFamily:"Cambria",textAlign:"center", width:"100%"}}>no sent friends request</span>;
       sentReq = this.props.open_requests?(
           <div className="search-res">
               <div className="search-wrapper">
                   <div className="close-search-mobile text-right">
                       <b style={{fontSize:"25px",color:"darkslategrey",cursor:"pointer"}} onClick={()=>this.props.sentRequests(this.props.open_requests)}>&times;</b>
                   </div>
                   <ul>
                       {req}
                   </ul>
               </div>
           </div>
       ):null;


        return (
            <div>
                <Navbar className="fixed-top my-navbar" expand="lg" onDoubleClick={$(window).width() < 987 ? this.props.toggleControl : null}>
                    <Navbar.Brand ><NavLink to={"/"} style={{textDecoration:"none",color:"white"}}>Pawrry</NavLink></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={()=>{this.props.NotifyToggle()}} className="navbar-toggle"/>
                    <div style={{position:"absolute",zIndex:"1",right:"0",marginRight:"13px"}}>
                        <span className="badge badge-danger">{badge}</span>
                    </div>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <ul className="navbar-nav ml-auto ml-5 mr-auto">

                            <Form inline onSubmit={(e)=>this.props.search(e,this.props.needle)}>
                                <FormControl type="text" list="needle" placeholder="Search" className="mr-sm-2 search-input"
                                             value={this.props.needle.toLowerCase()} onChange={(e)=>this.props.searchHandler(e,this.props.stack)}
                                onFocus={()=>{
                                    this.props.query();
                                    this.props.searchLog()
                                }}/>
                            </Form>
                            {searchDisplay}
                            <Nav className="ml-lg-4 mr-4 nav-bar-item">
                                <Nav.Link className="nav-link" onClick={()=>this.props.toggleNotify(this.props.openNotify)}>
                                    <img src={'/img/bell.svg'} alt={'alert'} height={'34'} width={'34'}/>
                                    {this.props.user_info.notify > 0 ?<span className="badge badge-danger">{this.props.user_info.notify}</span> : ''}
                                </Nav.Link>
                            </Nav>
                            {notifier}
                            <Nav className="mr-4 nav-bar-item link-display-sm">
                                <NavLink to="/archive" className="nav-link text-sm-light" style={{color:"white"}}>Archive</NavLink>
                            </Nav>

                            <Nav className="mr-4 nav-bar-item">
                                <NavLink to="/chats" className="nav-link">
                                    <img src={'/img/pet-chat.svg'} alt={'chats'} width={'41'} height={'41'}/>
                                    {this.props.user_info.messages > 0 ?<span className="badge badge-danger ml-1">{this.props.user_info.messages}</span> : ''}
                                </NavLink>
                            </Nav>

                            <Nav className="mr-4 nav-bar-item">
                                <span onClick={()=>this.props.sentRequests(this.props.open_requests)} className="nav-link">
                                    <img src={'/img/requests.svg'} alt={'chats'} width={'28'} height={'28'}/>
                                </span>
                            </Nav>
                            {sentReq}

                            <Nav className="mr-4 nav-bar-item link-display-sm">
                                <NavLink to="/pet-listing" className="nav-link" style={{color:"white"}}>Listings</NavLink>
                            </Nav>
                            {
                                $(window).width() > 990 ?
                                    <Nav className="mr-4 nav-bar-item">

                                    </Nav>
                                    : null
                            }
                            <Nav className="mr-4 nav-bar-item">
                                <NavLink to="/profile" className="nav-link">
                                    <img src={'/img/user.svg'} alt={'user'} width="30" height="30" /> <b style={{color:"white",position:'relative',top:'6px'}}> {this.props.user_info.username}</b>
                                </NavLink>
                            </Nav>

                            {$(window).width() < 990 ?
                                <Nav className="mr-4 nav-bar-item">
                                    <Nav.Link className="nav-link" style={{color:"white", fontSize:"17px", fontFamily:"Cambria", cursor:"pointer"}}>
                                        <img src={'/img/coin.svg'} alt="coins" width="28" height="28" className="mr-2"/>
                                        {this.props.user_info.coins}
                                    </Nav.Link>
                                </Nav> : null
                            }

                            <Nav className="mr-4 mt-2 nav-bar-item link-display-sm">
                                <NavLink to="#" className="btn btn-danger log-out" onClick={this.props.logout}>Log Out</NavLink>
                            </Nav>

                        </ul>

                        <ul className="navbar-nav hide-sidenav-small">
                            <li className="nav-item">
                                <span className="nav-link" onClick={this.props.open}> <img src={"/img/process.svg"} alt="open-nav" width="25" height="25" className="nav-slider"/> </span>
                            </li>

                            <div id="mySidenav" className="sidenav" style={style}>
                                <span className="closebtn text-light" onClick={this.props.close}>&times;</span>
                                <div className="user text-light d-flex justify-content-center">
                                    {userImage}
                                </div>
                                {/*<NavLink to="/activities" className="link" >Activities</NavLink>*/}
                                <NavLink to="/archive" className="link" >Archive</NavLink>
                                {/*<NavLink to="" className="link" >Games</NavLink>*/}
                                <NavLink to="/pet-listing" className="link" >Listings</NavLink>
                                <div id="logout">
                                    <NavLink to="#" className="btn btn-danger log-out" onClick={this.props.logout}>Log Out</NavLink>
                                </div>
                            </div>

                        </ul>
                    </Navbar.Collapse>
                </Navbar>

            </div>

        )
    }
}
const MapState = (state) =>{
    return{
        sidenavWidth : state.Layout.sidenavWidth,
        toggled : state.Layout.toggleSearch,
        needle : state.Layout.needle,
        openNotify : state.Layout.toggleNotify,
        stack : state.Layout.stacks ,
        showSearchLog : state.Layout.showSearchLog ,
        user_info : state.User.user_info,
        queryResults : state.Layout.queryResults,
        notify_collapse : state.Notifications.notify_collapse,
        myRequests : state.Notifications.sentRequests,
        open_requests : state.Notifications.open_requests,
    }
};
const MapDispatch = (dispatch) =>{
    return{
        user : () =>{dispatch(fetchUser())},
        open : () => {dispatch(openNav())},
        close : () => {dispatch(closeNav())},
        toggleSearch : () => {dispatch(toggleSearch())},
        toggleNotify : (toggle) => {dispatch(toggleNotifier(toggle))},
        sentRequests : (notifier) => {dispatch(sentRequests(notifier))},
        NotifyToggle : () => {dispatch({type:"COLLAPSED_ALERT"})},
        searchHandler : (e,haystack)=>{dispatch(searchHandler(e.target.value,haystack))},
        search : (e,data)=>{e.preventDefault();dispatch(Search(data))},
        cancelRequest : (id) => {dispatch(cancelRequest(id,null,"NOTIFICATIONS"))},
        query : ()=>{dispatch(HayStack())},
        toggleControl : ()=>{dispatch({type:"TOGGLE_CONTROL"})},
        searchLog : () =>{dispatch({type:"SEARCH_LOG"})},
        logout : (e) => {
            e.preventDefault();
            dispatch(signOut())
        }
    }
};
export default connect(MapState,MapDispatch)(NavComponent);
