import React, {Component} from 'react';
import {connect} from "react-redux";
import './CSS/Notify.css'
import {
    acceptRequest as requestResponse,
    markAllAsRead,
    NotificationChecked,
    Notifications,
    receivedRequest
} from "./STORE/Actions";
import {NavLink} from "react-router-dom";
import ReactEmoji from "react-emoji";
class Notify extends Component {

    componentDidMount() {
       // this.props.receivedRequest();
        this.props.notifications();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log()
    }

    componentWillUnmount() {
        //
    }

    render() {

        let friendsRequest;
        let requests ;
        let senderPhoto;
        let alerts;
       /* if (this.props.friendRequests.length > 0){
                requests = this.props.friendRequests.map((item,index)=>{
                    senderPhoto = item.sender_user.photo_path === null ?
                        <img src={"/img/avatar.png"} alt="user" width={"20"} className="rounded-circle"/>
                    : <img src={`/storage/${item.sender_user.id}/${item.sender_user.photo_path}`} alt="user" height={"20"} width={"20"} className="rounded-circle mr-2"/>;
                return <li key={index} className="request row">
                        <div className="col-sm-5 row mr-5">
                            <span className="senderPhoto col-sm-3">{senderPhoto}</span>
                            <span className="requestSender col-sm-3">{item.sender_user.username}</span>
                        </div>
                        <div className="col-sm-4 row">
                            <span className="far fa-thumbs-up text-success col-sm-3 mr-3" onClick={()=>this.props.acceptRequest(item.id)}/>
                            <span className="far fa-thumbs-down text-danger col-sm-3"/>
                        </div>
                </li>
            });
            friendsRequest = <ul>
                <h6>Friend Requests</h6>
                {requests}
            </ul>
        }else{friendsRequest = <h6>No friend requests</h6>} */
        if (this.props.alerts.length > 0){
            alerts = this.props.alerts.map((item,index)=>{
                let bgColor = item.read_at === null ? "#ccd5de" : '';

                if(item.data.type === "Post Like"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.userId}`}>
                                        {item.photo === null ? <img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.userId}/${item.photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.username}
                                    </NavLink> liked <b>{item.petName}'s</b> post
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                } else if (item.data.type === "Friendship"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> sent you a <b>friends request</b> <br/>
                                    {item.status === 0
                                        ?
                                        <div>
                                            <button onClick={()=>{this.props.requestResponse(item.requestId,1)}}>accept</button>
                                            <button onClick={()=>{this.props.requestResponse(item.requestId,2)}}>reject</button>
                                        </div> : item.status === 1
                                        ?
                                        <small className="text-success"> <b>Accepted</b> {item.resTime !== null ? item.resTime : ''}</small>
                                        :
                                        <small className="text-success"> <b>Rejected</b> {item.resTime !== null ? item.resTime : ''}</small>
                                    }

                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "Admirer"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> <b>admires</b> <NavLink to={`/pet/${item.petId}`}>{item.petName}</NavLink> <br/>

                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "Comment Like"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> <b>Likes</b> your comment on <NavLink to={`/pet/${item.petId}`}>{item.petName}</NavLink>'s post <br/>
                                    <span style={{fontFamily:"Cambria", fontSize:"15px"}} className="text-success">
                                       <small ><b>comment: </b></small> {item.comment}
                                    </span>
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "New Pet"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> added <b>New Pet : </b> <NavLink to={`/pet-view/${item.petId}`}>{item.petName} </NavLink>
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "Comment"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> wrote a <b>Comment</b> on <NavLink to={`/pet/${item.petId}`}> {item.petName}</NavLink>'s post.
                                    <span style={{fontFamily:"Cambria", fontSize:"15px"}} className="text-success">
                                       {item.comment}
                                    </span>
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "Comment Reply"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> replied your comment:
                                     <span style={{fontFamily:"Cambria", fontSize:"15px"}} className="text-success mr-2">
                                       {item.comment}
                                    </span>
                                    <div>
                                        <small onClick={()=>this.props.viewReply(index)} style={{color:"darkcyan",cursor:"pointer"}}>view reply</small>
                                        {this.props.replyViewNotify && this.props.notifComment === index?
                                            <div style={{fontFamily:"Cambria"}}>
                                               <small className="text-success">{ReactEmoji.emojify(item.reply)}</small>
                                            </div>:null
                                        }
                                    </div>
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }else if (item.data.type === "Friendship Accepted"){
                    return (
                        <li key={index} className="p-2 mt-1" onClick={()=>{this.props.checked(item.id)}}>
                            <div className="text-center" style={{width:"100%",display:"flex", flexDirection:"column",backgroundColor:bgColor}}>
                                <span style={{fontFamily:"Cambria", fontSize:"17px"}}>
                                    <NavLink to={`/pet-owner/${item.sender[0].id}`}>
                                        {item.sender[0].photo === null ?<img src={`/img/avatar.png`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${item.sender[0].id}/${item.sender[0].photo}`} alt="user" width="25" height="25" className="mr-2 rounded-circle"/>
                                        }
                                        {item.sender[0].username}
                                    </NavLink> accepted your friends request.
                                </span>
                                <span style={{fontFamily:"Cambria", fontSize:"13px", fontColor:"grey"}}>
                                    {item.time}
                                </span>
                            </div>
                        </li>
                    )
                }
            })
        } else alerts = <div className="text-center p-3">
                <b>You have no notifications</b>
        </div>;

        return (
            <div className="notification-wrapper text-center">{/*onMouseLeave={this.props.notify}*/}
                <div className="close-alert-mobile text-right">
                    <b style={{fontSize:"25px",color:"darkslategrey",cursor:"pointer"}} className="mr-3" onClick={this.props.notify}>&times;</b>
                </div>
                {this.props.unread>0?
                    <div className="p-2 mt-2">
                        <button onClick={this.props.allRead}>mark all as read</button>
                    </div>:null
                }
                {alerts}
            </div>
        );
    }
}

const mapState = (state) =>{
    return{
        friendRequests : state.Notifications.friendRequests,
        alerts : state.Notifications.alerts,
        replyViewNotify : state.Notifications.replyView,
        notifComment : state.Notifications.notifComment,
    }
};

const mapDispatch = (dispatch) =>{
    return {
        receivedRequest : () =>{dispatch(receivedRequest())},
        requestResponse : (id,res) => {dispatch(requestResponse(id,false,res))},
        notifications : () => {dispatch(Notifications())},
        checked : (id) => {dispatch(NotificationChecked(id))},
        allRead : () =>{dispatch(markAllAsRead())},
        viewReply : (id) =>{dispatch({type:"VIEW_NOTIF_REPLY",id})},
    }
};

export default connect(mapState,mapDispatch)(Notify);
