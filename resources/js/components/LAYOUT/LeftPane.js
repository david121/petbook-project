import React, {Component} from 'react';

class LeftPane extends Component {
    render() {
        return (
            <div>
                {/*Left Pane For  Trends and Profile Suggestions*/}
                <div id="trends" className="text-center mb-3 position-fixed">
                    <small className="text-center">Trends & Suggestions</small>
                    <div className="add-post mt-3">
                        <button className="btn" onClick={this.props.showPostPane}> <i className="far fa-plus-square mr-3"> </i> Add Post</button>
                    </div>
                </div>

                {/* Add Post Modal*/}
                <div className='text-center' id="add-post-wrapper">

                    {this.props.addPost}

                </div>

            </div>
        );
    }
}

export default LeftPane;