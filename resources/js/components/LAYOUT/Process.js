
import React from 'react';
import Loader from "react-loader-spinner";

const Process = () =>{
        return (
            <div className="api-process">
                <div style={{width:'100%',height:'100vh'}} className="d-flex justify-content-center align-content-center align-items-center">
                    <Loader
                        type="ThreeDots"
                        color="darkslategrey"
                        height={100}
                        width={100}
                    />
                </div>;
                {/*<span className="process-message"> {props.message} </span>*/}
            </div>
        )
};

export default  Process