
const initialState = {
    activePets : [],
    likedPets : [],
    visitProfilePets: [],
    fetching : false,
    showFriends : false,
    cancelBtn : false,
    unfriendBtn : false,
    friendToToggle : '',
    addFriendBtn: false,
    listId : '',
    requestBtn : false,
    sentReqFriendId: '',
    //admiring suggested Pets
    admireLoader : false,
    admireClick : false,
    admiredItem : '',
    //adding suggested user
    addUserClick : false,
    sendingReq : false,
    addedItem : '',
};

const FriendsReducer = (state = initialState, actions)=>{
    switch (actions.type) {
        case "TOGGLE_VISITOR_FRIENDS" :
            return {
                ...state,
                showFriends: actions.closeModal ? false : !state.showFriends
            };

        case "TOGGLE_FRIENDSHIP" :
            return {
                ...state,
                cancelBtn: !state.cancelBtn
            };

        case "UNFRIEND_BUTTON" :
            return {
                ...state,
                unfriendBtn: !state.unfriendBtn,
                friendToToggle : actions.value
            };
        case "FRIENDSHIP_CANCELED" :
            return {
                ...state,
                addFriendBtn : true,
                listId : actions.value
            };
        case "FRIENDS_REQUEST_SENT" :
            return {
                ...state,
                requestBtn : true,
                sentReqFriendId : actions.value
            };
        case "ACTIVE_USER_PETS" :
            return {
                ...state,
                activePets : actions.pets,
                likedPets : actions.likedPets
            };
        case "ADMIRED_PETS" :
            return {
                ...state,
                visitProfilePets : actions.pets
            };
        case "FETCHING_ADMIRED_PETS" : //Pets admired by active user
            return {
                ...state,
                fetching : !state.fetching
            };
        case "SUGGESTION_ADMIRED" :
            return {
                ...state,
                admireLoader : !state.admireClick,
                admireClick: !state.admireClick,
                admiredItem : actions.value
            };
        case "SUGGESTED_USER_CLICKED" :
            return {
                ...state,
                 admireLoader : !state.admireClick,
                 addUserClick: !state.addUserClick,
                 addedItem : actions.value
            };
        case "ADD_SUGGEST_ERROR" :
            return {
                ...state,
                 admireLoader : false,
                 addUserClick: false,
                 addedItem : ''
            };
        case "SENDING_REQUEST" :
            return {
                ...state,
                sendingReq:!state.sendingReq
            };

    }
    return state;
};

export default FriendsReducer;
