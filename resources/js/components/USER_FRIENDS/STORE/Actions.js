import axios from "axios";
import {fetchUser, fetchVisitingProfile, fetchVisitPet} from "../../USER PROFILE/STORE/Actions";
import {getClient, nearestUsers, suggestPet} from "../../SEARCH/Actions";
import {fetchAllPosts} from "../../HOME/STORE/Actions";
import {Api_Process} from "../../LAYOUT/STORE/Actions";

export const showFriends = (closeAction = false, closeModal = false) =>{
    return dispatch=>{
       dispatch({
            type : "TOGGLE_VISITOR_FRIENDS",
           closeAction,
           closeModal
       })
    }
};

export const showUserFriends = (closeAction = false) =>{
    return dispatch=>{
        dispatch({
            type : "TOGGLE_USER_FRIENDS",
            closeAction
        })
    }
};

export const showAdmiredPets = (closeAction = false, visitorProfile) =>{
    return dispatch=>{
        if (visitorProfile){
            dispatch({
                type : "TOGGLE_ADMIRED_PETS_VISITOR",
                closeAction
            })
        }else
        dispatch({
            type : "TOGGLE_ADMIRED_PETS",
            closeAction
        })
    }
};


export const toggleFriendship=()=>{
    return dispatch =>{
        dispatch({
            type : "TOGGLE_FRIENDSHIP"
        })
    }
};


export const unfriendBtn = (id) =>{
    // console.log(id)
    return dispatch =>{
        dispatch({type: "UNFRIEND_BUTTON",value:id})
    }
};

export const admireProcess = (petId) =>{
    return dispatch =>{
        dispatch({
            type : "SUGGESTION_ADMIRED",
            value : petId
        })
    }
};

export const addSuggestedUserProcess = (userId) =>{
    return dispatch => {
            dispatch({
                type : "SUGGESTED_USER_CLICKED",
                value : userId
            })
    }
};

export const addSuggestedUser = (userId) =>{

    return dispatch => {
        dispatch(addSuggestedUserProcess(userId));
        const data = {
            user_id : userId,
        };
        axios.post('/api/friends/request',data,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch(getClient());
                    dispatch(addSuggestedUserProcess(userId))
                }else{
                    dispatch({type:"ADD_SUGGEST_ERROR"})
                }
            })
            .catch(error=>{
                console.log(error);
                dispatch({type:"ADD_SUGGEST_ERROR"})
            })
    }
};

export const admSuggestedPet = (petId) =>{
    return dispatch =>{
        dispatch(admireProcess(petId));
        axios.post('/api/admire',{pet_id:petId},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success) {
                    dispatch(admireProcess(''));
                    dispatch(suggestPet());
                    dispatch(fetchAllPosts())
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};



export const admire=(petId,page)=>{
    return dispatch =>{
        //dispatch(admireProcess());
        dispatch({type:"ADMIRATION_PROCESS"});
        axios.post('/api/admire',{pet_id:petId},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({type:"ADMIRATION_PROCESS"});
                    dispatch(fetchVisitPet(petId));
                    if (page === 'user'){
                        dispatch({
                            type:"PET_ADMIRED",
                        })}
                    else if (page === 'Visitor'){
                        dispatch({
                            type : "PET_ADMIRED_FROM_VISITOR",
                            pet : petId
                        })
                    }
                }
            })
            .catch(()=>{
                dispatch({type:"ADMIRATION_PROCESS"});
                // console.log(error)
            })
    }
};

export const cancelAdmiration = (admireId,petId,page) =>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.post('/api/admire/cancel',{admire_id:admireId,pet_id : petId},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    switch (page) {
                        case "pet":dispatch(fetchVisitPet(petId));break;
                        case "User":dispatch(fetchUser());break;
                    }
                    dispatch({
                        type : "ADMIRATION_CANCELED",
                        pet : petId
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};

export const unadmire = (petId,profileId) =>{
    return dispatch =>{
        axios.post('/api/admire/unadmire',{pet_id : petId},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch(fetchAdmiredPets(profileId));
                    dispatch({
                        type : "ADMIRATION_CANCELED",
                        pet : petId
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const fetchActiveUserPets = () =>{
    return dispatch =>{
        dispatch({type:"FETCHING_ADMIRED_PETS"});
        axios.get('/api/activePets',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({type:"FETCHING_ADMIRED_PETS"});
                    dispatch({
                        type : "ACTIVE_USER_PETS",
                        pets : response.data.pets,
                        likedPets : response.data.likedPets
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};
export const fetchAdmiredPets = (profileId) =>{
    return dispatch =>{
        axios.get('/api/admiredPets',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            },params:{profileId : profileId}
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : "ADMIRED_PETS",
                        pets : response.data.pets,
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};
