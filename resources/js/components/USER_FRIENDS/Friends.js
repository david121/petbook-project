import React, {Component} from 'react';
import {connect} from "react-redux";
import "./CSS/Friends.css"
import {NavLink} from "react-router-dom";
import {cancelRequest, friendsRequest, toggleRequest} from "../VISITING PROFILE/STORE/Actions";
import {unfriendBtn} from "./STORE/Actions";
import Loader from "react-loader-spinner";

class Friends extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.toggleRequestBtn)
    }

    render() {
        let friends = <div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="Hearts"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        let friendPic ;
        let friendStatusBtn;
        let navLink;
        if (this.props.type === "VisitorPage"){

                friends= this.props.friends.length > 0 ? this.props.friends.map((friend,index)=>{
                    if (friend.friend_status !== null){
                        if (friend.friend_status.status === 1){
                            friendStatusBtn = <button className="friend" onClick={()=>this.props.unfriend(friend.friend_status.id, this.props.profile, "VISITOR") }>unfriend</button>;
                        }else
                            if (friend.friend_status.status === 0){
                                if (this.props.toggleRequestBtn && this.props.toggledFriendship === friend.id){
                                    friendStatusBtn=<button className="friend" onMouseLeave={()=>this.props.toggleRequest('')} onClick={()=>this.props.unfriend(friend.friend_status.id, this.props.profile,"VISITOR")}>cancel</button>;
                                }else friendStatusBtn=<button className="friend" onMouseEnter={()=>this.props.toggleRequest(friend.id)}>request sent</button>;
                        }else friendStatusBtn = <button className="friend" onClick={()=>this.props.friendsRequest(friend.id,this.props.profile,"VISITOR")}>add friend</button>;
                    }else friendStatusBtn = <button className="friend" onClick={()=>this.props.friendsRequest(friend.id,this.props.profile,"VISITOR")}>add friend</button>;

                    navLink = friend.id === this.props.activeUser.id? `/profile` : `/pet-owner/${friend.id}`;

                    friendPic = friend.photo_path === null ? <img src={"/img/avatar.png"} alt="user" className={"rounded-circle mr-2"} width="20" height="20"/>
                        : <img src={`/storage/users/${friend.id}/${friend.photo_path}`} width="20" height="20" alt="user" className="rounded-circle mr-2"/>;

                    return <li key={index} className="breedInfo">
                           <div className="row">
                               <div className="col-sm-7">
                                   <NavLink to={navLink} className="friends">
                                       {friendPic}
                                       {friend.username}
                                   </NavLink>
                               </div>
                               <div className="col-sm-4 text-right">{friend.id === this.props.activeUser.id ? '' : friendStatusBtn}</div>
                           </div>
                      </li>
                    // }
                }): <b className="p-4 m-2">No friendship</b>;
            // }
        }else{
            friends = this.props.user_friends !== null ?this.props.user_friends.map((friend,index)=>{
                friendPic = friend.photo_path === null ? <img src={"/img/avatar.png"} alt="user" className="rounded-circle mr-3" width="20" height="20"/>
                    : <img src={`/storage/users/${friend.id}/${friend.photo_path}`} width="20" height="20" alt="user" className="rounded-circle mr-3"/>;

                if (friend.id !== this.props.profile){
                    return <li key={index} className="friend-item">
                            <div className="row">
                                <div className="col-sm-7">
                                    <NavLink to={`/pet-owner/${friend.id}`} className="friends">
                                        {friendPic}
                                        {friend.username}
                                    </NavLink>
                                </div>
                                <div className="col-sm-4 text-right">
                                    {
                                        this.props.requestBtn && this.props.sentReqFriendId === friend.id? <button className="unfriend">request sent</button> :
                                        this.props.addFriendBtn && this.props.listId === friend.friendship? <button className="unfriend" onClick={()=>this.props.friendsRequest(friend.id)}>Add Friend</button> :
                                        this.props.toggleFriendBtn && this.props.toggleId === friend.id?<button className="unfriend" onMouseLeave={()=>this.props.unfriendButton(friend.id)} onClick={()=>this.props.unfriend(friend.friendship)}>Unfriend</button>:
                                        <button className="unfriend" onMouseEnter={()=>this.props.unfriendButton(friend.id)}>Friend</button>
                                    }
                                </div>
                            </div>
                    </li>
                }
            }) : <b className="p-4 m-2">You have no friends</b>;
        }
        return (
                <div className="friends-wrapper">
                    <ul>{friends}</ul>
                </div>
        );
    }
}

const mapState = (state) =>{
    return {
        activeUser : state.User.user_info,
        // friends : state.User.profileFriends,
        toggleFriendBtn : state.VisitorFriends.unfriendBtn,
        toggleId : state.VisitorFriends.friendToToggle,
        addFriendBtn : state.VisitorFriends.addFriendBtn,
        listId : state.VisitorFriends.listId,
        requestBtn : state.VisitorFriends.requestBtn,
        sentReqFriendId : state.VisitorFriends.sentReqFriendId,
        toggleRequestBtn : state.Visitor.toggleRequest,
        toggleRequestId : state.VisitorFriends.friendId,
        toggledFriendship : state.Visitor.toggledItem
    }
};

const mapDispatch = (dispatch) =>{
    return {
        unfriend : (friendshipId,userId,page) => {dispatch(cancelRequest(friendshipId,userId,page))},
        unfriendButton : (id) => {dispatch(unfriendBtn(id))},
        friendsRequest : (id,profileId,page) => {dispatch(friendsRequest(id,profileId,page))},
        toggleRequest : (id) =>{dispatch(toggleRequest(id))}
    }
};

export default   connect(mapState,mapDispatch)(Friends);
