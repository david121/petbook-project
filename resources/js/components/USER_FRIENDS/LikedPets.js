import React, {Component} from 'react';
// import Tab from "./Tab";
import {connect} from "react-redux";
import "../AUTHENTICATION/REGISTER PET/CSS/Pet.css"
import {NavLink} from "react-router-dom";
import {admire, cancelAdmiration, fetchActiveUserPets, fetchAdmiredPets, unadmire} from "./STORE/Actions";

class LikedPets extends Component {

    componentDidMount() {
        // console.log(this.props.activeUserPets)
        this.props.fetchActiveUserPets();
        this.props.fetchAdmiredPets(this.props.profileId)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.activeUserPets)
    }

    render() {
        let petList = "Loading";
        let petPic ;

        if (this.props.profile === "User"){
                petList = this.props.pets!== null? this.props.pets.map((pet,index)=>{
                    petPic = pet.profile_photo === null ? <img src={"/img/rodent.svg"} alt="pet" className={"rounded-circle"} width="20" height="20"/>
                        : <img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} width="20" height="20" alt="user" className="rounded-circle mr-2"/>;
                        return <li key={index} className="breedInfo">
                                <div className="row">
                                    <div className="col-sm-7">
                                        <NavLink to={`/pet-view/${pet.id}`} className="friends">
                                            {petPic}
                                            {pet.username}
                                        </NavLink>
                                    </div>
                                    <div className="col-sm-4 text-right">
                                        {this.props.admireBtn && this.props.unadmiredPet === pet.id?
                                            <img src={'/img/paw.svg'} className="likeIcon" alt="like" width="25" height="25" onClick={()=>this.props.admire(pet.id,'user')}/>
                                            :
                                            <img src={'/img/paws.svg'} className="likeIcon" alt="like" width="25" height="25" onClick={()=>this.props.unadmire(pet.admire_id,pet.id,this.props.page)}/>
                                            }
                                    </div>
                                </div>
                        </li>
                }) :<b className="p-4 m-2">No admired pets</b>;
        }else{
            // petList = this.props.fetching ? "Loading" :
                if (Object.keys(this.props.visitProfilePets)){

                petList = this.props.visitProfilePets.length > 0 ?this.props.visitProfilePets.map((pet,index)=>{

                    petPic = pet.profile_photo === null ? <img src={"/img/rodent.svg"} alt="user" className="rounded-circle mr-3" width="20" height="20"/>
                        : <img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} width="20" height="20" alt="user" className="rounded-circle mr-3"/>;

                    let url = this.props.activeUserPets.includes(pet.id)?`/pet/${pet.id}` : `/pet-view/${pet.id}`;

                    let admBtn;
                    if(this.props.activeUserPets.includes(pet.id)){
                        admBtn = '';
                    }else{
                        if ( this.props.activeUserAdmPets.includes(pet.id)) {
                            if (this.props.admireBtn && this.props.unadmiredPet === pet.id) {
                                admBtn =  <img src={'/img/paw.svg'} className="likeIcon" alt="like" width="25" height="25" onClick={()=>this.props.admire(pet.id,'Visitor')}/>
                            } else admBtn = <img src={'/img/paws.svg'} className="friend likeIcon" alt="like" width="25" height="25" onClick={() => this.props.cancelAdm(pet.id, this.props.profileId)}/>
                        }else
                            if (this.props.admireBtn && this.props.unadmiredPet === pet.id) {
                                admBtn = <img src={'/img/paws.svg'} className="friend likeIcon" alt="like" width="25" height="25" onClick={() => this.props.cancelAdm(pet.id, this.props.profileId)}/>
                            }else
                            admBtn = <img src={'/img/paw.svg'} className="likeIcon" alt="like" width="25" height="25" onClick={()=>this.props.admire(pet.id,'Visitor')}/>
                    }

                return <li key={index} className="breedInfo">
                        <div className="row">
                            <div className="col-sm-7">
                                <NavLink to={url} className="friends">{petPic}{pet.username}</NavLink>
                            </div>
                            <div className="col-sm-4 text-right">
                                {admBtn}
                            </div>
                        </div>
                    </li>
            }) : <b className="p-4 m-2">No admired pets</b>;
        }else petList = "loading"
        }
        return (
                <div className="">
                    <ul style={{listStyle:'none'}}>{petList}</ul>
                </div>
        );
    }
}

const mapState = (state) =>{
    return {
        visitProfilePets : state.VisitorFriends.visitProfilePets,
        activeUser : state.User.user_info,
        activeUserPets : state.VisitorFriends.activePets,
        activeUserAdmPets : state.VisitorFriends.likedPets,
        admireBtn : state.User.admire,
        unadmiredPet : state.User.unadmiredPet,
        fetching : state.VisitorFriends.fetching
    }
};

const mapDispatch = (dispatch) =>{
    return {
        unadmire : (id,petId,page) =>{ dispatch(cancelAdmiration(id,petId,page))},
        cancelAdm : (petId,profileId)=>{dispatch(unadmire(petId,profileId))},
        admire : (petId,page) =>{dispatch(admire(petId,page))},
        fetchActiveUserPets : () => {dispatch(fetchActiveUserPets())},
        fetchAdmiredPets : (id) => {dispatch(fetchAdmiredPets(id))},
    }
};

export default connect(mapState,mapDispatch)(LikedPets);
