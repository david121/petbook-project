import React, {Component} from 'react';
import {connect} from "react-redux";

class UserActivity extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h6>Activity : {this.props.activity}</h6>
                </div>
                <div className="card-body">
                    <span> </span>
                </div>
            </div>
        );
    }
}

export default connect(null,null)(UserActivity);
