
const initialState = {
    activities : []
};

const ActivityReducer = (state = initialState, action) =>{
    switch (action.type) {
        case 'ACTIVITIES':
            return{
                activities: action.value
            };
        case 'POST_TO':
            return{
                ...state,
                postTo : action.value,
                next : true
            };
        default : return state
    }
};

export default ActivityReducer;
