import axios from 'axios'
export const fetchActivities = () =>{
  return dispatch=>{
      axios.get('/api/activities',{
          headers:{
              "Content-Type" : "application/json",
              "Accept" : "application/json",
              "Authorization" : "Bearer " + localStorage.getItem("token")
          }
      })
          .then(response=>{
            if (response.data.success){
                dispatch({
                    type : "ACTIVITIES",
                    value : response.data.activities
                })
            }
      }).catch(error=>{
          console.log(error)
      })

  }
};
