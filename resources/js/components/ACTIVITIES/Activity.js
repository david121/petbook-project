import React, {Component} from 'react';
import Nav from "../LAYOUT/Nav";
import {connect} from "react-redux";
import {fetchActivities} from "./Store/Actions";
import UserActivity from "./UserActivity";

class Activity extends Component {

componentDidMount() {
    this.props.activities();
}

componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(this.props.activityList)
}
    render() {
        const activity = this.props.activityList.map((activity,index)=>{
                 return <UserActivity
                        key = {index}
                        activity={activity.activity}
                        />
        });
        return (
            <div>
                <Nav/>
                <div className="container mt-5">
                    {activity}
                </div>
            </div>
        );
    }
}

const mapState = (state) =>{
  return{
      activityList : state.Activities.activities
  }
};

const mapDispatch = (dispatch)=>{
    return{
        activities : ()=>{dispatch(fetchActivities())}
    }
};

export default connect(mapState, mapDispatch)(Activity);
