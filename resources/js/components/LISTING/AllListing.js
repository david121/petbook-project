import React, {Component} from 'react';
import NavComp from "../LAYOUT/Nav";
import ListPet from "./ListPetModal";
import {connect} from "react-redux";
import {filter_change,filter as filterSearch, listingModal, navigateListing} from "./STORE/Actions";
import Listing from "./Listing";
import MyListing from "./MyListing";
import './CSS/Listing.css'
import {Nav} from "react-bootstrap";
import Process from "../LAYOUT/Process";
import Scams from "./Scams";
class AllListing extends Component {

    componentDidMount() {
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Listing";
    }

    componentWillUnmount(){
        this.props.scamModal ? this.props.scams() : null
    }

    render() {

        let listingClass = ["listing-links"]; let myListingClass = ["listing-links tab-text"];
        if (this.props.component==="LISTINGS"){
            listingClass.push("listings-active tab-text")
        }else
        if(this.props.component==="MY_LISTINGS"){
            myListingClass.push("my-listings-active")
        }



        /*const filterData = new FormData();
        filterData.append('city',this.state.city,);
        filterData.append('country',this.state.country,);
        filterData.append('Dog',this.state.Dog,);
        filterData.append('Cat',this.state.city,);
        filterData.append('Adoption',this.state.Adoption,);
        filterData.append('Mating',this.state.Mating,);
        filterData.append('Sale',this.state.Sale,);*/

        return (
            <div>
                <NavComp/>
                <div className="container below-nav">
                    <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                        <Nav.Item>
                            <Nav.Link eventKey="timeline" onClick={()=>this.props.navigate("LISTINGS")} className={listingClass.join(' ')} >Listings</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="link-1" onClick={()=>this.props.navigate("MY_LISTINGS")} className={myListingClass.join(' ')}>My Listings</Nav.Link>
                        </Nav.Item>
                    </Nav>
                    <div style={{width:"100%",fontFamily:"Cambria"}} className="text-center p-2">
                        <button className="text-danger" onClick={this.props.scams}>Please beware of scams!!!</button>
                    </div>
                    <div className="col-sm-12 mt-3">{/*listing-main-pane listings_main col-md-6*/}
                        {this.props.showListingModal?<ListPet close={this.props.closeModal}/> : ''}
                        {this.props.component === "LISTINGS" ? <Listing
                            coins={this.props.user_info.coins}
                            activeUserId={this.props.user_info.id}
                            city={this.props.user_info.city !== null?this.props.user_info.city : ''}
                            country={this.props.user_info.country !== null ? this.props.user_info.country : ''}
                        /> : <MyListing/>}
                    </div>

                </div>
                {this.props.scamModal?<Scams close={this.props.scams}/>:null}
            </div>
        );
    }
}

const mapState = (state) =>{
    return{
        showListingModal : state.Listing.listingModal,
        component : state.Listing.component,
        user_info : state.User.user_info,
        //listing filter
        //city : state.Filter.city,
        country : state.Filter.country,
        dog : state.Filter.Dog,
        cat : state.Filter.Cat,
        adop : state.Filter.Adoption,
        mating : state.Filter.Mating,
        sale : state.Filter.Sale,
        scamModal : state.Listing.scamModal,
        // fullLoader:state.Layout.fullLoader
    }
};

const mapDispatch = (dispatch) =>{
    return {
        navigate : (comp) =>{dispatch(navigateListing(comp))},
        listPetModal : () => {dispatch(listingModal())},
        closeModal : () => {dispatch(listingModal())},
        filter_change : (event,num,name) => {dispatch(filter_change(event,num,name))},
        filter_listing : (e,data) => {e.preventDefault();dispatch(filterSearch(data))},
        scams : () => {dispatch({type:"SCAMS"})},
    }
};

export default connect(mapState,mapDispatch)(AllListing);
