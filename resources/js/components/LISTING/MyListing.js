import React, {Component} from 'react';
import {deleteListing, listingModal, listings, myListings} from "./STORE/Actions";
import {toggleProfilePreview} from "../USER PROFILE/STORE/Actions";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import ProfilePreview from "../USER PROFILE/ProfilePreview";

class MyListing extends Component {
    componentDidMount() {
        this.props.myListings()
    }

    componentDidUpdate(){
       // console.log(this.props.listings);
    }

    render() {
        let listings = 'loading';

            if (!Object.keys(this.props.listings).length){
                listings = <div className="text-center mt-5">
                    <h5 className="mb-5">You haven't listed any pets</h5>
                    <img src={'/img/petsale.svg'} alt="pet sale" width={'80'} height={'80'}/>
                    {/*<button onClick={this.props.listPetModal} className="ml-4 btn btn-default"><img src={'/img/add.svg'} alt="list pet" height={'40'} width={'40'}/></button>*/}
                </div>
            }else
            listings = this.props.listings.map((item,index)=>{
                // console.log(item.id)
                let type;
                if(item.type === 0){
                    type = "Adopt"
                }else if(item.type === 1){
                    type = "Mate"
                }else type = "Sale";
                return <div key={index} className="mb-2 col-sm-12 col-lg-4">
                    <div className="card">{/* listing-card*/}

                        <div className="listing-image-container">
                            <div className="listing-image">
                                {
                                    item.profile_photo === null ? <img src={"/img/rodent.svg"} alt="pet" className="card-img-top listing-pet-image" /> :
                                        <img className="card-img-top listing-pet-image" src={`/storage/pets/${item.pet_id}/${item.profile_photo}`} alt="pet"/>

                                }
                                {/*
                                    this.props.showNote && this.props.listingToView === item.id ?
                                        <div className="listing-note">
                                            <span>City: <span className="list-info">{item.city}</span></span><br/>
                                            <span>Gender: <span className="list-info">{gender}</span></span><br/>
                                            <span>Listing: <span className="list-info">{type}</span></span><br/>
                                            {
                                                type === "Mate" ? <span>Mate Breed: <span className="list-info">{item.mate_breed[0]}</span></span> :
                                                    type === "Sale" ? <span>Price: <span className="list-info">{item.price}</span></span> : null
                                            }
                                            <p className="mt-2">Info: {item.note!==null? <span className="list-info">{item.note}</span> :<span className="list-info">None</span>}</p>
                                            <div style={{position: "absolute",bottom:0}}><button className="btn btn-success" onClick={()=>this.props.viewNote(item.id)}>hide note</button></div>
                                        </div>:""
                               */}
                            </div>
                            <div className="listing-type-overlay">
                                <span className="card-title bg-secondary">{type}</span>
                            </div>
                        </div>

                        <div className="listing-desc">{/*onMouseEnter={()=>{this.props.profile("pet")}} onMouseLeave={()=>{this.props.profile("pet",true)}}*/}
                            <div className="usn-views">
                                <div className="list-pet-usn text-center">
                                    <NavLink to={`/pet/${item.pet_id}`} className="listed-pet-name">
                                        {item.username}
                                    </NavLink>
                                </div>
                                <div className="list-pet-views">
                                    <img src={'/img/eye.svg'} alt="views" width={'20'} height={'20'} className="mr-2"/> {item.views?item.views : 0}
                                    {/*<span className="listing-view fas fa-eye"> </span>*/}
                                </div>
                            </div>
                            {/*
                                type === "Mate"?
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="fas fa-search mr-2"/>
                                        {item.mate_breed[0]}
                                    </div>
                                :
                                type === "Sale" ?
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="far fa-money-bill-alt mr-2"/>
                                        ${item.price}
                                    </div>
                                    :
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="fas fa-search mr-2"/>
                                        New owner
                                    </div>
                            */}

                            <div className="p-1 text-info mt-2">
                                <i className="far fa-sticky-note mr-2"/>
                                <span style={{cursor: 'pointer'}}>
                                                <span style={{width:"100%"}} className="mt-2 text-center">
                                                    <button style={{width:"95%",marginTop:"10px",fontFamily:"Cambria",backgroundColor:"darkslategrey",color:"white"}}
                                                            onClick={()=>this.props.deleteListing(item.id)}>delete</button>
                                                </span>

                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            });
        // }
        return (
            <div className="row">
                <div className="col-sm-12">
                    <button onClick={this.props.listPetModal} className="btn btn-default"><img src={'/img/add.svg'} alt="list pet" height={'40'} width={'40'}/></button>
                </div>
                {listings}
            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        listings : state.Listing.myListings
    }
};

const mapDispatch = (dispatch) =>{
    return {
        listPetModal : () => {dispatch(listingModal())},
        myListings : () =>{ dispatch(myListings())},
        deleteListing : (listingID) =>{
            if (confirm('Do you want to proceed with this action ?')){
                dispatch(deleteListing(listingID))
            }
        },
    }
};


export default connect(mapState,mapDispatch)(MyListing);
