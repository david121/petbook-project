import React, {Component} from 'react';
import {connect} from "react-redux";
import {filter as filterSearch, listingModal, listings, listingView, viewNote} from "./STORE/Actions";
import './CSS/Listing.css'
import {profilePreview, toggleProfilePreview} from "../USER PROFILE/STORE/Actions";
import ProfilePreview from "../USER PROFILE/ProfilePreview";
import {NavLink} from "react-router-dom";
class Listing extends Component {

    constructor(props){
        super(props);
        this.state={
            city: true,
            country: false,
            Dog: true,
            Cat: true,
            Adoption: true,
            Mating: true,
            Sale: true,
        }
    }

    componentDidMount() {
        this.props.getListings();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        //console.log("IN",this.props.filtered)
      }

    render() {
        let filter = {
            city:{type:'location',value:this.state.city},
            country:{type:'location',value:this.state.country},
            Dog: {type:'pet-type',value:this.state.Dog},
            Cat: {type:'pet-type',value:this.state.Cat},
            Adoption:{type:'list-type',value:this.state.Adoption},
            Mating: {type:'list-type',value:this.state.Mating},
            Sale: {type:'list-type',value:this.state.Sale},
        } ;
        let listing;
        if (this.props.filter_mode){
            if (this.props.filtered.length > 0){
            listing = this.props.filtered.map((item,index)=>{
                let link = item.user_id === this.props.activeUserId ? `/pet/${item.pet_id}` : `/pet-view/${item.pet_id}`;
                let type;
                let gender = item.gender === 0 ? "Male" : "Female";
                if(item.type === 0){
                    type = "Adopt"
                }else if(item.type === 1){
                    type = "Mate";
                }else type = "Sale";
                return <div key={index} className="mb-2 col-sm-12 col-lg-4">
                    <div className="card">{/* listing-card*/}

                        <div className="listing-image-container">
                            <div className="listing-image">
                                <img className="card-img-top listing-pet-image" src={`/storage/pets/${item.pet_id}/${item.profile_photo}`} alt="pet"/>
                                {
                                    this.props.showNote && this.props.listingToView === item.id ?
                                        <div className="listing-note">
                                            <span>City: <span className="list-info">{item.city}</span></span><br/>
                                            <span>Gender: <span className="list-info">{gender}</span></span><br/>
                                            <span>Listing: <span className="list-info">{type}</span></span><br/>
                                            {
                                                type === "Mate" ? <span>Mate Breed: <span className="list-info">{item.mate_breed[0]}</span></span> :
                                                    type === "Sale" ? <span>Price: <span className="list-info">{item.price}</span></span> : null
                                            }
                                            <p className="mt-2">Info: {item.note!==null? <span className="list-info">{item.note}</span> :<span className="list-info">None</span>}</p>
                                            <div style={{position: "absolute",bottom:0}}><button className="btn btn-success" onClick={()=>this.props.viewNote(item.id)}>hide note</button></div>
                                        </div>:""
                                }
                            </div>
                            <div className="listing-type-overlay">
                                <span className="card-title bg-secondary">{type}</span>
                            </div>
                        </div>

                        <div className="listing-desc">{/*onMouseEnter={()=>{this.props.profile("pet")}} onMouseLeave={()=>{this.props.profile("pet",true)}}*/}
                            <div className="usn-views">
                                <div className="list-pet-usn text-center">
                                    <NavLink to={link} className="listed-pet-name" onClick={()=>this.props.listingView(item.id)}>
                                        {item.username}
                                    </NavLink>
                                </div>
                                <div className="list-pet-views">
                                    <img src={'/img/eye.svg'} alt="views" width={'20'} height={'20'} className="mr-2"/> {item.views?item.views : 0}
                                    {/*<span className="listing-view fas fa-eye"> </span>*/}
                                </div>
                            </div>

                            <div className="p-1 text-info mt-2">
                                <i className="far fa-sticky-note mr-2"/>
                                <span style={{cursor: 'pointer'}} onClick={()=>this.props.viewNote(item.id)}>
                                            {this.props.showNote && this.props.listingToView === item.id ?
                                                "" :
                                                <span style={{width:"100%"}} className="mt-2 text-center">
                                                    <button style={{width:"95%",marginTop:"10px",fontFamily:"Cambria",backgroundColor:"darkslategrey",color:"white"}}>details</button>
                                                </span>
                                            }
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

            })
        }else listing = <div className="text-center mt-3" style={{width:"100%"}}>
                <span style={{fontFamily:"Cambria",fontSize:"20px"}} className="text-center">
                    Your filter settings returned no results.
                </span>
            </div>
        }else
        if (this.props.listings.length === 0){
            listing= <div className="text-center mt-5">
                <h5 className="mb-5">There are no active listings</h5>
                <img src={'/img/petsale.svg'} alt="pet sale" width={'80'} height={'80'}/>
                <button onClick={this.props.listPetModal} className="ml-4 btn btn-default"><img src={'/img/add.svg'} alt="list pet" height={'40'} width={'40'}/></button>
            </div>
        }else{
            listing = this.props.listings.map((item,index)=>{
                // console.log(item.user_id)
                let link = item.user_id === this.props.activeUserId ? `/pet/${item.pet_id}` : `/pet-view/${item.pet_id}`;
                let type;
                let gender = item.gender === 0 ? "Male" : "Female";
                if(item.type === 0){
                    type = "Adopt"
                }else if(item.type === 1){
                    type = "Mate";
                }else type = "Sale";
                return <div key={index} className="mb-2 col-sm-12 col-lg-4">
                    <div className="card">{/* listing-card*/}

                        <div className="listing-image-container">
                            <div className="listing-image">
                                {
                                    item.profile_photo === null ? <img src={"/img/rodent.svg"} alt="pet" className="card-img-top listing-pet-image" /> :
                                        <img className="card-img-top listing-pet-image" src={`/storage/pets/${item.pet_id}/${item.profile_photo}`} alt="pet"/>

                                }

                                {
                                    this.props.showNote && this.props.listingToView === item.id ?
                                        <div className="listing-note">
                                            <span>City: <span className="list-info">{item.city}</span></span><br/>
                                            <span>Gender: <span className="list-info">{gender}</span></span><br/>
                                            <span>Listing: <span className="list-info">{type}</span></span><br/>
                                            {
                                                type === "Mate" ? <span>Mate Breed: <span className="list-info">{item.mate_breed[0]}</span></span> :
                                                type === "Sale" ? <span>Price: <span className="list-info">{item.price}</span></span> : null
                                            }
                                            <p className="mt-2">Info: {item.note!==null? <span className="list-info">{item.note}</span> :<span className="list-info">None</span>}</p>
                                            <div style={{position: "absolute",bottom:0}}><button className="btn btn-success" onClick={()=>this.props.viewNote(item.id)}>hide note</button></div>
                                        </div>:""
                                }
                            </div>
                            <div className="listing-type-overlay">
                                <span className="card-title bg-secondary">{type}</span>
                            </div>
                        </div>

                        <div className="listing-desc">{/*onMouseEnter={()=>{this.props.profile("pet")}} onMouseLeave={()=>{this.props.profile("pet",true)}}*/}
                            <div className="usn-views">
                                <div className="list-pet-usn text-center">
                                    <NavLink to={link} className="listed-pet-name" onClick={()=>this.props.listingView(item.id)}>
                                        {item.username}
                                    </NavLink>
                                </div>
                                <div className="list-pet-views">
                                    <img src={'/img/eye.svg'} alt="views" width={'20'} height={'20'} className="mr-2"/> {item.views?item.views : 0}
                                    {/*<span className="listing-view fas fa-eye"> </span>*/}
                                </div>
                            </div>
                            {/*
                                type === "Mate"?
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="fas fa-search mr-2"/>
                                        {item.mate_breed[0]}
                                    </div>
                                :
                                type === "Sale" ?
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="far fa-money-bill-alt mr-2"/>
                                        ${item.price}
                                    </div>
                                    :
                                    <div className="text-info text-left p-1 list-type">
                                        <i className="fas fa-search mr-2"/>
                                        New owner
                                    </div>
                            */}

                            <div className="p-1 text-info mt-2">
                                    <i className="far fa-sticky-note mr-2"/>
                                    <span style={{cursor: 'pointer'}} onClick={()=>this.props.viewNote(item.id)}>
                                            {this.props.showNote && this.props.listingToView === item.id ?
                                                "" :
                                                <span style={{width:"100%"}} className="mt-2 text-center">
                                                    <button style={{width:"95%",marginTop:"10px",fontFamily:"Cambria",backgroundColor:"darkslategrey",color:"white"}}>details</button>
                                                </span>
                                            }
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            })
        }
        return (
            <div>
            <div className='filter-wrapper'>
                <div className="listing-filter text-sm-center filter-lg">
                    <form className="row mt-2" onSubmit={(e)=>{this.props.filter_listing(e,filter)}}>
                        <div className="listing-location listing-d col-sm-*">
                            <label htmlFor="" className="mr-3 text-center">
                                <input type="radio" name="region" value={this.props.city} disabled={true} defaultChecked={true} onChange={()=>{this.setState({...this.state,city:!this.state.city})}}/><br/>
                                {this.props.city}
                            </label>
                            <label htmlFor="" className="text-center">
                                <input type="checkbox" name="region" value={this.props.country} disabled={false} onChange={()=>{this.setState({...this.state,country:!this.state.country})}}/><br/>
                                {this.props.country}
                            </label>
                        </div>

                        <div className="listing-pet-types listing-d col-sm-*">
                            <label htmlFor="" className="mr-3 text-center">
                                <input type="checkbox" name="pet-type" value={this.props.dog} defaultChecked={true} onChange={()=>{this.setState({...this.state,Dog:!this.state.Dog})}}/><br/>
                                Dogs
                            </label>
                            <label htmlFor="" className="text-center">
                                <input type="checkbox" name="pet-type" value={this.props.cat} defaultChecked={true} onChange={()=>{this.setState({...this.state,Cat:!this.state.Cat})}}/><br/>
                                Cats
                            </label>
                        </div>
                        <div className="listing-types listing-d col-sm-*">
                            <label htmlFor="" className="mr-3 text-center">
                                <input type="checkbox" name="listing-type" value={this.props.adop} defaultChecked={true} onChange={()=>{this.setState({...this.state,Adoption:!this.state.Adoption})}}/><br/>
                                Adoption
                            </label>
                            <label htmlFor="" className="mr-3 text-center">
                                <input type="checkbox" name="listing-type" value={this.props.mating} defaultChecked={true} onChange={()=>{this.setState({...this.state,Mating:!this.state.Mating})}}/><br/>
                                Mating
                            </label>
                            <label htmlFor="" className="text-center">
                                <input type="checkbox" name="listing-type" value={this.props.sale} defaultChecked={true} onChange={()=>{this.setState({...this.state,Sale:!this.state.Sale})}}/><br/>
                                Sale
                            </label>
                        </div>
                        <div className="ml-3 filter-sm">
                            <input type="submit" className="btn btn-info" value={'filter'}/>
                        </div>
                    </form>
                </div>
                <div className="listing-coins listing-d ml-4 col-sm-*">
                    <div className="d-flex flex-column">
                        <span className="text-center" style={{cursor:'pointer',fontFamily:'Cambria',fontSize:'22px',color:'darkslategrey',}} onClick={this.props.addCoinsBtn}>
                            <img src={'/img/coin.svg'} alt="coins" width="28" height="28" className="mr-2"/>
                            {this.props.coins}
                        </span>
                        {this.props.coins_btn?
                            <span className="add-coins-link" onClick={()=>alert("Service currently unavailable")}>
                                Add Coins
                            </span> : null
                        }
                        {/*  <div className="payment-options"></div>*/}
                    </div>
                </div>
            </div>
                <div className="row mt-3">
                    {listing}
                </div>
            </div>
        );
    }
}
const mapState = (state) =>{
    return {
        listings : state.Listing.listings,
        filtered : state.Listing.filtered_listings,
        filter_mode : state.Listing.filter_mode,
        previewPet : state.PetProfile.profilePreview,
        showNote : state.Listing.viewNote,
        listingToView : state.Listing.listingToView,
        list : state.Filter.list,
        region : state.Filter.region,user_info : state.User.user_info,
        coins_btn : state.Listing.coins_btn,
    }
};

const mapDispatch = (dispatch) =>{
    return {
        getListings : () =>{dispatch(listings())},
        profile : (user,leave) =>{dispatch(toggleProfilePreview(user,leave))},
        listPetModal : () => {dispatch(listingModal())},
        listingView : (listingId) => {dispatch(listingView(listingId))},
        viewNote : (listingId) => {dispatch(viewNote(listingId))},
        filter_listing : (e,data) => {e.preventDefault();dispatch(filterSearch(data))},
        addCoinsBtn : ()=>{dispatch({type:"ADD_COINS_BTN"})}
    }
};

export default connect(mapState,mapDispatch)(Listing);
