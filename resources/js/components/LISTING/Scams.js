import React from 'react'
import './CSS/Scams.css'
const Scams = (props) => {

    return (
        <div className="scams-modal">
            <div className="scams-content">
                <div className="scams-header">
                    <span className="close" onClick={props.close}>&times;</span>
                    <h5>Scams</h5>
                </div>
                <div className="scams-body">
                    <div className="scam-note">
                        <span>
                            While measures such as necessitating users' location during the listing process have been taken in order to mitigate the abuse of Listings,
                            it is still essential to remind our users of potential scams and how to identify them.
                            <br/><br/>

                            So, if you are interested in a listing:

                        </span>
                        <div className="what-to-do">
                            <h4 className="mt-3">What you should do</h4>
                            <ul>
                                <li>
                                    Find out how long the pet has been registered. If recently, Beware!
                                </li>
                                <li>
                                    Browse the pet's page, if there are't enough activities - posts, likes, admirers - Beware!
                                </li>
                                <li>
                                    Browse the pet owner's page, if there are't enough activities - posts, likes, friends - Beware!
                                </li>
                                <li>
                                    Engage in physical transactions.
                                </li>
                            </ul>
                        </div>
                        <div className="what-not-to-do">
                            <h4 className="mt-3">What you shouldn't do</h4>
                            <ul>
                                <b>Do not</b>
                                <li>
                                    Give sensitive information out.
                                </li>
                                <li>
                                    Transfer money before completing any transaction. Pets are not clothing!
                                </li>
                                <li>
                                    Receive advance payments for a transaction that's not taken place of yet. IT IS SCAM!
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    )

};

export default Scams;