import React from "react";
import {OverlayTrigger, Tooltip} from "react-bootstrap";


const Adoption = (props) =>{
    const style ={
        fontWeight:"bolder",
        fontFamily:"Cambria",
        color:"#451414"
    };
    let listingPhase;
    
    if(props.pets.length === 0){ //User has pets
        listingPhase = <div className="no-pets-wrap">
            <span className="no-pets-listing"> You have no pets </span>
        </div>
    }
    
    else if (props.pets.length === 1){// User has one pet
        // console.log(.props.pets[0])
        if (props.pets[0].listed !== null){
            return listingPhase = <div className="text-center">
                <b className="text-success">{props.pets[0].name}  is already listed.</b>
            </div>
        }
        const data = {type:props.list_type,petId:props.pets[0].id,note:props.note,coins:props.coins};
        listingPhase = <div className="text-center">
            <div className="mt-3">
                {/*<div className="m-3"><h5>Complete listing {.props.pets[0].name} for adoption</h5></div>*/}
                <form onSubmit={(e)=>props.submit(e,data)}>
                    <div>
                        <textarea className="listing-info-input" cols="40" rows="4" value={props.note} onChange={(e)=>props.inputHandler("note",e)} placeholder="write additional information about your listing...(optional)"/>
                    </div>

                    <div className="mt-4 p-2">
                        {/*<h6>Listing Overview</h6>*/}
                        <div className="mt-2 row" style={{width:"100%"}}>
                            <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                <b className="listing-type-header">Type: </b>
                                <span className="lis-info">
                                    {props.list_type}
                                </span>
                            </div>
                            <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                <b className="listing-pet-header">Pet: </b>
                                <span className="lis-info">
                                    {props.pets[0].name}
                                </span>
                            </div>

                            <div className="col-sm-12 d-flex justify-content-center p-1">
                                <b className="listing-pet-header">Location: </b>
                                <small className="text-danger lis-info"><b>required</b>
                                    <OverlayTrigger
                                        placement={'right'}
                                        overlay={
                                            <Tooltip id={`tooltip-top`}>
                                                Location is automatically determined by the application in order to mitigate fake listings.
                                            </Tooltip>
                                        }
                                    >
                                        <img src={'/img/info.svg'} alt="info" width="15" height="15" className="ml-2"/>
                                    </OverlayTrigger>{' '}
                                </small>
                            </div>

                            {props.note?
                                <div className="text-center d-flex justify-content-center col-sm-12 p-1">
                                    <b className="listing-note-header mr-1">Note: </b>
                                    {props.note?<div className="listing-note-display"><span className="">{props.note}</span></div>:''}
                                </div> : ''}
                        </div>
                        <button onClick={props.startOver} className="mt-3 mr-2 listing-restart">Start Over</button>
                        <button type='submit' className="submit-listing">submit | <img src={'/img/pet-coin.jpg'} alt="coin" width="22" height="22"/> 250 </button>
                    </div>
                </form>
            </div>
        </div>


    }else{ //User has many pets
        let pets = props.pets.map((pet,index)=>{

            let petImg;
            if (pet.listed !== null){
                petImg = pet.profile_photo === null?
                    <div>
                        <img src={"/img/rodent.svg"} alt="pet" width={"130"} className="rounded-circle pet-image post-by-pic" height="110" style={{opacity:"0.7"}}/>
                        <span style={style}>{pet.name}</span>
                    </div>
                        :
                    <div>
                        <img key={index} src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt={"pet photo"} className="rounded-circle pet-image post-by-pic" width="130" height="110" style={{opacity:"0.8"}}/>
                        <span style={style}>{pet.name}</span>
                    </div>
            }else petImg = pet.profile_photo === null?
                <div>
                    <img src={"/img/rodent.svg"} alt="pet" width={"130"} onClick={()=>props.nextPhase("petId",pet.id)} className="rounded-circle pet-image post-by-pic" height="110" style={{opacity:"0.8"}}/>
                    <span style={style}>{pet.name}</span>
                </div>
                :
                <div>
                    <img key={index} src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt={"pet photo"} onClick={()=>props.nextPhase("petId",pet.id)} className="rounded-circle pet-image post-by-pic" width="130" height="110"/>
                    <span style={style}>{pet.name}</span>
                </div>;
            return <label key={index} className="col-sm-4">
                {petImg}
            </label>
        });
        listingPhase = <div className="text-center listing-phase p-3 mt-3">
            <div className="mb-3">
                <h5>Which of your pets do you want to list for adoption?</h5>
            </div>
            <div className="mt-4 mb-3">{pets}</div>
            <button type="button" onClick={props.startOver} className="listing-restart mt-2">Start Over</button>
        </div>
    }
    return listingPhase;
};

export default Adoption;