
const initialState = {
    city: true,
    country: false,
    Dog: true,
    Cat: true,
    Adoption: true,
    Mating: true,
    Sale: true,

    list : [],
    listRegion : ''
};


const FilterListing = (state = initialState, action) =>{
    switch (action.type) {
        case "FILTER_CHANGE":
            console.log("city",state.city);
            return{
                ...state,
                [action.name]: ! `${action.name}`
            };
        case "LISTINGS_FILTERED":
            return{
                ...state,
                list:action.list,
                listRegion:action.region
            };

        default:  return state
    }
};

export default FilterListing;