import axios from 'axios'
import {fetchPets, fetchUser, findPetBreed} from "../../USER PROFILE/STORE/Actions";
import {axiosHeader} from "../../index";
import {Api_Process} from "../../LAYOUT/STORE/Actions";

export const navigateListing = (component) =>{
  return dispatch =>{
      dispatch({type:"NAVIGATE",component:component})
  }
};


export const filter_change = (event,num,name) =>{
  return dispatch =>{
        dispatch({
            type:"FILTER_CHANGE",
            name:name,
            value:event.target.value
        })
  }
};
export const filter= (data) =>{
  return dispatch =>{
      dispatch(Api_Process);
      dispatch({type:"FILTERING"});
        axios.post('/api/listing/filter',data,{headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(res=>{
                dispatch(Api_Process);
            if (res.data.success){
                // console.log("ACTIONS", res.data.listings);
                dispatch({
                    type:"FILTERED_LISTINGS",
                    list : res.data.listings,
                    region : res.data.location
                })
            }
        }).catch(err=>{
            dispatch(Api_Process);
            dispatch({type:"FILTER_ERROR"})
        })
  }
};

export const listings = () =>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.get('/api/listings',{
            headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        }).then(response=>{
            dispatch(Api_Process());
            if (response.data.success){
                dispatch({type:"LISTINGS",listings:response.data.listings  })}
        }).catch(()=>{dispatch(Api_Process());})
    }
};
export const myListings = () =>{
    return dispatch =>{
        dispatch(Api_Process());
       const  req =  axios.get('/api/my-listings',{
            headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        });
       req.then(response=>{
           dispatch(Api_Process());
            if (response.data.success){
                dispatch({type:"MY_LISTINGS",listings:response.data.listings  })
            }
        }).catch(()=>{dispatch(Api_Process());})
    }
};

export const deleteListing = (listingId) =>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.post('/api/listings/delete',{id : listingId},{
            headers: {
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        }).then(response=>{
            dispatch(Api_Process());
            if (response.data.success){
                dispatch(myListings())
            }
        }).catch(error=>{console.log(error)})
    }
};

export const listingModal = () =>{
    return dispatch =>{
        dispatch({type:"LISTING_MODAL"})
    }
};

export const listingLoader = () =>{
    return dispatch=>{dispatch({type : "LOAD"})}
};

export const fetchPetsForListing = () =>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.get(`/api/fetchPets`,{headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    if(response.data.pet.length===1){
                        dispatch(findPetBreed(response.data.pet[0].breed_id));
                    }
                    dispatch({
                        type : 'FETCH_LISTING_PETS',
                        data: response.data.pet,
                        src : response.data.src
                    })
                } else{
                    console.log(response.data.msg)
                }

            })
            .catch(()=>{
                dispatch(Api_Process());
            });
    }
};

export const nextPhase = (currentPhase, answer)=>{
    // console.log(answer)
    return dispatch=>{

        if (currentPhase === 'listing_type')dispatch(fetchPetsForListing());
        if (currentPhase === "petId_mating"){
           dispatch(findPetBreed(answer.breed_id));
            dispatch({type:"LISTING_PHASE_ANSWER",phase:"petId",value:answer.id} );//defaultMate:answer.breed_id
            return;
        }

        dispatch({
            type : "LISTING_PHASE_ANSWER",
            phase: currentPhase,
            value : answer
        })
    }
};

export const additionalInfo = (name,info,breeds)=>{
   // console.log("actions", info)
    return dispatch=>{

        if (name === 'mateBreed'){
            let breedToMate = breeds.filter((breed)=>{
                return info === breed.name;
            });
            //console.log(breedToMate);
            dispatch({
                type:"LISTING_INFO",
                name : name,
                value:info,
                selectBreed :true,
                breed : Object.keys(breedToMate) ? breedToMate : null
            })
        }
        dispatch({
            type:"LISTING_INFO",
            name : name,
            value:info
        })
    }
};

export const startOver = () =>{
    return dispatch =>{ dispatch({type : "START_OVER"})}
};


export const getUserLocation = (data) =>{
    // console.log(data.coins)
    //pk.3800bd9bd575c363768b7ba6173b547d
    // console.log(data)
    const price = 250;
    let error = false;
    return dispatch =>{
        // dispatch(Api_Process());
    if (data.type === "Mating"){
        if (!data.mateBreed){
            error = true;
            alert('Specify a mate breed')
        }else
        data.mateBreed = data.mateBreed.id
    }
    if (data.type === "Sale"){
        if (!data.price){
            error = true;
            alert('Specify a valid price')
        }
    }
    if (!error){
        // console.log()

        if (data.coins >= price){
            // alert("true here")
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition((position)=>{
                let settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": `https://us1.locationiq.com/v1/reverse.php?key=1c5175d4b1c4c9&lat= ${position.coords.latitude}&lon=${position.coords.longitude}&format=json`,
                    "method": "GET"
                };
                $.ajax(settings).done(response=> {
                    let city = response.address.city ? response.address.city : response.address.state;
                    data.location = {
                        suburb : response.address.suburb,
                        city : city,
                        country : response.address.country,
                    };
                    data.coins >= price?dispatch(submitListing(data)): dispatch({type:"INSUFFICIENT_BALANCE"})
                });
            }, (error)=>{
                switch (error.code) {
                    case error.PERMISSION_DENIED : return dispatch("LOCATION_DENIED");//alert("You denied location access");
                    case error.POSITION_UNAVAILABLE : alert("Location unavailable");break;
                    case error.TIMEOUT : alert("Request time out");break;
                }
            })
        }else {
            alert("Oops! We couldn't determine your location. You might want to try a different browser.")
        }
    }else{dispatch({type:"INSUFFICIENT_BALANCE"})}
    }
    }
    //return location;
};

export const submitListing = (data) =>{
    return dispatch =>{
        dispatch(Api_Process());
        axios.post(`/api/listings/list`,data,{headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(response=>{
                if (response.data.success){
                    if (response.data.list_error){
                        dispatch(Api_Process());
                        return dispatch({type:"LIST_ERROR"})
                    }
                    dispatch(Api_Process());
                    dispatch(myListings());
                    dispatch(fetchUser());
                    dispatch({type : "PET_LISTED"})
                } else{
                    dispatch(Api_Process());
                    console.log(response.data.msg)
                }
            })
            .catch(error=>{
                console.log(error)
            });
    }
};


export const viewNote = (listingId)=>{
    return dispatch =>{
        dispatch(listingView(listingId));
        dispatch({type:"VIEW_NOTE",value:listingId})
    }
};

export const showPayment = ()=>{
    return dispatch =>{
        dispatch({type:"COINS_PAYMENT"})
    }
};


export const listingView = (listingId) =>{
    return dispatch =>{
        axios.post(`/api/listings/view`,{listing_id:listingId},{headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }})
            .then(response=>{
                if (response.data.success){

                }
            })
            .catch(error=>{
                console.log(error)
            });
    }
};
