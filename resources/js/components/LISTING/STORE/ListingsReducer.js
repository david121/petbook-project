
const initialState = {
    component : 'LISTINGS',
    listings : [],
    filtered_listings : [],
    filter_mode : false,
    myListings : [],
    listingModal : false,
    listingLoader : false,
    phase : 1,
    pets : [],
    breeds : [],
    listing_type : '',
    petId : '',
    note : '',
    mateBreed: '',
    breedToMate : '',
    foundBreed: [], // breed to submit
    price : '',
    viewNote : false,
    listingToView:'',
    listing_error : false,
    paymentOptions : false,
    coins_btn : false,
    scamModal : false,
    location_denied : false,
    listError : false,
};


const ListingReducer = (state=initialState,actions)=>{
    switch (actions.type) {
        case "NAVIGATE" :
            return {
                ...state,
                component: actions.component
            };
        case "FILTERING" :
            return {
                ...state,
                filter_mode: true
            };
        case "SCAMS" :
            return {
                ...state,
                scamModal: !state.scamModal
            };
        case "LOCATION_DENIED" :
            return {
                ...state,
                location_denied: true
            };
        case "LISTINGS" :
            return{
                ...state,
                listings : actions.listings
            };
        case "FILTERED_LISTINGS" :
            return{
                ...state,
                filtered_listings : actions.list
            };

        case "MY_LISTINGS" :
            return{
                ...state,
                myListings : actions.listings
            };
        case "LISTING_MODAL" :
            return {
              ...state,
                listingModal: !state.listingModal
            };
        case "LISTING_PHASE" :
            return {
                ...state,
                phase : actions.value
            };
        case "COINS_PAYMENT" :
            return {
                ...state,
                paymentOptions : !state.paymentOptions
            };
        case "LOAD" :
            return {
                  ...state,
                listingLoader: !state.listingLoader,
            };
        case "LISTING_PHASE_ANSWER" :
            const newPhase = state.phase + 1;
            // if (actions.breed){
            //     // console.log("value reducer",actions.breed);
            //     return {
            //         ...state,
            //         [actions.phase] : actions.value,
            //         phase: newPhase,
            //         foundBreed:actions.breed,
            //     }
            // }
            return {
                ...state,
                [actions.phase] : actions.value,
                phase: newPhase,
            };
        case "START_OVER" :
            return {
                ...state,phase:1
            };
        case "FETCH_LISTING_PETS" :
            return {
            ...state,
            pets: actions.data,
        };
        case "FIND_PET_BREED":
            return{
                ...state,
              breeds : actions.breeds
            };
        case "LISTING_INFO":
            if (actions.selectBreed){
                return {
                    ...state,
                    [actions.name]: actions.value,
                    foundBreed: actions.breed
                }
            }
            return {
                ...state,
                [actions.name]: actions.value
            };
        case "PET_LISTED":
            return {
                ...state,
                listingModal : false,
                phase : 1,
                pets : [],
                breeds : [],
                listing_type : '',
                petId : '',
                note : '',
                mateBreed: '',
                price : ''
            };
        case "LIST_ERROR":
            return{
                ...state,
                listError: true
            };
        case "VIEW_NOTE":
            return{
                ...state,
                viewNote : !state.viewNote,
                listingToView: actions.value
            };
        case "INSUFFICIENT_BALANCE":
            return{
                ...state,
                listing_error : true
            };case "ADD_COINS_BTN":
            return{
                ...state,
                coins_btn : !state.coins_btn
            };
        default : return state
    }
};

export default ListingReducer;

