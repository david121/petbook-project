import React, {Component} from 'react';
import {connect} from "react-redux";
import {additionalInfo, getUserLocation, nextPhase, showPayment, startOver, submitListing} from "./STORE/Actions";
import Adoption from "./Adoption";
import Mating from "./Mating";
import Sale from "./Sale";
import PaymentOptions from "./BUY COINS/PaymentOptions";
import Process from "../LAYOUT/Process";
import {Button, ButtonToolbar, OverlayTrigger, Tooltip} from "react-bootstrap";

class ListPetModal extends Component {

    componentDidMount() {
        // if (this.props.pets.length === 1){
        //     this.props.fetchBreeds(this.props.pets[0].breed_id);
        // }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
         // console.log(this.props.foundBreed)
    }

    render() {
        let buyCoins = this.props.buyCoins ? <PaymentOptions/> : '';
        let listingPhase;
      //  let loader = this.props.loader ? <div className="loader">Loading</div> : '';
        switch (this.props.phase) {
            case 1 :
                listingPhase = <div className="text-center">
                               <div className="m-3"> <h5>I want to list my pet for</h5> </div>
                            <div className = "d-flex flex-column p-5 justify-content-center align-items-center" style={{width:"100%"}}>
                                <button className="btn btn-secondary mb-3 list-type-select" onClick={()=>this.props.nextPhase("listing_type", "Adoption")}>Adoption</button>
                                <button className="btn btn-secondary mb-3 list-type-select" onClick={()=>this.props.nextPhase("listing_type", "Mating")}>Mating</button>
                                <button className="btn btn-secondary mb-3 list-type-select" onClick={()=>this.props.nextPhase("listing_type", "Sale")}>Sale</button>
                            </div>
                        </div>;
                        break;
            case 2 :
                //Adoption
                if (this.props.list_type === "Adoption"){
                    listingPhase = <Adoption pets={this.props.pets} list_type={this.props.list_type} note={this.props.note} submit={this.props.submitListing} inputHandler={this.props.inputHandler}
                                startOver={this.props.startOver} nextPhase={this.props.nextPhase} coins={this.props.user.coins}
                    />;
                } //Mating
                else if (this.props.list_type === "Mating"){
                    listingPhase = <Mating pets={this.props.pets} breeds={this.props.breeds} list_type={this.props.list_type} note={this.props.note} submit={this.props.submitListing} inputHandler={this.props.inputHandler}
                               startOver={this.props.startOver} nextPhase={this.props.nextPhase} foundBreed={this.props.foundBreed} mateBreed={this.props.mateBreed} coins={this.props.user.coins}/>
                }else{//Sale
                    listingPhase = <Sale pets={this.props.pets} list_type={this.props.list_type} note={this.props.note} submit={this.props.submitListing} inputHandler={this.props.inputHandler}
                                 startOver={this.props.startOver} nextPhase={this.props.nextPhase} price={this.props.price} coins={this.props.user.coins}/>
                }
                break;

            case 3:
                //Submit adoption listing
                if (this.props.list_type === "Adoption" && this.props.petId !== ''){
                    const data = {type:this.props.list_type,petId:this.props.petId,note:this.props.note, coins:this.props.user.coins};
                    const petToList = this.props.pets.filter((pet)=>{
                        return this.props.petId === pet.id;
                    });
                    listingPhase = <div className="text-center">
                        <div className="mt-3">
                            {/*<h5>Additional information (Optional)</h5>*/}
                        <form onSubmit={(e)=> this.props.submitListing(e,data) }>
                            <div className="mt-3">
                                <textarea className="listing-info-input" cols="40" rows="4" value={this.props.note} onChange={(e)=>this.props.inputHandler("note",e)} placeholder="write additional information about your listing...(optional)"/>
                            </div>
                            {/*<button className="btn btn-success">Complete Listing</button>*/}
                            <div className="mt-4 p-2">
                                {/*<h6>Listing Overview</h6>*/}
                                <div className="mt-2 row" style={{width:"100%"}}>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-type-header">Type: </b>
                                        <span className="lis-info">
                                      {this.props.list_type}
                                    </span>
                                    </div>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Pet: </b>
                                        <span className="lis-info">
                                      {petToList[0].name}
                                    </span>
                                    </div>

                                    <div className="col-sm-12 d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Location: </b>
                                        <small className="text-danger lis-info"><b>required</b>
                                            <OverlayTrigger
                                                placement={'right'}
                                                overlay={
                                                    <Tooltip id={`tooltip-top`}>
                                                        Location is automatically determined by the application in order to mitigate fake listings.
                                                    </Tooltip>
                                                }
                                            >
                                                <img src={'/img/info.svg'} alt="info" width="15" height="15" className="ml-2"/>
                                            </OverlayTrigger>{' '}
                                        </small>
                                    </div>

                                    {this.props.note?
                                        <div className="text-center d-flex justify-content-center col-sm-12 p-1">
                                            <b className="listing-note-header mr-1">Note: </b>
                                            {this.props.note?<div className="listing-note-display"><span className="">{this.props.note}</span></div>:''}
                                        </div> : ''}
                                </div>

                                {this.props.error? <div><b style={{color:"#db9432", fontSize:"17px", fontFamily:"Cambria" }}>You dont have enough <b style={{cursor:"pointer",color:"darkslategrey", textDecoration:"underline"}} onClick={this.props.showPayment}>Pawrry coins</b></b></div> : ''}
                                {/*buyCoins*/}
                                <button onClick={this.props.startOver} className="mt-3 mr-2 listing-restart">start over <img src={'/img/restart.svg'} alt="coin" width="22" height="22"/></button>
                                <button className="submit-listing">submit | <img src={'/img/pet-coin.jpg'} alt="coin" width="22" height="22"/> 250 </button>
                            </div>
                        </form>
                        </div>
                    </div>
                }else

                    //submit Mating listing
                if (this.props.list_type === "Mating" && this.props.petId !== ''){
                    // console.log(this.props.mateBreed);

                    const data = {type:this.props.list_type,petId:this.props.petId,note:this.props.note,mateBreed:this.props.foundBreed[0],coins:this.props.user.coins};

                    const petToList = this.props.pets.filter((pet)=>{
                        return this.props.petId === pet.id;
                    });

                    // console.log(petToList);

                    let options;//breeds
                    if(Object.keys(this.props.breeds).length){
                       // console.log("yes")
                        options = this.props.breeds.map((breed,index)=>{
                            return  <option key={index} value={breed.name}>{breed.name}</option>
                        })
                    }

                    listingPhase = <div className="text-center">
                        <div className="mt-3">
                        <span className="mating-instruction">Input a breed you're looking to mate <b>{petToList[0].name}</b> with</span>
                        <form onSubmit={(e)=>{this.props.submitListing(e,data)}}>
                            <div>
                                <input list="breeds" autoFocus={true} className="mate-breed-input" required={true} value={this.props.mateBreed} onChange={(e)=>this.props.inputHandler("mateBreed",e,this.props.breeds)}/>
                                <datalist id='breeds'>{options}</datalist>
                                {/*<select className="m-3" onChange={(e)=>this.props.inputHandler("mateBreed",e,this.props.breeds)}>
                                       {options}
                                    </select>*/}
                            </div>
                            <div className="mt-3">
                                <textarea className="listing-info-input" cols="40" rows="4" value={this.props.note} onChange={(e)=>this.props.inputHandler("note",e)} placeholder="write additional information about your listing...(optional)"/>
                            </div>
                            {/*<button className="btn btn-success">Complete Listing</button>*/}
                            <div className="mt-4 p-2">
                                {/*<h6>Listing Overview</h6>*/}
                                <div className="mt-2 row" style={{width:"100%"}}>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-type-header">Type: </b>
                                        <span className="lis-info">
                                      {this.props.list_type}
                                    </span>
                                    </div>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Pet: </b>
                                        <span className="lis-info">
                                      {petToList[0].name}
                                    </span>
                                    </div>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-pet-header"> Mate breed: </b>
                                        {this.props.foundBreed.length === 0 ? <span className="text-danger lis-info"> <small><b> required</b></small></span> : <span className="lis-info"> {this.props.mateBreed}</span> }
                                    </div><br/>

                                    <div className="col-sm-12 d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Location: </b>
                                        <small className="text-danger lis-info"><b>required</b>
                                            <OverlayTrigger
                                                placement={'right'}
                                                overlay={
                                                    <Tooltip id={`tooltip-top`}>
                                                        Location is automatically determined by the application in order to mitigate fake listings.
                                                    </Tooltip>
                                                }
                                            >
                                                <img src={'/img/info.svg'} alt="info" width="15" height="15" className="ml-2"/>
                                            </OverlayTrigger>{' '}
                                        </small>
                                    </div>

                                    {this.props.note?
                                        <div className="text-center d-flex justify-content-center col-sm-12 p-1">
                                            <b className="listing-note-header mr-1">Note: </b>
                                            {this.props.note?<div className="listing-note-display"><span className="">{this.props.note}</span></div>:''}
                                        </div> : ''}
                                </div>
                                {this.props.error? <div><b style={{color:"#db9432", fontSize:"17px", fontFamily:"Cambria" }}>You dont have enough Pawrry coins</b></div> : ''}
                                <button onClick={this.props.startOver} className="mt-3 mr-2 listing-restart">start over <img src={'/img/restart.svg'} alt="coin" width="22" height="22"/></button>
                                <button className="submit-listing">submit | <img src={'/img/pet-coin.jpg'} alt="coin" width="22" height="22"/> 250 </button>
                            </div>
                        </form>
                        </div>
                    </div>
                }

                //submit sale listing
                else{
                    const data = {type:this.props.list_type,petId:this.props.petId,note:this.props.note,price:this.props.price,coins:this.props.user.coins};
                    const petToList = this.props.pets.filter((pet)=>{
                        return this.props.petId === pet.id;
                    });
                    listingPhase = <div className="text-center">
                        <div className="mt-3">
                            <span className="sale-instruction">Place a price on <b>{petToList[0].name}</b></span>
                            <form onSubmit={(e)=>{this.props.submitListing(e,data)}}>
                            <label>
                                <input type="number" placeholder="price" autoFocus={true} className="sale-input" onChange={(e)=>this.props.inputHandler("price",e)} />
                            </label>
                            <div className="mt-3">
                                <textarea className="listing-info-input" cols="40" rows="4" value={this.props.note} onChange={(e)=>this.props.inputHandler("note",e)} placeholder="write additional information about your listing...(optional)"/>
                            </div>
                            {/*<button className="btn btn-success">Complete Listing</button>*/}
                            <div className="mt-4 p-2">
                                {/*<h6>Listing Overview</h6>{!this.props.price? <small className="text-danger"><b> required </b></small>*/}
                                <div className="mt-2 row" style={{width:"100%"}}>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-type-header">Type: </b>
                                        <span className="lis-info">
                                      {this.props.list_type}
                                    </span>
                                    </div>
                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Pet: </b>
                                        <span className="lis-info">
                                      {petToList[0].name}
                                    </span>
                                    </div>

                                    <div className="col-sm-12 text-center d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Price: </b>
                                        {
                                           !this.props.price? <small className="text-danger lis-info"><b> required </b></small>
                                                :
                                            <span className="lis-info">
                                                {this.props.price}
                                            </span>
                                        }

                                    </div>

                                    <div className="col-sm-12 d-flex justify-content-center p-1">
                                        <b className="listing-pet-header">Location: </b>
                                        <small className="text-danger lis-info"><b>required</b>
                                            <OverlayTrigger
                                                placement={'right'}
                                                overlay={
                                                    <Tooltip id={`tooltip-top`}>
                                                       Location is automatically determined by the application in order to mitigate fake listings.
                                                    </Tooltip>
                                                }
                                            >
                                                <img src={'/img/info.svg'} alt="info" width="15" height="15" className="ml-2"/>
                                            </OverlayTrigger>{' '}
                                        </small>
                                    </div>

                                    {this.props.note?
                                        <div>
                                            <b className="listing-note-header">Note:</b><br/>
                                            <div className="text-center d-flex justify-content-center">
                                                {this.props.note?<div className="listing-note-display"><span className="">{this.props.note}</span></div>:''}
                                            </div>
                                        </div>: ''
                                    }
                                </div>
                                {this.props.error? <div><b style={{color:"#db9432", fontSize:"17px", fontFamily:"Cambria" }}>You dont have enough Pawrry coins</b></div> : ''}
                                <button onClick={this.props.startOver} className="mt-3 mr-2 listing-restart">start over <img src={'/img/restart.svg'} alt="coin" width="22" height="22"/></button>
                                <button className="submit-listing">submit | <img src={'/img/pet-coin.jpg'} alt="coin" width="22" height="22"/> 250 </button>
                            </div>
                        </form>
                        </div>
                    </div>
                }
        }


        return (
            <div className="list-pet-modal" style={{height:"100%",width:"100%"}}>
                {this.props.fullLoader? <Process message="Listing..."/> : null}
                <div className="listPet-content">
                    <div className="listPet-header">
                        <span className="close" onClick={this.props.close}>&times;</span>
                        <h5>Listing</h5>
                    </div>
                    <div className="listPet-body">
                        {this.props.location_denied || this.props.listError?<b style={{fontFamily:"Cambria",color:"red"}}>Listing couldn't be completed</b>:null}
                        {listingPhase}
                    </div>
                </div>
            </div>
        );
    }
}

const MapState = (state) =>{
    return {
        pets : state.Listing.pets,
        breeds : state.Listing.breeds,
        phase : state.Listing.phase,
        loader : state.Listing.listingLoader,
        list_type: state.Listing.listing_type,
        petId : state.Listing.petId,
        note : state.Listing.note,
        mateBreed: state.Listing.mateBreed,
        location_denied: state.Listing.location_denied,
        foundBreed: state.Listing.foundBreed,
        price : state.Listing.price,
        user : state.User.user_info,
        error : state.Listing.listing_error,
        buyCoins : state.Listing.paymentOptions,
        listError : state.Listing.listError,
        fullLoader:state.Layout.fullLoader,
    }
};

const mapDispatch = (dispatch) =>{
    return {
        nextPhase : (currentPhase,answer)=>{dispatch(nextPhase(currentPhase,answer))},
        inputHandler : (note,e,breeds)=>{dispatch(additionalInfo(note,e.target.value,breeds))},
        startOver : ()=>{dispatch(startOver())},
        showPayment : ()=>{dispatch(showPayment())} ,
        submitListing : (e,data)=>{e.preventDefault();
                    dispatch(getUserLocation(data)
        )}//submitListing
    }
};

export default connect(MapState,mapDispatch)(ListPetModal);
