
const initialState = {
    toggleRequest : false,
    toggledItem : '',
    showRequestOptions : false,
    aboutPet: false,
    cancelAdm : false,
    showAdmPets : false,
    VisitorPic : false,
    fullPetPic : false,
};

const VisitorReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case 'TOGGLE_REQUEST':
            return{
                ...state,
                toggleRequest : !state.toggleRequest,
                toggledItem : actions.value
            };

        case 'TOGGLE_REQUEST_OPTIONS':
                return{
                    ...state,
                    showRequestOptions: !state.showRequestOptions
                };
        case 'LOAD_VISITOR_PIC':
                return{
                    ...state,
                    VisitorPic: !state.VisitorPic
                };
        case 'FULL_PET_PIC':
                return{
                    ...state,
                    fullPetPic: !state.fullPetPic
                };
        case 'FRIENDSHIP_ESTABLISHED':
            return{
                ...state,
            };
        case "ABOUT_VISITOR_PET" :
            return{
                ...state,
                aboutPet: !state.aboutPet
            };
        // case 'TOGGLE_ADMIRE':
        //     return{
        //         ...state,
        //         cancelAdm : !state.cancelAdm
        //     };
        case 'TOGGLE_ADMIRED_PETS_VISITOR':
            return{
                ...state,
                showAdmPets : !state.showAdmPets
            };


        default: return state;
    }
};

export default VisitorReducer;
