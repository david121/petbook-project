import axios from 'axios';
import {fetchPet, fetchVisitingProfile, fetchVisitPet} from './../../USER PROFILE/STORE/Actions'
import {Notifications, receivedRequest, sentRequests} from "../../LAYOUT/STORE/Actions";
import {petAdmirers} from "../../PET/Actions";
export const friendsRequest = (recipientId,id,page) =>{
    // console.log(id)
    return dispatch => {
        dispatch({type:"SENDING_REQUEST"});
        const data = {
            user_id : recipientId,
        };
        axios.post('/api/friends/request',data,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({type:"SENDING_REQUEST"});
                    switch (page) {
                        case "VISITOR": dispatch(fetchVisitingProfile(id));break;
                        case "USER_PET": dispatch(petAdmirers(id));break;
                        case "VISITOR_PET": dispatch(fetchVisitPet(id));break;
                    }
                    dispatch({
                        type : "FRIENDS_REQUEST_SENT",
                        value : recipientId
                    })
                }
            })
            .catch(()=>{
                dispatch({type:"SENDING_REQUEST"});
                //console.log(error)
            })
    }
};

/**
 * @params friendshipId :::: for friendship action.
 *@params userId :::: Id of the profile visited for page refresh
 * */
export const cancelRequest = (friendshipId,id,page) =>{
    return dispatch => {
        axios.post('/api/friends/cancelRequest',{friendshipId : friendshipId},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            },

        })
            .then(response=>{
                if (response.data.success){
                    switch (page) {
                        case "USER_PET":dispatch(petAdmirers(id));break;
                        case "VISITOR":dispatch(fetchVisitingProfile(id));break;
                        case "NOTIFY":dispatch(Notifications());break;
                        case "NOTIFICATIONS":dispatch(sentRequests());break;
                    }
                    // userId ? dispatch(fetchVisitingProfile(userId)) : dispatch(receivedRequest());
                    dispatch({
                        type : "FRIENDSHIP_CANCELED",
                        value: friendshipId
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};


export const requestOptions = () =>{
    return dispatch =>{
        dispatch({
            type: "TOGGLE_REQUEST_OPTIONS"
        })
    }
};


export const toggleRequest = (id) =>{
    // console.log("hello")
  return dispatch =>{
      dispatch({
          type: "TOGGLE_REQUEST",
          value : id
      })
  }
};
