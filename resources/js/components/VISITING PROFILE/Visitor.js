import React, {Component} from 'react';
import NavComp from "../LAYOUT/Nav";
import {
    clearPetsPanel,
    fetchPets,
    fetchVisitingProfile,
    fetchVisitorFriends,
    navigateComponent
} from "../USER PROFILE/STORE/Actions";
import {connect} from "react-redux";
import {NavLink, withRouter} from "react-router-dom";
import '../USER PROFILE/CSS/Profile.css'
import './CSS/ProfileVisitor.css'
import {
    fetchVisitorPosts,
    likePost,
    postComment,
    toggleComments,
    writeComment
} from "../POSTS/STORE/Actions";
import VisitorPosts from "../POSTS/VisitorPosts";
import {cancelRequest, friendsRequest, requestOptions, toggleRequest} from "./STORE/Actions";
import {acceptRequest} from "../LAYOUT/STORE/Actions";
import {showAdmiredPets, showFriends, toggleFriendship} from "../USER_FRIENDS/STORE/Actions";
import Friends from "../USER_FRIENDS/Friends";
import LikedPets from "../USER_FRIENDS/LikedPets";
import {messageHandler, messageModal, OpenChat, sendVisitMessage} from "../MESSAGING/STORE/ChatActions.Js";
import {Nav} from "react-bootstrap";
import ScrollContainer from "react-indiana-drag-scroll";
import EditPet from "../PET/EditPet";
import TransferPet from "../PET/TransferPet";
import Loader from "react-loader-spinner";

class Visitor extends Component {
    componentDidMount() {
        if (!localStorage.getItem('token')){
            this.props.history.push('/')
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Pet Owner";
            let id = this.props.match.params.id;
            this.props.profile(id);
            this.props.pets(id);
            this.props.posts(id);
            this.props.navigateComponent('timeline');
    }
    componentWillUnmount() {
        if(this.props.friendsShow){
            this.props.showFriends()
        }if(this.props.showAdmPets){
            this.props.showAdmiredPets()
        }if(this.props._messageModal){
            this.props.messageModal('')
        }
        this.props.clearPets();
        this.props.navigateComponent('timeline');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {


        //console.log("SENDING",this.props.sending);


        const currentId = this.props.match.params.id ? parseInt(this.props.match.params.id) : null;
        const prevId = prevProps.match.params.id ? parseInt(prevProps.match.params.id) : null;


        if(currentId && prevId && currentId !== prevId){
            this.props.showFriends(true);
            this.props.profile(currentId);
            this.props.pets(currentId);
            this.props.posts(currentId);
        }
    }

    render() {
        let data =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        let image = '';
        let fullImage = null;
        let pets = "";
        let posts;

        //user photo
        if(Object.keys(this.props.user_profile).length) {
            this.props.profilePhoto.map((item,index)=>{
                if (this.props.profilePhoto[index] === 'None') {
                    image = <img src={"/img/avatar.png"} alt="user" width="150" height="130" className="rounded-circle"/>
                } else {
                    image = <img src={item} alt="user" width="150" height="150" className="rounded-circle" onClick={this.props.fullPic} style={{cursor:"pointer"}}/>;
                    fullImage = this.props.VisitorPic ?
                        <div className="profile-pic-modal text-center">
                            <span className="profile-pic-modal-close" onClick={this.props.fullPic}>&times;</span>
                            <img className="profile-pic-modal-content" src={item} alt="profile picture"/>
                        </div> : '';
                }
            });
            pets = this.props.userPets.map((item, index)=>{
                // console.log(item)
                return <div key={index} className="item mt-1">
                        <div className="pet-image mb-3">
                            <span>
                             <NavLink to={`/pet-view/${item.id}`} >
                                 {
                                     item.profile_photo === null ?
                                         <img src={"/img/rodent.svg"} alt="pet" className="rounded-circle pet-image" width="150" height="130"/>:
                                         <img key={index} src={`/storage/pets/${item.id}/${item.profile_photo}`} alt={"pet photo"} className="rounded-circle pet-image" width="150" height="130"/>
                                 }
                             </NavLink>
                            </span>
                        </div>
                        <span className="pet-name">
                            {item.name}
                        </span>
                </div>
            });


            let userPetsId;
            if (Object.keys(this.props.userPets).length){
                userPetsId = this.props.userPets.map(item=>{
                    return item.id
                });
            }

            let friendButton = this.props.sendingReq?
                <button className="friend-request-btn" disabled={true}>sending</button>
                :
                <button className="friend-request-btn" onClick={()=>this.props.friendsRequest(this.props.user_profile.id,this.props.user_profile.id,'VISITOR')}>
                Add Friend
            </button>;
            if (this.props.user_profile.is_friend !== null){

                if (this.props.user_profile.is_friend.status === 1){
                    friendButton = <span style={{cursor:"pointer"}} >
                        <img src={"/img/friends.svg"} alt="friends" width="30" height="30" onClick={this.props.toggleFriendship}/>
                        {this.props.cancelBtn?
                            <button onClick={()=>this.props.cancelRequest(this.props.user_profile.is_friend.id,this.props.user_profile.id)}
                                    className="cancel-request ml-3">unfriend</button>
                            : ''
                        }
                    </span>;
                }
                else if (this.props.user_profile.is_friend.status === 0){
                    if (this.props.user_profile.is_friend.sender_user_id === this.props.activeUser.id){
                        friendButton = <div>
                                            <img src={'/img/hourglass.svg'} alt="pending" className="friend-request-btn" width={'23'} height={'23'} onMouseEnter={this.props.toggleRequest}/>
                                    </div>;

                        if (this.props.requestToggled){
                            friendButton = <button className="friend-request-btn"
                                                   onMouseLeave={this.props.toggleRequest}
                                                   onClick={()=>this.props.cancelRequest(this.props.user_profile.is_friend.id,this.props.user_profile.id)}
                            >
                                cancel request
                            </button>;
                        }
                    }else if (this.props.user_profile.is_friend.receiver_user_id === this.props.activeUser.id){
                        friendButton = <div>
                                         <div className="btn-group">
                                             <button className="friend-request-btn"
                                            onClick={()=>this.props.acceptRequest(this.props.user_profile.is_friend.id,this.props.user_profile.id)}>
                                                accept request
                                            </button>
                                            <button onClick={this.props.requestOptions}>
                                                <img src={'/img/right.svg'} alt={'option'} width={10} height={10} className="fas fa-caret-down"/>
                                            </button>
                                         </div>

                                         {this.props.showRequestOptions?
                                             <button className="cancel-request"
                                                   onClick={()=>this.props.cancelRequest(this.props.user_profile.is_friend.id,this.props.user_profile.id)}>
                                            reject request</button>
                                            :''
                                         }
                                    </div>
                    }
                }

                }
            let numOfFriends = this.props.friends.length>0 ? this.props.friends.length : 0;

            //Loop and Load Posts
            if (this.props.userPosts === 'None'){
                posts = <div className="card" id='post'>
                    <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>{this.props.user_profile.firstname} has no posts !</h2>
                    </span>
                        <span className={"post-photo mt-4"}>
                        <img src={"/img/sad.jpg"} className={"post-photo"} width={"350"}/>
                    </span>
                    </div>
                </div>
            }else {
                posts = this.props.userPosts.map((post,index)=>{
                    return <VisitorPosts
                        key = {index}
                        user = {"petOwner"}
                        postId = {post.id}
                        commentLike = "VISITOR"
                        post_owner = {post.owner_user_id}
                        post_owner_username = {post.postOwner}
                        userPhoto = {this.props.profilePhoto[0]}
                        numOfPics = {post.post_photo.length}
                        postPic = {post.post_photo}
                        petId = {post.pet_id}
                        userId = {post.user_id}
                        petPhoto = {post.pet.profile_photo}
                        pet = {post.pet.username}
                        username = {this.props.user_profile.username}
                        time = {post.time}
                        caption = {post.caption}
                        totalLikes = {post.likes.length}
                        totalComments = {post.comments}
                        like = {this.props.likePost} //Like a Post
                        liked= {post.is_liked_count}
                        clicked ={this.props.showComments}
                        clickedPost ={this.props.clickedPost}
                        comments = {()=>this.props.comments(post.id,this.props.showComments)}
                        comment = {this.props.writtenComment}
                        commentHandler = {this.props.writeComment}
                        submitComment = {this.props.submitComment}
                    />
                })
            }

            //page rendering
            let activeComp;
            let tmlStyle;
            let admStyle;
            let brdStyle;
            let profStyle;
            if(this.props.activePage === 'timeline'){
                tmlStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div>
                    {
                        posts
                    }
                </div>
            }else if (this.props.activePage === 'friends'){
                admStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="admirer-component">
                    <Friends type="VisitorPage" profile={this.props.user_profile.id} friends={this.props.friends} />
                </div>
            }else if (this.props.activePage === 'likedPets'){
                brdStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="breed-component">
                    <LikedPets profile="Visitor" pets={this.props.user_profile.admired_pets} profileId={this.props.user_profile.id} />
                </div>;
            }else if (this.props.activePage === 'profile'){
            profStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="breed-component">
                <div className="col-sm-12 col-lg-4 mb-3 profile-sm-component">

                    <div className="text-center col-sm-12 bg-light">
                        <b className="text-center profile-intro">Profile Intro</b>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                        {
                            this.props.user_profile.city === null ? "No city indicated" :
                                this.props.user_profile.location === 1 ?
                                    <div>
                                        <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                        <span className="ml-3 user-location"> {this.props.user_profile.city + " "+ this.props.user_profile.country}</span>
                                    </div>
                                    : <div>
                                        <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                        <span className="ml-3 user-location text-center"> void </span>
                                    </div>
                        }
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                        {
                            this.props.user_profile.bio === null || this.props.user_profile.bio === ''
                                ?
                                <div className="text-left">
                                    <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-3'}/>
                                    <b style={{fontFamily:"Cambria", color:"darkslategrey"}}>N/A</b>
                                </div>
                                :
                                <div className="pet-bio-wrapper p-2">
                                    <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                                    <span className="ml-4 user-bio">{this.props.user_profile.bio}</span>
                                </div>
                        }
                    </div>
                </div>
            </div>;
        }

            const sender = {
                sender_name : this.props.activeUser.username,
                sender_photo : this.props.activeUser.photo_path,
                id : this.props.activeUser.id
            };

            data =  (
                <div>
                    <NavComp/>

                    <div className="container mt-1">
                     <div className="row">


                      <div className="col-sm-12 mb-1">
                          <div className="user-page-info">
                            <button onClick={()=>this.props.history.goBack()} className="btn btn-light ml-1 mt-1 text-info">&#8592;</button>

                                <div className="row">
                                {/*User photo*/}
                                <div className="user-image-holder text-center col-sm-12 col-lg-6 p-3">
                                    {fullImage}
                                    {image}
                                </div>

                                <div className="col-sm-12 col-lg-6">
                                    <div className="">
                                        <div className="mt-lg-5 mt-sm-1 user-name-holder">
                                            <span className="pet-name-profile mr-1">
                                                {this.props.user_profile.firstname + " " + this.props.user_profile.lastname }
                                                {
                                                    this.props.user_profile.verified === 1 ?
                                                        <img src={'/img/verified.svg'} alt="mark" width={20} height={20} className="ml-3"/> : null
                                                }
                                            </span><br/>
                                            <span className="pet-name-profile user-username-profile">( {this.props.user_profile.username} )</span>
                                        </div>
                                        <div className="mt-4 text-center" style={{height:"100%"}}>
                                            <div className="row">
                                                <div className="col-sm-12 col-lg-6">
                                                    {friendButton}
                                                </div>
                                                <div className="col-sm-12 col-lg-6 text-right">
                                                    <span style={{cursor:'pointer'}} onClick={this.props.messageModal} className="p-4">
                                                        <img src={'/img/message.svg'} alt={'chats'} width={'35'} height={'35'}/>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>


                         { this.props._messageModal?
                             <div className="send-message-modal">
                                 <div className="message-content-wrapper">
                                     <div className="send-message-header">
                                         <span className="close" onClick={()=>this.props.messageModal('')}>&times;</span>
                                         <h5>Message</h5>
                                     </div>
                                     <div className="send-message-body">
                                         <form>
                                             <textarea rows="5" onChange={this.props.messageHandler} value={this.props.composedMessage} className="message-input-field" style={{width:'100%',resize:'none',border:'1px solid darkslategrey'}} placeholder="write a message ..."/>
                                            <div className="text-right">
                                                {/*this.props.sending ? <button disabled={true} style={{backgroundColor:'darkslategrey', color:'white'}}>sending <span className="spinner-grow spinner-grow-sm"/> </button>
                                                    :
                                                    <button onClick={(e)=>this.props.sendVisitorMessage(e,this.props.user_profile.id,this.props.composedMessage)} style={{backgroundColor:'darkslategrey', color:'white'}}>send message</button>
                                                */
                                                }
                                                <button onClick={(e)=>this.props.sendVisitorMessage(e,this.props.user_profile.id,sender,this.props.composedMessage)} style={{backgroundColor:'darkslategrey', color:'white'}}>send message</button>
                                            </div>
                                         </form>
                                     </div>
                                 </div>
                             </div> : ''
                         }


                         {/*Pets*/}
                         <div className="col-sm-12">
                             <div className="pets pet-panel">
                                 <ScrollContainer className="scroll-container ml-3">
                                     {pets}
                                 </ScrollContainer>
                             </div>
                         </div>

                     </div>


                         <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                             <Nav.Item>
                                 <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                             </Nav.Item>
                             <Nav.Item>
                                 <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("friends")} className='tab-text' style={admStyle}>Friends ({numOfFriends})</Nav.Link>
                             </Nav.Item>
                             <Nav.Item>
                                 <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("likedPets")} className='tab-text' style={brdStyle}>Admired Pets ({this.props.user_profile.admired_pets !== null ? this.props.user_profile.admired_pets : 0})</Nav.Link>
                             </Nav.Item>
                             <Nav.Item className="sm-profile-tab">
                                 <Nav.Link eventKey="link-3" onClick={()=>this.props.navigateComponent("profile")} className='tab-text' style={profStyle}>Profile</Nav.Link>
                             </Nav.Item>
                         </Nav>

                         <div className="mt-3">
                             <div className="row">

                                 <div className="col-sm-12 col-lg-4 mb-3 lg-profile-intro">

                                     <div className="text-center col-sm-12 bg-light">
                                         <b className="text-center profile-intro">Profile Intro</b>
                                     </div>

                                     <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                                         {
                                             this.props.user_profile.city === null ? "No city indicated" :
                                                 this.props.user_profile.location === 1 ?
                                             <div>
                                                 <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                                 <span className="ml-3 user-location"> {this.props.user_profile.city + " "+ this.props.user_profile.country}</span>
                                             </div>
                                                 : <div>
                                                         <span><img src={"/img/address1.svg"} alt="city" height={30} width={30}/></span>
                                                         <span className="ml-3 user-location text-center"> void </span>
                                                     </div>
                                         }
                                     </div>

                                     <div className="mt-2 user-bio-wrapper col-sm-12 bg-light">
                                         {
                                             this.props.user_profile.bio === null || this.props.user_profile.bio === ''
                                             ?
                                         <div className="text-left">
                                             <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-3'}/>
                                             <b style={{fontFamily:"Cambria", color:"darkslategrey"}}>N/A</b>
                                         </div>
                                             :
                                         <div className="pet-bio-wrapper p-2">
                                             <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                                             <span className="ml-4 user-bio">{this.props.user_profile.bio}</span>
                                         </div>
                                         }
                                     </div>
                                 </div>

                                 <div className="col-sm-12 col-lg-8">
                                     {activeComp}
                                 </div>

                             </div>
                         </div>

                    </div>
                    </div>
            );

        }

        return data
    }
}
const MapState = (state) =>{
    return{
        activePage : state.User.activeComponent,
        user_profile : state.User.visitingProfile,
        friends : state.User.profileFriends,
        activeUser : state.User.user_info,
        followed: state.User.followed,
        profilePhoto :state.User.visitingProfilePhoto,
        userPets : state.User.user_pets,
        userPetsPic : state.User.user_pet_photos,
        userPosts : state.Posts.visitorPosts,
        //Comments
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,
        //
        requestToggled :state.Visitor.toggleRequest,
        showRequestOptions : state.Visitor.showRequestOptions,
        VisitorPic : state.Visitor.VisitorPic,
        //
        friendsShow : state.VisitorFriends.showFriends,
        showAdmPets : state.Visitor.showAdmPets,
        cancelBtn : state.VisitorFriends.cancelBtn,
        //
        _messageModal: state.Chat.messageModal,
        composedMessage:state.Chat.visitorMessage,
        sending : state.Chat.sendingMessage,
        sendingReq : state.VisitorFriends.sendingReq,
    }
};
const MapDispatch = (dispatch) => {

    return{
        profile : (usn) =>{dispatch(fetchVisitingProfile(usn))},
        fullPic: () =>{dispatch({type:"LOAD_VISITOR_PIC"})},
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},
        pets : (user) =>{dispatch(fetchPets(user))},
        clearPets : () =>{dispatch(clearPetsPanel())},
        posts : (user)=>{dispatch(fetchVisitorPosts(user))},
        comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        writeComment : (e) => {dispatch(writeComment(e.target.value))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId))},
        likePost : (postId,user,pet) =>{dispatch(likePost(postId,user,pet,"USER_VISIT"))},

        friendsRequest : (id,usn) => {dispatch(friendsRequest(id,usn,"VISITOR"))},
        toggleRequest : () => {dispatch(toggleRequest())},
        cancelRequest : (id,userId) => {dispatch(cancelRequest(id,userId,"VISITOR"))},
        requestOptions : () => {dispatch(requestOptions())},
        acceptRequest : (id,userId) => {dispatch(acceptRequest(id,userId))},

        // fetchVisitorFriends : (usn) =>{dispatch(fetchVisitorFriends(usn))},
        toggleFriendship : ()=>{dispatch(toggleFriendship())},
        showAdmiredPets : ()=> {dispatch(showAdmiredPets(false,true))},
        showFriends : (closeModal) =>{dispatch(showFriends(false, closeModal))},
        messageModal : () =>  {dispatch(messageModal())},
        messageHandler : (e) =>  {dispatch(messageHandler(e.target.value))},
        sendVisitorMessage : (e,id,sender,msg) =>  {e.preventDefault(); dispatch(sendVisitMessage(id,sender,msg))},
    }
};
export default connect(MapState,MapDispatch)(withRouter(Visitor));
