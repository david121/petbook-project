import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    fetchVisitPet,
    fetchVisitPetPosts,
    fetchHobbies,
    toggleAboutPet,
    navigateComponent,
} from "./../USER PROFILE/STORE/Actions";
import NavComp from "../LAYOUT/Nav";
import './../USER PROFILE/CSS/PetProfile.css'
import {admireOptions, likePost, postComment, toggleComments, writeComment,} from "../POSTS/STORE/Actions";
import AboutPet from "./../PET/AboutPet";
import VisitorPosts from "../POSTS/VisitorPosts";
import {admire, cancelAdmiration} from "../USER_FRIENDS/STORE/Actions";
import Admirers from "../PET/Admirers";
import Loader from 'react-loader-spinner'
import {Nav} from "react-bootstrap";
import ShowPosts from "../POSTS/ShowPosts";
import {NavLink} from "react-router-dom";
import moment from "moment";

class VisitingPetProfile extends Component {
    componentDidMount(){
        //console.log(this.props);
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Pet";
        let id = this.props.match.params.petId;
        this.props.fetchPet(id);
        this.props.pet_post(id,this.props.petOwner.id);
        this.props.fetchHobbies(id);
        this.props.navigateComponent('timeline')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.pet)
    }

    componentWillUnmount() {
        this.props.navigateComponent('timeline')
    }

    render() {
        let pet =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
             <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        let bio = "loading bio";
        let hobbies ="listing ";
        let posts;

        let about = this.props.toggleAbout? <AboutPet breed={this.props.pet.breed.name} about={this.props.pet.breed.description} petName={this.props.pet.name}/> : '';

        if(Object.keys(this.props.pet).length){
            //bio rendering
            // bio = this.props.pet.bio === '' || this.props.pet.bio === null ?
            //     <span>
            //         No bio written
            //     </span>
            //     :<span className="pet-bio">{this.props.pet.bio}</span>;
            //listing hobbies
            let listHobbies = this.props.hobbies.map((hobby,index)=>{
                return <li key={index} className={"pet-hobby"}>{hobby.hobby}</li>
            });
            let petImage = this.props.pet.profile_photo === null ? <img src={"/img/rodent.svg"} alt={"pet"} className="image rounded-circle m-3" width="150" height="130" />
                 : <img style={{pointer:"cursor"}} src={`/storage/pets/${this.props.pet.id}/${this.props.pet.profile_photo}`} onClick={this.props.fullPetPic} alt={"pet"} className="rounded-circle m-3" width="150" height="130"/>;

            let fullImage = this.props.fullPic ?
                <div className="profile-pic-modal text-center">
                    <span className="profile-pic-modal-close" onClick={this.props.fullPetPic}>&times;</span>
                    <img className="profile-pic-modal-content" src={`/storage/pets/${this.props.pet.id}/${this.props.pet.profile_photo}`} alt="profile picture"/>
                </div> : '';

            let admireButton = this.props.admire_process?
                <img src={'/img/paw.svg'} className="likeIcon ml-5" alt="like" width="50" height="50" style={{opacity:"0.3"}}/>
                : <img src={'/img/paw.svg'} className="likeIcon ml-5" alt="like" width="50" height="50" onClick={()=>this.props.admire(this.props.pet.id)}/>;
            if(this.props.pet.admirers_count === 1){
                        admireButton= <div>
                            <img src={'/img/paws.svg'} className="likeIcon ml-5" alt="like" width="50" height="50" onClick={()=>this.props.unadmire(this.props.pet.id)}/>
                            {/*<div className="btn-group">
                                    <button>admired</button>
                                    <button onClick={this.props.toggleAdmire}><span className="fas fa-caret-down"/></button>
                                </div>
                                {this.props.cancelAdm ?
                                    <div><button className="unadmire" onClick={()=>this.props.unadmire(this.props.pet.id)}>unadmire</button></div>: ''
                                }*/}
                        </div>
            }

            //Loop and Load Posts
            if (this.props.petPosts === 'None'){
                posts = <div className="card" id='post'>
                    <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>{this.props.pet.name} has no posts !</h2>
                    </span>
                    <span className={"post-photo mt-4"}>
                        <img src={"/img/sad.jpg"} className={"post-photo"} width={"350"}/>
                    </span>
                    </div>
                </div>
            }else {
                posts = this.props.petPosts.map((post,index)=>{
                    return <VisitorPosts
                        key = {index}
                        user = {'pet'}
                        page = {"VISITOR_PET"}
                        commentLike = {"VISITOR_PET"}
                        post_owner = {post.owner_user_id}
                        post_owner_username = {post.postOwner}
                        username = {post.pet.username}
                        postId = {post.id}
                        userPhoto = {post.pet.profile_photo}
                        numOfPics = {post.post_photo.length}
                        postPic = {post.post_photo}
                        petId = {post.pet_id}
                        userId = {post.user_id}
                        petPhoto = {post.pet.profile_photo}
                        pet = {post.pet.pet_name}
                        time = {post.time}
                        caption = {post.caption}
                        totalLikes = {post.likes.length}
                        totalComments = {post.comments}
                        like = {()=>this.props.likePost(post.id,post.pet_id,post.user_id)} //Like a Post
                        liked= {post . is_liked_count}
                        clicked ={this.props.showComments}
                        clickedPost ={this.props.clickedPost}
                        comments = {()=>this.props.comments(post.id,this.props.showComments)}
                        submitComment = {this.props.submitComment}
                    />
                })
            }


            //page rendering
            let activeComp;
            let tmlStyle;
            let admStyle;
            let brdStyle;
            let profStyle;
            if(this.props.activePage === 'timeline'){
                tmlStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp =  <div>
                        <div className="">
                            {posts}
                        </div>
                    </div>
            }else if (this.props.activePage === 'admirers'){
                admStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="admirer-component">
                    <Admirers petId={this.props.pet.id} petName={this.props.pet.name}/>
                </div>
            }else if (this.props.activePage === 'breed'){
                brdStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 breed-component">
                    <AboutPet petId={this.props.pet.id} breed={this.props.pet.breed.name} about={this.props.pet.breed.description} petName={this.props.pet.name} breed_id={this.props.pet.breed_id}/>
                </div>;
            }else if (this.props.activePage === 'profile'){
                profStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                let breedIconSm = parseInt(this.props.pet.breed.pet_type) === 2 ? <img src={'/img/dog-icon.svg'} alt="gender" width="25" height="25"/>:<img src={'/img/cat-gender.svg'} alt="gender" width="25" height="25"/>;
                let joined = moment.duration(moment().diff(this.props.pet.created_at))._data;
                let joinYears = joined.years === 0 ? '' : joined.years === 1 ? '1 year' : joined.years+' years';
                let joinMonths = joined.months === 0 ? '' : joined.months === 1 ? '1 month' : joined.months+' months';
                let joinDays = joined.days === 0 ? '' : joined.days === 1 ? '1 day' : joined.days+' days ';

                let DOBsm = this.props.pet.date_of_birth;

                let AgeSm = moment.duration(moment().diff(DOBsm))._data;
                let yearsSm = AgeSm.years === 0 ? '' : AgeSm.years === 1 ? '1 year' : AgeSm.years+' years';
                let monthsSm = AgeSm.months === 0 ? '' : AgeSm.months === 1 ? '1 month' : AgeSm.months+' months';
                let daysSm = AgeSm.days === 0 ? '' : AgeSm.days === 1 ? '1 day' : AgeSm.days+' days ';

                activeComp = <div className="profile-sm-component ">
                    <div className="text-center col-sm-12 bg-light p-2">
                        <b className="text-center profile-intro">Profile</b>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12">
                        <span><img src={'/img/pet-owner.svg'} alt="breed" width="25" height="25"/></span>
                        <NavLink className="ml-4 user-name" to={`/pet-owner/${this.props.pet.user.id}`}>
                            {this.props.pet.user.username}
                            {
                                this.props.pet.user.photo_path === null ? <img src={'/img/avatar.png'} alt="user" width={'15'} height={'15'} className="rounded-circle"/>
                                    :
                                <img src={`/storage/users/${this.props.pet.user.id}/${this.props.pet.user.photo_path}`} alt="user" width={'20'} height={'20'} className="ml-2 rounded-circle"/>
                            }
                        </NavLink>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/timetable.svg'} alt="breed" width="25" height="25"/></span>  <span className='ml-4 pet-profile-time'>{joinYears} {joinMonths} {joinDays}  on Pawrry</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span>{breedIconSm}</span> <span className='ml-4 pet-profile-breed'>{this.props.pet.breed.name}</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/gender.svg'} alt="breed" width="25" height="25"/></span><span className='ml-4 pet-profile-gender'>{this.props.pet.gender ===  0 ? 'Male' : 'Female'}</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/cake.svg'} alt="cake" width="25" height="25"/></span> <span className='ml-4 pet-profile-bd'> {DOBsm?moment(DOBsm).format("MMM Do"):null} ({yearsSm} {monthsSm} {daysSm} {DOBsm?"old":" Not Filled "})</span>
                    </div>


                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span  className='mr-4'><img src={'/img/petbio.svg'} alt="breed" width="25" height="25" /></span>
                        {this.props.pet.bio === null || this.props.pet.bio === '' ? <b style={{fontSize:"13px",color:"grey", fontFamily:"Cambria"}}>N/A</b> : <p>{this.props.pet.bio}</p>}
                    </div>
                </div>;
            }

            let joined = moment.duration(moment().diff(this.props.pet.created_at))._data;
            let joinYears = joined.years === 0 ? '' : joined.years === 1 ? '1 year' : joined.years+' years';
            let joinMonths = joined.months === 0 ? '' : joined.months === 1 ? '1 month' : joined.months+' months';
            let joinDays = joined.days === 0 ? '' : joined.days === 1 ? '1 day' : joined.days+' days ';

            let DOB = this.props.pet.date_of_birth;
            let Age = moment.duration(moment().diff(DOB))._data;
            let years = Age.years === 0 ? '' : Age.years === 1 ? '1 year' : Age.years+' years';
            let months = Age.months === 0 ? '' : Age.months === 1 ? '1 month' : Age.months+' months';
            let days = Age.days === 0 ? '' : Age.days === 1 ? '1 day' : Age.days+' days ';

            let breedIcon = parseInt(this.props.pet.breed.pet_type) === 2 ? <img src={'/img/dog-icon.svg'} alt="gender" width="25" height="25"/>:<img src={'/img/cat-gender.svg'} alt="gender" width="25" height="25"/>;


            pet = <div>

                <NavComp/>
                <div className="container">

                    <div className="user-page-info p-4">
                        <button onClick={()=>this.props.history.goBack()} className="btn btn-light ml-2 mt-3 text-info">&#8592; back </button>

                        <div className="row">
                            {/*User photo  */}
                            <div className="user-image-holder text-center col-sm-12 col-lg-6">
                                {petImage}
                                {fullImage}
                            </div>
                            {/*Pet relationships*/}
                            <div className="col-sm-12 col-lg-6">
                                <div className="persona mt-5">{/* d-flex justify-content-start align-items-end*/}
                                    <div className="mb-3">
                                        <span className="pet-name-profile mr-1">{this.props.pet.name}</span>
                                        <span className="pet-name-profile pet-username-profile">( {this.props.pet.username} )</span>
                                    </div>
                                    {admireButton}
                                </div>
                            </div>

                        </div>
                    </div>

                    <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                        <Nav.Item>
                            <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("admirers")} className='tab-text' style={admStyle}>Admirers ({this.props.pet.admirers})</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("breed")} className='tab-text' style={brdStyle}>{this.props.pet.breed.name}s</Nav.Link>
                        </Nav.Item>
                        <Nav.Item className="sm-profile-tab">
                            <Nav.Link eventKey="link-3" onClick={()=>this.props.navigateComponent("profile")} className='tab-text' style={profStyle}>Profile</Nav.Link>
                        </Nav.Item>
                    </Nav>

                    <div className="mt-2">
                        <div className="row">

                            {/* About Pet*/}
                            <div className="col-sm-12 col-lg-4 mb-3 lg-profile-intro">

                                <div className="text-center col-sm-12 user-bio-wrapper">
                                    <b className="text-center profile-intro">Profile</b>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12">
                                    <span><img src={'/img/pet-owner.svg'} alt="breed" width="25" height="25"/></span>
                                    <NavLink className="ml-4 user-name" to={`/pet-owner/${this.props.pet.user.id}`}>
                                        {this.props.pet.user.username}
                                        {
                                            this.props.pet.user.photo_path === null ? <img src={'/img/avatar.png'} alt="user" width={'15'} height={'15'} className="rounded-circle"/>
                                            :
                                            <img src={`/storage/users/${this.props.pet.user.id}/${this.props.pet.user.photo_path}`} alt="user" width={'20'} height={'20'} className="ml-2 rounded-circle"/>
                                        }
                                    </NavLink>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 p-2">
                                    <span><img src={'/img/timetable.svg'} alt="time" width="25" height="25"/></span>  <span className='ml-4 pet-profile-time'>{joinYears} {joinMonths} {joinDays}  on Pawrry</span>
                                </div>


                                <div className="mt-2 user-bio-wrapper col-sm-12 p-2">
                                    <span>{breedIcon}</span> <span className='ml-4 pet-profile-breed'>{this.props.pet.breed.name}</span>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 p-2">
                                    <span><img src={'/img/gender.svg'} alt="breed" width="25" height="25"/></span><span className='ml-4 pet-profile-gender'>{this.props.pet.gender === 0 ? 'Male' : 'Female'}</span>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 p-2">
                                    <span><img src={'/img/cake.svg'} alt="cake" width="25" height="25"/></span> <span className='ml-4 pet-profile-bd'> {DOB?moment(DOB).format("MMM Do") : null} ({years} {months} {days} {DOB?"old ":"Not Filled"})</span>
                                </div>

                                <div className="lp-section-one mt-2 user-bio-wrapper p-2">
                                    <span  className='mr-4'><img src={'/img/petbio.svg'} alt="breed" width="25" height="25" /></span>
                                    {this.props.pet.bio === null || this.props.pet.bio === '' ? <b style={{fontSize:"13px",color:"grey", fontFamily:"Cambria"}}>N/A</b> : <p>{this.props.pet.bio}</p>}
                                </div>


                            </div>

                            {/* About Pet*/}

                            <div className="col-sm-12 col-lg-8">
                                {activeComp}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        }
        return pet
    }
}

const MapState = (state) =>{

    return {
        petOwner : state.User.visitingProfile,
        activePage : state.User.activeComponent,
        activeUser : state.User.user_info,
        pet : state.PetProfile.pet,
        profilePic : state.PetProfile.petPic,
        pet_hobby: state.PetProfile.hobby,
        hobbies : state.PetProfile.hobbies,
        admire_process : state.PetProfile.admire_process,
        //
        // cancelAdm : state.Visitor.cancelAdm,
        toggleAbout : state.Visitor.aboutPet,
        fullPic : state.Visitor.fullPetPic,
        //Fetch Posts
        petPosts : state.PetProfile.visitPetPosts,
        postTime : state.Posts.postTime,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,
    }

};

const MapDispatch=(dispatch)=>{
    return {
        fetchPet : (id)=>{dispatch(fetchVisitPet(id))},
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},
        fetchHobbies: (id)=>{dispatch(fetchHobbies(id))},
        aboutPet : ()=>{dispatch(toggleAboutPet(true))},
        fullPetPic : ()=>{dispatch({type:"FULL_PET_PIC"})},
        //Post actions
        pet_post : (id,petOwner)=> {dispatch(fetchVisitPetPosts(id,petOwner))},
        comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        likePost : (postId,pet,petOwner) =>{dispatch(likePost(postId,petOwner,pet,"PET_VISIT"))},
        writeComment : (e) => {dispatch(writeComment(e.target.value))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId))},
        //
        // toggleAdmire : () => {dispatch(admireOptions())},
        admire : (petId) =>{dispatch(admire(petId))},
        unadmire : (id) =>{ dispatch(cancelAdmiration(null,id,'pet'))}
    }
};

export default connect(MapState, MapDispatch)(VisitingPetProfile);
