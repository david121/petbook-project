import React, {Component} from 'react';

class SendPost extends Component {
    render() {
        return (
            <div>
                <form className="text-center ">
                    <div className="form-group">
                        <input type="file" onChange={this.props.fileHandler} className="form-control-file"/>
                    </div><br/>
                    <div className="form-group">
                        <textarea type="text" onChange={this.props.inputHandler} className="form-control-file"/>
                    </div>
                    <input type="submit" className="btn btn-block" value="Add Post" onClick={this.props.submit}/>
                </form>
            </div>
        );
    }
}

export default SendPost;