import axios from "axios";
import {axiosHeader} from "../../index";
import {Api_Process} from "../../LAYOUT/STORE/Actions";

export const generalError = () =>{
    return dispatch=>{dispatch({type:"SERVER_ERROR"})}
};
export const explore = () =>{
    return dispatch=>{dispatch({type:"EXPLORE"})}
};
export const makePostMobile = () =>{
    return dispatch=>{
        axios.post('/api/mobilePost',null,{headers:axiosHeader})
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type:"MOBILE_POST",
                        pets:response.data.pets
                    })
                }
            })
            .catch(err=>{

            });
    }
};

export const fetchAllPosts=()=>{
   return dispatch =>{
       dispatch(Api_Process());
        axios.get('/api/posts',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    dispatch({
                        type : "HOME_POSTS",
                        posts : response.data.posts,
                        postTime : response.data.postTime
                    })
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};

export const factOfTheDay = () =>{
    return dispatch =>{
         axios.get('/api/fact',{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        })
            .then(response=>{
                 if (response.data.success){
                    dispatch({
                        type : "DAY_FACT",
                        value : response.data.fact[0]
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const addPostBtnClick = () =>{
    return dispatch =>{
        dispatch({
            type : "OPEN_ADD_POST",
        })
    }
};

export const postingProfile = (profile) =>{
    return dispatch =>{
        dispatch({
            type : "POST_TO",
            value: profile
        })
    }
};

export const handleFile = (file) =>{
    return dispatch =>{
        dispatch({
            type : "POST_PHOTO",
            value: file
        })
    }
};


export const sendPost = (data) =>{
    return dispatch =>{
        dispatch({
            type : "POST",
        })
    }
};


