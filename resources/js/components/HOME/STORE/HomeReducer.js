
const initialState = {
    posts: [],
    postTime : [],
    explore: false,
    post : false,
    generalError: false,
    user_pets:[],
};

const Home = (state = initialState, actions) =>{
    switch (actions.type) {
        case 'HOME_POSTS':
            return{
                posts: actions.posts,
                postTime: actions.postTime
            };
        case 'GENERAL_ERROR':
            return{
                posts: actions.posts,
                postTime: actions.postTime
            };


        case 'EXPLORE':
            if (state.post){
                return {
                    ...state,
                    explore : !state.explore,
                    post : !state.post
                }
            }
            return{
                ...state,
                explore : !state.explore
            };

        case 'MOBILE_POST':
            if (state.explore){
                return {
                    ...state,
                    explore : !state.explore,
                    post : !state.post,
                    user_pets:actions.pets
                }
            }
            return{
                ...state,
                post : !state.post,
                user_pets:actions.pets
            };
        case "POSTED_MOBILE":
            return {
                ...state,
                post:!state.post
            };

        default : return state
    }
};

export default Home;
