import React, {Component} from 'react';

class AddPost extends Component {
    render() {
        return (
            <div>

                <div className="text-center">
                    <h6>Add New Post To</h6>
                    <div className="text-center d-flex justify-content-center">
                        <button className="btn rounded-circle" style={this.props.style} onClick={this.props.click} value="user-profile">
                            <span>My Profile</span>
                        </button>
                        <button style={this.props.style} className="btn rounded-circle" onClick={this.props.click} value="pet1">
                            <span>Pet1 Profile</span>
                        </button>
                        <button style={this.props.style} className="btn rounded-circle" onClick={this.props.click} value="pet2">
                            <span>Pet2 Profile</span>
                        </button>
                    </div>
                </div>

            </div>
        );
    }
}

export default AddPost;