import React, {Component} from 'react';
import {connect} from "react-redux";
import './Home.css'
import './CSS/MQHome.css'
import Nav from "../LAYOUT/Nav";
import {fetchUser, toggleProfilePreview} from './../USER PROFILE/STORE/Actions'
import {NavLink, withRouter} from "react-router-dom";
import {explore, factOfTheDay, fetchAllPosts, makePostMobile} from "./STORE/Actions";
import AllPosts from "../POSTS/AllPosts";
import {getClient, suggestPet} from "../SEARCH/Actions";
import ProfilePreview from "../USER PROFILE/ProfilePreview";
import {addSuggestedUser, admire, admSuggestedPet} from "../USER_FRIENDS/STORE/Actions";
import Post from "../POSTS/Post";
import Preview from "../POSTS/Preview";
import Loader from "react-loader-spinner";
import moment from "moment";
import ExploreMobile from "./ExploreMobile";
// import Preview from "../POSTS/Preview";
// import {friendsRequest} from "../VISITING PROFILE/STORE/Actions";
class Home extends Component {

    componentDidMount() {
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Home";
        this.props.getUserInfo();
        setTimeout(()=>{
            this.props.fetchAllPosts();
        },500);
        this.props.nearbyUsers();
        this.props.petSuggest();
        this.props.dayFact();
        if ($(window).width() < 990 && !localStorage.getItem('mobileControl')){
           this.props.confirmControl()
        }
    }

    componentWillUnmount() {
        this.props.profile()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
       // console.log(this.props.postMobile)
    }

    render() {
        let suggestPets;
        let nearbyUsers;
        let img ;
        let admBtn;
        let frdBtn;
            if(Object.keys(this.props.suggestedPets).length){
                suggestPets= this.props.suggestedPets.map((item,index)=>{
                            // console.log("Sugg",item);
                        if (this.props.admireClick && this.props.admiredItem === item.id){
                            admBtn = <img src={'/img/paws.svg'} alt="admire" height={'25'} width={'25'} className={'float-right mr-3'}/>
                        }else if (!this.props.userAdmiredPets.includes(item.id)){
                            admBtn = <img src={'/img/paw.svg'} alt="admire" height={'25'} width={'25'} className={'float-right mr-3'} onClick={()=>this.props.admire(item.id)}/>
                        }
                    return <div key={index} className='suggestions' >
                                    {item.profile_photo === null ?
                                        <img src={`/img/rodent.svg`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                                        :
                                        <img src={`/storage/pets/${item.id}/${item.profile_photo}`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                                    }
                                       <NavLink to={`pet-view/${item.id}`}>
                                                {/*onMouseOver={()=>this.props.profile('pet',item.id)}
                                                   onMouseOut={this.props.profile} >*/}
                                           {item.username}
                                       </NavLink>

                                    {admBtn}
                                    {/* this.props.profilePreview && this.props.previewId === item.id?
                                         <ProfilePreview
                                        admirer = {item.admirer_usn}
                                        admirerId = {item.admirer}
                                        profile={'pet'}
                                        id={item.id} />
                                    : '' */}
                            </div>
                })
            }
        if(Object.keys(this.props.usersNearby).length) {
            nearbyUsers = this.props.usersNearby.map((item, index) => {
                if(item.photo_path !==null){
                    img = <img src={`/storage/users/${item.id}/${item.photo_path}`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>
                }else img=<img src={`/img/avatar.png`} alt="user" width={'30'} height={'30'} className="rounded-circle mr-2"/>;

                if (this.props.addUserClick && this.props.addedItem === item.id) {
                    frdBtn = <i className={'float-right mr-3'}>sending request</i>
                } else if (item.status === 0) {//!this.props.userFriends.includes(item.id)
                    frdBtn = <img src={'/img/hourglass.svg'} alt={"pending"} className={'float-right mr-3'} width={'23'} height={'23'}/>;
                }else frdBtn = <img src={'/img/add-user.svg'} alt={"user"} className={'float-right mr-3'}
                                    onClick={() => this.props.friendsRequest(item.id)} width={'25'} height={'25'}/>;
                return <span key={index} className='text-info suggestions'>
                    {img}
                    <NavLink to={`pet-owner/${item.id}`}>{item.username}</NavLink>
                    {frdBtn}
                    </span>
            });
        }
       let dayFact;

        if (this.props.factOfTheDay){
             // console.log(this.props.factOfTheDay)
            dayFact = <div className="text-center p-2">
                          <p style={{fontFamily:"Cambria"}}>{this.props.factOfTheDay.fact}</p>
                          <i className="far fa-thumbs-up"/>
                     </div>
        }else dayFact = <div className="text-center">
                            <p style={{fontFamily:"Cambria"}}>Unavailable</p>
                        </div>;

        //Loop and Load Posts
        let posts = <div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        // if (Object.keys(this.props.posts).length){
        if (this.props.posts.length === 0){
            // console.log(8787);
            posts = <div className="card" id='post'>
                <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>No Posts to show !</h2>
                    </span>
                </div>
            </div>
        }else {
            posts = this.props.posts.map((post,index)=>{
                return <AllPosts
                    key = {index}
                    post_owner = {post.owner_user_id}
                    post_owner_username = {post.postOwner}
                    activeUser = {this.props.user_info.username}
                    userId = {post.user_id} //pet owner ID
                    petOwner = {post.user.username} // pet owner username
                    petOwnerId = {post.user.id} // pet owner username
                    ownerPhoto = {post.user.photo_path}// pet owner profile photo
                    petId = {post.pet_id}
                    petPhoto ={post.pet.profile_photo}
                    username = {post.pet.username}
                    postId = {post.id}
                    caption = {post.caption}
                    totalLikes ={post.likes.length}
                    totalComments = {post.comments}
                    numOfPics = {post.post_photo.length}
                    postPic = {post.post_photo}
                    postTime = {this.props.postTime[index]}
                    liked = {post.is_liked_count}
                    saved = {post.saved}
                    saved_id = {post.saved_id}
                />
            })
        }
        // }
        return (
            <div>
                <Nav username={this.props.user_info.username}/>

                <div className="container below-nav">
                    <div className="row">

                    <div className="col-sm-12 col-lg-4 home-left-pane lg-home-left-pane">  {/*left-pane*/}
                        <div className="did-you-know p-2">
                            <div className ="text-center suggestion-intro">
                                <h5>Did you know?</h5>
                            </div>
                            <small className="text-info"><u>{moment().format("MMM Do YY")}</u></small>
                            {dayFact}
                        </div>

                        <div className="pets-suggestion">
                            <div className ="text-center suggestion-intro">
                                <h5>Pets you may like</h5>
                            </div>
                            <div className="d-flex flex-column p-2">
                                {suggestPets}
                                <i className="text-center fas fa-redo-alt mt-3 text-success"/>
                            </div>

                        </div>

                        <div className="users-suggestion">
                            <div className ="text-center suggestion-intro">
                                <h5>People you may know</h5>
                            </div>
                            <div className="d-flex flex-column p-2">
                                {nearbyUsers}
                                <i className="text-center fas fa-redo-alt mt-3 text-success"/>
                            </div>
                        </div>

                    </div>

                    <div className="col-sm-12 col-lg-8 sm-home-posts">   {/*mt-4*/}
                        {posts}
                        {/*{this.props.showXplore?*/}
                        {/*    <ExploreMobile*/}
                        {/*        close={this.props.explore}*/}
                        {/*        dayFact={dayFact}*/}
                        {/*        nearbyUsers={nearbyUsers}*/}
                        {/*        suggestPets={suggestPets}*/}
                        {/*    />*/}
                        {/*        :null}*/}
                        {/*    /!*<div className="xplore">*/}
                        {/*        <div className="sm-did-you-know p-2">*/}
                        {/*            <div className ="text-center suggestion-intro">*/}
                        {/*                <small className="text-info"><u>{moment().format("MMM Do YY")}</u></small>*/}
                        {/*            </div>*/}
                        {/*            <small className="text-info"><u>February 2nd</u></small>*/}
                        {/*            {dayFact}*/}
                        {/*        </div>*/}

                        {/*        <div className="sm-users-suggestion">*/}
                        {/*            <div className ="text-center suggestion-intro">*/}
                        {/*                <h5 className="text-dark">People you may know</h5>*/}
                        {/*            </div>*/}
                        {/*            <div className="d-flex flex-column p-2">*/}
                        {/*                {nearbyUsers}*/}
                        {/*                <i className="text-center fas fa-redo-alt mt-3 text-success"/>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}

                        {/*        <div className="sm-pets-suggestion">*/}
                        {/*            <div className ="text-center suggestion-intro">*/}
                        {/*                <h5>Pets you may like</h5>*/}
                        {/*            </div>*/}
                        {/*            <div className="d-flex flex-column p-2">*/}
                        {/*                {suggestPets}*/}
                        {/*                <i className="text-center fas fa-redo-alt mt-3 text-success"/>*/}
                        {/*            </div>*/}

                        {/*        </div>*/}

                        {/*    </div>*!/*/}


                        {/*{this.props.postMobile?*/}
                        {/*    <div className="postMobile">*/}
                        {/*        <Post/>*/}
                        {/*        <Preview*/}
                        {/*            poster = 'user'*/}
                        {/*            page = "HOME"*/}
                        {/*            user = {this.props.user_info.username}*/}
                        {/*            user_ = {this.props.user_info}*/}
                        {/*            userId = {this.props.user_info.id}*/}
                        {/*            cover = {this.props.photo}*/}
                        {/*            numOfpets = {this.props.user_pets.length}*/}
                        {/*            pets = {this.props.user_pets}*/}
                        {/*            petPhoto = {this.props.pet_cover}*/}
                        {/*        />*/}
                        {/*    </div>*/}
                        {/*    :''*/}
                        {/*}*/}
                        {/*<div className="explore-sm">*/}
                        {/*        <div className="col-sm-4 nav-sm-btn nav-sm-btn-1">*/}
                        {/*            <img src={"/img/find.svg"} alt="explore" width={"30"} height={"30"} onClick={this.props.explore}/>*/}
                        {/*        </div>*/}
                        {/*        <div className="col-sm-4 nav-sm-btn nav-sm-btn-2 text-center">*/}
                        {/*            <img src={"/img/camera.svg"} alt="camera" width={"30"} height={"30"} onClick={this.props.makePostMobile}/>*/}
                        {/*        </div>*/}
                        {/*        <div className="col-sm-4 nav-sm-btn nav-sm-btn-3 text-right">*/}
                        {/*            <NavLink to={'/profile'}>  <img src={"/img/user2.svg"} alt="user" width={"30"} height={"30"}/> </NavLink>*/}
                        {/*        </div>*/}
                        {/*</div>*/}
                    </div>
                 </div>
                </div>
            </div>
        )
    }
}

const MapState = (state)=>{
    return{
        user_info : state.User.user_info,
        usersNearby : state.Search.nearestUsers,
        posts : state.Home.posts,
        postTime : state.Home.postTime,
        profilePreview : state.PetProfile.profilePreview,
        previewId : state.PetProfile.profileId,
        factOfTheDay : state.Layout.fact,
        suggestedPets : state.Search.pets,
        userAdmiredPets : state.Search.likedPets,
        //admiring suggested Pets
        admireLoader : state.VisitorFriends.admireLoader,
        admireClick : state.VisitorFriends.admireClick,
        admiredItem : state.VisitorFriends.admiredItem,

        addUserClick : state.VisitorFriends.addUserClick,
        addedItem : state.VisitorFriends.addedItem,
        //
        showXplore : state.Home.explore,
        postMobile : state.Home.post,
        photo : state.User.user_photo,
        user_pets : state.Home.user_pets,
        pet_cover : state.User.user_pet_photos,
    }
};

const MapDispatch = (dispatch)=>{
    return {
        dayFact : () =>{dispatch(factOfTheDay())},
        petSuggest : () =>{dispatch(suggestPet())},
        getUserInfo: () => { dispatch(fetchUser())},
        fetchAllPosts: () => { dispatch(fetchAllPosts())},
        nearbyUsers : () =>{dispatch(getClient())},
        profile : (user,id) =>{dispatch(toggleProfilePreview(user,id))},
        admire : (petId) =>{dispatch(admSuggestedPet(petId))},
        friendsRequest : (id) => {dispatch(addSuggestedUser(id))},
        explore : () =>{dispatch(explore())},
        makePostMobile : () =>{dispatch(makePostMobile())},
        confirmControl : () =>{
            if ( confirm("Double tap the navigation bar to toggle hide/show the controls at the bottom")){
                localStorage.setItem('mobileControl', 'confirmed');
            }
        },
    }
};

export default connect(MapState,MapDispatch)(withRouter(Home));
