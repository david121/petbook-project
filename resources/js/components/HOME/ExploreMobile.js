import React, {Component} from 'react';
import moment from "moment";
import './CSS/MobileExplore.css'
class ExploreMobile extends Component {
    render() {
        return (
            <div className="explore-modal">
                <div className="explore-content">
                    <div className="explore-header">
                        <span className="close" onClick={this.props.close}>&times;</span>
                        <h5>Explore</h5>
                    </div>
                    <div className="explore-body">
                        <div className="">
                            <div className="sm-did-you-know p-2">
                                <div className ="text-center suggestion-intro">
                                    <div className ="text-center suggestion-intro">
                                        <h5>Did you know?</h5>
                                    </div>
                                    <small className="text-info"><u>{moment().format("MMM Do YY")}</u></small>
                                </div>
                                {this.props.dayFact}
                            </div>

                            <div className="sm-users-suggestion">
                                <div className ="text-center suggestion-intro">
                                    <h5>Pets you may like</h5>
                                </div>
                                <div className="d-flex flex-column p-2">
                                    {this.props.suggestPets}
                                    <i className="text-center fas fa-redo-alt mt-3 text-success"/>
                                </div>
                            </div>

                            <div className="sm-pets-suggestion">
                                <div className ="text-center suggestion-intro">
                                    <h5>People you may know</h5>
                                </div>
                                <div className="d-flex flex-column p-2">
                                    {this.props.nearbyUsers }
                                    <i className="text-center fas fa-redo-alt mt-3 text-success"/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExploreMobile;