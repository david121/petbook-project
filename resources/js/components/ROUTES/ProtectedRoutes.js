import React from 'react'
import PasswordReset from "../AUTHENTICATION/PasswordReset";
import PetForm from "../AUTHENTICATION/REGISTER PET/PetForm";
import Message from "../MESSAGING/Chat";
import Chats from "../MESSAGING/ChatList";
import AllListing from "../LISTING/AllListing";
import ArchivedPosts from "../ARCHIVE/Archive";
import Activity from "../ACTIVITIES/Activity";
import Home from "../HOME/Home";
import Tests from "../TESTS/Layout";
import Search from "../SEARCH/Search";
import PetProfile from "../USER PROFILE/PetProfile";
import PetVisitor from "../VISITING PROFILE/Pet";
import Visitor from "../VISITING PROFILE/Visitor";
import Profile from "../USER PROFILE/Profile";
import {Route} from "react-router-dom";

const ProtectedRoutes = (props)=>{
    return (
        <div>
            <Route path="/password-recovery" component={PasswordReset}/>
            <Route path="/form" component={PetForm}/>
            <Route path="/messages" component={Message}/>
            <Route path="/chats" component={Chats}/>
            {/*<Route path="/find-pet" component={FindPet}/>*/}
            <Route path="/pet-listing" component={AllListing}/>
            <Route path="/archive" component={ArchivedPosts}/>
            <Route path="/activities" component={Activity}/>
            <Route path="/home" component={Home}/>
            {/*<Route path="/add-pet" component={AddPet}/>*/}
            <Route path="/test-route" component={Tests}/>
            <Route path="/search" component={Search}/>
            <Route path="/pet/:petId" component={PetProfile}/>
            <Route path="/pet-view/:petId" component={PetVisitor}/>
            <Route path="/pet-owner/:id" render={(props) => {
                return <Visitor/>
            }}/>
            <Route path="/profile" component={Profile}/>
        </div>
    )
};

export default ProtectedRoutes