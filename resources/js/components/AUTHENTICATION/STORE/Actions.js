import axios from 'axios';
import {addPet, fetchPets} from "../../USER PROFILE/STORE/Actions";
import {axiosHeader} from "../../index";
import {Api_Process} from "../../LAYOUT/STORE/Actions";
export const profileType = (type) =>{
      return{
          type: "PROFILE_TYPE",
          value: type
      }
};

export const inputHandler = (value, inputName) =>{
    return{
        type : 'INPUT',
        name : inputName,
        value: value,
    }
};

export const registerLoader = () =>{
    return{
        type : 'REG_LOADER',
    }
};


export const registerSubmitHandler = (data,error) =>{
    return dispatch=>{
        if (error.fnameErr !== '' || error.lnameErr !== '' || error.emailErr !== '' || error.passwErr !== '' || error.vPasswErr !== ''|| error.userErr !== ''){
         alert("Cant Register");
            dispatch({
                type:"REG_ERROR",
            })
        }
        //if Validation passed, Send User data to Server
        else
        {
            dispatch(registerLoader());
            //data to Register
            const userdata = {
                profileType: sessionStorage.getItem("profileType"),
                firstName : data.firstname,
                lastName : data.lastname,
                username : data.username,
                email : data.email,
                password : data.password
            };

            axios.post('/api/register',userdata)
                .then(response=>{
                    if(response.data.success){
                        sessionStorage.setItem("user",response.data.user_id);
                        dispatch(registerLoader());
                        const data = {
                            grant_type:"password",
                            client_id:"2",
                            client_secret:"BjjP6En8thtjtmd9HehKjxSZUNWiKaJx1sRKkuek",
                            username : userdata.email,
                            password : userdata.password
                        };

                        let param="";

                        Object.keys(data).map(key =>{
                            return param += ""+key+"="+data[key]+"&"
                        });

                        param = param.slice(0, param.length-1);

                        axios.post('oauth/token',param)
                            .then(response=>{
                                if(response.data.access_token){
                                    localStorage.setItem("token", response.data.access_token);
                                    dispatch({
                                        type: 'USER_REGISTERED',
                                    })
                                }else{
                                    alert("You've been registered but Server couldn't progress the application at the minute. Please Sign in.");
                                }
                            }).catch(error=>{
                            alert(error.response.data.message);
                        });
                    }else{
                        if (response.data.userExists){
                            console.log("user",response.data.username);
                            console.log("email",response.data.email);
                            dispatch({
                                type:"REGISTERING_USER_EXISTS",
                                username: response.data.username,
                                email: response.data.email,
                            })
                        }
                        dispatch(registerLoader());
                        //console.log("Server Response : " + response.data.msg)
                    }
                    // console.log("Server Response : " + JSON.stringify(response));
                }).catch(error=>{
                //console.log("Server Response : " + error)
                dispatch(registerLoader());
            });
        }

    }
};

//User Profile Photo Handler
export const userPhotoHandler =(file)=>{
    return dispatch =>{

        dispatch({
                type: 'USER_PROFILE_PHOTO',
                value:file
        })

    }
};
//User Profile Photo submit Handler
export const userPhotoSubmitHandler =(file)=>{
    const formdata = new FormData();
    formdata.append('photo', file);
    formdata.append('id', sessionStorage.getItem('user'));

    return dispatch =>{

        axios.post('api/user_cover', formdata,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }
        })
            .then(response=>{
                if (response.data.success){
                    console.log(response.data);
                    dispatch({
                        type: 'USER_PROFILE_PHOTO_UPLOADED',
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            });

    }
};


//Login Form submit handler
export const loginSubmitHandler = (email, password) =>{
    return dispatch => {
        const data = {
            grant_type:"password",
            client_id:"2",
            client_secret:"BjjP6En8thtjtmd9HehKjxSZUNWiKaJx1sRKkuek",
            username : email,
            password : password
        };

        let param="";

        Object.keys(data).map(key =>{
            return param += ""+key+"="+data[key]+"&"
        });

        param = param.slice(0, param.length-1);

        axios.post('oauth/token',param)
            .then(response=>{
                if(response.data.access_token){
                    localStorage.setItem("token", response.data.access_token);
                    dispatch({
                        type: 'LOG_SUBMIT',
                    });
                }else{
                    dispatch({type:"LOGIN_SERVER_ERROR"});
                    //console.log(response.data.msg);
                }
            }).catch(error=>{
                dispatch({type:"INVALID_CREDENTIALS"});
            //alert(error.response.data.message);
    });
    }
};

// Log Out

export const signOut = (history) =>{
    return dispatch=>{
          localStorage.removeItem('token');
          // history.push('/');
          window.location.replace("/");
        dispatch({
            type : "LOGOUT"
        })
    }
};

export const blurHandler = (value, name) =>{
    return{
        type: 'BLUR',
        name : name,
        value : value
    }
};

export const fileHandler = (file) =>{
//    console.log(file)
    return{
        type: 'PHOTO',
        value : file
    }
};

export const nextHandler = () =>{

    return{
        type: 'NEXT',
    }
};

export const prevHandler = () =>{

    return{
        type: 'PREVIOUS',
    }
};

export const petQuestions = (value,name,types,breeds) =>{
    return dispatch=>{
        let typeName = [];
        if (name === 'pet_type' && value !== ''){
            typeName = types.filter((type)=>{return parseInt(value) === type.id});

            axios.get('api/fetchBreed',{headers:{
                    "Content-Type" : "application/json",
                    "Accept" : "application/json",
                    "Authorization" : "Bearer " + localStorage.getItem("token"),
                },
                params:{type:value}
            })
             .then(response=>{
                    if (response.data.success){
                        dispatch({
                            type : 'PET_BREED',
                            breeds : response.data.breeds
                        })
                    }
                    // else console.log(response.response);
                }).catch(error=>{
                alert(error)
            });
            dispatch({
                type : 'ANSWER',
                value : value,
                typeName:typeName[0].name
            });
            return
        }
        let breedName = [];
        if (name === 'pet_breed'){
            breedName = breeds.filter((breed)=>{return parseInt(value) === breed.id});
            dispatch({
                type : 'ANSWER',
                value : value,
                breedName:breedName[0].name
            });
            return
        }

        dispatch({
            type : 'ANSWER',
            value : value,
        })
    }

};

export const petSubmitHandler = (data,page) =>{
    // console.log("registered from sign up ");
    return dispatch =>{
        dispatch(Api_Process());
        axios.post('api/pet',data,{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }})
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    if (page === 'auth'){
                        dispatch({
                            type: 'PET_REGISTERED'
                        })
                    }else if(page === 'profile'){
                        dispatch(addPet(true));
                        dispatch(fetchPets());
                    }else if (page === 'post-add'){
                        dispatch(fetchPets());
                        dispatch({type:"PET_ADDED"})
                    }
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log("FROM PET SUBMIT ACTION", error);
            });
    }
};

export const sendCode = (context,email) =>{
    return dispatch=>{
        dispatch({type:"VERIFY_ACTION"});
        if (context === "PASSWORD_RESET") {
            axios.post('/api/recoveryTestPassed',{email},{
                headers:{
                    "Content-Type" : "application/json",
                    "Accept" : "application/json"}
            }).then(res=>{
                if (res.data.success){
                    dispatch({type:"VERIFY_ACTION"});
                    dispatch({type:"CODE_RESENT"})
                }
            }).catch(err=>{

            });
            return;
        }
        axios.post('/api/resendCode',null,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token"),
            }
        }).then(res=>{
            if (res.data.success){
                dispatch({type:"VERIFY_ACTION"});
                dispatch({type:"CODE_RESENT"})
            }
        }).catch()
    }
};

export const codeInputHandler =(val,name)=>{
    return dispatch =>{
              dispatch({
                  type:"VERIFY_CODE_INPUT",
                  name:name,
                  value:val
              })
    }
};
export const VerifyCode = (code,context,email) =>{
    return dispatch =>{
        dispatch({type:"VERIFY_ACTION"});
        if(context === "PASSWORD_RESET"){

            axios.post('/api/verifyRecoveryCode',{code,email},{
                headers:{
                    "Content-Type" : "application/json",
                    "Accept" : "application/json",
                }
            })
                .then(res=>{
                    if (res.data.msg === "Wrong Code"){
                        dispatch({type:"VERIFY_ACTION"});
                        dispatch({type:"WRONG_CODE"});
                        return false
                    }
                    dispatch({type:"VERIFY_ACTION"});
                     dispatch({type:"CHANGE_PASSWORD"})
                })
                .catch();

            return;
        }
        axios.post('/api/verifyUser',{code:code},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem("token")
            }
        })
            .then(res=>{
                if (res.data.success){
                    if (res.data.msg === "Wrong Code"){
                        dispatch({type:"VERIFY_ACTION"});
                        dispatch({type:"WRONG_CODE"});
                       return false
                    }
                    localStorage.removeItem('authenticate');
                    dispatch({type:"VERIFY_ACTION"});
                    dispatch({type:"VERIFIED"})
                }
            }).catch(err=>{

        })
    }
};

export const mailInputHandler =(email)=>{
    return dispatch =>{
        if (localStorage.getItem(`${email}`)){
            return dispatch({type:"BL_ANONYMOUS"})
        }
        dispatch({type:"PASSWORD_EMAIL",value:email})
    }
};
export const checkEmailExists =(email)=>{
    return dispatch =>{
        dispatch({type:"VERIFY_ACTION"});
        axios.post('/api/checkEmailExists',{email},{
             headers:{
                 "Content-Type" : "application/json",
                 "Accept" : "application/json",
             }
         })
         .then(response=>{
             if (response.data.message === "NONE"){
                 dispatch({type:"VERIFY_ACTION"});//loader
                 return dispatch({type:"EMAIL_NOT_FOUND"})
             }
            if (response.data.success){
                dispatch({type:"VERIFY_ACTION"});//loader
                dispatch({type:"ANSWER_QUESTION",question:response.data.q});
            }
         })
         .catch()
    }
};

export const CheckRecoveryAnswer = (choice,answer,email) =>{
    return dispatch =>{
        let res = choice === answer ? "CORRECT" : "WRONG";
        dispatch({type:"VERIFY_ACTION"});//loader
        switch (res) {
            case "CORRECT":
                axios.post('/api/recoveryTestPassed',{email},{
                    headers:{
                        "Content-Type" : "application/json",
                        "Accept" : "application/json",
                    }
                })
                    .then(res=>{
                        if (res.data.success){
                            dispatch({type:"VERIFY_ACTION"});//loader
                            dispatch({type:"PROVIDE_CODE"})
                        }
                    })
                    .catch(err=>{});break;
            case "WRONG":
                dispatch({type:"VERIFY_ACTION"});
                dispatch({type:"RECOVERY_ATTEMPT"})
        }
    }
};

export const passInputHandler = (value,name)=>{
    return dispatch =>{
        dispatch({
            type: "PASSWORD_INPUT",
            name,
            value
        })
    }
};


export const changePassword = (newPassword,email) =>{
    return dispatch =>{
        dispatch({type:"VERIFY_ACTION"});//loader
        axios.post('/api/changePassword',{password:newPassword,email},{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
            }
        })
            .then(res=>{
                if (res.data.success){
                    dispatch({type:"VERIFY_ACTION"});//loader
                    dispatch({type:"PASSWORD_CHANGED"})
                }
        }).catch(err=>{

        })
    }
};

export const ViewPet = (id,pets) =>{
    return dispatch =>{
        let pet = pets.filter(pet=>id===pet.id);
        dispatch({type:"VIEW_PET",id,pet})
    }
};
export const BackToBrowse = (login)=>{
    return dispatch=>{
        dispatch({type:"BACK_TO_BROWSE",login})
    }
};