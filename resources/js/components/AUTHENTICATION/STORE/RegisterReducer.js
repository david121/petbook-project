// REG_LOADER
const initialState = {

    //Errors on input
    submitDisabled: true,
    fnameErr: '',
    lnameErr: '',
    userErr: '',
    emailErr: '',
    passwErr: '',
    vPasswErr: '',
    submitBtnDisable: false,

    //Toggle Error tooltip
    name: false,
    userN: false,
    emailE: false,
    pass: false,
    vpass: false,
    // Errors on Blur
    fnameEmpty : null,
    lnameEmpty : null,
    emailEmpty : null,
    passEmpty : null,
    vpassEmpty : null,
    usernameEmpty : null,
    // Register data
    type: '',
    firstname : '',
    lastname : '',
    email : '',
    username:'',
    password: '',
    verifyPass: '',
    registered : false,
    userId : null,
    regLoader: false,
    //Profile Photo
    coverPhoto : null,
    photoPreview: null,
    coverPhotoUploaded : false,
    usernameExists : false,
    emailExists : false,
};

const Register = (state= initialState, action) => {
    switch (action.type) {
        case 'PROFILE_TYPE':
            return {
                ...state,
                type: action.value
            };
        case 'INPUT':
            const pattern = /[\W]|[0-9]/;
            const emailPattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            const usernamePattern = /[!@#$%^&*()=+{}?.<>[/|\-]/g;
            if (action.name === "firstname"){
                if(pattern.test(action.value)){
                    return{
                        ...state,
                        fnameErr: "Please remove number or special characters from first name field.",
                    }
                }else return{...state,fnameErr : '',firstname: action.value}
            }else
            if (action.name === "lastname"){
                if(pattern.test(action.value)){
                    return{
                        ...state,
                        lnameErr: "Please remove number or special characters from last name field.",
                    }
                }else return{...state,lnameErr : '',lastname: action.value}
            }
            else
            if (action.name === "email"){
                if(!emailPattern.test(action.value)){
                    return{
                        ...state,
                        emailErr: "Please enter a valid email address. We will send a verification message to provided email address.",
                    }
                }else return{...state,emailErr : '',email: action.value}
            }else
            if (action.name === "username"){
                if(usernamePattern.test(action.value)){
                    return{
                        ...state,
                        userErr: " Invalid username (consider removing special characters).",
                    }
                }else return{...state,userErr : '',username: action.value}
            }else
            if (action.name === "password"){ // Make regExp for password validation
                if(action.value.length < 8 ){
                    return{
                        ...state,
                        passwErr: " Enter a stronger password (min 8 characters).",
                    }
                }else return{...state,passwErr : '',password: action.value}
            }else
            if (action.name === "verifyPass"){
                if(state.password !=='' && action.value !== state.password){
                    return{
                        ...state,
                        vPasswErr: " Passwords don't match.",
                    }
                }else return{...state,vPasswErr : ''}
            }
            return {
                ...state,
                submitBtnDisable:!state.submitBtnDisable,
            };

        case 'BLUR' :
            if (action.value === ''){
                return{
                    ...state,
                    [action.name]: true
                }
            }
            return {
                ...state,
                [action.name]:false
            };

        case "USER_PROFILE_PHOTO":
            let cover = action.value;
            const blobObj = window.URL.createObjectURL(cover);
            return{
                ...state,
                coverPhoto: action.value,
                photoPreview: blobObj
            };

        case "USER_PROFILE_PHOTO_UPLOADED":
            return{
                ...state,
                coverPhotoUploaded: !state.coverPhotoUploaded,
            };

        case 'PHOTO' :
            let file = action.value;
            const photo = window.URL.createObjectURL(file);
            return{
                ...state,
                petPhoto : photo
            };

        case 'REG_ERROR':
            return{
                ...state,
            };

        case 'REG_LOADER':
            return{
                ...state,
                regLoader: !state.regLoader,
            };
        case 'REGISTERING_USER_EXISTS':
            return{
                ...state,
                usernameExists:action.username,
                emailExists:action.email,
            };

        case 'USER_REGISTERED':
            return{
                ...state,
                registered:true,
                regLoader:false
            };


        default : return state;
    }
};

export default Register;
