const initialState = {
    petId: null,
    pet:[],
    view : false,
    activeComp: "AUTH"
};


const Browse = (state=initialState, actions)=>{
    switch (actions.type) {
        case "CHANGE_COMP" : {
            return {
                activeComp:actions.comp
            }
        }
        case "VIEW_PET":{
            return{
                ...state,
                petId : actions.id,
                view:true,
                pet:actions.pet
            }
        }
        case "BACK_TO_BROWSE":{
            if(actions.login){
                return{
                    ...state,
                    activeComp:"AUTH",
                    view:false
                }
            }
            return {
                ...state,
                view: false
            }
        }
        default : return state;
    }
};

export default Browse;