
const initialState = {
    error : null,
    email : '',
    password: '',
    authenticated: false,
    // Errors
    emailEmpty : null,
    passEmpty : null,

    //Logged User
    user : {},
    //User Verification
    input1:'',
    input2:'',
    input3:'',
    input4:'',
    input5:'',

    verifyLoader:false,
    // recoveryLoader : false,
    codeResent:false,
    verified:false,
    wrongCode:false,
    //password recovery
    blocked:false,
    passRecoveryEmail : '',
    emailErr : false,
    attempts:0,
    test_failed:false,
    recoveryComp : "ENTER_EMAIL",
    question : null,
    mailNotFound:false,
    newPassword:'',
    verifyNewPassword:'',
    newPassErr:"",
    verPassErr:"",
    passChanged: false,

};

const Login = (state= initialState, action) => {
    switch (action.type) {
        case 'INPUT':
            return {
                ...state,
                [action.name] : action.value
            };
        case 'LOG_SUBMIT':
            return{
                ...state,
                authenticated:true
            };
        case 'INVALID_CREDENTIALS':
            return{
                ...state,
                invalid_details:true,
                passChanged:false,
            };
        case 'VERIFY_CODE_INPUT':
            return{
                ...state,
                [action.name]:action.value
            };
        case 'WRONG_CODE':
            return{
                ...state,
                wrongCode:true,
                codeResent:false
            };
        case 'VERIFY_ACTION':
            return{
                ...state,
                verifyLoader: !state.verifyLoader
            };
        case 'CODE_RESENT':
            return{
                ...state,
                codeResent: true,//!state.codeResent,
                wrongCode:false,
            };
        case 'VERIFIED':
            return{
                ...state,
                verified: true
            };

        case 'PASSWORD_EMAIL':
            const emailPattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (!emailPattern.test(action.value)){
                return{
                    ...state,
                    emailErr: true,
                    passRecoveryEmail:action.value,
                    blocked:false
                }
            }
            return{
                ...state,
                emailErr: false,
                passRecoveryEmail:action.value
            };
        case "BL_ANONYMOUS":
            return {
                ...state,
                blocked:true,
                passRecoveryEmail:''
            };
        case 'EMAIL_NOT_FOUND':
            return{
                ...state,
                mailNotFound: true
            };
        case 'RECOVERY_ATTEMPT':
            let attempts = state.attempts;
            let increment = attempts +1;
            if (increment === 2){
                localStorage.setItem(`${state.passRecoveryEmail}`,'BL');
                return {
                    ...state,
                    test_failed:true,
                    passRecoveryEmail:''
                }
            }
            return{
                ...state,
                attempts: increment
            };
        case 'ANSWER_QUESTION':
            return{
                ...state,
                question: action.question,
                recoveryComp: "ANSWER_QUESTION",
            };
        case 'PROVIDE_CODE':
            return{
                ...state,
                recoveryComp: "PROVIDE_CODE",
            };
        case 'CHANGE_PASSWORD':
            return{
                ...state,
                recoveryComp: "CHANGE_PASSWORD",
            };
        case 'PASSWORD_INPUT':
            if (action.name === "newPassword"){ // Make regExp for password validation
                if(action.value.length < 8 ){
                    return{
                        ...state,
                        newPassErr: " Enter a stronger password (min 8 characters).",
                        // newPassword: action.value
                    }
                }else return{...state,newPassErr : '',newPassword: action.value}
            }else
            if (action.name === "verifyNewPassword"){
                if(state.newPassword !=='' && action.value !== state.newPassword){
                    return{
                        ...state,
                        verPassErr: " Passwords don't match.",
                        // verifyNewPassword:action.value
                    }
                }else return{...state,verPassErr : ''}
            }
            return{
                ...state,
            };
        case 'PASSWORD_CHANGED':
            return{
                ...state,
                passChanged:true,
                invalid_details:false
            };
        default : return state;
    }
};

export default Login;
