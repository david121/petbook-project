import React from 'react'
const initialState = {
    questions : [
        {
            q : "What type of pet do you have ?",
            ans:null,
            type: 'type',
            optional:false
        },
        {
            q : "What do you call your pet ?",
            ans:null,
            type: 'name',
            optional:false
        },
        {
            q : <div className="d-flex flex-column">
                    <span>Breed ? </span>
                    <small className="text-info">(please select <b>Other Breed</b> if you cant find your pet's breed)</small>
                </div> ,
            ans:null,
            type: 'breed',
            optional:false
        },
        {
            q : " Pet Gender ? ",
            ans:null,
            type: 'gender',
            optional:false
        },
        {
            q : "How old is your Pet ?  (optional)",
            ans:null,
            type: 'age',
            optional:true
        },
        {
            q : "Create a username for your pet",
            ans:null,
            type: 'username',
            optional:false
        },
        {
            q : "Upload a Pet profile photo ",
            ans:null,
            type: 'photo',
            optional:false
        }
    ],
    currentQues:0,
    optionalQ:false,
    answered: false,
    previousQue:null,
    type: '',
    typeName: '',
    breed: '',
    breedName: '',
    gender: '',
    age : '',
    name : '',
    file: null,
    photo: '',
    username: '',
    //age components
    years : '',
    months: '',
    days : '',

    pet_types: [],
    pet_breeds : [],
    //Form Submit
    registered: false,
    addPetModal : false
};

const Pet = (state = initialState, action) =>{

        switch (action.type) {
            case "PET_TYPES":
                return {
                    ...state,
                    pet_types:action.value
                };
            case "PET_BREED":
                return {
                    ...state,
                    pet_breeds:action.breeds
                };
            case 'ANSWER' :
                //form questions
                const questions = [...state.questions];
                //set user answer
                questions[state.currentQues].ans = action.value;

                //check if an answer was given and set values
                if (questions[state.currentQues].ans !== ''){
                   let typeName = action.typeName ? action.typeName : state.typeName;
                   let breedName = action.breedName ? action.breedName : state.breedName;
                   //const nextQ = state.currentQues+1;
                   //let optionalQ = questions[nextQ].optional;

                    return {
                        ...state,
                        answered:true,
                        [questions[state.currentQues].type] : action.value,
                        typeName,
                        breedName,
                    };
                }
                return {
                    ...state,
                    answered:false,
                    [questions[state.currentQues].type] : ''
                };
            case 'NEXT' :
                let questionIndex = state.currentQues;
                let nextQuestion;
                let previousQuestion;
                if (state.answered){
                    previousQuestion = questionIndex;
                    nextQuestion = questionIndex + 1;//Increment question index by 1
                    return{
                        ...state,
                        currentQues : nextQuestion,
                        answered : nextQuestion === 4
                    };
                }
                // else if (allQues[state.currentQues].ans !== ''){
                //     return {
                //         ...state,
                //         currentQues : questionIndex +1,
                //         answered: false
                //     }
                // }
                return{
                    ...state,
                    currentQues : questionIndex,
                };
            case 'PREVIOUS':
                const previous = state.currentQues - 1;
               // console.log(state.questions[current].type);
                return {
                    ...state,
                    currentQues: previous,
                    answered:true
                };
            case 'PHOTO' :
                const file = action.value;
                const src = window.URL.createObjectURL(file);
                let currentQuestionIndex = state.currentQues;
                const question = [...state.questions];
                question[state.currentQues].ans = action.value;
                if (question[state.currentQues].ans !== ''){
                    return {
                        ...state,
                        answered:true,
                        photo : src,
                        file : file
                    }
                }
                return {
                       ...state,
                        currentQues: currentQuestionIndex
                    };
            case 'PET_REGISTERED' :
                return {
                    ...state,
                    registered: true,
                    currentQues:0,
                    answered: false,
                    previousQue:null,
                    type: '',
                    breed: '',
                    gender: '',
                    age : '',
                    name : '',
                    photo: '',
                    username: '',
                };
            case 'ADD_PET_MODAL' :
                if(action.registered){
                    return {
                        ...state,
                        addPetModal: !state.addPetModal,
                        currentQues:0,
                        answered: false,
                        previousQue:null,
                        type: '',
                        breed: '',
                        gender: '',
                        age : '',
                        name : '',
                        photo: '',
                        username: '',
                    };
                }
                    return {
                        ...state,
                        addPetModal: !state.addPetModal
                    };
            default : return state;
        }
} ;

export default Pet;
