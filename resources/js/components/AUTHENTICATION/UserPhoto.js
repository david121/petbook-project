import React, {Component} from 'react';
import {NavLink, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import  './CSS/UserPhoto.css';
import './../TESTS/CSS/SmallScreen.css';
import {userPhotoHandler,userPhotoSubmitHandler} from './STORE/Actions'
class UserPhoto extends Component {

    componentDidMount(){
        if (!this.props.registered && !sessionStorage.getItem('user')){
            //this.props.history.push("/")
        }
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.photoUploaded){
            this.props.history.push('/register-pet')
        }
    }

    render() {

        const file = this.props.cover;
        let preview = this.props.preview? <img src={this.props.preview} className="rounded-circle mt-3" alt="profile picture" width="200" height="160"/>
        :<div className="p-4">
            <label htmlFor="photo" className='upload-photo-label text-center mt-4'>
                {/*<i className="fas fa-camera "/>*/}
                <img src={"/img/camera.svg"} alt="upload"/>
                <input type="file" id='photo' hidden={true} accept='image/*' onChange={this.props.handlePhoto}/>
            </label>
            </div>;
        return (
            <div>
                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand">Pawrry</span>
                </nav>
            <div className="container text-center">
                    <div className="mt-5">
                        <h4 id="header" style={{fontFamily:"Cambria"}}>A photo will look good on your profile</h4>
                        <span className="text-secondary" id="header2">upload your favorite photo</span>
                    </div>
                    <div className="photo-form">
                        <form className="text-center">
                            <div className="photo-preview mb-4">
                                {preview}
                            </div>

                            {this.props.preview?<label htmlFor="photo" className='mr-3 upload-photo-label'>
                                <i className="fas fa-camera "/>
                                <input type="file" id='photo' hidden={true} accept='image/*' onChange={this.props.handlePhoto}/>
                            </label> : ''}

                            <button className="" type="submit" disabled={this.props.preview === null} onClick={(e)=>this.props.handleSubmit(e,file)}>
                                <i className="fas fa-upload mr-2"/>
                                Upload
                            </button>

                        </form>
                    </div>

                <div className="upload-later">
                    <NavLink to='/register-pet' className="text-right later-link" style={{fontFamily:"Cambria",fontWeight:"bold"}}>Skip</NavLink>
                </div>
            </div>
            </div>
        );
    }
}


const MapState = (state) =>{
    return{
        registered : state.Register.registered,
        cover : state.Register.coverPhoto,
        preview : state.Register.photoPreview,
        photoUploaded: state.Register.coverPhotoUploaded,
    }
};

const MapDispatch = (dispatch) =>{
    return{
        handlePhoto : (e) =>{dispatch(userPhotoHandler(e.target.files[0]))},
        handleSubmit: (e,file)=>{
            e.preventDefault();
            dispatch(userPhotoSubmitHandler(file))
        }
    }
};

export default connect(MapState,MapDispatch)(withRouter(UserPhoto));
