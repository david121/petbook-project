import React, {Component} from 'react';
import './CSS/ErrorToolTip.css';
class FormError extends Component {
    render() {
        return (
            <div className="ErrorToolTip">
                <span className="tooltiptext">{this.props.message}</span>
            </div>
        );
    }
}

export default FormError;
