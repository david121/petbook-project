import React, {Component} from 'react';
import {Nav} from "react-bootstrap";
import {navigateComponent} from "../USER PROFILE/STORE/Actions";
import {connect} from 'react-redux'
import {NavLink} from "react-router-dom";
import {BackToBrowse} from "./STORE/Actions";
class ViewPetInfo extends Component {

    componentDidMount() {
        this.props.navigateComponent("breed")
    }

    render() {

        let activeComp;
        let tmlStyle;
        let admStyle;
        let brdStyle;
        let profStyle;
        /**/
        if(this.props.activePage === 'timeline'){
            tmlStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp =  <div style={{width:"100%"}}>
                <div className="posts text-center d-flex flex-column justify-content-center mt-5 ">
                    <span>
                        <img src={"/img/shield.svg"} alt="lock" width={90} height={90}/>
                    </span>
                    <span className="mt-3 log-reg">
                        <NavLink to={'/'} onClick={()=>this.props.Back(true)}>Login</NavLink> or <NavLink to={'/registering-user-type'}>Register</NavLink> to see {this.props.pet[0].name}'s posts.
                    </span>
                </div>
            </div>
        }else if (this.props.activePage === 'admirers'){
            admStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="admirer-component">
                <div style={{width:"100%"}}>
                    <div className="posts text-center d-flex flex-column justify-content-center mt-5">
                    <span>
                        <img src={"/img/shield.svg"} alt="lock" width={90} height={90}/>
                    </span>
                        <span className="mt-3 log-reg">
                        <NavLink to={'/'} onClick={()=>this.props.Back(true)}>Login</NavLink> or <NavLink to={'/registering-user-type'}>Register</NavLink> to see {this.props.pet[0].name}'s admirers.
                    </span>
                    </div>
                </div>
            </div>
        }else if (this.props.activePage === 'breed'){
            brdStyle ={
                borderTop:"2px solid darkslategrey",
                fontWeight : "bolder",
                fontFamily : "Cambria",
                color : "darkslategrey"
            };
            activeComp = <div className="breed-component">
                    <p className="browse-pet-breed">
                        {this.props.pet[0].description}
                    </p>
            </div>;
        }

        return (
            <div>
                <div className="row">
                    {/*Pet photo  */}
                    <div className="user-image-holder text-center col-sm-6">
                        {
                            this.props.pet[0].profile_photo === null ?
                                <img src={"/img/rodent.svg"} className="rounded-circle m-3" width="150" height="130" alt={"user"}/>
                                :
                                <img src={`/storage/pets/${this.props.pet[0].id}/${this.props.pet[0].profile_photo}`} className="rounded-circle m-3" width="150" height="130" alt={"user"}/>
                        }
                    </div>

                    {/*Pet relationships*/}
                    <div className="col-sm-12 col-lg-6">
                        <div className="persona mt-5">{/* d-flex justify-content-start align-items-end*/}
                            <div className="mb-3">
                                <span className="pet-name-profile mr-1">{this.props.pet[0].name}</span>
                                <span className="pet-name-profile pet-username-profile">( {this.props.pet[0].username} )</span>
                            </div>
                        </div>
                    </div>

                </div>


                <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light mt-3">
                    <Nav.Item>
                        <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                    </Nav.Item>
                    <Nav.Item onClick={()=>this.props.navigateComponent("admirers")}>
                        <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("admirers")} className='tab-text' style={admStyle}>Admirers (...)</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("breed")} className='tab-text' style={brdStyle}>
                            {this.props.pet[0].breedName}s
                        </Nav.Link>
                    </Nav.Item>
                </Nav>


                <div className="mt-4">
                    <div className="col-sm-12">
                        {activeComp}
                    </div>
                </div>

            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        activePage : state.User.activeComponent,
        // pet : state.Browse.pet
    }
};

const mapDispatch = (dispatch )=>{
    return {
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},
    }
};

export default connect(mapState,mapDispatch)(ViewPetInfo);