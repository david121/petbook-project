import React, {Component} from 'react';
import {connect} from "react-redux";
import './CSS/RegLargeScreen.css';
import './CSS/RegSmallScreen.css';
import './CSS/Register.css';
import {blurHandler, existingUsers, inputHandler, registerSubmitHandler as submitHandler} from './STORE/Actions';
import {withRouter} from "react-router-dom";
import FormError from './FormError';
class RegisterUser extends Component {
    componentDidUpdate(prevProps, prevState, snapshot) {
      //  console.log(sessionStorage.getItem("profileType"));
        if (!sessionStorage.getItem("profileType")){
            this.props.history.push("/")
        }
        if(this.props.registered){
            this.props.history.push("/photo");
        }
    }
    componentDidMount() {
        const {history} = this.props;
        if (!sessionStorage.getItem('profileType')){//session set in UserType.js
            history.push(document.referrer);
        }
    }

    render() {
        // dynamic input field classes
        const fnClass = ['form-control name-input'];
        const lnClass = ['form-control ml-3 name-input'];
        const emailClass =['form-control reg-input'];
        const usernameClass =['form-control reg-input'];
        const passClass =['form-control reg-input'];
        const vpassClass =['form-control reg-input'];
        // const userExistsClass =['form-control reg-input'];
        // const emailExistsClass =['form-control reg-input'];

        let nameErr = ''; let emailErr = ''; let userNErr = ''; let passErr = ''; let vPassErr = ''; let btnDisable = true;
        let userExistsErr = '', emailExistsErr = '';
        //let fnameEmpty = this.props.fnameEmpty?"first name field required":'';

        if (this.props.fnameErr !== '' ){
           fnClass.push('error');
           nameErr = <FormError message={this.props.fnameErr}/>
        }

        if (this.props.lnameErr !== '' ){
            lnClass.push('error');
            nameErr = <FormError message={this.props.lnameErr}/>
        }

        if (this.props.emailErr !== ''){
            emailClass.push('error');
            emailErr = <FormError message={this.props.emailErr}/>
        }
        if (this.props.userErr !== ''){
            usernameClass.push('error');
            userNErr = <FormError message={this.props.userErr}/>
        }
        if (this.props.passwErr !== ''){
            passClass.push('error');
            passErr = <FormError message={this.props.passwErr}/>
        }
        if (this.props.vPasswErr !== ''){
            emailClass.push('error');
            vPassErr = <FormError message={this.props.vPasswErr}/>
        }
        if (this.props.emailExists){
            emailClass.push('error');
            emailErr = <FormError message={"email address already registered"}/>
        }
        if (this.props.usernExists){
            usernameClass.push('error');
            userNErr = <FormError message={"username taken"}/>
        }

        if (this.props.fnameEmpty)
            fnClass.push('error');
        if (this.props.lnameEmpty)
            lnClass.push('error');
        if (this.props.emailEmpty)
            emailClass.push('error');
        if (this.props.usernameEmpty)
            usernameClass.push('error');
        if (this.props.passEmpty)
            passClass.push('error');
        if (this.props.vpassEmpty)
            vpassClass.push('error');

        const data = {
            profileType : this.props.profileType,
            firstname: this.props.firstname,
            lastname: this.props.lastname,
            username: this.props.username,
            email: this.props.email,
            password: this.props.password,
            verifyPass: this.props.verifyPass,
        };
        const error = {
            fnameErr : this.props.fnameErr,
            lnameErr : this.props.lnameErr,
            emailErr: this.props.emailErr,
            userErr: this.props.userErr,
            passwErr: this.props.passwErr,
            vPasswErr: this.props.vPasswErr,
        };
        if (this.props.fnameErr ==="" && this.props.lnameErr === "" && this.props.emailErr ===""
            && this.props.userErr==="" && this.props.passwErr ==="" && this.props.vPasswErr ===""
            && this.props.firstname !=="" && this.props.lastname !=="" && this.props.email !==""
            && this.props.username !=="" && this.props.password !=="" && this.props.verifyPass !=="")
        {
            btnDisable = false
        }
        let regButton = this.props.regLoader ? <button disabled={true} className="register-button">
                 ...
                <span className="spinner-grow spinner-grow-sm"/>
                </button>
            :<button type="submit" className="regBtn">Sign Up</button>;

        return (
            <div>
                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand">Pawrry</span>
                </nav>
            <div className="container reg-form-container">
                <div className="form-wrapper mt-5">

                    <form onSubmit={(e)=>this.props.handleSubmit(e,data,error)} className="mt-4 text-center">
                        <div className="mb-2">
                            <img src={'/img/cat-dog.svg'} alt="breed" width="35" height="35" className={"mr-2"}/><br/><span className="text-center" style={{fontFamily:"Cambria",fontWeight:"bold",color:"darkslategrey"}}> Register</span>
                            {/*<b><img src={'/img/like.svg'} alt="breed" width="35" height="35" className={"mr-2"}/>Pawrry<img src={'/img/like.svg'} alt="breed" width="35" height="35" className={"ml-2"}/></b>*/}
                        </div>
                            <span className="sm_error mb-1">{this.props.fnameErr}</span>
                            <div className = "input-group mb-3 reg-input-wrap">
                                <input type="text" placeholder="First Name" className={fnClass.join(' ')}  autoComplete="nope"  onChange={(e)=>this.props.handleInput(e,'firstname')} required={true} onBlur={(e)=>this.props.handleBlur(e,'fnameEmpty')}/>
                                <input type="text" placeholder="Last Name" className={lnClass.join(' ')} autoComplete="nope" onChange={(e)=>this.props.handleInput(e,'lastname')} required={true} onBlur={(e)=>this.props.handleBlur(e,'lnameEmpty')}/>
                            </div>
                            <span className="lg-error"> {nameErr} </span>

                            <span className="sm_error mb-1">{this.props.emailErr}</span>
                            <span className="sm_error mb-1">{this.props.emailExists? "Email exists" : null}</span>
                            <div className = "form-group mb-3 reg-input-wrap">
                                <input type="email" placeholder="Email Address" className={emailClass.join(' ')} autoComplete="nope" onChange={(e)=>this.props.handleInput(e,'email')} required={true} onBlur={(e)=>this.props.handleBlur(e,'emailEmpty')}/>
                            </div>
                            <div className="lg-error">  {emailErr} </div>

                             <span className="sm_error mb-1">{this.props.userErr}</span>
                             <span className="sm_error mb-1">{this.props.usernExists? "username exists" : null}</span>
                            <div className = "form-group mb-3 reg-input-wrap">
                                <input type="text" placeholder="Username" className={usernameClass.join(' ')} autoComplete="nope" onChange={(e)=>this.props.handleInput(e,'username')} required={true} onBlur={(e)=>this.props.handleBlur(e,'usernameEmpty')}/>
                            </div>
                            <div className="lg-error">
                                {userNErr}
                            </div>

                            <span className="sm_error mb-1">{this.props.passwErr}</span>
                            <div className = "form-group mb-3">
                                <input type="password" minLength="8" placeholder="Password" className={passClass.join(' ')}  onChange={(e)=>this.props.handleInput(e,'password')} required={true} onBlur={(e)=>this.props.handleBlur(e,'passEmpty')}/>
                            </div>
                            <div className="lg-error">
                                {passErr}
                            </div>

                            <span className="sm_error mb-1">{this.props.vPasswErr}</span>
                            <div className = "form-group mb-3">
                                <input type="password" placeholder="Verify Password" className={vpassClass.join(' ')}  onChange={(e)=>this.props.handleInput(e,'verifyPass')} required={true} onBlur={(e)=>this.props.handleBlur(e,'vpassEmpty')}/>
                            </div>
                            <div className="lg-error">
                                {vPassErr}
                            </div>

                            <div className="text-center">
                                {regButton}
                            </div>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}


const MapState = (state) =>{
    return{
        //Check if Fields empty (onBlur)
        fnameEmpty : state.Register.fnameEmpty,
        lnameEmpty : state.Register.lnameEmpty,
        emailEmpty : state.Register.emailEmpty,
        usernameEmpty : state.Register.usernameEmpty,
        passEmpty:state.Register.passEmpty,
        vpassEmpty: state.Register.vpassEmpty,

        //Input values (onChange)
        firstname: state.Register.firstname,
        lastname: state.Register.lastname,
        email: state.Register.email,
        username: state.Register.username,
        password: state.Register.password,
        verifyPass: state.Register.verifyPass,
        profileType:state.Register.type,

        //Input Error (validation onChange)
        fnameErr : state.Register.fnameErr,
        lnameErr : state.Register.lnameErr,
        emailErr: state.Register.emailErr,
        passwErr: state.Register.passwErr,
        userErr: state.Register.userErr,
        vPasswErr: state.Register.vPasswErr,

        disableSubmit : state.Register.submitBtnDisable ,
        usernExists : state.Register.usernameExists ,
        emailExists : state.Register.emailExists ,
        regLoader : state.Register.regLoader,//loader trigger
        registered : state.Register.registered
    }
};

const MapDispatch = (dispatch) =>{
    return{
        handleSubmit : (e,data,error) => {
            e.preventDefault();
            dispatch(submitHandler(data,error));
        },
        handleInput : (e,controlName) => {dispatch(inputHandler(e.target.value,controlName))},
        handleBlur : (e,name) => dispatch(blurHandler(e.target.value,name)),
        //toggle : () =>{dispatch(toggleError())}
    }
};


export default connect(MapState,MapDispatch)(withRouter(RegisterUser));
