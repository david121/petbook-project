import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import './CSS/LayoutLargeScreen.css'
import './CSS/LayoutSmallScreen.css'
class Signup extends Component {
    render() {
        return (
            // <div className="sign-up-comp">
                <div className="sign__up">
                    <h5 className='signup-text'>Sign up and connect with Animal Lovers</h5>
                    <h5 className='text-center signup-text mb-5'>all over the World!</h5>
                    <NavLink className="signupBtn" to="/registering-user-type">Register</NavLink>
                </div>
            // </div>
        );
    }
}

export default connect(null , null)(Signup);
