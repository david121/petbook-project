import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import {checkEmailExists, CheckRecoveryAnswer, mailInputHandler} from "./STORE/Actions";
import VerifyEmail from "./VerifyEmail";
import ChangePassword from "./ChangePassword";
class PasswordReset extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.test_failed){
            window.location.replace('/');
        }
    }

    render() {

        let comp;

        let errorResponse;
        if (this.props.mailNotFound){
            errorResponse =<span className="email-not-found"> {this.props.email} is not registered.</span>
        }

        let question;
        let options;
        if (this.props.question){
            switch (this.props.question.type) {
                case "pet_name":
                    options = this.props.question.options.map((item,index)=>{
                            return (
                                    <li key={index} onClick={()=>this.props.checkAnswer(item,this.props.question.answer,this.props.email)}>
                                        {item}
                                    </li>
                            )
                    });
                    question = <div style={{width:"100%",height:"100%"}}>
                        <div className="text-center m-4 p-3">
                            <span className="recover-question">{this.props.question.question} (<b><small className="text-danger">Attempts:{this.props.attempts}/2</small></b>)</span>
                        </div>

                        <div className="text-center">
                            {
                                this.props.question.photo === null ?
                                    <img src={'/img/rodent.svg'} alt="pet" width={100} height={100}/>:
                                    <img src={`/storage/pets/${this.props.question.petId}/${this.props.question.photo}`} alt="pet" width={100} height={100}/>
                            }
                        </div>
                        <div className="pass-options-wrap text-center">
                            <ul className="text-center pass-options-list">
                                {options}
                            </ul>
                        </div>
                    </div>;break;
                case "lastName":
                    options = this.props.question.options.map((item,index)=>{
                        return (
                            <li key={index} onClick={()=>this.props.checkAnswer(item,this.props.question.answer,this.props.email)}>
                                {item}
                            </li>
                        )
                    });
                    question = <div style={{width:"100%",height:"100%"}}>
                        <div className="text-center m-4 p-3">
                            <span className="recover-question">{this.props.question.question} (<b><small className="text-danger">Attempts:{this.props.attempts}/2</small></b>)</span>
                        </div>
                        <div className="pass-options-wrap text-center">
                            <ul className="text-center pass-options-list">
                                {options}
                            </ul>
                        </div>
                    </div>;break;
            }
        }


        switch (this.props.recoveryComp) {
            case "ENTER_EMAIL":
                comp =<div style={{height:"100%",width:"100%"}}>
                    { this.props.verifyLoader?<div className="verifyActivity"> </div>  :null }
                    <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                        <span className="navbar-brand">Pawrry</span>
                    </nav>
                    <div className="p-3">
                        <NavLink className="rec-back-btn" to={"/"}>Back</NavLink>
                    </div>
                    <div className="container">
                        <div className="text-center code-form-wrap">
                            <div>
                                <span className="enter-v-c">ENTER YOUR EMAIL ADDRESS</span><br/>
                                <small className="v-c-info">(An email containing password reset information will be sent to you)</small>
                            </div>
                            {errorResponse}
                            {this.props.blocked?<span className="blocked-user-message">Please contact our Team.</span>:null}
                            <form onSubmit={(e)=>{e.preventDefault();this.props.checkEmailExists(this.props.email)}} className={"mt-4"}>
                                <div className="">
                                <span>
                                    <input type="email"
                                           required={true}
                                           autoFocus={true}
                                           value={this.props.email}
                                           onChange={this.props.mailInputHandler}
                                           className="pass-reset"
                                           placeholder={"email address"}/>
                                </span>
                                </div>
                                <input type="submit" className={"btn btn-dark mt-5 mb-1 submit-rec-email"} disabled={this.props.email==='' ?true :this.props.emailErr} value={"send verification"}/>
                            </form>
                            <div className="resend-code" onClick={this.props.resendCode}>
                                {/*Resend verification*/}
                            </div>
                        </div>
                    </div>
                </div>;break;

            case "ANSWER_QUESTION" :
                comp = <div style={{height:"100%",width:"100%"}}>
                    { this.props.verifyLoader?<div className="verifyActivity"> </div> :null }
                    <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                        <span className="navbar-brand">Pawrry</span>
                    </nav>
                   {question}
                </div>;break;

            case "PROVIDE_CODE":
                comp = <VerifyEmail context="PASSWORD_RESET" email={this.props.email}/>;break;

            case "CHANGE_PASSWORD":
                comp = <ChangePassword email={this.props.email}/>
        }

        return comp
    }
}

const MapState = (state)=>{
    return{
        email : state.Login.passRecoveryEmail,
        emailErr : state.Login.emailErr,
        verifyLoader : state.Login.verifyLoader,
        recoveryComp : state.Login.recoveryComp,
        mailNotFound : state.Login.mailNotFound,
        question : state.Login.question,
        attempts : state.Login.attempts,
        test_failed : state.Login.test_failed,
        blocked : state.Login.blocked,
    }
};
const MapDispatch =(dispatch) =>{
    return {
        mailInputHandler : (e) =>{dispatch(mailInputHandler(e.target.value))},
        checkEmailExists : (email) =>{dispatch(checkEmailExists(email))},
        checkAnswer : (choice,ans,email) =>{dispatch(CheckRecoveryAnswer(choice,ans,email))}
    }
};

export default connect(MapState,MapDispatch)(PasswordReset);