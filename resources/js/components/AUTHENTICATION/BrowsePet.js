import React, {Component} from 'react';
import axios from 'axios'
import {ViewPet} from "./STORE/Actions";
import {connect} from "react-redux";
class BrowsePet extends Component {
    constructor(props){
        super(props);
        this.state = {
            pets : []
        }
    }
    componentDidMount() {
        axios.get('/api/browse-pets').then(response =>{
            if (response.data.success){
                // console.log(response.data.pets)
                this.setState({pets:response.data.pets})
            }
        }).catch(err=>{

        })

    }

    render() {

        const pets = this.state.pets.map((pet,index)=>{
                return (
                        <div key={index} className="col-sm-6 col-lg-4 col-xl-3 d-flex flex-column text-center mb-2 pet-item" onClick={()=>{this.props.ViewPet(pet.id,this.state.pets)}}>
                            <span>
                                {
                                    pet.profile_photo === null ? <img src={"img/rodent.svg"} alt="pet" width={"65"} height={"65"} className="rounded-circle"/>:
                                        <img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt="pet" width={"65"} height={"65"} className="rounded-circle"/>
                                }
                            </span>
                            <span className="pet_det mt-1">
                                <span>Name : {pet.name}</span><br/>
                                <span>Breed : {pet.breedName}</span>
                            </span>
                        </div>
                )
        });

        return (
            <div className="row mt-4">
                {pets}
            </div>
        );
    }
}

const mapState = (props) =>{
    return{

    }
};
const mapDispatch = (dispatch) =>{
    return {
        ViewPet : (id,pets)=>{dispatch(ViewPet(id,pets))}
    }
};

export default connect(mapState,mapDispatch)(BrowsePet);