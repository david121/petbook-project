import React, {Component} from 'react';
import {connect} from 'react-redux'
import {changePassword, codeInputHandler, passInputHandler, sendCode, signOut, VerifyCode} from "./STORE/Actions";
import FormError from "./FormError";
import {withRouter} from "react-router-dom";

class ChangePassword extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.passChanged){
            this.props.history.push('/');
        }
    }

    render() {
        let passErr;// = this.props.newPassErr ? this.props.newPassErr : this.props.verPassErr? this.props.verPassErr:null;
        let vPassErr;
        if (this.props.newPassErr !== ''){
            passErr = <FormError message={this.props.newPassErr}/>
        }
        if (this.props.verPassErr !== ''){
            // vpassClass.push('error');
            vPassErr = <FormError message={this.props.verPassErr}/>
        }
        let pass = null;
        if (!this.props.verPassErr && !this.props.newPassErr){
            pass = this.props.newPass
        }

        return (
            <div style={{height:"100%",width:"100%"}}>
                { this.props.verifyLoader?<div className="verifyActivity"> </div>  :null }

                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand">Pawrry</span>
                </nav>
                <div className="below-nav">

                    <div className="container">
                        <div className="text-center code-form-wrap">
                            <div>
                                <span className="enter-v-c">CHANGE PASSWORD</span><br/>
                                {/*<small className="v-c-info">(Please enter the code sent to your email)</small>*/}
                            </div>

                            <form onSubmit={(e)=>{e.preventDefault();this.props.changePassword(pass,this.props.email)}} className={"mt-5"}>
                                {this.props.codeResent?<span className="text-success">Verification Code Resent</span> : null}<br/>
                                {this.props.wrongCode? <span className="text-danger">Wrong Verification Code </span> :null}
                                <div className="pass-input-wrap mb-3">
                                    <span>
                                        <input type="password" required={true} onChange={(e)=>{this.props.passInputHandler(e,'newPassword')}} className="pass-input mb-3" placeholder={"new password"}/>
                                        <span>{passErr}</span><br/>
                                    </span>

                                    <span>
                                        <input type="password" required={true} onChange={(e)=>{this.props.passInputHandler(e,'verifyNewPassword')}} className="pass-input" placeholder={"verify new password"}/>
                                        <span>{vPassErr}</span>
                                    </span>
                                </div>
                                <input type="submit" className={"btn btn-success mt-1 mb-1"} disabled={this.props.verPassErr || this.props.newPassErr} value={"change password"}/>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const MapState=(state) =>{
    return{
        newPass:state.Login.newPassword,
        verNewPass:state.Login.verifyNewPassword,
        newPassErr:state.Login.newPassErr,
        verPassErr:state.Login.verPassErr,
        verifyLoader:state.Login.verifyLoader,
        passChanged:state.Login.passChanged,
    }
};
const MapDispatch=(dispatch) =>{
    return{
        changePassword : (password,email) =>{dispatch(changePassword(password,email))},
        passInputHandler : (e,name)=>{dispatch(passInputHandler(e.target.value,name))},
        logout : (history) => {
            dispatch(signOut(history))
        }
    }
};
export default connect(MapState,MapDispatch)(withRouter(ChangePassword));