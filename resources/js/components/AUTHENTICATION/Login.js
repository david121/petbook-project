import React, {Component} from 'react';
import {connect} from "react-redux";
import {inputHandler,loginSubmitHandler as submitHandler} from './STORE/Actions';
import {NavLink, withRouter} from "react-router-dom";
class Login extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.authenticated){
            this.props.history.push("/home");
        }
    }

    render() {
            return (
                    <form onSubmit={(e) => this.props.handleSubmit(e, this.props.email, this.props.password)}>
                        {this.props.passChanged ? <span className="text-success">Your password was changed successfully. You may now login</span> : ''}
                        {this.props.invalid_details ? <span className="invalid-details">Incorrect Email or Password</span> : ''}
                        <div className="input-group">
                            <input type="email" required={true} onChange={(e)=>this.props.handleInput(e,'email')} className="form-control login-mail" placeholder="Email Address" autoFocus={false}/>
                            <input type="password" required={true} onChange={(e)=>this.props.handleInput(e,'password')} className="form-control ml-3 login-pass" placeholder="Password"/>
                        </div>
                        <div className="text-right mt-1">
                            <NavLink to={'/password-recovery'} className="mr-4 reset-password">Reset Password</NavLink>
                        </div>
                        <div className="mt-1 text-center">
                            <input type="submit" className="login-btn" value="Login" disabled={!this.props.password || !this.props.email}/>
                        </div>
                    </form>
        );
    }
}

const MapState = (state) =>{
    return{
        authenticated : state.Login.authenticated,
        user_info : state.Login.user,
        email: state.Login.email,
        password: state.Login.password,
        passChanged:state.Login.passChanged,
        invalid_details:state.Login.invalid_details,
    }
};

const MapDispatch = (dispatch) =>{
    return{
        handleSubmit : (e, email, password) => {
            e.preventDefault();
            dispatch(submitHandler(email, password));
        },
        handleInput : (e,controlName) => {dispatch(inputHandler(e.target.value,controlName))}
    }
};


export default connect(MapState,MapDispatch)(withRouter(Login));
