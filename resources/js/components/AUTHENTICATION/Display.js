import React, {Component} from 'react';
//import Register from './RegisterUser';
import SignUp from './Signup';
import Login from './Login';
import './CSS/LayoutLargeScreen.css';
import './CSS/LayoutSmallScreen.css';
import {connect} from 'react-redux';
import BrowsePet from './BrowsePet'
import {BackToBrowse} from "./STORE/Actions";
import ViewPetInfo from "./ViewPetInfo";
class DisplayIndex extends Component {
    componentDidMount() {
        const {history} = this.props;
        if (localStorage.getItem('token')){
            history.push('/home');
        }
        document.title= "Pawrry | Sign Up | Login";
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // this.props.view ? this.setState({comp:"VIEW_PET"}) : null
    }

    constructor(props){
        super(props);
        this.state={
            comp: "AUTH",
            browse:false,
            viewPet:false
        }
    }

    render() {
        let renderPage ;
        if (this.props.view){
            renderPage = <div className="view-pet-info">
                <span className="bg-dark text-light p-2 mt-5 mb-3" style={{cursor:"pointer"}} onClick={()=>this.props.Back(false)}>Back</span>
                <ViewPetInfo pet = {this.props.pet} Back={this.props.Back}/>
            </div>
        } else
        switch (this.props.activeComp) {
            case "AUTH":
                renderPage = (
                    <div>
                        <div className="login-comp">
                            <Login/>
                        </div>
                        <div className="sign-up-comp">
                            <SignUp/>
                        </div>
                        <div className="about-comp">
                            <span className="mr-5 p-3" onClick={()=>{this.props.Change("ABOUT")}}>
                                <img src={"/img/question.svg"} alt="about" width={40} height={40}/>
                            </span>
                        </div>
                    </div>
                );break;
            case "ABOUT":
                renderPage=(
                    <div className="mb-5 about-page">
                        <div>
                            <span className="bg-dark text-light p-2 mt-5 mb-3" style={{cursor:"pointer"}} onClick={()=>{this.props.Change("AUTH")}}>Back</span> {/*this.setState({comp:"AUTH"})*/}
                        </div>
                        <p className="p-3 mb-4 site-note">
                            <span className="note">
                                Entertainment, compassion, companionship - whatever reasons are behind the nurturing of a Cat or Dog, one thing is without the shadow of a doubt - pet owners
                                make a wide range of significant investments in their furry buddies such as financial, time, and even vast, emotions.
                                Thus, it is rather agreeable to devise a network in the mould of a community, albeit for pets and their owners, hence, Pawrry. <br/><br/>
                                This platform is an avenue for dog and/or cat owners to put their companion animals in the spotlight, learn about every breed they come across, and also,
                                make friends with other pet owners. Furthermore, with Listing services available, finding or putting a pet up for adoption and mating is a cinch! <br/><br/>
                            </span>
                            <span className="mt-3 text-center">
                                <span><b>David Olurebi</b></span><br/>
                                <i><small><b>Founder</b></small></i>
                            </span>
                        </p>

                        {/* <div style={{width:"100%"}}>
                            <div className="mb-2">
                                <span className="partner-sponsor">Partner / Sponsor ?</span>
                            </div>
                        </div>*/}

                        <div style={{width:"100%"}} className="mb-2">
                            <div className="text-center">
                                <span className="browse-pets" onClick={()=>{this.setState({browse:true})}}> Browse Pets</span>
                            </div>
                        </div>
                        {this.state.browse ? <BrowsePet/> : null}
                    </div>
                );break;
            case "VIEW_PET":
                renderPage = (
                        <div>

                        </div>
                );break;
        }

        return (
                <div className="row">

                    {/*Site Information*/}
                    <div className="col-sm-6 left">

                        <div className="text-center">
                            <h2 className="site-heading">Pawrry</h2>
                            {/*<img src={"/img/paw-add.svg"} alt="paw" width={40} height={40}/>*/}
                        </div>

                        <ul className="intro-wrapper">
                            <li className="intro-list">
                                <span>Connect with pet lovers around the world</span>
                            </li>
                            <li className="intro-list">
                                <span>Discover variety of pets and breeds</span>
                            </li>
                            <li className="intro-list">
                                <span>Give your pet social recognition</span>
                            </li>
                        </ul>
                    </div>

                    {/*Login / Register Components*/}
                    <div className="col-sm-6 right">
                        {renderPage}
                    </div>
                    </div>
        );
    }
}

const mapState = (state)=>{
    return{
        pet: state.Browse.pet,
        view :state.Browse.view,
        activeComp :state.Browse.activeComp,
    }
};
const mapDispatch = (dispatch)=>{
    return{
        Back : (login)=>{dispatch(BackToBrowse(login))},
        Change: (comp)=>dispatch({type:"CHANGE_COMP",comp}),
        // ViewPet: ()=>dispatch({type:"CHANGE_COMP"})
        // Change : ()=>{dispatch(BackToBrowse())}
    }
};
export default connect(mapState,mapDispatch)(DisplayIndex);
