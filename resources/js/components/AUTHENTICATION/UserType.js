import React, {Component} from 'react';
import './CSS/Usertype.css';
import {ButtonToolbar,Tooltip,OverlayTrigger,Button} from "react-bootstrap";
import {connect} from "react-redux";
import {profileType} from './STORE/Actions';
import {withRouter} from "react-router-dom";
class UserType extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        //const {history} = this.props;
        if (this.props.user_type === 'petOwner'){
            sessionStorage.setItem('profileType',this.props.user_type);
           //console.log('petowner');
             this.props.history.push('/register-pet-owner');
        }else
         if (this.props.user_type === 'petStore'){
             sessionStorage.setItem('profileType',this.props.user_type);
             history.push('/register-pet-store');
            }
    }

    render() {

        return (
            <div>
                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand">Pawrry</span>
                </nav>
              <div className="container text-center">
                  <div className="d-flex flex-column justify-content-center align-items-center button-box">

                      <h2 className="p-5">What profile are you setting up ?</h2>

                      <ButtonToolbar className="mb-3">
                              <OverlayTrigger
                                  key="left"
                                  placement="left"
                                  overlay={
                                      <Tooltip id={`tooltip-left`} >
                                          Profile for individuals who own or want a companion animal.
                                      </Tooltip>
                                  }
                              >
                                  <Button className="user-type-btn" value="petOwner" onClick={this.props.type} >Pet Owner</Button>
                              </OverlayTrigger>
                      </ButtonToolbar>

                      <ButtonToolbar>
                          <OverlayTrigger
                              key="right"
                              placement="right"
                              overlay={
                                  <Tooltip id={`tooltip-right`}>
                                      Profile for entities that sells pet products.
                                  </Tooltip>
                              }
                          >
                              <Button className="user-type-btn" value="petStore" variant="secondary" disabled={true} onClick={this.props.type}>Pet Store</Button>
                          </OverlayTrigger>
                      </ButtonToolbar>

                  </div>
              </div>
            </div>
        );
    }
}
const MapState = (state) =>{
    return{
        user_type: state.Register.type,
    }
};
const MapDispatch = (dispatch)=>{
    return{
        type : (e)=> dispatch(profileType(e.target.value))
    }
};

export default connect(MapState, MapDispatch)(withRouter(UserType));
