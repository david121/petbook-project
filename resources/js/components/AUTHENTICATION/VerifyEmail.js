import React, {Component} from 'react';
import './CSS/VerifyEmail.css'
import {NavLink, withRouter} from "react-router-dom";
import {connect} from "react-redux"
import {codeInputHandler, sendCode, signOut, VerifyCode} from "./STORE/Actions";
import Process from "../LAYOUT/Process";
class VerifyEmail extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (localStorage.getItem('token')){
            if (!localStorage.getItem('authenticate')){
                this.props.history.push('/home');
            }
        }

    }

    render() {
        const code = parseInt(this.props.input1+""+this.props.input2+""+this.props.input3+""+this.props.input4+""+this.props.input5);

        return (
            <div style={{height:"100%",width:"100%"}}>
                { this.props.verifyLoader?<Process/>  :null }

                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand"><NavLink to={'/'} style={{color:"white",textDecoration:"none"}}>Pawrry</NavLink></span>
                    {this.props.context !== "PASSWORD_RESET"?
                        <span className="d-flex justify-content-end" style={{width:"80%",cursor:"pointer"}} onClick={()=>this.props.logout(this.props.history)}>
                            Logout
                        </span>:null
                    }

                </nav>
                <div className="below-nav">

                    <div className="container">
                        <div className="text-center code-form-wrap">
                            <div>
                                <span className="enter-v-c">ENTER VERIFICATION CODE</span><br/>
                                <small className="v-c-info">(Please enter the code sent to your email)</small>
                            </div>
                            <form onSubmit={(e)=>{e.preventDefault();this.props.verify(code,this.props.context,this.props.email)}} className={"mt-3"}>
                                {this.props.codeResent?<span className="text-success code-resent">Verification Code Resent</span> : null}<br/>
                                {this.props.wrongCode? <span className="text-danger invalid-code">Invalid Code </span> :null}
                                <span><small className="text-info"><b>Note: </b>Please make sure to check both inbox and junk/spam. Email delivery might take a few minutes. </small></span>
                                <div className="row input-wrap">
                                    <span><input id="ver-inp-1" type="text" onKeyUp={()=>{this.props.input1?document.getElementById('ver-inp-2').focus():null}}
                                                 required={true} autoFocus={true} value={this.props.input1} onChange={(e)=>{this.props.codeInputHandler(e,'input1')}}  maxLength={1} className="code-input" placeholder={"-"}/></span>
                                    <span><input id="ver-inp-2" type="text" onKeyUp={()=>{this.props.input2?document.getElementById('ver-inp-3').focus():null}}
                                                 required={true} value={this.props.input2} onChange={(e)=>{this.props.codeInputHandler(e,'input2')}} className="code-input" maxLength={1} placeholder={"-"}/></span>
                                    <span><input id="ver-inp-3" type="text" onKeyUp={()=>{this.props.input3?document.getElementById('ver-inp-4').focus():null}}
                                                 required={true} value={this.props.input3} onChange={(e)=>{this.props.codeInputHandler(e,'input3')}} className="code-input" maxLength={1} placeholder={"-"}/></span>
                                    <span><input id="ver-inp-4" type="text" onKeyUp={()=>{this.props.input4?document.getElementById('ver-inp-5').focus():null}}
                                                 required={true} value={this.props.input4} onChange={(e)=>{this.props.codeInputHandler(e,'input4')}} className="code-input" maxLength={1} placeholder={"-"}/></span>
                                    <span><input id="ver-inp-5" type="text"
                                                 required={true} value={this.props.input5} onChange={(e)=>{this.props.codeInputHandler(e,'input5')}} className="code-input" maxLength={1} placeholder={"-"}/></span>
                                </div>
                                <input type="submit" className={"btn btn-success mt-1 mb-1"} value={"verify"}/>
                            </form>
                            <div className="resend-code" onClick={()=>this.props.resendCode(this.props.context,this.props.email)}>
                                Resend Code
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const MapState=(state) =>{
    return{
        input1:state.Login.input1,
        input2:state.Login.input2,
        input3:state.Login.input3,
        input4:state.Login.input4,
        input5:state.Login.input5,
        verifyLoader:state.Login.verifyLoader,
        codeResent:state.Login.codeResent,
        verified:state.Login.verified,
        wrongCode:state.Login.wrongCode,
    }
};
const MapDispatch=(dispatch) =>{
    return{
        verify : (code,context,email) =>{dispatch(VerifyCode(code,context,email))},
        codeInputHandler : (e,name,filled)=>{dispatch(codeInputHandler(e.target.value,name,filled))},
        resendCode : (context,email) =>{dispatch(sendCode(context,email))},
        logout : (history) => {
            dispatch(signOut(history))
        }
    }
};

export default connect(MapState,MapDispatch)(withRouter(VerifyEmail));