import {fetchUser} from './../../../USER PROFILE/STORE/Actions'
import axios from 'axios';

export const fetchQuestions = () =>{
    return dispatch => {

        axios.get('api/fetchQuestions',{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }})
            .then(response=>{
                if (response.data.success){
                 dispatch({
                        type: "PET_QUESTIONS",
                        value: response.data.questions
                 })
                }
            })
            .catch(error=>{
                alert(error)
            })

    }
};
export const fetchPetTypes = () =>{
    return dispatch => {

        axios.get('api/fetchType',{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }})
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type: "PET_TYPES",
                        value: response.data.types
                    })
                }
            })
            .catch(error=>{
                alert(error)
            })

    }
};
export const aboutBreed = (breed) =>{
    return dispatch => {

        axios.get('/api/aboutBreed',{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },params:{
                breed: breed
            }})
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type: "ABOUT_BREED",
                        value: response.data.about
                    })
                }
            })
            .catch(error=>{
                alert(error)
            })

    }
};
export const newPet = (data) =>{
    return dispatch =>{
        axios.post('api/pet',data,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }
        }).then(response=>{
                if (response.data.success){
                    dispatch(fetchUser());
                    dispatch({
                        type : "NEW_PET"
                    })
                }else
                {
                    alert(response.data.msg);
                }
            })
            .catch(error=>{
                console.log("FROM REG PET ACTION", error);
            });

    }
};

export const petAnswers = (event,name) =>{
    return dispatch =>{
        let value = event.target.value;
         if (event.target.type === 'file'){
            value = event.target.files[0];
        }
         if (name === 'type'){
             axios.get('api/fetchBreed',{headers:{
                     "Content-Type":"application/json",
                     "Accept":"application/json",
                     "Authorization":"Bearer "+ localStorage.getItem('token')
                 },
                 params:{
                     type:value
                 }
             })
                 .then(response=>{
                     if (response.data.success){
                         dispatch({
                             type : "PET_SPEC",
                             withBreeds: true,
                             name: name,
                             value: value,
                             breeds : response.data.breeds
                         })
                     }
                     else console.log(response.response);
                 }).catch(error=>{
                 alert(error)
             });
         }else
         if (name === 'breed'){
             axios.get('api/fetchGender',{headers:{
                     "Content-Type":"application/json",
                     "Accept":"application/json",
                     "Authorization":"Bearer "+ localStorage.getItem('token')
                 },
             })
                 .then(response=>{
                     if (response.data.success){
                         dispatch({
                             type : "PET_SPEC",
                             withGender: true,
                             name: name,
                             value: value,
                             gender : response.data.gender
                         })
                     }
                     else console.log(response.response);
                 }).catch(error=>{
                 alert(error)
             });
         }
         else{
            dispatch({
                type : "PET_SPEC",
                name: name,
                value: value,
            })
        }
    }
};

export const incrementQuestion  = () =>{
    return dispatch =>{
        dispatch({
            type : "INCREMENT_QUESTION",
        })
    }
};
export const prevQuestion  = () =>{
    return dispatch =>{
        dispatch({
            type : "PREVIOUS_QUESTION",
        })
    }
};
