
const initialState = {
    questions : [],
    quesIndex: 0,
    petTypes : [],
    petBreeds: [],
    genders : [],
    name : '',
    type : '',
    breed : '',
    gender : '',
    age : '',
    username:'',
    photo:null,

    question_answered: false,
    disable : true,

    //control server request
    fetchPetQuestions : true,
    fetchPetTypes :true,
    //
    registered: false
};

const RegPet = (state = initialState, actions)=>{

    switch (actions.type) {
        case "PET_QUESTIONS":
            return{
                ...state,
                questions: actions.value,
                fetchPetQuestions: false
            };

        case "PET_TYPES":
            return{
                ...state,
                petTypes: actions.value,
                fetchPetTypes :false,

            };

        case "PET_SPEC":
            if (actions.value !== ''){
                if (actions.withBreeds){
                    return{
                        ...state,
                        [actions.name]: actions.value,
                        petBreeds: actions.breeds,
                        question_answered: true,
                        disable: false
                    };
                }else if (actions.withGender){
                   // console.log(actions.gender, "FROM REGPET REDUCER");
                    return{
                        ...state,
                        [actions.name]: actions.value,
                        genders: actions.gender,
                        question_answered: true,
                        disable: false
                    };
                }else{
                    return{
                        ...state,
                        [actions.name]: actions.value,
                        question_answered: true,
                        disable: false
                    };
                }
            }
            return {
              ...state,
              question_answered: false,
                disable: true
            };

        case "INCREMENT_QUESTION":
            let currIndex = state.quesIndex;
            let prevIndex;
            let newIndex;
            if (state.question_answered){
                prevIndex = currIndex;
                newIndex = currIndex +1;
                return {
                    ...state,
                    quesIndex: newIndex,
                    question_answered: false //why?
                }
            }
            return{
                ...state,
                quesIndex: prevIndex
            };
        case "PREVIOUS_QUESTION":
             let prev = state.quesIndex -1;

            return{
                ...state,
                quesIndex: prev,
                question_answered: false
            };
        case "NEW_PET":
            return{
                ...state,
                registered:true
            };
        default: return state;
    }

};

export default RegPet;
