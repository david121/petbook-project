import React, {Component} from 'react';
import {connect} from "react-redux";
import './CSS/AddPet.css'
import {fetchQuestions, fetchPetTypes,petAnswers,incrementQuestion,newPet,prevQuestion} from './STORE/Actions';
import {NavLink, withRouter} from "react-router-dom";

class DynamicPetQuestions extends Component {

    componentDidMount() {
      //  if (this.props.fetchPetQuestions)
        this.props.questions();
      //  if (this.props.fetchPetTypes)
        this.props.fetchTypes();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
          // if(this.props.reg)
          // this.props.history.push('/profile')
    }

    render() {

        const questionLength = this.props.question.length;
        let option;

        let types = this.props.petTypes.map((type,index)=>{
            return <option className="select-options" key={index} value={type.id}>{type.type}</option>
        });

        let btnMode = !this.props.answered;
        let nextBtn = this.props.index +1 !== questionLength ? <button disabled={btnMode} className="nextBtn" onClick={this.props.increment}>Next</button> : <button type="submit" className="create-pet">Finish</button> ;
        let prevBtn = this.props.index > 0 ? <button className="mr-2 prevBtn" onClick={this.props.decrement}>Back</button> : '';

        const registration = this.props.question.map((question,index)=>{

            if (question.type === 'type'){
                option = types
            }

            if(question.type === 'breed'){
                option = 'fetching breeds';
                if (Object.keys(this.props.petBreeds).length){
                    option = this.props.petBreeds.map((breed,index)=>{
                        return <option className="select-options" key={index} value={breed.breed}> {breed.breed}</option>
                    })
                }
            }
            if (question.type === 'gender'){
                if (Object.keys(this.props.petGender).length){
                    option = this.props.petGender.map((gender,index)=>{
                        return <option className="select-options" key={index} value={gender.gender}> {gender.gender}</option>
                    })
                }
            }

             let input = question.hasOptions === 0 ? <input type={question.input_type} autoFocus={true} className="question-input" onChange={(e)=>this.props.petSpec(e,question.type)} /> : <select className="options-select" onChange={(e)=>this.props.petSpec(e,question.type)}> {option} </select> ;

              return <div key={index} className="questionBox text-center">

                    <div className="question-wrapper">
                        <span>{question.question}</span>
                    </div>
                  <div className="inputBox">
                      {input}
                  </div>

                  <div className="">
                      {prevBtn}{nextBtn}
                  </div>

                  <NavLink to={"/home"}>I'll do this later</NavLink>

              </div>
        });

        const form = new FormData();
        form.append('user_id', this.props.user.id);
        form.append('name', this.props.name);
        form.append('gender', this.props.gender);
        form.append('type', this.props.type);
        form.append('breed', this.props.breed);
        form.append('age', this.props.age);
        form.append('username', this.props.username);
        form.append('photo', this.props.file);

        return (
            <div>
                <form onSubmit={(e)=>this.props.regPet(e,form)}>
                    {registration[this.props.index]}
                </form>
            </div>
        );
    }
}

const MapState = (state) =>{
    return{

        //control server request
            fetchPetQuestions : state.AddPet.fetchPetQuestions,
            fetchPetTypes :state.AddPet.fetchPetTypes,
        //
        question : state.AddPet.questions,
        index : state.AddPet.quesIndex,
        petTypes : state.AddPet.petTypes,
        petBreeds : state.AddPet.petBreeds,
        petGender : state.AddPet.genders,
        answered : state.AddPet.question_answered,
        disable : state.AddPet.disable,
        user: state.User.user_info ,

        type : state.AddPet.type,
        name : state.AddPet.name,
        gender: state.AddPet.gender,
        username : state.AddPet.username,
        age : state.AddPet.age,
        breed : state.AddPet.breed,
        file : state.AddPet.photo,

        reg : state.AddPet.registered

    }
};

const MapDispatch = (dispatch) =>{
    return{
        questions : () => {dispatch(fetchQuestions())},
        fetchTypes : ()=> {dispatch(fetchPetTypes())},
        petSpec : (e,name) => {dispatch(petAnswers(e,name))},
        increment : () => {dispatch(incrementQuestion())},
        decrement : () => {dispatch(prevQuestion())},
        regPet: (e,data)=> {
            e.preventDefault();
            dispatch(newPet(data))
        }
    }
};

export default connect(MapState,MapDispatch)(withRouter(DynamicPetQuestions));
