import React, {Component} from 'react';
import {fileHandler,nextHandler,prevHandler,petQuestions,petSubmitHandler} from './../STORE/Actions';
import './CSS/Pet.css';
import {connect} from 'react-redux';
import {NavLink, withRouter} from "react-router-dom";
import {fetchPetTypes} from "./STORE/Actions";
import moment from "moment";
class Pet extends Component {
    componentDidMount() {
        if (!this.props.userRegistered){
            this.props.history.push("/")
        }
        this.props.fetchTypes()
    }
    componentDidUpdate(nextProps, nextState, nextContext) {
        const {history} = this.props;
        if (this.props.reg){
            history.push('/home');
        }
        // console.log("COMP",this.props.pet_breeds)
    }
    render() {

        let ind = this.props.questionIndex;
        let input ;//to be input element to answer question
        let next;// to be button to load next question
        let previous;//to be button to load previous question
        let btnMode ; // next button disabled or not
        let typeSelect;

        let Age = moment.duration(moment().diff(this.props.age))._data;
        let years = Age.years === 0 ? '' : Age.years === 1 ? '1 year' : Age.years+' years';
        let months = Age.months === 0 ? '' : Age.months === 1 ? '1 month' : Age.months+' months';
        let days = Age.days === 0 ? '' : Age.days === 1 ? '1 day' : Age.days+' days ';

        // loop through and render questions
        const question = this.props.question.map((item,index)=>{
            return <h6 key={index} className="question">{item.q}</h6>
        });

        let types = this.props.pet_types.map((type,index)=>{
            return <option className="select-options" key={index} value={type.id}>{type.name}</option>
        });


        let breeds = this.props.pet_breeds.map((breed,index)=>{
            return <option className="select-options" key={index} value={breed.id}>{breed.name}</option>
        });


        //inputs for questions
        if (ind === 0){
            input = <select name="petType" className="text-center answerInput" onChange={(e)=>this.props.answer(e,"pet_type",this.props.pet_types)}>
                <option value="">Choose Type</option>
                        {types}
            </select>
        }else
        if (ind === 1){
            input = <input type="text" value={this.props.name} onChange={this.props.answer} className="answerInput"/>
        }else
         if (ind === 2){
            input = <select name="petBreed" className="text-center answerInput" onChange={(e)=>this.props.answer(e,"pet_breed",'',this.props.pet_breeds)}>
                <option value="">Choose Breed</option>
                {breeds}
            </select>
        }else
         if (ind === 3){
             input = <select name="gender" className="text-center answerInput" onChange={this.props.answer}>
                 <option value=""> </option>
                 <option value="Male">Male</option>
                 <option value="Female">Female</option>
             </select>
         }else
        if (ind === 4){
            input = <input type="date" value={this.props.age} onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 5){
            input = <input type="text" value={this.props.username} onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 6){
            input = <input type="file" value={''} onChange={this.props.handleFile} className=""/>
        }

        //button disabled if not answered
        btnMode = !this.props.answered;

        // Next / Submit Button render
        if (ind + 1 !== this.props.question.length){
            next = <button type="button" className="loadBtn" disabled={btnMode} onClick={this.props.nextQuestion}>Next</button>
        }else
        if (ind + 1 === this.props.question.length) {
            next = <button type="submit" className="loadBtn" disabled={btnMode}>submit</button>;
        }

        //Previous Btn render
        if (ind === 0){
            previous = <button type="button" className="loadBtn" disabled={true} onClick={this.props.prevQuestion}>Previous</button>
        }else previous = <button type="button" className="loadBtn" onClick={this.props.prevQuestion}>Previous</button>;


        // assigning answered pet details to variables to be rendered
        let type;let breed;let bd;let usname; let name; let gender; let photo;

        type = this.props.type?
                <div className="p-1"><h5 className="detail ">Type </h5><span className="reg-answer">{this.props.typeName}</span></div> : null;
        breed = this.props.breed?
                <div className="p-1"><h5 className="detail">Breed </h5><span className="reg-answer">{this.props.breedName}</span></div> : null;
        gender = this.props.gender?
                <div className="p-1"><h5 className="detail">Gender </h5><span className="reg-answer">{this.props.gender}</span></div> : null;

        name = this.props.name?
            <div className="p-1"><h5 className="detail">Name </h5><span className="reg-answer">{this.props.name}</span></div> : null;
        bd = this.props.age?
                    <div className="p-1"><h5 className="detail">Age </h5>
                        <span className="reg-answer">{years} {months} {days}</span>
                    </div> : null;
        usname = this.props.username?
                <div className="p-1"><h5 className="detail">Username </h5><span className="reg-answer">{this.props.breedName}</span></div> : null;

            photo = this.props.photo?
            <div className="p-2"><img className="rounded-circle" src={this.props.photo} alt="pet-photo" width="180" height="180"/></div> : null;

        let nameSmall = this.props.name?<span className="mr-2"><b>Name:</b> <span className="reg-answer">{this.props.name}</span></span>
                : '';
        let typeSmall = this.props.type? <span className="mr-2"><b>Type: </b><span className="reg-answer">{this.props.typeName}</span></span>
                : '';
        let breedSmall = this.props.breed? <span className="mr-2"><b>Breed:</b><span className="reg-answer">{this.props.breedName}</span></span>
                : '';
        let genderSmall = this.props.gender? <span className="mr-2"><b>Gender:</b><span className="reg-answer">{this.props.gender}</span></span>
                : '';
        let usnSmall = this.props.username? <span className="mr-2"><b>Username:</b><span className="reg-answer">{this.props.username}</span></span>
                : '';
        let bdSmall = this.props.age? <span className="mr-2"><b>Age:</b><span className="reg-answer">{years} {months} {days}</span></span>
                : '';
        let photoSmall = this.props.photo? <img src={this.props.photo} className="rounded-circle" alt="pet" width={"180"}/>
                : '';


        const formData = new FormData();
        formData.append('user_id', sessionStorage.getItem('user'));
        formData.append('name', this.props.name);
        formData.append('gender', this.props.gender);
        formData.append('type', this.props.type);
        formData.append('breed', this.props.breed);
        formData.append('age', this.props.age);
        formData.append('username', this.props.username);
        formData.append('photo', this.props.file);

            //RENDER ELEMENTS
        return (
            <div>
                <nav className="navbar navbar-expand-sm navbar-dark sign-up-nav">
                    <span className="navbar-brand">Petbook</span>
                </nav>

                <div className="container">

                    <div className="row">

                    <div id="petQuestions" className="col-sm-9 text-center">
                        <h3 id="pageTopic">REGISTER A PET</h3>

                        <div className="text-center sm_petDetails">
                            <div className="">
                                <div className={"text-center"}>
                                    {photoSmall}
                                </div>
                                <div className="p-2">
                                    {typeSmall}
                                    {nameSmall}
                                    {breedSmall}
                                    {genderSmall}
                                    {bdSmall}
                                    {usnSmall}
                                </div>
                            </div>
                        </div>


                        <div className="registration-progress"> </div>
                        <form onSubmit={(e)=>this.props.handleSubmit(e,formData)}>
                            <div className="questionBox">{question[this.props.questionIndex]}</div>

                            <div className="inputBox">{input}</div>

                            <div className="loadNext">{previous} {next}</div>

                        </form>
                        <div className="later text-right"> <NavLink to="/home">I'll do this later</NavLink></div>
                    </div>

                    <div className="petDetails col-sm-3 mt-2 text-center lg_petDetails">
                        {photo}
                        {type}
                        {name}
                        {breed}
                        {gender}
                        {bd}
                        {usname}
                    </div>

                </div>
                </div>
               </div>
        );
    }
}


//MapStateToProps
const MapState = (state) =>{
    return{
        userRegistered : state.Register.registered,

        question : state.PetReg.questions,
        questionIndex : state.PetReg.currentQues,
        pet_types :state.PetReg.pet_types,
        pet_breeds : state.PetReg.pet_breeds,
        typeName: state.PetReg.typeName,
        breedName: state.PetReg.breedName,
        //Was question answered?
        answered :state.PetReg.answered ,
        //pet details
        type: state.PetReg.type,
        breed: state.PetReg.breed,
        age : state.PetReg.age,
        name : state.PetReg.name,
        gender : state.PetReg.gender,
        file : state.PetReg.file,
        photo: state.PetReg.photo,
        username: state.PetReg.username,
        //Pet Age components
        year : state.PetReg.years,
        month : state.PetReg.months,
        day : state.PetReg.days,
        reg : state.PetReg.registered
    }
};

//MapDispatchToProps
const MapDispatch = (dispatch) =>{
        return{
            fetchTypes : ()=> {dispatch(fetchPetTypes())},
            answer : (e,name,types,breeds) => {dispatch(petQuestions(e.target.value,name,types,breeds))},
            nextQuestion : () =>{dispatch(nextHandler())},
            prevQuestion : () => {dispatch(prevHandler())},
            handleFile : (e) => {dispatch(fileHandler(e.target.files[0]))},
            handleSubmit: (e,data) => {
                e.preventDefault();
                dispatch(petSubmitHandler(data,'auth'))
            }
        }
};

export default connect(MapState,MapDispatch)(withRouter(Pet));
