import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
// import 'socket.io-client/dist/socket.io';
import {Provider} from 'react-redux';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import {StyleRoot} from 'radium';
import thunk from 'redux-thunk';
import {BrowserRouter} from "react-router-dom";

import App from './App';
import Login from './AUTHENTICATION/STORE/LoginReducer';
import Register from './AUTHENTICATION/STORE/RegisterReducer';
import Pet from './AUTHENTICATION/STORE/PetReducer';
import RegPet from './AUTHENTICATION/REGISTER PET/STORE/RegPetReducer';
import UserProfile from './USER PROFILE/STORE/Reducer';
import Layout from './LAYOUT/STORE/LayoutReducer';
import Home from './HOME/STORE/HomeReducer';
import PostsReducer from './POSTS/STORE/PostsReducer';
import Comments from  './POSTS/STORE/CommentsReducer'
import Search from "./SEARCH/SearchReducer";
import Test from './TESTS/TestReducer';
import PetProfile from './USER PROFILE/STORE/PetReducer';
import Activities from './ACTIVITIES/Store/ActivityReducer';
import AboutPet from './PET/STORE/PetProfileReducer';
import SlideShow from './POSTS/STORE/SlideShowReducer';
import Visitor from './VISITING PROFILE/STORE/VisitorReducer';
import Notifications from "./LAYOUT/STORE/NotifyReducer";
import VisitorFriends from './USER_FRIENDS/STORE/FriendsReducer.js';
import EditPet from "./PET/STORE/EditPetReducer";
import Archive from './ARCHIVE/STORE/ArchiveReducer.js';
import ManagePet from "./PET/STORE/ManagePetReducer";
import ListingReducer from "./LISTING/STORE/ListingsReducer";
import ChatReducer from './MESSAGING/STORE/ChatReducer.js';
import FilterListing from "./LISTING/STORE/FilterReducer";
import BrowseReducer from "./AUTHENTICATION/STORE/BrowseReducer";
import MyReducer from "./DAVID/STORE/MyReducer";
const reducers = combineReducers( {
        Register : Register,
        Browse: BrowseReducer,
        PetReg : Pet,
        AddPet : RegPet,
        Login : Login,
        User : UserProfile,
        Search : Search,
        Layout: Layout,
        Home : Home,
        Posts : PostsReducer,
        SlideShow : SlideShow,
        PetProfile: PetProfile,
        Tests : Test,
        Comments:Comments,
        Activities: Activities,
        AboutPet : AboutPet,
        Visitor : Visitor,
        Notifications : Notifications,
        VisitorFriends : VisitorFriends,
        EditPet : EditPet,
        Archive:Archive,
        ManagePet : ManagePet,
        Listing : ListingReducer,
        Chat : ChatReducer,
        Filter : FilterListing,
        MyProfile : MyReducer,
});
export const ENDPOINT = 'https://pawrry.com:5000';
export const axiosHeader = {
    "Content-type" : "application/json",
    "Accept" : "application/json",
    "Authorization" : "Bearer " + localStorage.getItem('token')
};

const closeModalMiddleware = (store) => {
    return (next) => {
        return (actions) => {
            if(actions.type === "TOGGLE_ADD_PET" && store.getState().Posts.showAddPet === false && actions.closeAction){
                return;
            }
            if(actions.type === "SELECT_PET" && store.getState().Posts.selectPet === false && actions.closeAction){
                return;
            }

            if(actions.type === "POST_OPTIONS" && store.getState().Posts.toggleOptions === false && actions.closeAction){
                return;
            }

            if(actions.type === "ABOUT_VISITOR_PET" && store.getState().Visitor.aboutPet === false && actions.closeAction){
                return;
            }

            if(actions.type === "ABOUT_PET" && store.getState().AboutPet.aboutPet === false && actions.closeAction){
                return;
            }
            if(actions.type === "TOGGLE_VISITOR_FRIENDS" && store.getState().VisitorFriends.showFriends === false && actions.closeAction){
                return;
            }
            return next(actions);
        }
    }
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const globalStore = createStore(reducers, composeEnhancers(applyMiddleware(thunk, closeModalMiddleware)),);

// console.log(globalStore.getState());

if (document.getElementById('root')) {
    ReactDOM.render(
        <Provider store={globalStore}>
            <BrowserRouter>
                <StyleRoot>
                    <App />
                </StyleRoot>
            </BrowserRouter>
        </Provider>
        ,
        document.getElementById('root'));
}
