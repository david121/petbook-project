
const initialState = {
    //message from visitor page
    messageModal: false,
    visitorMessage:'',
    sendingMessage:false,

    //chat Large Screen
    authUser:{},
    friendData :{},
    writtenMessage: '',
    messages : [],
    chatId : null,
    chatList : [],
    chatUser : false,
    friendId : null,
    contactId : null,
    friend:{},

    //Mobile Screen Chat
    screenWidth:'',
    componentSM:'LIST',
    openChat:false,

    //search
    search_value:'',
    friend_searched:{},

};

const ChatReducer = (state = initialState, actions) =>{
    switch (actions.type) {
        case 'SCREEN_SIZE':
            return{
                ...state,
                screenWidth : actions.width
            };
        case 'OPEN_CHAT':
            return{
                ...state,
                authUser : actions.authUser,
                friendData : actions.friend
            };
        case 'CHAT_NEEDLE':
            return{
                ...state,
                search_value:actions.value,
                friend_searched:actions.friend,
            };
        case 'CHAT_USER':
            // console.log("poop")
            return{
                ...state,
                chatUser: !state.chatUser,
                friendId : actions.friend.id ,
                contactId: actions.contactId,
                friend : actions.friend,
            };
        case 'CHAT_USER_MOBILE':
            return{
                ...state,
                openChat: true,
                friendId : actions.friend.id ,
                contactId: actions.contactId,
                friend : actions.friend,
                componentSM: "CHAT"
            };
        case 'USER_CHATS':
            return{
                ...state,
                chatList : actions.list
            };
        case 'BACK_TO_LIST':
            return{
                ...state,
                componentSM: "LIST",
                openChat:false
            };
        case 'MESSAGE':
            // console.log("RED", actions.message)
            return{
                ...state,
                writtenMessage: actions.message
            };
        case 'CHAT_HISTORY':

            return{
                ...state,
                chatId: actions.value
            };
        case 'VISITOR_MESSAGE_MODAL':
            return{
                ...state,
                messageModal: !state.messageModal,
            };
        case 'VISITOR_MESSAGE':
            return{
                ...state,
                visitorMessage:actions.message
            };
        case 'VISITOR_MESSAGE_SENT':
            return{
                ...state,
                visitorMessage:''
            };
        case 'SENDING_MESSAGE':
            return{
                sendingMessage: !state.sendingMessage
            };
        case 'MESSAGE_SENT':
            return{
                ...state,
                writtenMessage: ''
            };
        case 'OLD_MESSAGES':
            return{
                ...state,
                messages:actions.data
            };
        case 'LIVE_MESSAGES':
            let messages = [...state.messages];
            messages.push(actions.data);
            // console.log("RED", actions.data);
            return{
                ...state,
                messages: messages
            };
        case 'CLEAR_MESSAGES':
            return{
                ...state,
                messages: []
            };
        default : return state
    }
};

export default ChatReducer;
