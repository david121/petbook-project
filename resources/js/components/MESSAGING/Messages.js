import React, {Component} from 'react';
import {connect} from "react-redux";
import './CSS/Messages.css'
import ReactEmoji from 'react-emoji';
import {NavLink} from "react-router-dom";
class Messages extends Component {
    render() {

        let mess;

        if (this.props.messages.length > 0){
             mess = this.props.messages.map((item,index)=>{
                 // console.log(item);
                if (item.id === this.props.activeUser.id){
                    return <div key={index} className="messageContainer justifyEnd">
                        <div className="messageBox backgroundBlue">
                            <p className="messageText colorWhite">
                                {ReactEmoji.emojify(item.message)}
                            </p>
                        </div>
                        <p className="sentText pr-10">
                            <span className="sender-photo">
                                {item.sender_photo === "None" ?
                                   <NavLink to={'/profile'}> <img src={'/img/avatar.png'} alt="user" width={20} height={20} className='rounded-circle' /></NavLink>
                                    :
                                    <img src={`/storage/users/${item.id}/${item.sender_photo}`} alt="user" width={20} height={20} className='rounded-circle' />
                                }
                            </span>
                        </p>
                    </div>
                }
                return <div key={index} className="messageContainer justifyStart">

                    <p className="sentText pl-10 ">{/*item.sender_name*/}
                        <span className="sender-photo">
                            {item.sender_photo === "None" ?
                              <NavLink to={`/pet-owner/${item.id}`}> <img src={'/img/avatar.png'} alt="user" width={20} height={20} className='rounded-circle' /></NavLink>
                                :
                              <NavLink to={`/pet-owner/${item.id}`}>  <img src={`/storage/users/${item.id}/${item.sender_photo}`} alt="user" width={20} height={20} className='rounded-circle' /></NavLink>
                            }
                        </span>

                    </p>
                    <div className="messageBox backgroundLight">
                        <p className="messageText colorDark">{ReactEmoji.emojify(item.message)}</p>
                    </div>
                </div>
            })
        }

        return(
                <div>
                    {mess}
                </div>
        )
    }
}

const mapState=(state)=>{
    return {
        messages : state.Chat.messages,
        //old : state.Chat.oldMessages,
        activeUser : state.User.user_info,
    }
};

export default connect(mapState,null)(Messages);