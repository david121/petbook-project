import React,{Component} from 'react'
import Nav from "../LAYOUT/Nav";
import {
    backToList,
    browserType,
    chatSearchHandler,
    chatUser,
    fetchChats,
    openChatFromSearch
} from "./STORE/ChatActions.Js";
import {connect} from "react-redux";
import './CSS/ChatList.css'
import ScrollToBottom from 'react-scroll-to-bottom';
import ReactEmoji from 'react-emoji';
import Chat from "./Chat";

class ChatList extends Component {

    componentDidMount(){
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Chat";
        this.props.fetchChats();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let window_width = $(window).width();
        this.props.screen(window_width);
    }

    render() {
        let list;
        //let chatId;
        if(this.props.list){
            list = this.props.list.map((item,index)=>{
                // console.log(item);
                let bg = item.unread ? 'lightgrey' : '' ;

                let src = item.photo_path === null ? '/img/avatar.png' : `/storage/users/${item.id}/${item.photo_path}`;
                if (this.props.screenWidth < 1000){
                    let bgSm = item.unread ? 'cyan' : '' ;
                    return <li style={{backgroundColor:bgSm}} className="chat-list-item" key={index} onClick={()=>this.props.chatUser(item.chatId,item,'SM',item.unread_id)}>
                            <div className="d-flex">
                                <div className="img_cont">
                                    <img src={src} className="rounded-circle" alt={'user'} width={'35'} height={'35'} />
                                </div>
                                <div className="user_info">
                                    <span className="contact_name">{item.username}</span>
                                    <p className="chat-text-preview">{ReactEmoji.emojify(item.message)}</p>
                                </div>
                            </div>
                    </li>
                }
                return <li style={{backgroundColor:bg}} className="chat-list-item" key={index} onClick={()=>this.props.chatUser(item.chatId,item,'LG',item.unread_id)}>
                        <div className="d-flex">
                            <div className="img_cont">
                                <img src={src} className="rounded-circle" alt={'user'} width={'35'} height={'35'} />
                            </div>
                            <div className="user_info">
                                <span className="contact_name">{item.username}</span>
                                <p>{ReactEmoji.emojify(item.message)}</p>
                            </div>
                        </div>
                </li>
            })
        }else
            list = '';

        let friends;
        if (this.props.activeUser.friends){
            friends = this.props.activeUser.friends.map((item,index)=>{
                if (item.id !== this.props.activeUser.id)
                    return <option key={index} value={item.username} >{item.firstname + " " + item.lastname}</option>
            })
        }

        let smallDisplay;

        if (this.props.smCompo === "LIST"){
            smallDisplay = <ul className="chat-list mt-1">
                <li>
                    <input list={'friends'} value={this.props.search_value}
                           type="text" style={{width:"100%",fontFamily:"Cambria"}}
                           onChange={(e)=>{this.props.searchHandler(e.target.value,this.props.activeUser.friends)}}
                           placeholder="search a friend"/>
                    <datalist id={'friends'}>
                        {friends}
                    </datalist>
                </li>
                {list}
            </ul>
        }else{
            smallDisplay = <div>
                    <span className="ml-2">
                        <img src={"/img/return.svg"} alt="back" width="20" height="20" onClick={this.props.backToList}/>
                    </span>
                    <ScrollToBottom className="chat__main">
                        {this.props.openMobileChat ? <Chat chatId={this.props.chatId} friend={this.props.friend} activeUser={this.props.activeUser} media={"MOBILE"}/> : ''}
                    </ScrollToBottom>
                </div>

        }

        return (
            <div>
                <Nav/>
                <div className="chat lg-chat">
                    <div className="chat__sidebar" id="sidebar">
                        <div style={{paddingTop:"75px"}}>
                            <span style={{fontWeight:"bolder", fontSize:"22px", fontFamily:"Cambria"}} className="ml-3">Chat</span>
                        </div>
                        <ul className="chat-list mt-2">
                            <li>
                                <input list={'friends'} value={this.props.search_value}
                                       type="text" style={{width:"100%",fontFamily:"Cambria"}}
                                       onChange={(e)=>{this.props.searchHandler(e.target.value,this.props.activeUser.friends)}}
                                       placeholder="search a friend"/>
                                <datalist id={'friends'}>
                                    {friends}
                                </datalist>
                            </li>
                            {list}
                        </ul>
                    </div>

                    <ScrollToBottom className="chat__main">
                        {this.props.openChat ? <Chat chatId={this.props.chatId} friend={this.props.friend} activeUser={this.props.activeUser} /> : ''}
                    </ScrollToBottom>
                </div>

                <div className="sm-chat">
                    {smallDisplay}
                </div>
            </div>

        );
    }
}

const mapState = (state) =>{
    return {
        activeUser : state.User.user_info,
        list : state.Chat.chatList,
        openChat : state.Chat.chatUser,
        openMobileChat : state.Chat.openChat,
        chatId : state.Chat.contactId,
        friend : state.Chat.friend,
        friendId : state.Chat.friendId,
        screenWidth : state.Chat.screenWidth,
        smCompo : state.Chat.componentSM,

        //open chat from search
        search_value : state.Chat.search_value,
        friend_searched : state.Chat.friend_searched,
        chat_history_id : state.Chat.chatId,
    }
};

const mapDispatch = (dispatch) => {
    return {
        fetchChats : () =>{dispatch(fetchChats())},
        chatUser : (chatId,friend,screen,noti_id) =>{dispatch(chatUser(chatId,{friend},screen,noti_id))},
        screen : (width) =>{dispatch(browserType(width))},
        backToList : () =>{dispatch(backToList())},
        fromSearch : (id) =>{dispatch(openChatFromSearch(id))},
        searchHandler : (value,friends) =>{dispatch(chatSearchHandler(value,friends))},

    }
};
export default connect(mapState,mapDispatch)(ChatList);