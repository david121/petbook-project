import React, {useEffect} from 'react';
import Nav from "../LAYOUT/Nav";
import './CSS/Chat.css'
import {connect} from "react-redux";
import io from 'socket.io-client';
import {
    chatHandler,
    checkHistory,
    clearMessages,
    getMessages,
    Messages,
    messageSend,
    sendMessage, sendVisitMessage
} from "./STORE/ChatActions.Js";
import MessagesDisplay from './Messages'
import ScrollToBottom from 'react-scroll-to-bottom';
import Loader from "react-loader-spinner";

    const Chat = (props) =>{
        let socket;
        const ENDPOINT = 'https://pawrry.com:5000';//68.183.64.19
        useEffect(()=>{
            socket = io(ENDPOINT);
            socket.emit('join', {room : props.chatId, friend :props.friend, user:props.activeUser.id});
            props.showMessage(props.chatId);
        },[ENDPOINT]);

        useEffect(()=>{
            socket.on('message',({data})=>{//sender,text
                props.displayMessages(data)
            });
            return () => {
               // console.log("Unmounting Chat");
                socket.emit('disc');
               socket.off();
               props.clearMessages()
            }
        },[ENDPOINT]);

        const sender = {
            sender_name : props.activeUser.username,
            sender_photo : props.activeUser.photo_path,
            id : props.activeUser.id
        };
        let data =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        if (!props.chatId){ //if chat opened from search
            // console.log("poop")
            // check screen
           return data = <div className="chat__main">
                <ScrollToBottom className="messages">
                    <div id="messages" className="chat__messages" style={{paddingTop:"90px"}}>
                        <MessagesDisplay/>
                    </div>
                </ScrollToBottom>
                <div className="compose">
                    <form id="message-form" onSubmit={!props.message ? e=>{e.preventDefault()} :(e)=>props.sendVisitorMessage(e,props.friend_searched.id,sender,props.message,"FROM_SEARCH")}>
                        <input type="text" autoFocus={true} placeholder="Type a new message" className='message_input' value={props.message} onChange={(e)=>props.inputHandler(e.target.value)}/>
                    </form>
                </div>
            </div>;
        }

        if (props.messages){
            if (props.media === "MOBILE"){
                data = (
                    <div className="chat__main">
                        <ScrollToBottom className="messages">
                            <div id="messages" className="chat__messages" style={{paddingTop:"10px"}}>
                                <MessagesDisplay/>
                            </div>
                        </ScrollToBottom>

                        <div className="compose">
                            <form id="message-form" onSubmit={!props.message ? e=>{e.preventDefault()} :(e)=>props.sendMessageHandler(e,props.chatId,sender,props.message)}>
                                <input type="text" autoFocus={false} placeholder="Type a new message" className='message_input' value={props.message} onChange={(e)=>props.inputHandler(e.target.value)}/>
                            </form>
                        </div>
                    </div>
                );
             } else
            data =(
                    <div className="chat__main">
                        <ScrollToBottom className="messages">
                            <div id="messages" className="chat__messages" style={{paddingTop:"90px"}}>
                                <MessagesDisplay/>
                            </div>
                        </ScrollToBottom>
                        <div className="compose">
                            <form id="message-form" onSubmit={!props.message ? e=>{e.preventDefault()} :(e)=>props.sendMessageHandler(e,props.chatId,sender,props.message)}>
                                <input type="text" autoFocus={true} placeholder="Type a new message" className='message_input' value={props.message} onChange={(e)=>props.inputHandler(e.target.value)}/>
                            </form>
                        </div>
                    </div>
            )
        }
            return data
        };

const mapState = (state) =>{
    return {
        message : state.Chat.writtenMessage,
        messages : state.Chat.messages,
        friend_searched : state.Chat.friend_searched,// if chat was opened via search
    }
};

const mapDispatch = (dispatch) => {
    return {
        checkHistory : (id)=>{dispatch(checkHistory(id))} ,
        inputHandler : (message) =>{dispatch(chatHandler(message))},
        sendMessageHandler : (e,chatId,user,msg) =>{dispatch(sendMessage(e,chatId,user,msg))},
        showMessage : (chatId) =>{dispatch(getMessages(chatId))},
        displayMessages : (data) => { dispatch(Messages(data)) },
        clearMessages : () => { dispatch(clearMessages()) },
        sendVisitorMessage : (e,id,sender,msg,context) =>  {e.preventDefault(); dispatch(sendVisitMessage(id,sender,msg,context))},
    }
};

export default connect(mapState, mapDispatch)(Chat);
