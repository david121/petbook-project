import React, {Component} from 'react';
import {connect} from "react-redux";
import NavComp from './../LAYOUT/Nav';
import './CSS/Profile.css';
import './CSS/MQProfile.css';
import './../POSTS/CSS/PostPreview.css';
import './CSS/PicModal.css';
import {
    fetchPets,
    fetchUser,
    CancelPhotoUpload,
    toggleBio,
    addUserBio,
    toggleCity,
    addCity,
    uploadProfilePhoto,
    ProfilePhotoUploadHandler,
    userBioHandler,
    cityHandler,
    editBio,
    editBioIcon,
    editBioHandler,
    bioEditSubmit,
    editCityIcon,
    editCity,
    editCityHandler,
    cityEditSubmit,
    loadImage,
    toggleAddPet,
    managePets,
    petEdit,
    addPet,
    clearPetsPanel,
    navigateComponent,
    toggleLocation
} from './STORE/Actions';
import {fetchUserPosts, toggleComments, writeComment, postComment, likePost, showAddPet} from "../POSTS/STORE/Actions";
import AddPet from './AddPet';
import {NavLink, withRouter} from "react-router-dom";
import Post from "../POSTS/Post";
import ShowPosts from "../POSTS/ShowPosts";
import Preview from "../POSTS/Preview";
import {showAdmiredPets, showFriends, showUserFriends} from "../USER_FRIENDS/STORE/Actions";
import Friends from "../USER_FRIENDS/Friends";
import LikedPets from "../USER_FRIENDS/LikedPets";
import Pet from "../AUTHENTICATION/REGISTER PET/Pet";
import EditPet from "../PET/EditPet";
import ManagePet from "../PET/ManagePet";
import Admirers from "../PET/Admirers";
import AboutPet from "../PET/AboutPet";
import {Nav} from "react-bootstrap";
import ScrollContainer from "react-indiana-drag-scroll";
import Loader from "react-loader-spinner";
import {testMail} from "../MESSAGING/STORE/ChatActions.Js";
import Process from "../LAYOUT/Process";

class Profile extends Component {

    componentDidMount() {
        //Check if user authenticated
        if (!localStorage.getItem('token')){
            this.props.history.push('/')
        }
        else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
            this.props.navigateComponent('timeline');
            this.props.pets();
            this.props.user_posts();
        }
        document.title= "Pawrry | Profile";
    }

    componentWillUnmount() {
        this.props.clearPets();
        this.props.clearPosts();
        if(this.props.friendsShow){this.props.showFriends();}
        if(this.props.likedPetsShow){this.props.showAdmiredPets();}
        if (this.props.addPet){this.props.addPetModal()}
    }

    render() {
        let fullPicture;
        let bioLabel = !this.props.toggleUserBio? <span className="text-primary" id="add-bio" onClick={this.props.toggleBio}>Write Bio</span>:'';
        let bioForm = this.props.toggleUserBio?
            <form onSubmit={(e)=>this.props.addBio(e,this.props.writtenBio)}>
                <textarea placeholder="write something about you.. " className="bio-input" autoFocus={true} value={this.props.writtenBio} onChange={this.props.bioHandler}/>
                <button type="submit" className="col-sm-6 ">save</button>
                <button type="button" className="col-sm-6 " onClick={this.props.toggleBio}>cancel</button>
            </form>: '';



        // <--------------   CITY ------------>
            let cityLabel = !this.props.cityToggle?<span className="ml-3 text-primary" id="add-city" onClick={this.props.toggleCity}> Add your city</span> : '';

            let cityForm = this.props.cityToggle?<form onSubmit={(e)=>this.props.addCity(e,this.props.writtenCity)}>
                <input type={"text"} placeholder="e.g Tbilisi, Georgia" className="city-input" autoFocus={true} value={this.props.writtenCity} onChange={this.props.cityHandler}/>
                <button type="submit" className="col-sm-6 ">save</button>
                <button type="button" className="col-sm-6 " onClick={this.props.toggleCity}>cancel</button>
            </form>: '';

            let filledCity = this.props.user_info.city + ", " + this.props.user_info.country;

            let editCityIcon = this.props.inCity? <img src={"/img/more.svg"} alt={"edit"} width={20} height={20} className="edit-bio" onClick={()=>this.props.editCityAction(filledCity)}/> : '';

            let userLocation = this.props.user_info.city === null ?
                <div className="p-2">
                    <img src={"/img/more.svg"} alt="more" width={15} height={15}/>
                    {/*{cityLabel}{cityForm}*/}
                </div>
                : this.props.cityEdit ?
                    <form onSubmit={(e)=>this.props.cityEditSubmit(e,this.props.newCity,filledCity)}>
                        <input value={this.props.newCity} onChange={this.props.editCityHandler} id="editCityInput" autoFocus={true}/>
                        <button type="submit" className="col-sm-6 btn btn bio-edit-submit">submit</button>
                        <button type="button" className="col-sm-6 btn btn bio-edit-cancel" onClick={()=>this.props.editCityAction('')}>cancel</button>
                    </form>
                :this.props.user_info.location === 0
                        ? <div className="p-2">
                            <span><img src={"/img/address1.svg"} className="ml-3" alt="city" height={30} width={30}/></span>
                            <span className="ml-3 user-location"> hidden </span>
                            <span className="ml-5 user-location toggle-loc" onClick={this.props.toggleLocation}> show</span>
                        </div>
                        : <div className="p-2">
                            {/*editCityIcon*/}
                            <span><img src={"/img/address1.svg"} className="ml-3" alt="city" height={30} width={30}/></span>
                            <span className="ml-3 user-location"> {filledCity}</span>
                            <span className="ml-5 user-location toggle-loc" onClick={this.props.toggleLocation}> hide</span>
                        </div>;

        // <--------------   CITY ------------>



        let editBioIcon = this.props.inBio? <img src={'/img/pencil.svg'} alt='edit' className="edit-bio" width={'20'} height={'20'} onClick={()=>this.props.editBioAction(this.props.user_info.bio)}/> : '';

        let bio = this.props.user_info.bio === null || this.props.user_info.bio === ''?
            <div className="bio-setting p-2">
                <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                {bioLabel}{bioForm}
            </div> :

            this.props.bioEdit ? <form onSubmit={(e)=>this.props.bioEditSubmit(e,this.props.newBio,this.props.user_info.bio)}>

                    <textarea value={this.props.newBio} onChange={this.props.editBioHandler} cols="15" rows="5" id="editBioInput" autoFocus={true}/>
                    <button type="submit" className="col-sm-6 btn btn bio-edit-submit">submit</button>
                    <button type="button" className="col-sm-6 btn btn bio-edit-cancel" onClick={()=>this.props.editBioAction('')}>cancel</button>
                </form> :

                <div className="pet-bio-wrapper p-2">
                {editBioIcon}
                <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-2'}/>
                <span className="ml-4 user-bio">{this.props.user_info.bio}</span>
            </div>;


        let data =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        let pets = "";
        let confirmUpload; //Confirmation button for user cover upload
        let image;
        let formdata = new FormData();
        formdata.append('photo', this.props.user_cover);
        let posts;

        //user photo
        if(Object.keys(this.props.photo).length) {
            this.props.photo.map((item,index)=>{
                if (this.props.photo[index] === 'None') {
                    if (this.props.preview){
                        confirmUpload =   <form>
                                                <button onClick={this.props.cancelUpload}>Cancel</button>
                                                <button className="ml-2" onClick={(e)=>this.props.uploadCover(e,formdata)}>Upload</button>
                                            </form>;
                        image = <div className="image-wrapper">
                                    <img src={this.props.previewSrc}  className="rounded-circle m-3" alt="preview" width="150" height="130" />
                                    <div className="middle">
                                        <label className="text">Add Photo<input type="file" hidden={true} onChange={this.props.profilePhotoHandler}/></label>
                                    </div>
                                </div>
                    }else
                    image =<div className="image-wrapper">
                            <img src={"/img/avatar.png"}  className="image rounded-circle m-3" alt="avatar" width="150" height="130" />
                                <div className="middle">
                                    <label className="text">Add Photo<input type="file" hidden={true} onChange={this.props.profilePhotoHandler}/></label>
                                 </div>
                            </div>
                } else {
                    if (this.props.preview){
                        confirmUpload =   <form>
                            <button onClick={this.props.cancelUpload}>Cancel</button>
                            <button className="ml-2" onClick={(e)=>this.props.uploadCover(e,formdata)}>Upload</button>
                        </form>;
                        image = <div className="image-wrapper">
                            <img src={this.props.previewSrc}  className="rounded-circle m-3" alt="preview" width="150" height="130" />
                            <div className="middle">
                                <label className="text">change photo<input type="file" hidden={true} onChange={this.props.profilePhotoHandler}/></label>
                            </div>
                        </div>
                    }else
                    image = <div>
                              <img src={item} alt="user" className="profile-pic rounded-circle m-3" width="150" height="150" onClick={this.props.loadImage}/>
                                <div className="">
                                    <label className="">{/*text*/}
                                        <img src={'/img/camera.svg'} alt="upload" height={25} width={25} style={{cursor:"pointer"}}/>
                                        <input type="file" onChange={this.props.profilePhotoHandler} hidden={true}/>
                                    </label>
                                </div>
                          </div>;
                    fullPicture = this.props.loadProfilePic ?
                        <div className="profile-pic-modal text-center">
                            <span className="profile-pic-modal-close" onClick={this.props.loadImage}>&times;</span>
                            <img className="profile-pic-modal-content" src={item} alt="profile picture"/>
                        </div> : '';
                }
            });
        }


        if(this.props.uploaded){
            confirmUpload = ''
        }

        //Loop and Load Posts
        if (this.props.userPosts === 'None'){
            posts = <div className="card" id='post'>


                <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>You have no posts !</h2>
                    </span>
                    <span className={"post-photo mt-4"}>
                        <img src={"img/sad.jpg"} className={"post-photo"} width={"350"}/>
                    </span>
                </div>
            </div>
        }else {
            posts = this.props.userPosts.map((post,index)=>{
                return <ShowPosts
                        key = {index}
                        post = {post.id}
                        pet = {post.pet.name}
                        petId = {post.pet_id}
                        post_owner = {post.owner_user_id}
                        post_owner_username = {post.postOwner}
                        activeUser ={this.props.user_info.id}
                        // index = {index}
                        user = "petOwner"
                        page = "USER_PROFILE"
                        postPic = {post.post_photo}
                        numOfPics = {post.post_photo.length}
                        petPhoto = {post.pet.profile_photo}
                        userCover = {this.props.user_info.photo_path}
                        userId = {post.user_id}
                        userName = {this.props.user_info.username}
                        postCaption = {post.caption}
                        time = {post.time}
                        clicked = {this.props.showComments}
                        clickedPost = {this.props.clickedPost}
                        postId = {post.id}
                        totalComments = {post.comments}
                        totalLikes = {post.likes.length}
                        toggleComments = {()=>this.props.comments(post.id,this.props.showComments)}
                        commentHandler = {this.props.writeComment}
                        liked = {post.is_liked_count}
                        like = {this.props.likePost}
                        comment = {this.props.writtenComment}
                        commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,post.id)}
                        />
            })
        }

        if(Object.keys(this.props.user_info).length){

            pets = this.props.user_pets.map((item, index)=>{
                // console.log(item);
                if (this.props.managePetMode){
                    return <div key={index} className="item mt-1 mr-4">
                        <div className="pet-image mb-3">
                            <span className="shake-pets">

                                {item.profile_photo!== null
                                    ?
                                    <img key={index} src={`/storage/pets/${item.id}/${item.profile_photo}`} onClick={()=>this.props.petEdit(item)} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
                                    :
                                    <img key={index} src={"/img/rodent.svg"} alt={"pet photo"} onClick={()=>this.props.petEdit(item)} className="rounded-circle pet-image" width="130" height="110"/>
                                }

                            </span>
                        </div>
                        <span className="pet-name">
                            {item.name}
                        </span>
                    </div>
                }else
                return <div key={index} className="item mt-1 mr-4">
                    <div className="pet-image mb-3">
                        <span>
                            <NavLink to={`/pet/${item.id}`} >
                                {item.profile_photo!== null
                                    ?
                                    <img key={index} src={`/storage/pets/${item.id}/${item.profile_photo}`} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
                                    :
                                    <img key={index} src={"/img/rodent.svg"} alt={"pet photo"} className="rounded-circle pet-image" width="130" height="110"/>
                                }
                            </NavLink>
                        </span>
                    </div>
                        <span className="pet-name">
                            {item.name}
                        </span>
                </div>
            });

            let numOfFriends = this.props.user_info.friends === null ? 0 : this.props.user_info.friends.length / 2;

            //page rendering
            let activeComp;
            let tmlStyle;
            let admStyle;
            let brdStyle;
            let profStyle;
            if(this.props.activePage === 'timeline'){
                tmlStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp =  <div>
                        <Post id = {this.props.user_info.id} />
                        <Preview
                            poster = 'user'
                            user = {this.props.user_info.username}
                            user_ = {this.props.user_info}
                            userId = {this.props.user_info.id}
                            cover = {this.props.photo}
                            numOfpets = {this.props.user_pets.length}
                            pets = {this.props.user_pets}
                            petPhoto = {this.props.pet_cover}
                        />
                        <div>
                            {posts}
                        </div>
                </div>
            }else if (this.props.activePage === 'friends'){
                admStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="admirer-component">
                    <Friends profile={this.props.user_info.id} toggle={this.props.showFriends} user_friends={this.props.user_info.friends}/>
                </div>
            }else if (this.props.activePage === 'likedPets'){
                brdStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="breed-component">
                    <LikedPets profile="User" page={"User"} toggle={this.props.showAdmiredPets} pets={this.props.user_info.admired_pets} />
                </div>;
            }else if (this.props.activePage === 'profile'){
                profStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="breed-component">
                    <div className="col-sm-12 col-lg-4 mb-3 profile-sm-component">

                        <div className="text-center col-sm-12 bg-light">
                            <b className="text-center profile-intro">Profile Intro</b>
                        </div>

                        <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-3" onMouseEnter={this.props.cityEditIcon} onMouseLeave={this.props.cityEditIcon}>
                            {userLocation}

                        </div>

                        <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-3" onMouseEnter={this.props.bioEditIcon} onMouseLeave={this.props.bioEditIcon}>
                            {bio}
                        </div>
                    </div>
                </div>;
            }
            let addPetBtn = this.props.user_pets.length === 0
                ?
                    <button onClick={this.props.addPetModal} className="add-pet-lone">
                        {/*<span>add Pet</span><br/>*/}
                        <img src={"/img/paw-add.svg"} alt="paw" width="30" height="30"/>
                    </button>
                :
                    <button onClick={this.props.addPetModal} className='add-pet'>
                        add Pet
                    </button>;
            data = (
                <div>
                    <NavComp/>

                    <div className="container mt-1">
                       <div className="row">

                        <div className="col-sm-12 mb-1">
                         <div className="user-page-info">
                                {/*User photo*/}
                            <div className="row">
                                <div className="text-lg-right text-sm-center user-image-holder col-sm-12 col-lg-6 p-3s">
                                    {image}
                                    {fullPicture}
                                    {confirmUpload}
                                    {
                                        this.props.file_invalid ?<div className="mb-2 text-center"> <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                                            Warning: Invalid File{/* - Persistence may lead to a permanent deactivation of your account!*/}
                                        </b> </div>: this.props.user_file_large?<div className="mb-2 text-center"> <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                                            File too large{/* - Persistence may lead to a permanent deactivation of your account!*/}
                                        </b> </div> :null
                                    }
                                </div>
                                 <div className="col-sm-12 col-lg-6">
                                     <div className="mt-lg-5 mt-sm-1 user-name-holder">
                                         <span className="pet-name-profile mr-1">
                                             {this.props.user_info.firstname + " " + this.props.user_info.lastname }
                                             {
                                                 this.props.user_info.verified === 1 ?
                                                     <img src={'/img/verified.svg'} alt="mark" width={20} height={20} className="ml-3"/> : null
                                             }
                                         </span><br/>
                                         <span className="pet-name-profile user-username-profile">( {this.props.user_info.username} )</span>
                                     </div>
                                 </div>
                            </div>
                        </div>
                        </div>


                        <div className="col-sm-12">
                                {/*Pets*/}
                                    <div className="pets pet-panel">

                                    <ScrollContainer className="scroll-container ml-3">
                                        <div className="item mr-3">
                                            {addPetBtn}
                                        </div>
                                        {pets}
                                    </ScrollContainer>

                                        {this.props.user_pets.length > 0 ? <div className="manage-pets-icon-wrap">
                                            {this.props.managePetMode?
                                                <img src={'/img/stop.svg'} alt={'stop'} width={'20'} height={'20'} onClick={this.props.managePets} style={{cursor: "pointer"}}/>
                                                :
                                                <img src={'/img/settings.svg'} alt={'edit'} width={'20'} height={'20'} onClick={this.props.managePets} style={{cursor: "pointer"}}/>
                                            }

                                        </div> : ''}
                                    </div>
                             {this.props.addPet?<AddPet close={this.props.addPetModal}/> : ''}
                             {this.props.editPetModal?<ManagePet userId = {this.props.user_info.id} pet = {this.props.petToEdit} close={this.props.petEdit}/> : ''}
                        </div>
                      </div>

                            <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                                <Nav.Item>
                                    <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("friends")} className='tab-text' style={admStyle}>Friends ({numOfFriends})</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("likedPets")} className='tab-text' style={brdStyle}>Admired Pets ({this.props.user_info.admired_pets !== null?this.props.user_info.admired_pets.length : 0})</Nav.Link>
                                </Nav.Item>
                                <Nav.Item className="sm-profile-tab">
                                    <Nav.Link eventKey="link-3" onClick={()=>this.props.navigateComponent("profile")} className='tab-text' style={profStyle}>Profile</Nav.Link>
                                </Nav.Item>
                            </Nav>

                    <div className="mt-2">
                        <div className="row">
                            {/*LEFT PANE*/}
                            <div className="col-sm-12 col-lg-4 mb-3 lg-profile-intro">

                                <div className="text-center col-sm-12 bg-light">
                                    <b className="text-center profile-intro">Profile Intro</b>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light" onMouseEnter={this.props.cityEditIcon} onMouseLeave={this.props.cityEditIcon}>
                                    {userLocation}
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light" onMouseEnter={this.props.bioEditIcon} onMouseLeave={this.props.bioEditIcon}>
                                    {bio}
                                </div>
                            </div>


                            {/*/*******  MAIN *****/}
                            <div className="col-sm-12 col-lg-8">
                                    {activeComp}
                            </div>

                        </div>
                    </div>
                    </div>
                </div>
            )
        }
        return data
    }
}

const MapState = (state) =>{
    return{
        activePage : state.User.activeComponent,
        user_info : state.User.user_info,
        loadProfilePic: state.User.loadProfilePic,
        user_pets : state.User.user_pets,
        cityInput : state.User.inputCity,
        photo : state.User.user_photo,
        pet_cover : state.User.user_pet_photos,
        user_cover : state.User.uploadedPhoto,
        preview : state.User.isPreview,
        file_invalid: state.User.invalidFile,
        user_file_large: state.User.user_file_large,
        previewSrc : state.User.photoPreview,
        uploaded : state.User.uploaded,

        addPet : state.PetReg.addPetModal,

        managePetMode : state.User.managePetMode,
        editPetModal : state.User.editPetModal,
        petToEdit : state.User.petToEdit,
        //Posts and comments
        liked : state.Posts.liked ,
        userPosts : state.Posts.posts,
        //postLikers:state.Posts.likers,
       // likes : state.Posts.likes,
       // postPhotos: state.Posts.postPhotos,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,
        //user page info
        toggleUserBio : state.User.toggleUserBio,
        writtenBio : state.User.bio,
        writtenCity : state.User.city,
        cityToggle : state.User.toggleCity,

        //Editing
        inBio : state.User.toggleEditBioIcon,
        bioEdit : state.User.bioEditMode,
        newBio : state.User.newBio,

        inCity : state.User.toggleEditCityIcon,
        cityEdit : state.User.cityEditMode,
        newCity : state.User.newCity,
        //
        friendsShow : state.User.userFriendsToggle,
        likedPetsShow: state.User.likedPetsToggle,
        // fullLoader:state.Layout.fullLoader

    }
};
const MapDispatch = (dispatch) => {

    return{
        // user : () =>{dispatch(fetchUser())},
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},
        loadImage :() =>{dispatch(loadImage())},
        pets : () =>{dispatch(fetchPets())},
        clearPets : () =>{dispatch(clearPetsPanel())},
        managePets : () =>{dispatch(managePets())},
        petEdit : (pet) => {dispatch(petEdit(pet))},
        user_posts : ()=>{dispatch(fetchUserPosts())},
        comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        toggleBio: ()=>{dispatch(toggleBio())},
        bioHandler : (e)=>{dispatch(userBioHandler(e.target.value))},
        addBio: (e,bio)=>{e.preventDefault();dispatch(addUserBio(bio))},
        addCity : (e,city) =>{e.preventDefault();dispatch(addCity(city))},
        toggleCity : () =>{dispatch(toggleCity())},
        cityHandler : (e) =>{dispatch(cityHandler(e.target.value))},
        profilePhotoHandler : (e) =>{dispatch(ProfilePhotoUploadHandler(e.target.files[0]))},
        cancelUpload : (e) =>{e.preventDefault();dispatch(CancelPhotoUpload())},
        uploadCover : (e,data) =>{e.preventDefault();dispatch(uploadProfilePhoto(data))},
        writeComment : (e) => {dispatch(writeComment(e.target.value))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId,"USER_PROFILE",null))},
        likePost : (postId,owner,pet) =>{dispatch(likePost(postId,owner,pet,"USER_PROFILE"))},

        addPetModal : () => {dispatch(addPet())},

        //BIO EDIT ACTIONS
        bioEditIcon: () =>{dispatch(editBioIcon())},
        editBioAction: (bio) =>{dispatch(editBio(bio))},
        editBioHandler : (e)=>{dispatch(editBioHandler(e.target.value))},
        bioEditSubmit : (e,newBio,oldBio)=>{e.preventDefault();dispatch(addUserBio(newBio,oldBio))},
        //LOCATION EDIT ACTIONS
        cityEditIcon : () =>{dispatch(editCityIcon())},
        editCityAction: (bio) =>{dispatch(editCity(bio))},
        editCityHandler : (e)=>{dispatch(editCityHandler(e.target.value))},
        cityEditSubmit : (e,newCity,oldCity) =>{e.preventDefault();dispatch(cityEditSubmit(newCity,oldCity))},
        clearPosts : () =>{dispatch({type:"CLEAR_USER_PROFILE"})},
        toggleLocation : () =>{dispatch(toggleLocation())},

    }
};
export default connect(MapState,MapDispatch)(withRouter(Profile));
