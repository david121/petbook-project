import React, {Component} from 'react';
import {connect} from "react-redux";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import { fetchPet, fetchPetPosts, toggleBio, addBio, bioHandler, toggleHobby,hobbyHandler,addHobby,fetchHobbies,toggleAboutPet, editBioIcon,editBio,editBioHandler,petBioEditSubmit, closePetBio,
    navigateComponent,clearPetProfile} from "./STORE/Actions";
import NavComp from "../LAYOUT/Nav";
import Post from "../POSTS/Post";
import './CSS/PetProfile.css'
import './../POSTS/CSS/PostPreview.css';
import {likePost, postComment, postInputHandler, toggleComments, writeComment,} from "../POSTS/STORE/Actions";
import ShowPosts from "../POSTS/ShowPosts";
import Preview from "../POSTS/Preview";
import AboutPet from "./../PET/AboutPet";
// import VisitorPosts from "../POSTS/VisitorPosts";
import moment from 'moment'
import Admirers from "../PET/Admirers";
import {Nav} from "react-bootstrap";
 // import moment from "./AddPet";

class PetProfile extends Component {


    componentDidMount(){
        if(!localStorage.getItem('token')){
            this.props.history.push('/');
        }else{
            if (localStorage.getItem('authenticate')){
                this.props.history.push('/verify')
            }
        }
        document.title= "Pawrry | Pet";
        let id = this.props.match.params.petId;
        this.props.pet(id);
        this.props.pet_post(id);
        this.props.fetchHobbies(id);
        this.props.navigateComponent('timeline')
    }

        componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.user_pett.hobbies > 0 && this.props.run ){
            this.props.fetchHobbies(this.props.user_pett.id);
        }
    }

    componentWillUnmount() {
        this.props.clearPet()
    }

    constructor(){
        super();
        this.state={
            activeTab : ''
        }
    }

    render() {
        let pet =<div style={{width:'100%',height:'100vh',}} className="d-flex justify-content-center align-content-center align-items-center">
            <Loader
                type="ThreeDots"
                color="darkslategrey"
                height={100}
                width={100}
            />
        </div>;
        let bio = "loading bio";
        let hobbies ="listing ";
        let posts;
        let addHobby;
        let hobbyLabel = <span className="pet-bio write-hobby" onClick={this.props.toggleHobby}><i className="fas fa-heartbeat text-dark mr-4"/>Add Hobby</span>;
        let hobbyForm = this.props.writeHobby?
            <label onBlur={()=>this.props.addHobby(this.props.pet_hobby, this.props.user_pett.id)}>
                <input type={"text"} autoFocus={true} value={this.props.pet_hobby} onChange={this.props.hobbyHandler} className="hobby-input"/>
            </label>: '';
        let bioLabel = !this.props.writeBio? <span className="pet-bio write-bio" onClick={()=>this.props.toggleBio('pet')}>
        <img src={'/img/petbio.svg'} alt="breed" width="25" height="25" className={'mr-4'}/>
            Write Bio
        </span>:'';
        let bioForm = this.props.writeBio?
            <form onSubmit={(e)=>this.props.addBio(e,this.props.pet_bio, this.props.user_pett.id)}>
                <textarea autoFocus={true} className="bio-input" value={this.props.pet_bio} onChange={this.props.bioHandler}/>
                <button type="button" onClick={this.props.closeBio}>cancel</button>
                <button type="submit" >save</button>
            </form>: '';

            // let about = this.props.toggleAbout? <AboutPet breed={this.props.user_pett.breed.name} about={this.props.user_pett.breed.description}/> : '';

        if(Object.keys(this.props.user_pett).length){



            //bio rendering
            let editBioIcon = this.props.inBio? <img src={'/img/pencil.svg'} alt='edit' className="edit-bio" width={'20'} height={'20'} onClick={()=>this.props.editBioAction(this.props.user_pett.bio,'pet')}/> : '';
            bio = this.props.user_pett.bio === '' || this.props.user_pett.bio === null ?
                        <div>
                            {bioLabel}{bioForm}
                        </div>
                        :this.props.bioEdit ? <form onSubmit={(e)=>this.props.bioEditSubmit(e,this.props.newBio,this.props.user_pett.bio,this.props.user_pett.id)}>

                        <textarea value={this.props.newBio} onChange={(e)=>this.props.editBioHandler(e,pet)} cols="15" rows="5" id="editBioInput" autoFocus={true}/>
                        <button type="submit" className="col-sm-6 btn btn bio-edit-submit">save</button>
                        <button type="button" className="col-sm-6 btn btn bio-edit-cancel" onClick={()=>this.props.editBioAction('','pet')}>cancel</button>
                    </form> :
                    <div className="pet-bio-wrapper p-2">
                    {editBioIcon}
                        <span><img src={'/img/petbio.svg'} alt="breed" width="25" height="25" /></span>
                        <span className="ml-4 user-bio">{this.props.user_pett.bio}</span>
                     </div>;



            //listing hobbies
            let listHobbies = this.props.hobbies.map((hobby,index)=>{
                                return <li key={index} className={"pet-hobby p-1"}>{hobby.hobby}</li>
                            });
            addHobby = <span>{hobbyForm}</span>;
             hobbies = <div>
                        {listHobbies}
                     </div>;



            //Loop and Load Posts
            if (this.props.petPosts === 'None'){
                posts = <div className="card" id='post'>


                    <div className="card-body post-body">
                    <span className={"caption"}>
                        <h2 className={"text-center"}>{this.props.user_pett.name} has no posts !</h2>
                    </span>
                        <span className={"post-photo mt-4"}>
                    </span>
                    </div>
                </div>
            }else {
                posts = this.props.petPosts.map((post,index)=>{
                    return <ShowPosts
                        key = {index}
                        user = 'pet'
                        page = "PET"
                        petId = {post.pet_id}
                        userPhoto = {post.pet.profile_photo}
                        post_owner = {post.owner_user_id}
                        post_owner_username = {post.postOwner}
                        postPic = {post.post_photo}
                        numOfPics = {post.post_photo.length}
                        userCover = {this.props.user_pett.profile_photo}
                        userId = {post.user_id}
                        userName = {post.pet.username}
                        postCaption = {post.caption}
                        time = {post.time}
                        clicked = {this.props.showComments}
                        clickedPost = {this.props.clickedPost}
                        postId = {post.id}
                        totalComments = {post.comments}
                        totalLikes = {post.likes.length}
                        toggleComments = {()=>this.props.comments(post.id,this.props.showComments)}
                        commentHandler = {this.props.writeComment}
                        like = {()=>this.props.likePost(post.id,this.props.user_info.id,post.pet_id)}
                        liked = {post.is_liked_count}
                        comment = {this.props.writtenComment}
                        commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,post.id,"PET",post.pet_id)}
                    />
                })
            }


            //page rendering

            let activeComp;
            let tmlStyle;
            let admStyle;
            let brdStyle;
            let profStyle;
            if(this.props.activePage === 'timeline'){
                tmlStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp =  <div>
                                    <Post id = {this.props.user_pett.id} />
                                    <Preview
                                        poster = 'pet'
                                        pet ={this.props.user_pett}
                                        user = {this.props.user_pett.username}
                                        cover = {this.props.cover}
                                        id = {this.props.user_pett.id}
                                        activeUser ={this.props.user_info.id}
                                        photo = {this.props.user_info.photo_path}
                                        activeUserName = {this.props.user_info.username}
                                    />
                                <div className="posts">
                                    {posts}
                                </div>
                </div>
            }else if (this.props.activePage === 'admirers'){
                admStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="admirer-component">
                            <Admirers petId={this.props.user_pett.id} petName={this.props.user_pett.name}/>
                </div>
            }else if (this.props.activePage === 'breed'){
                brdStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                activeComp = <div className="breed-component">
                    <AboutPet breed={this.props.user_pett.breed.name} breed_id={this.props.user_pett.breed_id} about={this.props.user_pett.breed.description} petName={this.props.user_pett.name}/>
                </div>;
            }else if (this.props.activePage === 'profile'){
                profStyle ={
                    borderTop:"2px solid darkslategrey",
                    fontWeight : "bolder",
                    fontFamily : "Cambria",
                    color : "darkslategrey"
                };
                let breedIconSm = parseInt(this.props.user_pett.breed.pet_type) === 2 ? <img src={'/img/dog-icon.svg'} alt="gender" width="25" height="25"/>:<img src={'/img/cat-gender.svg'} alt="gender" width="25" height="25"/>;
                let joined = moment.duration(moment().diff(this.props.user_pett.created_at))._data;
                let joinYears = joined.years === 0 ? '' : joined.years === 1 ? '1 year' : joined.years+' years';
                let joinMonths = joined.months === 0 ? '' : joined.months === 1 ? '1 month' : joined.months+' months';
                let joinDays = joined.days === 0 ? '' : joined.days === 1 ? '1 day' : joined.days+' days ';

                let DOBsm = this.props.user_pett.date_of_birth;
                let AgeSm = moment.duration(moment().diff(DOBsm))._data;
                let yearsSm = AgeSm.years === 0 ? '' : AgeSm.years === 1 ? '1 year' : AgeSm.years+' years';
                let monthsSm = AgeSm.months === 0 ? '' : AgeSm.months === 1 ? '1 month' : AgeSm.months+' months';
                let daysSm = AgeSm.days === 0 ? '' : AgeSm.days === 1 ? '1 day' : AgeSm.days+' days ';

                activeComp = <div className="profile-sm-component ">
                    <div className="text-center col-sm-12 bg-light p-2">
                        <b className="text-center profile-intro">Profile</b>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/timetable.svg'} alt="breed" width="25" height="25"/></span>  <span className='ml-4 pet-profile-time'>{joinYears} {joinMonths} {joinDays}  on Pawrry</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span>{breedIconSm}</span> <span className='ml-4 pet-profile-breed'>{this.props.user_pett.breed.name}</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/gender.svg'} alt="breed" width="25" height="25"/></span><span className='ml-4 pet-profile-gender'>{this.props.user_pett.gender === 0 ? 'Male' : 'Female'}</span>
                    </div>

                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                        <span><img src={'/img/cake.svg'} alt="cake" width="25" height="25"/></span> <span className='ml-4 pet-profile-bd'> {DOBsm? moment( this.props.user_pett.date_of_birth).format("MMM Do"):null} ({yearsSm} {monthsSm} {daysSm} {DOBsm?"old" : "Not Filled"})</span>
                    </div>


                    <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2" onMouseEnter={()=>this.props.bioEditIcon('pet')} onMouseLeave={()=>this.props.bioEditIcon('pet')}>
                        {bio}
                    </div>
                </div>;
            }

            let joined = moment.duration(moment().diff(this.props.user_pett.created_at))._data;
            let joinYears = joined.years === 0 ? '' : joined.years === 1 ? '1 year' : joined.years+' years';
            let joinMonths = joined.months === 0 ? '' : joined.months === 1 ? '1 month' : joined.months+' months';
            let joinDays = joined.days === 0 ? '' : joined.days === 1 ? '1 day' : joined.days+' days ';

            let DOB = this.props.user_pett.date_of_birth;
            let Age = moment.duration(moment().diff(DOB))._data;
            let years = Age.years === 0 ? '' : Age.years === 1 ? '1 year' : Age.years+' years';
            let months = Age.months === 0 ? '' : Age.months === 1 ? '1 month' : Age.months+' months';
            let days = Age.days === 0 ? '' : Age.days === 1 ? '1 day' : Age.days+' days ';

            let breedIcon = parseInt(this.props.user_pett.breed.pet_type) === 2 ? <img src={'/img/dog-icon.svg'} alt="gender" width="25" height="25"/>:<img src={'/img/cat-gender.svg'} alt="gender" width="25" height="25"/>;


    pet = <div>

                <NavComp/>

                <div className="container">

                    <div className="user-page-info col-sm-12 p-4">
                        <button onClick={()=>this.props.history.goBack()} className="btn btn-light ml-2 mt-4 text-info">&#8592;</button>


                        <div className="row">
                            {/*User photo  */}
                            <div className="user-image-holder text-center col-sm-6">
                                {
                                    this.props.user_pett.profile_photo === null ?
                                        <img src={"/img/rodent.svg"} className="rounded-circle m-3 pet-profile-pic" alt={"pet"}/>
                                        :
                                        <img src={this.props.cover} className="rounded-circle m-3 pet-profile-pic" alt={"pet"}/>
                                }
                            </div>
                            {/*Pet relationships*/}
                            <div className="col-sm-12 col-lg-6">
                                <div className="persona mt-5">{/* d-flex justify-content-start align-items-end*/}
                                    <div className="mb-3">
                                        <span className="pet-name-profile mr-1">{this.props.user_pett.name}</span>
                                        <span className="pet-name-profile pet-username-profile">( {this.props.user_pett.username} )</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                            {/*<div className="user-image-holder text-center col-sm-6">*/}
                            {/*    <img src={this.props.cover} className="rounded-circle m-3" width="150" height="130" alt={"user"}/>*/}
                            {/*</div>*/}
                    </div>

                    {/*<div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-light">*/}
                        <Nav fill variant="tabs" defaultActiveKey="timeline" className="bg-light">
                            <Nav.Item>
                                <Nav.Link eventKey="timeline" onClick={()=>this.props.navigateComponent("timeline")} className='tab-text' style={tmlStyle}>Timeline</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="link-1" onClick={()=>this.props.navigateComponent("admirers")} className='tab-text' style={admStyle}>Admirers ({this.props.user_pett.admirers})</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="link-2" onClick={()=>this.props.navigateComponent("breed")} className='tab-text' style={brdStyle}>
                                    {this.props.user_pett.breed.name === "Other Breed" ? "Breed" : this.props.user_pett.breed.name+"s"}
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="sm-profile-tab">
                                <Nav.Link eventKey="link-3" onClick={()=>this.props.navigateComponent("profile")} className='tab-text' style={profStyle}>Profile</Nav.Link>
                            </Nav.Item>
                        </Nav>


                    <div className="mt-2 ">
                        <div className="row">

                            {/* LEFT PANE*/}

                            <div className="col-sm-12 col-lg-4 mb-3 lg-profile-intro">

                                <div className="text-center col-sm-12 bg-light p-2">
                                        <b className="text-center profile-intro">Profile</b>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                                    <span><img src={'/img/timetable.svg'} alt="breed" width="25" height="25"/></span>  <span className='ml-4 pet-profile-time'>{joinYears} {joinMonths} {joinDays}  on Pawrry</span>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                                    <span>{breedIcon}</span> <span className='ml-4 pet-profile-breed'>{this.props.user_pett.breed.name}</span>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                                    <span><img src={'/img/gender.svg'} alt="breed" width="25" height="25"/></span><span className='ml-4 pet-profile-gender'>{this.props.user_pett.gender === 0 ? 'Male' : 'Female'}</span>
                                </div>

                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2">
                                    <span><img src={'/img/cake.svg'} alt="cake" width="25" height="25"/></span> <span className='ml-4 pet-profile-bd'> {DOB? moment(DOB).format("MMM Do") : ""} ({years} {months} {days} {DOB?"old":"Not Filled"} )</span>
                                </div>


                                <div className="mt-2 user-bio-wrapper col-sm-12 bg-light p-2" onMouseEnter={()=>this.props.bioEditIcon('pet')} onMouseLeave={()=>this.props.bioEditIcon('pet')}>
                                    {bio}
                                </div>

                                {/*<div className="mt-2 col-sm-12 bg-light">
                                        <div className="mb-1">
                                            <b className="hobby-intro">{this.props.user_pett.name}'s Hobbies: </b>
                                        </div>
                                        {hobbyLabel}
                                        {addHobby}
                                        <ul className="hobby-list mt-2">
                                            {hobbies}
                                        </ul>
                                </div>*/}
                                </div>

                            {/*MAIN*/}
                            <div className="col-sm-12 col-lg-8">
                                {activeComp}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        }
        return pet
    }
}

const MapState = (state) =>{

    return {
        user_info : state.User.user_info,
        user_pett : state.User.pet,
        activePage : state.User.activeComponent,
        cover : state.User.petProfilePhoto,
        writeBio : state.PetProfile.toggleBio,
        writeHobby : state.PetProfile.toggleHobbies,
        pet_bio : state.PetProfile.bio,
        pet_hobby: state.PetProfile.hobby,
        hobbies : state.PetProfile.hobbies,
        //
        toggleAbout : state.AboutPet.aboutPet,
        //Post props
        caption : state.Posts.caption,//written caption
        photos : state.Posts.photos,// photo to be uploaded
        photo_preview : state.Posts.preview_photos,//preview of photo to be uploaded (Blob Obj)

        //Fetch Posts
        petPosts : state.PetProfile.petPosts,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,

        //EDIT BIO
        inBio : state.PetProfile.toggleEditBioIcon,
        bioEdit :state.PetProfile.editBioMode ,
        newBio : state.PetProfile.newBio,
    }

};


const MapDispatch=(dispatch)=>{
    return {
        navigateComponent : (component)=>{dispatch(navigateComponent(component))},
        pet : (id)=>{dispatch(fetchPet(id))},
        aboutPet : ()=>{dispatch(toggleAboutPet())},
        toggleBio : (pet) => {dispatch(toggleBio(pet))},
        bioHandler : (e)=> {dispatch(bioHandler(e.target.value))},
        addBio : (e,bio,id) => {e.preventDefault();dispatch(addBio(bio,id))},
        closeBio : () =>{dispatch(closePetBio())},
        toggleHobby : () => {dispatch(toggleHobby())},
        hobbyHandler : (e)=>{dispatch(hobbyHandler(e.target.value))},
        addHobby : (hobby,id) =>{dispatch(addHobby(hobby,id))},
        fetchHobbies: (id)=>{dispatch(fetchHobbies(id))},
        //Post actions
        postInputHandler : (name,e) => {dispatch(postInputHandler(name,e)) },
        pet_post : (id)=> {dispatch(fetchPetPosts(id))},
        comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        likePost : (postId,user_id,petId) =>{dispatch(likePost(postId,user_id,petId,"PET_PROFILE",))},
        writeComment : (e) => {dispatch(writeComment(e.target.value))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId))},

        //BIO EDIT
        bioEditIcon : (pet)=> {dispatch(editBioIcon(pet))},
        editBioAction : (newBio,pet)=> {dispatch(editBio(newBio,pet))},
        editBioHandler : (e,pet)=>{dispatch(editBioHandler(e.target.value,pet))},
        bioEditSubmit : (e,newBio,oldBio,id) => {e.preventDefault();dispatch(petBioEditSubmit(newBio,oldBio,id))},

        clearPet : () =>{dispatch(clearPetProfile())}
    }
};

export default connect(MapState, MapDispatch)(PetProfile);
