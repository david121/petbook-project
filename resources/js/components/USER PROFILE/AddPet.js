import React, {Component} from 'react';
import {fetchPetTypes} from "../AUTHENTICATION/REGISTER PET/STORE/Actions";
import {fileHandler, nextHandler, petQuestions, petSubmitHandler, prevHandler} from "../AUTHENTICATION/STORE/Actions";
import {connect} from "react-redux";
import './CSS/AddPet.css';
import moment from 'moment'
import Process from "../LAYOUT/Process";
class AddPet extends Component {
    componentDidMount() {
        this.props.fetchTypes();
    }

    componentDidUpdate(){
        if(this.props.age){
           console.log( this.props.optional);
        }
    }

    render() {
        let ind = this.props.questionIndex;
        let input ;//to be input element to answer question
        let next;// to be button to load next question
        let previous;//to be button to load previous question
        let btnMode ; // next button disabled or not

        // loop through and render questions
        const question = this.props.question.map((item,index)=>{
            return <h6 key={index} className="question">{item.q}</h6>
        });
        let types = this.props.pet_types.map((type,index)=>{
            return <option className="select-options" key={index} value={type.id}>{type.name}</option>
        });
        let breeds = this.props.pet_breeds.map((breed,index)=>{
            return <option className="select-options" key={index} value={breed.id}>{breed.name}</option>
        });

        //inputs for questions
        if (ind === 0){
            input = <select name="petType" className="text-center" value={this.props.type} onChange={(e)=>this.props.answer(e,"pet_type",this.props.pet_types)}>
                <option value="">select type</option>
                {types}
            </select>
        }else
        if (ind === 1){
            input = <input type="text" value={this.props.name} onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 2){
            input = <select name="petBreed" className="text-center answerInput" value={this.props.breed} onChange={(e)=>this.props.answer(e,"pet_breed",null,this.props.pet_breeds)}>
                <option value="">Choose Breed</option>
                {breeds}
            </select>
        }else
        if (ind === 3){
            input = <select name="gender" className="text-center answerInput" value={this.props.gender} onChange={this.props.answer}>
                <option value="">Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
        }else
        if (ind === 4){
            input = <input type="date" value={this.props.age} onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 5){
            input = <input type="text" value={this.props.username} onChange={this.props.answer} className="answerInput"/>
        }else
        // if (ind === 6){
            input = <input type="file" value={''} onChange={this.props.handleFile} className=""/>;
        // }

        //button disabled if not answered
        btnMode = !this.props.answered;

        // Next / Submit Button render
        if (ind + 1 !== this.props.question.length){
            next = <button type="button" className="next-btn-profile" disabled={btnMode} onClick={this.props.nextQuestion}>Next</button>
        }else
        if (ind + 1 === this.props.question.length) {
            next = <button type="submit" className="submit-pet-btn-profile" disabled={btnMode}>complete registration</button>;
        }

        //Previous Btn render
        if (ind === 0){
            previous = <button type="button"  disabled={true} onClick={this.props.prevQuestion}>Previous</button>
        }else previous = <button type="button" onClick={this.props.prevQuestion}>Previous</button>;


        //Age components
        let Age = moment.duration(moment().diff(this.props.age))._data;
            let years = Age.years === 0 ? '' : Age.years === 1 ? '1 year' : Age.years+' years';
            let months = Age.months === 0 ? '' : Age.months === 1 ? '1 month' : Age.months+' months';
            let days = Age.days === 0 ? '' : Age.days === 1 ? '1 day' : Age.days+' days ';


        const formData = new FormData();
        formData.append('user_id', sessionStorage.getItem('user'));
        formData.append('name', this.props.name);
        formData.append('gender', this.props.gender);
        formData.append('type', this.props.type);
        formData.append('breed', this.props.breed);
        formData.append('age', this.props.age);
        formData.append('username', this.props.username);
        formData.append('photo', this.props.file);


        let page = this.props.fromPostAdd ? 'post-add' : 'profile';


        return (
            <div className="list-pet-modal" style={{height:"100%",width:"100%"}}>
                {this.props.fullLoader? <Process message="Listing..."/> : null}
                <div className="add-pet-modal">

                    <div className="addPet-content">
                        <div className="addPet-header">
                            <span className="close" onClick={this.props.close}>&times;</span>
                            <h5>Add Pet</h5>
                        </div>
                        <div className="addPet-body">
                            <div className="row mt-2">
                                <div className="col-lg-7 pet-reg-questions">
                                    <h5>Registration Questions</h5>
                                    <form onSubmit={(e)=>this.props.handleSubmit(e, formData,page)} className="mt-5">
                                        <div className="add-pet-question-wrapper">
                                            {question[this.props.questionIndex]}
                                        </div>

                                        <div className="add-pet-questionInput-box">
                                            {input}
                                        </div>
                                        <div className="question-control">{previous} {next}</div>
                                    </form>

                                    <div className="addPet-footer">
                                        <h3> </h3>
                                    </div>
                        </div>
                        <div className="col-lg-4 pet-reg-display">
                            <h5>Registration Details</h5>
                            <div className="p-3 lg-reg-details">
                                {this.props.photo!== '' ? <div><img src={this.props.photo} alt="pet photo" className="rounded-circle" width={"80"} height={"80"}/></div> : ''}
                                {this.props.type!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Type:</b></span><span className="col-sm-7 reg-answer">{this.props.typeName}</span></div> : ''}
                                {this.props.name!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Name:</b> </span><span className="col-sm-7 reg-answer">{this.props.name}</span></div> : ''}
                                {this.props.breed!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Breed:</b></span><span className="col-sm-7 reg-answer">{this.props.breedName}</span></div> : ''}
                                {this.props.gender!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Gender:</b></span><span className="col-sm-7 reg-answer">{this.props.gender}</span></div> : ''}
                                {this.props.age!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Age:</b></span><span className="col-sm-7 reg-answer">{years} {months} {days}</span></div> : ''}
                                {this.props.username!== '' ? <div className="row p-1"><span className="col-sm-4"><b className={"reg-title"}>Username:</b></span><span className="col-sm-7 reg-answer" style={{wordWrap:"break-word"}}>{this.props.username}</span></div> : ''}
                            </div>
                            <div className="sm-reg-details">
                                <div>
                                    {this.props.photo !== '' ? <span className="mr-2"><img src={this.props.photo} alt="pet photo" className="rounded-circle" width={"80"} height={"80"}/></span> : ""}
                                </div>
                                {this.props.type !== '' ? <span className="mr-2"><b>Type: </b> <span className='reg-answer'>{this.props.typeName}</span></span> : ""}
                                {this.props.name !== '' ? <span className="mr-2"><b>name: </b> <span className='reg-answer'>{this.props.name}</span></span> : ""}
                                {this.props.breed !== '' ? <span className="mr-2"><b>breed: </b> <span className='reg-answer'>{this.props.breedName}</span></span> : ""}
                                {this.props.gender !== '' ? <span className="mr-2"><b>gender: </b> <span className='reg-answer'>{this.props.gender}</span></span> : ""}
                                {this.props.age !== '' ? <span className="mr-2"><b>age: </b> <span className='reg-answer'>{years} {months} {days}</span></span> : ""}
                                {this.props.username !== '' ? <span className="mr-2"><b>username: </b> <span className='reg-answer'>{this.props.username}</span></span> : ""}
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        </div>
        </div>
        );
    }
}


const mapState = (state) =>{
    return {
        question : state.PetReg.questions,
        optional : state.PetReg.optionalQ,
        questionIndex : state.PetReg.currentQues,
        pet_types :state.PetReg.pet_types,
        pet_breeds : state.PetReg.pet_breeds,
        //Was question answered?
        answered :state.PetReg.answered ,
        //pet details
        type: state.PetReg.type,
        typeName: state.PetReg.typeName,
        breedName: state.PetReg.breedName,
        breed: state.PetReg.breed,
        age : state.PetReg.age,
        name : state.PetReg.name,
        gender : state.PetReg.gender,
        file : state.PetReg.file,
        photo: state.PetReg.photo,
        username: state.PetReg.username,
        fullLoader:state.Layout.fullLoader
    }
};

const mapDispatch = (dispatch)=>{
    return{
        fetchTypes : ()=> {dispatch(fetchPetTypes())},
        answer : (e,name,types,breeds) => {dispatch(petQuestions(e.target.value,name,types,breeds))},
        nextQuestion : () =>{dispatch(nextHandler())},
        prevQuestion : () => {dispatch(prevHandler())},
        handleFile : (e) => {dispatch(fileHandler(e.target.files[0]))},
        handleSubmit: (e,data,page) => {
            e.preventDefault();
            dispatch(petSubmitHandler(data,page))
        }
    }
};
export default connect(mapState,mapDispatch)(AddPet);
