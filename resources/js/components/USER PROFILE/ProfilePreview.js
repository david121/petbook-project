import React, {Component} from 'react';
import './CSS/ProfilePreview.css'
import {connect} from "react-redux";
import {profilePreview} from "./STORE/Actions";
import {NavLink} from "react-router-dom";


class ProfilePreview extends Component {
    componentDidMount() {
       this.props.fetchProfile(this.props.profile,this.props.id)
    }
    componentWillUnmount(){
        this.props.fetchProfile(this.props.profile,this.props.id,true)
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.pet.breed.name)
    }

    render() {
        let breed = this.props.pet.breed? this.props.pet.breed.name : "loading";
        let photo = this.props.petPhoto === '' ? "loading" : this.props.petPhoto ;
        let petProfile = <div>
                            <div className={'text-center'}>
                                <img src={photo} alt="photo" width={'100'} height={'100'} className='rounded-circle'/>
                            </div>
                        <div className='row'>{/* d-flex flex-column */}
                            <div className="col-sm-12">
                                <span className='text-light col-sm-5 text-left'><b>Name</b></span>
                                <span className='text-light col-sm-8 text-center ml-4'><b className="text-warning"> {this.props.pet.name}</b></span>
                            </div>
                            <div className="col-sm-12">
                                <span className='text-light col-sm-5 text-left'><b>Age</b></span>
                                <span className='text-light col-sm-8 text-center ml-4'><b className="text-warning"> {this.props.pet.date_of_birth}</b></span>
                            </div>
                            <div className="col-sm-12">
                                <span className='text-light col-sm-5 text-left'><b>Breed</b></span>
                                <span className='text-light col-sm-8 text-center ml-4'><b className="text-warning"> {breed}</b></span>
                            </div>
                            <div className="col-sm-12">
                                <span className='text-light col-sm-5 text-left'><b>Admirers</b></span>
                                <span className='text-light col-sm-8 text-center ml-4'><b className="text-warning"> {this.props.pet.admirers}</b></span>
                            </div>
                        </div>
                            <div className="text-center mt-1">
                                {
                                    this.props.admirer?
                                        <i className="text-center text-light">admired by: <NavLink to={`/pet-owner/${this.props.admirerId}`}>{this.props.admirer}</NavLink></i>
                                    : ''
                                }
                            </div>
                        </div> ;
        return (
            <div className='preview-container'>
                {petProfile}
            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        pet : state.User.pet,
        petPhoto : state.User.petProfilePhoto
    }
};

const mapDispatch = (dispatch) =>{
    return {
        fetchProfile : (profile,id,clear) =>{dispatch(profilePreview(profile,id,clear))},
    }
};


export default connect(mapState,mapDispatch)(ProfilePreview);
