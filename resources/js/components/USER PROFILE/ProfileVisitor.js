import React, {Component} from 'react';
import {connect} from "react-redux";
import Nav from './../LAYOUT/Nav';
import '../VISITING PROFILE/CSS/ProfileVisitor.css';
import {fetchVisitingProfile, follow} from './STORE/Actions';
import {NavLink, withRouter} from "react-router-dom";

class ProfileVisitor extends Component {

    componentDidMount() {
        //Check if user successfully logged in
        if (!localStorage.getItem('token')){
            this.props.history.push('/')
        }
        else{
            //Fetch Profile
            let usn = this.props.match.params.username;
            this.props.profile(usn);
            // Fetch User's Pets
//            this.props.pets();
        }
    }


    render() {

        let data = "loading";
        let image;
        //user photo
        if(Object.keys(this.props.user_profile).length) {
            this.props.usersNearbyCover.map((item,index)=>{
                if (this.props.usersNearbyCover[index] === 'None'){
                    image = <img src="/img/avatar.png" alt="user" width="150" height="130"/>
                } else {
                    image = <img src={item} alt="user" width="150" height="130"/>
                }
            });

        }

        //user bio
        let bio = this.props.user_profile.bio === null ? '' :  <div className="row">
            <span className="fas fa-sticky-note"> </span>
            <span className="ml-4">{this.props.user_profile.bio}</span>
        </div> ;

        //user pets
        let pets = "";

        if(Object.keys(this.props.user_profile).length){

            data = (
                <div>
                    <Nav/>

                    <div className="container">
                    <button onClick={()=>this.props.history.goBack()} className="btn btn-light mt-4 text-info"> back </button>
                        <div className="user-page-info mb-3">
                            <div className="row">
                                {/*User photo*/}
                                <div className="user-image-holder text-center col-sm-6 mt-5">
                                    {image}
                                </div>
                                {/*User follows*/}
                                <div className="col-sm-6 ">
                                    <div className="persona d-flex justify-content-start align-items-end">

                                        <div className="col-sm-6 text-center">
                                            <div className="col-sm-12">
                                                <span>{this.props.user_profile.followers}</span>
                                            </div>
                                            <div className="text-center d-flex justify-content-center border-dark">
                                                <h6>Followers</h6>
                                                <i className="fas fa-user-friends ml-4" onClick={()=>this.props.follower(this.props.user_profile.username,this.props.user_profile.followers,this.props.followed)}> </i>
                                            </div>

                                        </div>

                                        <div className="col-sm-6 text-center">
                                            <span>{this.props.user_profile.liked_pets}</span>
                                            <h6>Liked Pets</h6>
                                        </div>

                                    </div>
                                </div>

                                {/*User Bio*/}
                                <div className="col-sm-12 d-flex justify-content-end">
                                    <div className="user-bio p-2">

                                        <div className="col-sm-12">
                                            <div className="row">
                                                <span className="fas fa-user"> </span>
                                                <span className="ml-4">{this.props.user_profile.username}</span>
                                            </div>
                                        </div>

                                        <div className="col-sm-12">
                                            <div className="row">
                                                <span className="fas fa-city"> </span>
                                                <span className="ml-4 text-primary" id="add-city">{this.props.user_profile.city}, {this.props.user_profile.country} </span>
                                            </div>
                                        </div>

                                        <div className="col-sm-12 mt-2">
                                            {bio}
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        {/*Pets*/}
                        <div className="row">
                            {pets}
                        </div>

                        {/*Posts*/}
                        <div className="d-flex flex-row">

                        </div>


                    </div>
                </div>
            )
        }
        return data
    }
}

const MapState = (state) =>{
    return{
        user_profile : state.User.visitingProfile,
        followed: state.User.followed,
        usersNearbyCover :state.User.visitingProfilePhoto,
    }
};
const MapDispatch = (dispatch) => {

    return{
        profile : (usn) =>{dispatch(fetchVisitingProfile(usn))},
        follower : (usn,num,fol) => {dispatch(follow(usn,num,fol))}
//        pets : () =>{dispatch(fetchPets())},
    }
};
export default connect(MapState,MapDispatch)(withRouter(ProfileVisitor));