
const initialState = {
    pet : {},
    petPic : '',
    visitPetPosts : [],
    profilePreview: false,
    profileId:'',
    toggleBio : false,
    bio : "",
    toggleHobbies : false,
    hobby : "",
    hobbies: [],
    fetchHobby : true,
    petPosts : [],

    //EDITING
    toggleEditBioIcon : false,
    newBio : '',
    editBioMode: false,
    admire_process: false,
};

const PetReducer = (state = initialState, actions) => {
    switch (actions.type) {

        case 'FETCH_VISIT_PET':
            return{
                ...state,
                pet : actions.data[0],
                petPic: actions.src
            };
        case 'VISIT_PET_POSTS':
            return{
                ...state,
                visitPetPosts: actions.posts,
            };
        case 'TOGGLE_BIO_PET':
            return{
                ...state,
                toggleBio: !state.toggleBio
            };
        case 'BIO_HANDLER':
            return{
                ...state,
                bio : actions.value
            };
        case 'ADD_BIO':
            return{
                ...state,
                toggleBio: !state.toggleBio
            };
        case 'BIO_ADDED':
            return{
                ...state,
                bio : '',
                editBioMode:!state.editBioMode
            };

        case 'EDIT_BIO_ICON_PET':
            return{
                ...state,
                toggleEditBioIcon: !state.toggleEditBioIcon,
            };

        case 'TOGGLE_EDIT_BIO_PET':
            return{
                ...state,
                toggleEditBioIcon: !state.toggleEditBioIcon,
                newBio : actions.newBio,
                editBioMode : !state.editBioMode
            };

        case 'NEW_BIO_PET':
            return{
                ...state,
                newBio: actions.edited
            };

        case 'TOGGLE_HOBBY':
            return{
                ...state,
                toggleHobbies: !state.toggleHobbies
            };
        case 'HOBBY_HANDLER':
            return{
                ...state,
                hobby : actions.value
            };
        case 'ADD_HOBBY':
            return{
                ...state,
                toggleHobbies: !state.toggleHobbies
            };
        case 'HOBBY_ADDED':
            return{
                ...state,
                hobby: '',
                toggleHobbies: !state.toggleHobbies
            };
        case 'HOBBIES':
            return{
                ...state,
                hobbies : actions.hobbies,
                fetchHobby: false
            };
        case 'PET_POSTS':
            return{
                ...state,
                petPosts : actions.data,
            };
        case 'ADMIRATION_PROCESS':
            return{
                ...state,
                admire_process: !state.admire_process
            };
        case 'PREVIEW_PET':
            return{
                ...state,
                profilePreview:!state.profilePreview,
                profileId : actions.value
            };
        case 'CLOSE_PREVIEW':
            return{
                ...state,
                profilePreview:!state.profilePreview,
            };
        case 'CLEAR_PET':
            return{
                ...state,
                pet:{},
            };

        default: return state;
    }
};

export default PetReducer;
