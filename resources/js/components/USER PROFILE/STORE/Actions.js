import axios from "axios";
import {Api_Process} from "../../LAYOUT/STORE/Actions";

export const fetchUser = () =>{
    return dispatch => {
      axios.get(`/api/getUser`, {headers:{
          "Content-type":"application/json",
          "Accept":"application/json",
          "Authorization":"Bearer "+ localStorage.getItem('token')
        }})
        .then(response=>{
          if (response.data.success){
             const authenticated = response.data.user.email_verified_at !== null;
             if (!authenticated){
                 localStorage.setItem('authenticate', '!')
             }else localStorage.removeItem('authenticate');
              dispatch({
                type : 'FETCH_USER',
                data: response.data.user,
                photo : response.data.src,
                verified:authenticated
              });
            //console.log( response.data.user);
          }else{
            console.log(response.data.msg);
          }
        })
        .catch(error=>{
          console.log("Error : " + error)
        });
    };
};

export const fetchPets = (user) =>{
  return dispatch => {
    axios.get(`/api/fetchPets`,{headers:{
        "Content-type":"application/json",
        "Accept":"application/json",
        "Authorization":"Bearer "+ localStorage.getItem('token')
      },params:{profileId:user}})
      .then(response=>{
        if (response.data.success){
         // console.log("response", response.data.pet)
          dispatch({
            type : 'FETCH_USER_PETS',
            data: response.data.pet,
            src : response.data.src
          })
        } else{
          console.log(response.data.msg)
        }

      })
      .catch(error=>{
        console.log(error)
      });
  }
};

export const managePets = () =>{
    return dispatch =>{
        dispatch({
            type : "MANAGE_PETS"
        })
    }
};

export const clearPetsPanel = () =>{
    return dispatch =>{
           dispatch({type:"CLEAR_PETS"})
    }
};
export const clearPetProfile = () =>{

    return dispatch =>{
           dispatch({type:"CLEAR_PET"})
    }
};

export const toggleAboutPet=(visitor, closeAction = false)=>{
    return dispatch=>{
        if (visitor){
            dispatch({type:"ABOUT_VISITOR_PET", closeAction})
        }
        dispatch({type:"ABOUT_PET", closeAction})
    }
};

export const fetchVisitPet = (pet) =>{
    return dispatch => {
        axios.get(`/api/fetchPet`,{
            headers:{
                "Content-type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{pet:pet}
        })
            .then(response=>{
                if (response.data.success){
                    // console.log("response", response.data.pet)
                    dispatch({
                        type : 'FETCH_VISIT_PET',
                        data: response.data.pet,
                        src : response.data.src
                    })
                } else{
                    console.log(response.data.msg)
                }

            })
            .catch(error=>{
                console.log(error)
            });
    }
};
export const fetchVisitPetPosts = (pet,petOwner) =>{
    return dispatch => {
        axios.get(`/api/petPosts`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{pet:pet,petOwner:petOwner}
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : 'VISIT_PET_POSTS',
                        posts: response.data.posts,
                    })
                } else{
                    console.log(response.data.msg)
                }

            })
            .catch(error=>{
                console.log(error)
            });
    }
};

export const fetchPet = (petId) =>{
    return dispatch => {
        axios.get(`/api/fetchPet`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer " + localStorage.getItem('token'),
            },
        params:{
           'pet_id' : petId,
        }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : 'FETCH_PET',
                        data: response.data.pet,
                        petPhoto : response.data.src
                    })
                } else{
                    console.log(response.data.msg)
                }

            })
            .catch(error=>{
                console.log(error)
            });

    }
};

export const similarPets = (breedId) =>{
    return dispatch =>{
            axios.get('/api/pets/similarPets',{headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":"Bearer "+ localStorage.getItem('token')
                },params:{breedId:breedId}})
                .then(response=>{
                    if (response.data.success){
                        dispatch({type:"SIMILAR_PETS",pets:response.data.pets})
                    }
                })
                .catch(error=>{

                })
    }
};

export const fetchPetPosts = (petId) =>{
    return dispatch => {
        axios.get(`/api/petPosts`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{
                'id' : petId,
            }
        })
            .then(response=>{
                if (response.data.success){
                   // console.log(response, "FETCH PET POSTS")
                    dispatch({
                        type : 'PET_POSTS',
                        data: response.data.posts,
                    })
                } else{
                    console.log(response.data.msg)
                }
            })
            .catch(error=>{
                console.log(error)
            });

    }
};

export const fetchHobbies = (petId) =>{
    return dispatch => {
        axios.get(`/api/hobbies`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{
                'pet_id' : petId,
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : 'HOBBIES',
                        hobbies: response.data.hobbies,
                    })
                } else{
                    //console.log(response.data.msg)
                }

            })
            .catch(error=>{
                console.log(error)
            });
    }
};

export const fetchVisitingProfile = (id) =>{
    return dispatch => {
        axios.get(`/api/fetchProfile`,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{
                'profileId' : id,
            }
        })
            .then(response=>{
                if (response.data.success){
                    // console.log(response.data.friends);
                    dispatch({
                        type : 'VISITING_PROFILE',
                        data: response.data.profile,
                        src: response.data.src,
                        friends:response.data.friends
                    })
                } else{
                    console.log(response.data.msg)
                }
            })
            .catch(error=>{
                console.log(error)
            });
    }
};

export const ProfilePhotoUploadHandler = (photo) =>{
    // console.log(photo.name);
    return dispatch =>{
        if(!photo.name.match(/.(jpg|png|gif|JPG|jpeg|JPEG|TIFF)$/i)){
            dispatch({type:"INVALID_FORMAT_USER"});
            return false;
        }else if (photo.size > 10000000){
            dispatch({type:"FILE_LARGE_USER"});
            return false;
        }
        dispatch({
            type: "SELECTED_PROFILE_PHOTO",
            photo : photo
        })
    }
};

export const CancelPhotoUpload = () =>{
    return dispatch =>{
        dispatch({
            type: "CANCEL_UPLOAD",
        })
    }
};

export const addPet= (registered = false)=>{
    return dispatch =>{
        dispatch ({
            type : "ADD_PET_MODAL",
            registered : registered,
        })
    }
};

export const petEdit= (pet) =>{
    return dispatch =>{
        dispatch({
            type: "EDIT_PET_MODAL",
            pet : pet
        })
    }
};

export const navigateManagePet = (profile) =>{
    return dispatch =>{
        let value="EDIT_PET";
        if (profile === 'transfer'){
           value = "TRANSFER_PET"
        }else if (profile === 'lineage'){value = "LINEAGE"}
        dispatch({
            type : "NAVIGATE_PAGE",
            value: value
        })
    }
};


export const editPetHandler= (name,e) =>{
    return dispatch =>{
        // console.log(name, e.target.value)
        if (name === "photo"){
            const file = e.target.files[0];
            if(!file.name.match(/.(jpg|png|gif|JPG|jpeg|JPEG|TIFF)$/i)){
                dispatch({
                    type : "INVALID_FORMAT"
                });
                return false;
            }else if (file.size > 10000000){
                dispatch({type:"FILE_LARGE_PET"});
                return false;
            }
            const ext = file.name.substring(file.name.lastIndexOf('.')+1, file.name.length) || file.name;
            const preview = window.URL.createObjectURL(file);
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = function() {
                let base64data = reader.result.split(',')[1];
                dispatch({
                    type: "EDIT_PET_INPUT_HANDLER",
                    name:name,
                    value:base64data,
                    ext: ext,
                    content_type: file.type,
                    preview : preview
                })
            }
        }
        dispatch({
            type: "EDIT_PET_INPUT_HANDLER",
            name:name,
            value:e.target.value
        })
    }
};

export const removePetPic = () =>{
    return dispatch=>{
              dispatch({type:"REMOVE_PIC"})
    }
};

export const deletePetProfile = (id) =>{
    return dispatch=>{
        dispatch(Api_Process());
        axios.post('/api/pet/destroy',{petId:id},{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
        })
            .then(res=>{
                dispatch(Api_Process());
                if (res.data.success){
                    dispatch(fetchPets());
                    dispatch({type:"CLOSE_EDIT_PET_MODAL"});
                }
            })
            .catch(()=>dispatch(Api_Process()))
    }
};


// export const changePetPic = (file) =>{
//     return dispatch =>{
//         // console.log(file);
//         if(!file.name.match(/.(jpg|png|gif)$/i)){
//             dispatch({
//                 type : "INVALID_FORMAT"
//             });
//             return false;
//         }
//     }
// };

const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {type: contentType});
};

export const saveEditedPet = (data) =>{

    // console.log(data);

    if(data.content_type !== '')data.photo = b64toBlob(data.photo, data.contentType);
    // if (data.photo === "avatar") data.photo =
    const formData = new FormData();
    formData.append("name", data.name);
    formData.append("petId", data.petId);
    formData.append("username",data.username);
    formData.append("d_o_b",data.d_o_b);
    formData.append("breed",data.breed);
    formData.append("photo", data.photo);
    formData.append("ext", data.ext);

    return dispatch =>{
        dispatch(Api_Process());
        axios.post('/api/pet/update',formData,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
        })
            .then(response=>{
                dispatch(Api_Process());
                if (response.data.success){
                    dispatch(fetchPets());
                    dispatch({type : "PET_PROFILE_UPDATED"})
                }
            })
            .catch(error=>{
                dispatch(Api_Process());
                // console.log(error)
            })
    }
};


export const uploadProfilePhoto = (photo) =>{
        return dispatch =>{
            dispatch(Api_Process());
            axios.post('api/user_cover',photo,{
                    headers:{
                        "Content-Type":"application/json",
                        "Accept":"application/json",
                        "Authorization":"Bearer "+ localStorage.getItem('token')
                    },
                })
                    .then(response=>{
                        dispatch(Api_Process());
                        if (response.data.success){
                            dispatch({
                                type : "USER_COVER_UPLOADED"
                            })
                        }
                    })
                    .catch(error=>{
                        dispatch(Api_Process());
                        // console.log(error)
                    })
        }
};

export const toggleCity = () =>{
    return dispatch =>{
        dispatch({
            type : "TOGGLE_CITY",
        })
    }
};

export const cityHandler = (city) =>{
    return dispatch =>{
        dispatch({
            type : "CITY_HANDLER",
            value: city
        })
    }
};

export const cityEditSubmit = (newCity,oldCity) =>{
  return dispatch =>{
      if (newCity === oldCity){
           dispatch(editCity(''));
      }else{
          axios.post('/api/user/city',{city:newCity},{
              headers:{
                  "Content-Type" : "application/json",
                  "Accept" : "application/json",
                  "Authorization" : "Bearer " + localStorage.getItem('token')
              }
          })
              .then(response=>{
                  if (response.data.success){
                      dispatch(fetchUser());
                      dispatch({
                          type : "CITY_EDITED",
                      })
                  }
              })
              .catch(error=>{
                  console.log(error)
              });
      }

  }
};

export const addCity = (city) =>{
    return dispatch =>{
          if (city === ''){
              dispatch(toggleCity());
          }else{
            axios.post('/api/user/city',{city:city},{
                headers:{
                    "Content-Type" : "application/json",
                    "Accept" : "application/json",
                    "Authorization" : "Bearer " + localStorage.getItem('token')
                }
            })
                .then(response=>{
                    if (response.data.success){
                        dispatch(fetchUser());
                        dispatch({
                            type : "CITY_ADDED",
                            oldCity: oldCity
                        })
                    }
                })
                .catch(error=>{
                    console.log(error)
                });
    }}
};

export const toggleBio = (pet) =>{
    return dispatch =>{
        if (pet){
            // console.log("PET");
            dispatch({
                type: 'TOGGLE_BIO_PET'
            })
        }
        dispatch({
            type : "TOGGLE_BIO",
        })
    }
};

export const bioHandler = (bio) =>{
    //console.log(bio,"onchange");
    return dispatch =>{
        dispatch({
            type : "BIO_HANDLER",
            value: bio
        })
    }
};

export const findPetBreed = (breedId) =>{
    return dispatch =>{
        axios.get('/api/findBreed',{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
            params:{breedId:breedId}
        }).then(response=>{
            if (response.data.success){
                dispatch({
                    type : "FIND_PET_BREED",
                    breed : response.data.breed,
                    breeds : response.data.breeds
                })
            }
        }).catch(error=>{
            console.log(error)
        })
    }
};


export const userBioHandler = (bio) =>{
    return dispatch =>{

        dispatch({
            type : "USER_BIO_HANDLER",
            value: bio
        })
    }
};

export const editBioHandler = (edited,pet) =>{
    return dispatch =>{
        if (pet){
            dispatch({
                type : "NEW_BIO_PET",
                edited: edited
            })
        }
        dispatch({
            type : "NEW_BIO",
            edited: edited
        })
    }
};



export const addUserBio = (newBio,oldBio) =>{
    return dispatch =>{
        if (newBio === '' || oldBio !== ''){
            if (newBio === oldBio && oldBio === null){
                dispatch({
                    type : "TOGGLE_EDIT_BIO",
                    newBio : ''
                })
            }else{
            const data = {
                bio : String(newBio),
            };
            axios.post('/api/user/bio',data,{
                headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":"Bearer "+ localStorage.getItem('token')
                },
            }).then(response=>{
                if (response.data.success){
                    dispatch(fetchUser());
                    dispatch({
                        type : "USER_BIO_ADDED",
                        oldBio:oldBio
                    })
                }
            }).catch(error=>{
                console.log(error)
            })
        }}else{
            dispatch({
                type : "ADD_USER_BIO",
            })
        }
    }
};


export const petBioEditSubmit = (newBio, oldBio, petId) =>{
     //console.log("NEW: ", newBio, "OLD: ", oldBio)
    return dispatch =>{
            let old = false;
              if (newBio === oldBio){
                  dispatch(editBio('','pet'))
              }else{
                  old=true;
                  dispatch(addBio(newBio,petId,old))
              }
    }
};

export const closePetBio = () =>{
    return dispatch =>{
        dispatch({type:"ADD_BIO"})
    }
};

export const addBio = (bio,id,old) =>{
    return dispatch =>{

        if (bio.length > 0 || old){
            const data = {
                bio : String(bio),
                id : id
            };
            axios.post('/api/petBio',data,{
                headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":"Bearer "+ localStorage.getItem('token')
                },
            }).then(response=>{
                if (response.data.success){
                    dispatch(fetchPet(id));
                    dispatch({
                        type : "BIO_ADDED",
                    })
                }
            }).catch(error=>{
                console.log(error)
            })
        }else{
        dispatch({
            type : "ADD_BIO",
        })
        }
    }
};


export const editCityIcon = () =>{
    return dispatch =>{
        dispatch({
            type : "EDIT_CITY_ICON",
        })
    }
};
export const editCity = (newCity) =>{
    return dispatch =>{
        dispatch({
            type : "TOGGLE_EDIT_CITY",
            newCity: newCity
        })
    }
};
export const editCityHandler = (edited) =>{
    return dispatch =>{
        dispatch({
            type : "NEW_CITY",
            edited: edited
        })
    }
};

export const editBioIcon = (pet) =>{
    return dispatch =>{
        if (pet){
            dispatch({
                type : "EDIT_BIO_ICON_PET",
            })
        }
        dispatch({
            type : "EDIT_BIO_ICON",
        })
    }
};
export const editBio = (newBio,pet) =>{
    return dispatch =>{
        if (pet){
            dispatch({
                type : "TOGGLE_EDIT_BIO_PET",
                newBio :newBio
            })
        }
        dispatch({
            type : "TOGGLE_EDIT_BIO",
            newBio: newBio
        })
    }
};
export const toggleHobby = () =>{
    return dispatch =>{
        dispatch({
            type : "TOGGLE_HOBBY",
        })
    }
};

export const hobbyHandler = (hobby) =>{
    return dispatch =>{
        dispatch({
            type : "HOBBY_HANDLER",
            value: hobby
        })
    }
};
export const addHobby = (hobby,id) =>{
    return dispatch =>{

        if (hobby.length > 0){
            const data = {
                hobby : String(hobby),
                id : id
            };
            axios.post('/api/hobbies',data,{
                headers:{
                    "Content-Type":"application/json",
                    "Accept":"application/json",
                    "Authorization":"Bearer "+ localStorage.getItem('token')
                },
            }).then(response=>{
                if (response.data.success){
                    dispatch(fetchHobbies(id));
                    dispatch({
                        type : "HOBBY_ADDED",
                    })
                }
            }).catch(error=>{
                console.log(error)
            })
        }else{
            dispatch({
                type : "ADD_HOBBY",
            })
        }
    }
};

export const loadImage = () =>{
    return dispatch =>{
        dispatch({
            type:"LOAD_PROFILE_IMAGE"
        })
    }
};

export const toggleProfilePreview = (user,id) =>{
    return dispatch =>{
        if (user === 'user'){

        }else if (user==='pet'){
            dispatch({
                type:"PREVIEW_PET",
                value : id,
            })
        }
        dispatch({
            type:"CLOSE_PREVIEW",
        })
    }
};

export const profilePreview = (profile,id,clearProfile) =>{
    // console.log(id)
    return dispatch =>{
        if (profile === 'user'){

        }else{
           if (clearProfile)dispatch({type:"FETCH_PET",clearProfile:true});
            dispatch(fetchPet(id))
        }
    }
};


export const navigateComponent = (component) =>{
    return dispatch =>{
        dispatch({
            type : "RENDER_COMPONENT",
            component : component
        })
    }
};

export const toggleLocation = () =>{
    return dispatch =>{
        axios.post('/api/location',null,{
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            },
        }).then(res=>{
            if (res.data.success){
                dispatch(fetchUser());
            }
        });
    }
};
