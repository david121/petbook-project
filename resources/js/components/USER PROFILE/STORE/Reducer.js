const initialState = {
    user_info : {},
    user_pets : [],
    activeComponent : 'timeline',
    pet : {},
    user_photo:[],
    photoPreview:null,
    isPreview : false,
    uploadedPhoto : null,
    invalidFile: false,
    user_file_large: false,
    uploaded :false,
    user_pet_photos : [],// user pets on user page
    petProfilePhoto: '',// user pet profile page
    visitingProfile: {},
    profileFriends:[],
    visitingProfilePhoto: '',
    inputCity : false,
    followed : false,
    follow : 0,
    loadProfilePic : false,

    addPetModal : false,
    managePetMode : false,
    editPetModal : false,
    petToEdit:null,
    petBreed : '',
    typeBreeds : [],
    //control server requests
    fetchUser : true,
    fetchUserPets : true,
    //user page info
    toggleCity : false,
    city : "",
    toggleUserBio : false,
    bio : "",
    //Editing
    toggleEditBioIcon:false,
    bioEditMode : false,
    newBio : '',

    toggleEditCityIcon : false,
    cityEditMode : false,
    newCity : '',
    //
    userFriendsToggle : false,
    likedPetsToggle : false,
    //
    admire : false,
    unadmiredPet : '',

    userVerified: false,

};

const UserProfile = (state = initialState, actions) => {
    switch (actions.type) {
        case 'FETCH_USER':
            return{
                ...state,
                user_info: actions.data,
                user_photo : actions.photo,
                fetchUser: false,
                userVerified :actions.verified
            };

        case 'RENDER_COMPONENT':
            return{
                ...state,
                activeComponent: actions.component
            };

        case 'TOGGLE_USER_FRIENDS':
            return{
                ...state,
                userFriendsToggle: !state.userFriendsToggle,
            };
        case 'CLEAR_PETS':
            return{
                ...state,
                user_pets:[]
            };

        case 'TOGGLE_ADMIRED_PETS':
            return{
                ...state,
                likedPetsToggle: !state.likedPetsToggle,
            };

        case "ADMIRATION_CANCELED" :
            return {
                ...state,
                admire: !state.admire,
                unadmiredPet: actions.pet
            };
        case "PET_ADMIRED" :
            return {
                ...state,
                admire: false,
            };
        case "PET_ADMIRED_FROM_VISITOR" :
            return {
                ...state,
                admire: !state.admire,
                unadmiredPet: actions.pet
            };

        case "MANAGE_PETS" :
            return {
                ...state,
                managePetMode : !state.managePetMode,
            };

        case " AAS" : //ADD_PET_MODAL

            return {
                ...state,
                addPetModal: !state.addPetModal
            };

        // case 'PET_SUBMIT' :
        //     return {
        //         ...state,
        //         addPetModal: !state.addPetModal
        //     };

        case "EDIT_PET_MODAL" :
            return {
                ...state,
                editPetModal: !state.editPetModal,
                petToEdit: actions.pet,
            };
        case "CLOSE_EDIT_PET_MODAL" :
            return {
                ...state,
                editPetModal: !state.editPetModal,
                petToEdit: null,
                managePetMode:false
            };


        case "PET_TRANSFERRED" :
            return {
                ...state,
                editPetModal: !state.editPetModal,
                managePetMode:false
            };

        case "PET_PROFILE_UPDATED" :
            return {
                ...state,
                editPetModal: !state.editPetModal,
                managePetMode : !state.managePetMode
            };

        case "FIND_PET_BREED" :
            return {
                ...state,
                 petBreed : actions.breed,
                typeBreeds: actions.breeds

            };

        case 'SELECTED_PROFILE_PHOTO':
            let preview;
            if (actions.photo !== '' && actions.photo !==undefined){
                 preview = window.URL.createObjectURL(actions.photo);
                return{
                    ...state,
                    photoPreview: preview,
                    isPreview: true,
                    uploadedPhoto: actions.photo,
                    invalidFile: false,
                };
            }

            return{
                ...state,
                isPreview: false,
                photoPreview: preview,
                uploadedPhoto: actions.photo
            };

        case 'INVALID_FORMAT_USER':
            return{
                ...state,
                invalidFile: true
            };
        case 'FILE_LARGE_USER':
            return{
                ...state,
                user_file_large: true
            };

        case 'CANCEL_UPLOAD':
            return{
                ...state,
                uploadedPhoto: null,
                isPreview: false
            };

        case 'USER_COVER_UPLOADED':
            return{
                ...state,
                isPreview: true,
                uploadedPhoto: null,
                uploaded: true,
                invalidFile:false
            };

        case 'FETCH_USER_PETS':
            return{
                ...state,
              user_pets: actions.data,
              user_pet_photos : actions.src,
              fetchUserPets: false
            };
        case 'FETCH_PET':
            if (actions.clearProfile){
                // console.log("poop")
                return {
                    ...state,
                    pet: {},
                    petProfilePhoto: '',
                }
            }
            return{
                ...state,
                pet: actions.data[0],
                petProfilePhoto: actions.petPhoto,
            };
        case 'VISITING_PROFILE':
            return{
                ...state,
                visitingProfile: actions.data[0],
                profileFriends :actions.friends,
                visitingProfilePhoto: actions.src
            };

        case 'FOLLOW':
            const follow = state.follow === 0? state.follow +1 : state.follow -1;
            return{
                ...state,
                followed: !state.followed,
                follow : follow
            };
        case 'EDIT_CITY_ICON':
            return{
                ...state,
                toggleEditCityIcon: !state.toggleEditCityIcon,
            };
        case 'TOGGLE_EDIT_CITY':
            if (actions.newCity === ''){
                return{
                    ...state,
                    newCity : actions.newCity,
                    cityEditMode : !state.cityEditMode,
                    toggleEditCityIcon: !state.toggleEditCityIcon,
                };
            }
            return{
                ...state,
                newCity : actions.newCity,
                cityEditMode : !state.cityEditMode
            };
        case 'CITY_EDITED':
            return{
                ...state,
                newCity : '',
                cityEditMode : !state.cityEditMode,
                toggleEditCityIcon: !state.toggleEditCityIcon,
            };
        case 'NEW_CITY':
            return{
                ...state,
                newCity : actions.edited,
            };
        case 'TOGGLE_CITY':
            return{
                ...state,
                toggleCity: !state.toggleCity
            };
        case 'CITY_HANDLER':
            return{
                ...state,
                city: actions.value
            };

        case 'TOGGLE_BIO':
            return{
                ...state,
                toggleUserBio: !state.toggleUserBio
            };
        case 'USER_BIO_HANDLER':
            return{
                ...state,
                bio: actions.value
            };
        case 'ADD_USER_BIO':
            return{
                ...state,
                toggleUserBio: !state.toggleUserBio
            };
        case 'USER_BIO_ADDED':
            if (actions.oldBio === undefined){
                return {
                    ...state,
                    bio: '',
                    toggleUserBio: !state.toggleUserBio,
                    toggleEditBioIcon: !state.toggleEditBioIcon,
                }
            }
            return{
                ...state,
                newBio: '',
                bioEditMode : !state.bioEditMode,
                toggleEditBioIcon: !state.toggleEditBioIcon,
            };
        case 'CITY_ADDED':
                return {
                    ...state,
                    city: '',
                    toggleCity: !state.toggleCity,
                };

        case 'EDIT_BIO_ICON':
            return{
                ...state,
                toggleEditBioIcon: !state.toggleEditBioIcon,
            };
        case 'TOGGLE_EDIT_BIO':
            if (actions.newBio === ''){
                return{
                    ...state,
                    newBio : actions.newBio,
                    bioEditMode : !state.bioEditMode,
                    toggleEditBioIcon: !state.toggleEditBioIcon,
                };
            }
            return{
                ...state,
                newBio : actions.newBio,
                bioEditMode : !state.bioEditMode
            };
        case 'NEW_BIO':
            return{
                ...state,
                newBio : actions.edited,
            };
        case 'LOAD_PROFILE_IMAGE':
            return{
                ...state,
                loadProfilePic : !state.loadProfilePic,
            };
        case 'CLEAR_USER_PROFILE':
            return{
                ...state,

            };

        default: return state;
    }
};

export default UserProfile;
