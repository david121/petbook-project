import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import "./CSS/Likers.css"
import {closeLikesModal, postLikers} from "./STORE/Actions";
class Likers extends Component {
    componentDidMount() {
       //this.props.postLikers(this.props.postId)
    }
    componentWillUnmount(){
        this.props.closeLikesModal(false)
    }

    render() {
        let likers = "loading";
        if (Object.keys(this.props.likers).length) {
            if (this.props.likers === "None") {
                likers = <div className={"text-center"}>
                    <b className="text-center mt-3" style={{fontFamily:"Cambria", color:"darkslategrey"}}>No likes on this post</b>
                </div>
            } else {
                likers = this.props.likers.map((liker, index) => {
                    return <li key={index} className="text-left p-2">
                            {liker.photo_path === null ? <img src={'/img/avatar.png'} alt="user" width="30" height="25"
                                                            className="rounded-circle mr-2"/> :
                                <img src={`/storage/users/${liker.id}/${liker.photo_path}`} alt={'user'}
                                     className="rounded-circle mr-2" width="30" height="25"/>
                            }
                            <NavLink style={{fontFamily:"Cambria", fontSize:"17px"}}
                                to={this.props.activeUser === liker.username ? `/profile` : `pet-owner/${liker.id}`}>
                                {liker.username}
                            </NavLink>
                    </li>
                });
            }
        }
        return (
            <div className="liker-modal">
                <div className="liker-modal-content">
                    <div className="liker-modal-header">
                        <span className="liker-close" onClick={()=>this.props.closeLikesModal(true)}>&times;</span>
                        <h5>Likes</h5>
                        {/*<img src={'/img/paws.svg'} alt="likes" width={30} height={30}/>*/}
                    </div>
                    <div className="liker-modal-body">
                        <ul>{likers}</ul>
                    </div>
                    <div className="aboutPet-footer">
                        <h3> </h3>
                    </div>
                </div>
            </div>
        );
    }
}

const mapState = (state) =>{
    return{
        likers : state.Posts.likers,
    }
};

const mapDispatch = (dispatch) =>{
    return {
        closeLikesModal : (clicked) => dispatch(closeLikesModal(clicked)),
        postLikers : (postId)=>{dispatch(postLikers(postId))},
    }
};

export default connect(mapState,mapDispatch)(Likers);