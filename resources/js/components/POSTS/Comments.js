import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    clearComments,
    commentDots, deleteComment, fetchComments, likeComment,
    replyComment,
    replyCommentInput,
    submitReply,
    toggleCommentOptions,
    viewReplies
} from "./STORE/Actions";
import './CSS/Comment.css';
import {NavLink} from "react-router-dom";
import ReactEmoji from "react-emoji";
class Comments extends Component {

    componentDidMount() {
        if(this.props.allComments > 0)
        this.props.fetchComments(this.props.postId)
    }
    componentWillUnmount() {
        this.props.clearComments()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.page)
    }

    getComments(commentList, level = 0)  {
        let commentReply;
        let replies ;
        let viewReply;
        let commentDropdown;


        let comments = "loading";
        if (this.props.allComments === 0){
            comments =  <div className="card comments">
                <div className="card-header comment-header"> </div>
                <div className="card-body comment-body">Be the first to comment !</div>
            </div>
        }
        else{
            comments = commentList.map((comment,index)=>{
                let link = comment.user_id === this.props.activeUser.id ? `/profile` : `/pet-owner/${comment.user_id}`;
                let likeIcon = comment.liked ===1
                    ?
                    <img src={'/img/paws.svg'} alt={"like"} width="20" height="20" className="reply-liked comment-like mr-1" onClick={()=>this.props.likeComment(comment.id,this.props.postId,this.props.page)}/>
                    :
                    <img src={'/img/paw.svg'} alt={"like"} width="20" height="20" className="comment-like mr-1" onClick={()=>this.props.likeComment(comment.id,this.props.postId,this.props.page)}/>;

                let children = null;
                if(comment.hasOwnProperty('children') && comment.children.length > 0){
                    /*
                    let numOfReplies = comment.children.length;
                    if (numOfReplies === 1){
                        viewReply = this.props.showReplies && this.props.commentToToggle === comment.id? ' hide reply ' : ' view reply';
                    }else if (numOfReplies > 1){
                        viewReply = this.props.showReplies && this.props.commentToToggle === comment.id? ' hide replies ' : 'view ' + numOfReplies + " replies";
                    }
                    replies = comment.replies.length > 0 ? comment.replies.map((item,index)=>{
                        let userPic = item.user.photo_path === null ? <img src={"/img/avatar.png"} alt="user" className="rounded-circle" width={"30"} height={"32"}/>
                            : <img src={`/storage/${item.user_id}/${item.user.photo_path}`} alt="user" className="rounded-circle" width={"30"} height={"32"}/>;
                        return <div key={index} className="ml-5 p-2">
                            <div onMouseEnter={null} onMouseLeave={null}>
                                <span className="mr-2">{userPic} <b className="ml-2 username commentReplies-username">{item.user.username}</b></span>
                                <span className="comment-body">{item.comment}</span>
                                <span className="ml-2 text-muted comment-options-dots">...</span>
                                <div className="ml-5">
                                    <span className="far fa-heart comment-like mr-3" />
                                    <span className="comment-reply" onClick={()=>this.props.replyComment(comment.id)}><i className="fas fa-reply"/></span>
                                </div>
                            </div>
                        </div>
                    }): '';*/

                    children = this.getComments(comment.children, level + 1);
                }

                commentReply = this.props.reply && this.props.commentId === comment.id?
                    <form onSubmit={(e)=>this.props.submitReply(e,comment.post_id,comment.id,this.props.repliedComment,this.props.page)}>
                        <div className="comment-reply-wrapper">
                            <div className="">
                                {
                                    this.props.activeUser.photo_path === null ?
                                    <img src={`/img/avatar.png`} alt={"user"} width={"30"} height={"32"} className="rounded-circle reply-img"/>
                                    :
                                    <img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle reply-img"/>
                                }
                                {/*<img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle reply-img"/>*/}
                            </div>
                            <div className="">
                                <input onChange={this.props.replyCommentInput} value={this.props.repliedComment} onBlur={()=>this.props.replyComment(comment.id)} autoFocus={true} className="comment-reply-input" placeholder="press enter to submit"/>
                            </div>
                        </div>
                    </form>
                : '';

                return (
                    <div key={level + '_' + index}>
                        <div className="card comments" style={{marginLeft: `${level * 40}px`}}>
                            <div className="card-body" onMouseEnter={()=>this.props.commentDots(comment.id)} onMouseLeave={()=>this.props.commentDots(comment.id)}>
                                <span className="mr-2">
                                    {comment.photo_path === null ?
                                        <img src={'/img/avatar.png'} alt="user" width={"30"} height={"32"} className="rounded-circle"/>
                                            :
                                        <img src={`/storage/users/${comment.user_id}/${comment.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle"/>
                                    }
                                    <NavLink to={link} style={{textDecoration:"none"}}> <b className="ml-2 username comment-username">{comment.username}</b></NavLink>
                                </span>

                                <span className="comment-body">{ReactEmoji.emojify(comment.comment)}</span>

                                <span className="ml-2 text-muted">
                                    {this.props.showCommentOptions && this.props.commentToShowOptions === comment.id && comment.user_id === this.props.activeUser.id?<b className="comment-options-dots" onClick={()=>this.props.toggleCommentOptions(comment.id)}>...</b> : ''}
                                    {/*<span>{commentDropdown}</span>*/}
                                    {this.props.commentOptions && this.props.commentToToggleOptions === comment.id?
                                        <ul className="post-option-drop text-left" onClick={e => e.stopPropagation()}>
                                            {/*<li onClick={()=>this.props.editComment(comment.id)}>Edit</li>*/}
                                            <li onClick={()=>this.props.deleteComment(comment.post_id,comment.id,this.props.page,this.props.visitId)}>Delete</li>
                                        </ul> : null
                                    }

                                </span>
                                <div className="ml-5">
                                    {likeIcon}{comment.likes > 0 ? <small className="p-1 comment-like-num">{comment.likes}</small> : ''}
                                    <span className="comment-reply ml-3" onClick={()=>this.props.replyComment(comment.id)}><img src={"/img/reply.svg"} alt="reply" width={'20'} height={'20'}/></span>
                                    <small className="text-muted ml-2"> {comment.time}</small><small className="ml-2 text-info view-reply" onClick={()=>this.props.viewReplies(comment.id)}>{viewReply}</small>
                                </div>
                                <div>{commentReply}</div>
                                <span>{this.props.showReplies && this.props.commentToToggle === comment.id ? replies : ''}</span>
                            </div>
                        </div>
                        {children}
                    </div>
                )
            })
        }
        return comments
    }



    render() {
        let commentPreview;

        if(this.props.writtenComment !== ''){
            commentPreview = <div className="post-comment-prev ml-4 mb-3">
                    <span>
                           {
                               this.props.activeUser.photo_path === null
                                   ?
                                   <img src={'/img/avatar.png'} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                                   :
                                   <img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                           }
                    </span>
                <span className="the-comm-prev"><span className="mr-2">{this.props.activeUser.username} </span>{this.props.writtenComment}</span>
            </div>
        }
        let content;
        if (Object.keys(this.props.comments).length){
            content = this.getComments(this.props.comments);
        }else content = "loading comments";
        return (
            <div className={"comment-container"}>
                {commentPreview}
                {content}
            </div>
        );
    }
}

const MapState = (state)=>{
    return{
        activeUser : state.User.user_info,
        comments : state.Comments.comments,
        covers : state .Comments.profilePics,
        reply : state.Comments.reply,
        commentId : state.Comments.commentToReplyId,
        repliedComment : state.Comments.repliedComment,
        showReplies : state.Comments.showReplies,
        commentToToggle : state.Comments.commentToShowRepliesId,
        showCommentOptions : state.Comments.commentDots,
        commentToShowOptions : state.Comments.commentToShowOptions,
        commentOptions : state.Comments.commentOptions,
        commentToToggleOptions : state.Comments.commentToToggle,

        //post comment preview
        writtenComment : state.Comments.comment,
    }
};

const MapDispatch = (dispatch) =>{
    return {
        fetchComments : (id) =>{dispatch(fetchComments(id))},
        replyComment : (commentId) =>{dispatch(replyComment(commentId))},
        replyCommentInput : (e) =>{dispatch(replyCommentInput(e.target.value))},
        submitReply : (e,postId,commentId,comment,page) => {e.preventDefault();dispatch(submitReply(postId,commentId,comment,page))},
        viewReplies : (commentId) => {dispatch(viewReplies(commentId))},
        commentDots : (commentId) => {dispatch(commentDots(commentId))},
        toggleCommentOptions : (commentId) => {dispatch(toggleCommentOptions(commentId))},
        likeComment : (commentId,postId,page) => {dispatch(likeComment(commentId,postId,page))},
        // editComment : (commentId,) => {dispatch(editComment(commentId))},
        deleteComment : (postId,commentId,page,id) => {dispatch(deleteComment(postId,commentId,page,id))},
        clearComments : () =>{dispatch(clearComments())},
    }
};


export default connect(MapState,MapDispatch)(Comments);
