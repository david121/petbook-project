import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    archivePost,
    captionEdit, commentFocus,
    deletePost, hidePost,
    likePost, LikesModal,
    postComment, postInfoIcon,
    postLikers,
    postOptions, submitEdit, switchEdit,
    toggleComments,
    writeComment
} from "./STORE/Actions";
import SlideShow from "./SlideShow";
import Comments from "./Comments";
import {NavLink} from "react-router-dom";
import PostsInfo from "./PostsInfo";
import {toggleProfilePreview} from "../USER PROFILE/STORE/Actions";
import ProfilePreview from "../USER PROFILE/ProfilePreview";
import {unsavePost} from "../ARCHIVE/STORE/Actions";
import Likers from "./Likers";

class AllPosts extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        //console.log(this.props.likersModal)
    }

    render() {
        let caption = this.props.caption;
        let likers;
        let postOptions;
        // post's three dots options.
        if(this.props.toggleOptions && this.props.postToToggle === this.props.postId) {
            if (this.props.petOwnerId === this.props.activeUser.id){
                postOptions=<ul className="post-option-drop text-left">
                    <li onClick={()=>this.props.edit(this.props.postId, this.props.caption)}>Edit Post</li>
                    <li onClick={()=>this.props.delete(this.props.postId,this.props.petId)}>Delete Post</li>
                    <li onClick={()=>this.props.hidePost(this.props.postId,this.props.petId,"HOME")}>Hide Post</li>
                </ul>;
            }else{
                postOptions = <ul className="post-option-drop text-left">
                            {this.props.saved === 1?
                                <li onClick={()=>this.props.unsavePost(this.props.saved_id)}><small className='mr-2'><b>unsave</b></small> <img src={'/img/dog.svg'} alt="unsave post" width={'20'} height={'20'}/></li>
                                 :
                                <li onClick={()=>this.props.addToArchive(this.props.postId)}><small className='mr-2'><b>save</b></small> <img src={'/img/like.svg'} alt="unsave post" width={'20'} height={'20'}/></li>
                            }
                        </ul>
            }
        }

        //Post Likers
        /*if (this.props.likers === "None"){
            likers = <span className="text-center">No likes on this post</span>;
        }else {
            likers =this.props.likers.map((liker,index)=>{
                return <li key={index} className="text-left">
                        {liker.photo_path === ''
                                ?
                            <img src={'/img/avatar.png'} alt="user" width="20" height="15" className="rounded-circle mr-2" />
                                :
                            <img src={`/storage/users/${liker.id}/${liker.photo_path}`} alt={'user'} className="rounded-circle mr-2" width="20" height="15"/>
                        }
                    <NavLink to={'/profile'}>{liker.username}</NavLink>
                </li>
            });
        }*/

        {/*
            <Likers activeUser={this.props.activeUser}/>
            handling mail progress for likes
                        this.props.sendingLike && this.props.likingPost === this.props.postId?this.props.totalLikes+1 :
            this.props.sendingLike && this.props.likingPost === this.props.postId?
            <img src={'/img/paws.svg'} alt="like" width="30" height="30" style={{opacity:"0.5"}}/>
            :
        */}
        let likeIcon =  <img src={'/img/paw.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.likePost(this.props.postId, this.props.petOwnerId,this.props.petId)}/>;
        if(this.props.liked !== 0){
            likeIcon = <img src={'/img/paws.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.likePost(this.props.postId,this.props.petOwnerId,this.props.petId)}/>
        }

        /*else if(this.props.liked__ && this.props.likedPost === this.props.postId){
            likeIcon = <img src={'/img/paws.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.likePost(this.props.postId,this.props.petOwnerId,this.props.petId)}/>
        }*/

        //post comments
        let comments;
        if (this.props.clickedPost === this.props.postId){ // removed this.props.showComments
            if (this.props.totalComments === 0 && this.props.writtenComment !== ''){
                comments = <div className="post-comment-prev ml-4 mb-3">
                    <span>
                        {
                            this.props.activeUser.photo_path === null
                                ?
                                <img src={'/img/avatar.png'} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                                :
                                <img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                        }
                    </span>
                    <span className="the-comm-prev"><span className="mr-2">{this.props.activeUser.username} </span>{this.props.writtenComment}</span>
                </div>
            } else if (this.props.totalComments === 0 && this.props.writtenComment === ''){
                comments = <span className="text-center first-comment">Write the first comment for this post</span>
            }else
            comments = <Comments
                postId = {this.props.postId}
                allComments={this.props.totalComments}// existing comments on post (number)
                commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,this.props.postId)}
                page = "HOME"
            />;
        }

        //Post pictures
        let postPics;
        if (this.props.numOfPics === 0 ){
            postPics = ''
        }else{
            postPics = <SlideShow
                numOfPics = {this.props.numOfPics}
                userId = {this.props.userId}
                petId = {this.props.petId}
                postPic = {this.props.postPic}
            />
        }
        //pet owner picture
        let ownerImage;
        if (this.props.ownerPhoto === '' || this.props.ownerPhoto === null){
            ownerImage = <img src={`/img/avatar.png`} alt={"user"} className="rounded-circle ml-1 mr-1" width={"25"} height={"20"}/>
        }
        else{
            ownerImage = <img src={`/storage/users/${this.props.userId}/${this.props.ownerPhoto}`} alt={"user"} className="rounded-circle ml-1 mr-1" width={"25"} height={"20"}/>
        }

        //Pet Owner
        let petOwnedBy;
        let userName;
        if (this.props.userId === this.props.activeUser.id){
            petOwnedBy = <i className="postBy">owner: <NavLink to={`/profile`} className="pet-link">
                {ownerImage}
                { this.props.petOwner} </NavLink>
            </i>;
            userName = <span className="ml-4 username">
                            <NavLink to={`/pet/${this.props.petId}`} className="text-dark">{this.props.username} </NavLink>
                        </span>
        }else{
            petOwnedBy = <i className="postBy">owner: <NavLink to={`/pet-owner/${this.props.userId}`} className="pet-link">
                {ownerImage}
                { this.props.petOwner} </NavLink>
            </i>;
            userName = <span className="ml-4 username">
                            <NavLink to={`/pet-view/${this.props.petId}`} className="text-dark" >{this.props.username}</NavLink>
                            {
                                this.props.profilePreview && this.props.previewId === this.props.postId ?
                                <ProfilePreview />
                                :
                            ''
                            }
                        </span>;
        }

        //edit post caption
        if (this.props.enableEdit && this.props.postToEdit === this.props.postId){
            caption = <div>
                <input value={this.props.newCaption} autoFocus={true} onChange={this.props.captionEdit}/>
                <button type={"submit"} onClick={(e)=>this.props.submitEdit(this.props.newCaption ,this.props.postId)}>Submit</button>
                <button type={"button"} onClick={()=>this.props.edit(this.props.postId,this.props.caption)}>Cancel</button>
            </div>
        }


        let postInfo = this.props.userId !== this.props.post_owner ?  <i className="fas fa-info-circle post-info-icon ml-3" onClick={()=>this.props.postMessage(this.props.postId)}/> : '';
        let message = "Post was made by ";
        return (
            <div className="card mb-4" id='post'>

                <div className="card-header post-header">
                    <span>
                        {
                            this.props.petPhoto === null ?
                            <img src={"/img/rodent.svg"} alt={"pet photo"} className="rounded-circle mr-2" width="50" height="40"/>
                                :
                            <img src={`/storage/pets/${this.props.petId}/${this.props.petPhoto}`} alt={"pet photo"} className="rounded-circle mr-2" width={"50"} height={"40"}/>
                        }

                        {userName}
                    </span>

                    {postInfo}
                    <span className="ml-2">{this.props.showMessage && this.props.postMessageId === this.props.postId ? <PostsInfo id={this.props.post_owner} message={message} username={this.props.post_owner_username}/> : ''}</span>

                    <div className="float-right" onClick={(e)=>{e.stopPropagation();this.props.options(this.props.postId)}} onBlur={()=>this.props.options(this.props.postId)}>
                        <img src={'/img/more.svg'} alt={'options'} height={'20'} width={'20'} className="post-options"/>
                        {postOptions}
                    </div>
                    <div className="ml-5 post-time">
                        <small className="text-muted col-sm-12" style={{fontFamily:"Cambria"}}> {this.props.postTime}</small>
                    </div>

                </div>

                <div className="card-body post-body">
                    <span className={"caption"}>
                        {caption}
                    </span>
                    <span className="post-photo-wrapper mt-4">
                        {postPics}
                    </span>
                </div>

                <div className="card-footer post-footer">
                    {petOwnedBy}
                    <div className="mt-4 reactions text-center row">

                        <div className="col-sm-2 likes">
                            <span id={"like"}>
                                {likeIcon}
                                <span className="likers ml-2" data-toggle="modal"
                                      data-target="#likesModal"
                                      onClick={()=>this.props.likesModal(this.props.postId) /*()=>this.props.postLikers(this.props.postId)*/}
                                      style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>
                                    { this.props.totalLikes}
                                </span>
                            </span>
                        </div>

                        <div className="col-sm-8 comment-space">
                            <form onSubmit={e=>this.props.submitComment(e,this.props.writtenComment,this.props.postId)}>
                                <label className="row">
                                    {/*<img src={`/img/avatar.png`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>*/}
                                    {/*{ value = this.props.postOnFocus === this.props.postId ? this.props.writtenComment : ''}*/}
                                    <input onFocus={()=>this.props.commentFocus(this.props.postId,this.props.postOnFocus)}
                                           onChange={(e)=>this.props.writeComment(e,this.props.showComments,this.props.postId,this.props.postOnFocus)}
                                           value={this.props.postOnFocus === this.props.postId ? this.props.writtenComment : ''}
                                           className="post-comment-input float-right" placeholder="press enter to submit comment"/>
                                </label>
                            </form>
                        </div>

                        <div id={"tags"} className="col-sm-2 comments-div">
                            <span>
                                <img src={'/img/comment.svg'} alt={"comment"} width="27" height="27" className="comment-icon" style={{color:"darkslategrey"}} onClick={()=>this.props.toggleComments(this.props.postId,this.props.showComments)}/>
                                <span className="total-comm ml-2" style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>{this.props.totalComments}</span>
                            </span>
                        </div>

                    </div>
                </div>

                <div className='col-sm-12' id="comment-wrapper">
                    {comments}
                </div>

                {this.props.likersModal ? <Likers postId={this.props.postToShowLikes} activeUser={this.props.activeUser.username}/> : null}

                {/*<div className="modal" id="likesModal">
                    <div className="modal-dialog modal-sm modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6>Likes</h6><button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div className="modal-body d-flex flex-column justify-content-center">
                                {likers}
                            </div>
                        </div>
                    </div>
                </div>*/}

            </div>
        );
    }
}

const mapState = (state) =>{
    return {
        activeUser : state.User.user_info,
        //likers : state.Posts.likers,
        postToShowLikes:state.Posts.postToShowLikes,
        likersModal : state.Posts.likerModal,
        toggleOptions : state.Posts.toggleOptions,
        postToToggle : state.Posts.postToToggle,

        //comments prop
        postOnFocus : state.Comments.focusPostId,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,
        generalError : state.Home.generalError,
        //Edit props
        enableEdit : state.Posts.enableEdit,
        postToEdit: state.Posts.postToEdit,
        newCaption : state.Posts.newCaption,
        /*liked__ : state.Posts.liked,
        likedPost : state.Posts.likedPostId,*/
        showMessage : state.Posts.showMessage,
        /*sendingLike : state.Posts.likingFromHome,
        likingPost : state.Posts.likingPostId,*/
        postMessageId : state.Posts.postId,
        profilePreview : state.PetProfile.profilePreview,
        previewId : state.PetProfile.profileId,
    }
};

const mapDispatch = (dispatch) =>{
    return {
        options : (postId) => {dispatch(postOptions(postId))},
        delete : (postId,petId) => {dispatch(deletePost(postId,petId,'HOME'))},
        edit : (post,caption) => {dispatch(switchEdit(post,caption))},
        captionEdit : (e) => {dispatch(captionEdit(e.target.value))},
        submitEdit : (caption,postId) => {dispatch(submitEdit(caption,postId,"HOME"))},
        likesModal : (id) => {dispatch(LikesModal(id))},//{type:"LIKERS_MODAL",post:id});postLikers(id)
        postMessage : (post) => {dispatch(postInfoIcon(post))},
        likePost : (postId,owner,petId) =>{dispatch(likePost(postId,owner,petId,"HOME"))},
        commentFocus : (postId,prevPost) =>{dispatch(commentFocus(postId,prevPost))},
        toggleComments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        writeComment : (e,comments,postID,prevToggledPost) => {dispatch(writeComment(e.target.value,comments,postID,prevToggledPost))},
        submitComment : (e,comment,postId) => {e.preventDefault();dispatch(postComment(comment,postId,'HOME',null))},
        addToArchive : (postId) =>{dispatch(archivePost(postId,'HOME'))},
        hidePost : (postId,petId,page) =>{dispatch(hidePost(postId,petId,page))},
        profile : (user,id) =>{dispatch(toggleProfilePreview(user,id))},
        unsavePost : (id) => {dispatch(unsavePost(id,'HOME'))}
    }
};

export default connect(mapState, mapDispatch)(AllPosts);

