import axios from "axios";
import {fetchPetPosts, fetchPets, fetchVisitPetPosts} from "../../USER PROFILE/STORE/Actions";
import {fetchAllPosts, generalError} from "../../HOME/STORE/Actions";
import {archivedPosts, hiddenPosts} from "../../ARCHIVE/STORE/Actions";
import {Api_Process} from "../../LAYOUT/STORE/Actions";
//import {axiosHeader} from "../../index";
//import SavedPosts from "../../ARCHIVE/SavedPosts";

export const postInputHandler = (name,event) =>{
        return dispatch =>{
            let value;
            if (name === 'photos'){
                value = event.target.files;
               //console.log(value);
                for(const file of value){
                    if (!file.name.match(/.(jpg|png|gif|JPG|jpeg|JPEG|TIFF|MP4|mp4|3GP|3gp|AVI|MPEG|MOV)$/i)){
                        dispatch({type:"INVALID_POST_PHOTO"});
                        return false;
                    }else if (file.size > 10000000){
                        dispatch({type:"FILE_LARGE"});
                        return false;
                    }
                }
                dispatch({
                   type : "POST",
                   name: name,
                   value : value,
                });
            }else
            if (name === 'caption'){
                value = event.target.value
            }
            dispatch({
                type : "POST",
                name: name,
                value : value
            })
        }
};

export const removePic = () =>{
    return dispatch => {
        dispatch({
            type: "REMOVE_PIC"
        })
    }
};

export const closePreview = () =>{
    // console.log("click")
    return dispatch => {
        dispatch({
            type: "CLOSE_PREVIEW"
        })
    }
};

export const showAddPet = (closeAction = false) =>{
    return dispatch =>{
        dispatch({
            type : "TOGGLE_ADD_PET",
            closeAction
        })
    }
};

export const addNewPet = (data) =>{
    return dispatch =>{
        axios.post('api/pet',data,{headers:{
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer "+ localStorage.getItem('token')
            }})
            .then(response=>{
                if (response.data.success){
                    dispatch(fetchPets());
                    dispatch({
                        type: 'PET_ADDED',
                    })
                }
            })
            .catch(error=>{
                console.log("FROM PET SUBMIT ACTION", error);
            });
    }
};


export  const selectPet = (closeAction = false) =>{
    return dispatch =>{
        dispatch({
            type : "SELECT_PET",
            closeAction
        })
    }
};

export  const postBy = (petId) =>{
   return dispatch =>{
       dispatch({
           type : "POST_BY",
           value : petId
       })
   }
};

export const addPost = (id,caption,files,page) => {
    return dispatch =>{
        if (id === ''){
            dispatch({
                type:"POST_BY",
                value : ''
            })
        }else
          if (id === null){
              dispatch({
                  type:"TOGGLE_ADD_PET",
                  value : ''
              })
        }
        else{
            const form = new FormData();
            form.append('id', id);
            form.append('caption', caption);
            for (let file of files){
                form.append('photo[]', file, file.name )
            }
            dispatch(Api_Process());
            axios.post('/api/posts',form,{headers:{
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Authorization": "Bearer " +localStorage.getItem('token')
                }})
                .then(response=>{
                    dispatch(Api_Process());
                    if (response.data.success){
                        dispatch({type : "POST_ADDED"});
                        if (page === 'pet'){
                            dispatch(fetchPetPosts(id))
                        }else if (page === 'user'){
                            dispatch(fetchUserPosts())
                        }else{
                            dispatch(fetchAllPosts());
                            dispatch(({type:"POSTED_MOBILE"}))
                        }

                    }
                })
                .catch(error=>{
                    dispatch(Api_Process());
                    // console.log(error)
                })
        }

    }
};
export const fetchUserPosts = (user) => { //
    return dispatch =>{
        axios.get('/api/userPosts',{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token')
            },params:{user:user}})
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : "USER_POSTS",
                        posts : response.data.posts,
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const fetchVisitorPosts = (user) => {
    return dispatch =>{
        axios.get('/api/userPosts',{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token')
            },params:{profileId:user}})
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type : "VISITOR_POSTS",
                        posts : response.data.posts,
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const likePost = (postId,userId,petId,page) =>{
    // console.log(petId);
    return dispatch =>{
        // dispatch({type:"LIKING",page,postId});
        axios.post('/api/likes',{'post':postId,'postOwner':userId,'pet':petId},{
            headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        }).then(response=>{
                if (response.data.success){
                    // dispatch({type:"LIKING",page});
                    switch (page) {
                        case "HOME":dispatch(fetchAllPosts());dispatch({type:"HOME_LIKE",postId});break;
                        case "USER_PROFILE":dispatch(fetchUserPosts());break;
                        case "PET_PROFILE":dispatch(fetchPetPosts(petId));break;
                        case "USER_VISIT":dispatch(fetchVisitorPosts(userId));break;
                        case "PET_VISIT":dispatch(fetchVisitPetPosts(petId,userId));break;
                        case "HIDDEN":dispatch(hiddenPosts());break;
                        case "SAVED":dispatch(archivedPosts());break;
                    }
                    dispatch({
                        type: "LIKE",
                        liked: response.data.liked
                    })
                }
            })
            .catch(()=>{
                // dispatch({type:"LIKING",page});
                // console.log(error)
            });
    }
};


export const postLikers=(postId)=>{
  return dispatch=>{
      axios.get(`/api/likes/${postId}`, {
                headers: {
              "Content-Type": "application/json",
              "Accept": "application/json",
              "Authorization": "Bearer " + localStorage.getItem("token"),
          },
            })
          .then(response=>{
              if (response.data.success){
                 // console.log(response.data.likers);
                  dispatch({
                      type: "POST_LIKERS",
                      value : response.data.likers
                  })
              }
          })
          .catch(error=>{
              console.log(error)
          })
  }
};
export const LikesModal =(postId)=>{
    return dispatch =>{
        dispatch({
            type:"LIKERS_MODAL",post:postId
        });
        dispatch(postLikers(postId));
    }
};
export const closeLikesModal = (clicked) =>{
    return dispatch =>{
        if (!clicked){
            dispatch({type:"LIKERS_MODAL_AUTO",post:null});
            dispatch({type:"POST_LIKERS",value:[]});
            return false;
        }
        dispatch({type:"LIKERS_MODAL",post:null});
        dispatch({type:"POST_LIKERS",value:[]});
    }
};


export const fetchComments=(postId)=>{
    return dispatch =>{
        axios.get(`/api/comments/${postId}`,{
            headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token')
            },
            params:{
                "post" : postId
            }
        })
            .then(response=>{
                if (response.data.success){
                    dispatch({
                        type: "POST_COMMENTS",
                        withComments: true,
                        clickedPost : postId,
                        comments : response.data.comments,
                        // covers : response.data.photo
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};
// export const refreshComments = (postId) =>{
//     return dispatch=>{
//         axios.get('/api/comments',{
//             headers:{
//                 "Content-Type": "application/json",
//                 "Accept": "application/json",
//                 "Authorization": "Bearer " +localStorage.getItem('token')
//             },
//             params:{
//                 "post" : postId
//             }
//         })
//             .then(response=>{
//                 if (response.data.success){
//                     dispatch({
//                         type: "REFRESH_COMMENTS",
//                         clickedPost : postId,
//                         comments : response.data.comments,
//                         covers : response.data.photo
//                     })
//                 }
//             })
//             .catch(error=>{
//                 console.log(error)
//             })
//     }
// }

export const commentFocus = (postId , prevFocus) =>{
    // console.log("prevFocus",prevFocus);
    return dispatch =>{

            if (postId !== prevFocus){dispatch({type:"CLEAN_PREV_COMMENT"})}
              dispatch({type:"COMMENT_FOCUS",id:postId,prevFocus})
    }
};

export const toggleComments = (postId,toggled) =>{
    return dispatch =>{
        // if (toggled){
            dispatch({
                type: "TOGGLE_COMMENT",
                clickedPost : postId,
            })
        // } else{
        //     dispatch(fetchComments(postId));
        // }

    }
};

export  const clearComments = () =>{
    return dispatch =>{
        dispatch({type:"CLEAR_COMMENTS"})
    }
};


export const writeComment = (value,commentsShown,postId,toggledPost) =>{
    // console.log("Post on focus : " ,postId);
    return dispatch =>{
        if (!commentsShown){
            dispatch(toggleComments(postId))
        }else if (commentsShown && postId !== toggledPost){dispatch(toggleComments(postId))}
        dispatch({
            type: "WRITE_COMMENT",
            value: value
        })
    }
};

export const replyComment = (commentId) =>{
    return dispatch =>{
        dispatch({
            type: "REPLY_COMMENT",
            value : commentId
        })
    }
};

export const viewReplies = (commentId) =>{
    return dispatch =>{
        dispatch({
            type: "SHOW_REPLIES",
            value : commentId
        })
    }
};

export const commentDots = (commentId) =>{
    return dispatch =>{
        dispatch({
            type: "COMMENT_DOTS",
            value : commentId
        })
    }
};

export const toggleCommentOptions = (commentId) =>{
    return dispatch =>{
        dispatch({
            type: "TOGGLE_COMMENT_OPTIONS",
            value : commentId
        })
    }
};
export const deleteComment = (postId,commentId,page,id) =>{
    return dispatch =>{
        axios.delete(`/api/comments/${commentId}`,{
            headers:{
                "Content-Type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }
        }).then(res=>{
            if (res.data.success){
                switch (page) {
                    case "USER_PROFILE":
                        dispatch(fetchUserPosts());
                        dispatch(fetchComments(postId));break;
                    case "PET":
                        dispatch(fetchUserPosts());
                        dispatch(fetchComments(postId));break;
                    case "HOME":
                        dispatch(fetchAllPosts());
                        dispatch(fetchComments(postId));break;
                    case "VISITOR_PET":
                        dispatch(fetchPetPosts(id));
                        dispatch(fetchComments(postId));break;
                    case "SAVED_POSTS":
                        dispatch(archivedPosts());
                        dispatch(fetchComments(postId));break;
                    case "ARCHIVE": // hidden posts
                        dispatch(hiddenPosts());
                        dispatch(fetchComments(postId));break;
                }
            }
        }).catch();
        dispatch({
            type: "TOGGLE_COMMENT_OPTIONS",
            value : commentId
        })
    }
};


export const likeComment = (commentId,postId,page) =>{
    return dispatch =>{
        axios.post('/api/commentLike',{commentId:commentId},{
            headers:{
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
            }
        }).then(response=>{
            if (response.data.success){
                 console.log(page);
                switch (page){
                    case "USER_PROFILE":dispatch(fetchComments(postId));break;
                    case "PET":dispatch(fetchComments(postId));break;
                    case "HOME":dispatch(fetchComments(postId));break;
                    case "VISITOR":dispatch(fetchComments(postId));break;
                    case "VISITOR_PET":dispatch(fetchComments(postId));break;
                    case "SAVED_POSTS":dispatch(fetchComments(postId));break;//saved posts
                    case "ARCHIVE":dispatch(fetchComments(postId));break;//hidden posts
                }
                dispatch({
                    type: "COMMENT_LIKED"
                })
            }
        }).catch(error=>{
            console.log(error)
        });
    }
};

export const replyCommentInput = (comment) =>{
    return dispatch =>{
        dispatch({
            type: "REPLY_COMMENT_INPUT",
            value : comment
        })
    }
};

export const submitReply = (postId,commentId,comment,page,id) =>{
    return dispatch =>{
        const data = {
            postId : postId,
            parentId : commentId,
            comment : comment
        };
        axios.post('/api/comments',data,{
            headers:{
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
            }
        }).then(response=>{
            if (response.data.success){
                switch (page) {
                    case "HOME" : dispatch(fetchAllPosts());break;
                    case "USER_PROFILE" : dispatch(fetchUserPosts());break;
                    case "PET" : dispatch(fetchUserPosts());break;
                    case "VISITOR" : dispatch(fetchVisitorPosts(id));break;
                    case "VISITOR_PET" : dispatch(fetchVisitPetPosts(id));break;
                }
                dispatch(fetchComments(postId));
                dispatch({
                    type: "COMMENT_REPLY_POSTED"
                })
            }
        }).catch(error=>{
            console.log(error)
        });
    }
};

export const postComment = (comment,postId , page,id) =>{
    return dispatch =>{
        const data ={
            comment : String(comment),
            postId : postId,
        };
        axios.post('/api/comments',data,{
            headers:{
                "Content-type" : "application/json",
                "Accept" : "application/json",
                "Authorization" : "Bearer " + localStorage.getItem('token')
            }

        }).then(response=>{
            if (response.data.success){
                switch (page) {
                    case "HOME":dispatch(fetchAllPosts());break;
                    case "VISITOR":dispatch(fetchVisitorPosts(id));break;
                    case "VISITOR_PET":dispatch(fetchPetPosts(id));break;
                    case "PET":dispatch(fetchPetPosts(id));break;
                    case "USER_PROFILE":dispatch(fetchUserPosts())
                    ;dispatch(fetchComments(postId));break;

                }
                dispatch(fetchComments(postId));
                dispatch({
                    type: "COMMENT_POSTED"
                })
            }
        }).catch(error=>{
            console.log(error)
        });
        dispatch({
            type: "POST_COMMENT",
        })
    }
};
export const postOptions = (post, closeAction = false)=>{
    return dispatch => {
        dispatch({
            type : "POSTS_OPTIONS",
            clickedPost: post,
            closeAction
        })
    }
};

export const postInfoIcon = (post)=>{
    return dispatch => {
        dispatch({
            type : "POST_MESSAGE",
            postId : post
        })
    }
};

export const deletePost = (post,petId,page)=>{
return dispatch=>{
    dispatch(Api_Process());
    axios.delete(`/api/posts/${post}`,{
        headers:{
            "Content-type" : "application/json",
            "Accept" : "application/json",
            "Authorization" : "Bearer " + localStorage.getItem('token')
        },params:{petId:petId}
    })
        .then(response=>{
            if(response.data.success){
                dispatch(Api_Process());
                switch (page) {
                    case "USER_PROFILE": dispatch(fetchUserPosts());break;
                    case "PET": dispatch(fetchPetPosts(petId));break;
                    case "HOME": dispatch(fetchAllPosts());break;
                    case "ARCHIVE": dispatch(archivedPosts());break;

                }
            dispatch({
                type: "POST_DELETED"
            })
            }
        })
        .catch(error=>{
            dispatch(Api_Process());
            // console.log(error);
            dispatch(generalError())
        })
}
};

export const switchEdit = (post, caption)=>{
    return dispatch => {
        dispatch({
            type : "EDIT_POST",
            clickedPost: post,
            postCaption: caption
        })
    }
};

export const captionEdit = (caption)=>{
    return dispatch => {
        dispatch({
            type : "CAPTION",
            value: caption
        })
    }
};

export const submitEdit = (caption,postId,page)=>{
    return dispatch => {
        let data = new FormData();
        data.append('caption', caption);
        data.append('id', postId);
        data.append('_method','PATCH');
        axios.post('/api/posts/edit',data,{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                if (response.data.success){
                    // if(page === "ARCHIVE"){
                    //      dispatch(hiddenPosts()) ;
                    // }
                    switch (page) {
                        case "ARCHIVE" :
                            dispatch(hiddenPosts());
                            return dispatch({type:"POST_EDITED"});
                        case "USER_PROFILE" :
                            dispatch(fetchUserPosts());
                            return dispatch({type:"POST_EDITED"});
                        case "HOME" :
                            dispatch(fetchAllPosts());
                            return dispatch({type:"POST_EDITED"});
                        default : return dispatch({ type : "POST_EDITED"})
                    }
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

export const archivePost = (postId,page) =>{
    return dispatch =>{
        axios.post('/api/archive/add',{postId:postId},{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                if (response.data.success){
                    switch (page) {
                        case "HOME": dispatch(fetchAllPosts());break;
                        case "PROFILE_USER": dispatch(fetchUserPosts(null));break;
                    }
                    dispatch({
                        type : "POST_ARCHIVED",
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};



export const hidePost = (postId,petId,page) =>{
    return dispatch =>{
        axios.post('/api/archive/hide',{postId:postId},{headers:{
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " +localStorage.getItem('token'),
            }})
            .then(response=>{
                if (response.data.success){
                    switch (page) {
                        case "USER_PROFILE":dispatch(fetchUserPosts());break;
                        case "PET":dispatch(fetchPetPosts(petId));break;
                        case "HOME":dispatch(fetchAllPosts());break;
                    }
                    dispatch({
                        type : "POST_HIDDEN",
                    })
                }
            })
            .catch(error=>{
                console.log(error)
            })
    }
};

// export const admireOptions=()=>{
//     return dispatch=>{
//         dispatch({
//             type : "TOGGLE_ADMIRE"
//         })
//     }
// };
