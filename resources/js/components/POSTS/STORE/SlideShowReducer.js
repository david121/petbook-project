
const initialState = {
    totalPics : null,
    currentIndex : 1,
    postId:'',

};

const SlideShowReducer = (state = initialState, actions)=>{
    switch (actions.type) {
        case "NEW_SLIDE":
            console.log("clicked post : " + actions.post)
            const totalPics = state.totalPics;
            let current = state.currentIndex;
            let newSlide = current + actions.action;
            if (newSlide > totalPics){
                return {
                    ...state,
                    currentIndex: 1
                }
            }else
                if(newSlide < 1){
                    return {
                        ...state,
                        currentIndex: totalPics
                    }
                }
            return{
                ...state,
                currentIndex: newSlide,
                postId : actions.post
            };

        case "TOTAL":
            return {
                ...state,
                totalPics: actions.value
            };
        default : return state;
    }
};

export default SlideShowReducer;
