
const initialState = {
    liked : false,
    likedPostId:'',

    showComments : false,
    comment : '',
    //writing post
    caption : '',
    photos : [],
    invalid_photo:false,
    file_size:false,
    //preview_photos: null,
    media_preview :[],
    postBy :'',
    selectPet: false,

    showAddPet: false,

    //fetching posts
    posts:[],
    visitorPosts:[],
    // visitorPostTime:[],
    postPhotos: [],
    likerModal : false,
    likers:[],
    postToShowLikes:null,
    //dropdown
    toggleOptions : false,
    postToToggle: null,
    //postEdit
    enableEdit: false,
    postToEdit: null,
    newCaption : '',
    showMessage : false,
    postId : null,

    likingFromHome: false,
    likingPostId : null,
};

const PostReducer = (state = initialState, action)=>{
    switch (action.type) {

        case "POST":
           let prev = [] ;
            if (action.name === 'photos'){
                let files = action.value;
                console.log(files[0].size);
                Array.from(files).forEach(file => {
                    if (file.name.match(/.(mp4|3gp)$/i)){
                        prev.push({video:window.URL.createObjectURL(file)})
                    }else
                        prev.push({photo:window.URL.createObjectURL(file)});
                });
                return{
                    ...state,
                    [action.name]: action.value,
                     media_preview: prev,
                    invalid_photo:false
                    //preview_photos: preview,
                };
            }
            return{
                ...state,
                [action.name]: action.value,
            };

        case "INVALID_POST_PHOTO":
            return{
                ...state,
                invalid_photo : true
            };
        case "FILE_LARGE":
            return{
                ...state,
                file_size : true
            };

        case "TOGGLE_ADD_PET":
            return{
                ...state,
                showAddPet: !state.showAddPet,
                postBy : action.value
            };


        case "PET_ADDED":
            return{
                ...state,
                showAddPet: !state.showAddPet,
            };
        case "SELECT_PET":
            return{
                ...state,
                selectPet: !state.selectPet
            };
        case "POST_BY":
            return{
                ...state,
                postBy: action.value,
                selectPet: !state.selectPet
            };
        case "REMOVE_PIC":
            return{
                ...state,
                photos:'',
                media_preview: [],
                // preview_photos: null
            };
        case "CLOSE_PREVIEW":
            return{
                ...state,
                caption : '',
                photos:'',
                media_preview: [],
                // preview_photos: null
            };

        case "POST_ADDED":
            return{
                ...state,
                caption: '',
                media_preview:[],
                preview_photos: null
            };

        case "USER_POSTS":
            return{
                ...state,
                posts: action.posts,
            };
        case "VISITOR_POSTS":
            return{
                ...state,
                visitorPosts: action.posts,
            };
        case "LIKERS_MODAL":
            return{
                ...state,
                likerModal:!state.likerModal,
                postToShowLikes:action.post
            };
        case "LIKERS_MODAL_AUTO":
            return{
                ...state,
                likerModal:false,
                postToShowLikes:null
            };
        case "POST_LIKERS":
            return{
                ...state,
                likers : action.value,
                //likerModal:!state.likerModal
            };

        /*case "LIKE":
            const Like = state.like === 0? state.like +1 : state.like -1;
            return{
                ...state,
               liked: action.liked,
               like : Like,
            };*/
        case "SHOW_COMMENT":
            return {
                ...state,
                showComments: !state.showComments
            };
        case "WRITE_COMMENT":
            return {
                ...state,
                comment: action.value
            };
        case "POSTS_OPTIONS":
            return {
                ...state,
                toggleOptions: !state.toggleOptions,
                postToToggle : action.clickedPost
            };

        case "POST_MESSAGE":
            return {
                ...state,
                showMessage:!state.showMessage,
                postId : action.postId
            };
        case "EDIT_POST":
          //  console.log(typeof(action.postCaption, "REDUCER"));
            return {
                ...state,
                enableEdit: !state.enableEdit,
                postToEdit : action.clickedPost,
                newCaption : action.postCaption
            };
        case "CAPTION":
            return {
                ...state,
                newCaption:action.value
            };
        case "POST_EDITED":
            return {
                ...state,
                enableEdit: !state.enableEdit,
            };
        case "HOME_LIKE":
            return {
                ...state,
                likedPostId:action.postId,
                liked:!state.liked,
            };
        case "LIKING":
            switch (action.page) {
                case "HOME":
                    return {
                    ...state,
                    likingFromHome: !state.likingFromHome,
                    likingPostId: action.postId,
                }
            }
            return {
                ...state,
            };
        case "CLEAR_USER_PROFILE":
            return {
                ...state,
                // posts:[]
            };
        default : return state;
    }
};

export default PostReducer;
