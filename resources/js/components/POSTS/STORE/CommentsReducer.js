
const initialState = {
    focusPostId: '',
    clickedPost : null,
    showComments : false,
    comment : '',
    comments : [],
    profilePics : [],
    reply: false,
    commentToReplyId : null,
    repliedComment : '',
    showReplies : false,
    commentToShowRepliesId : null,
    commentDots : false,
    commentToShowOptions: null,
    commentOptions : false,
    commentToToggle : null,
};

const CommentsReducer = (state = initialState, action)=>{
    switch (action.type) {

        case "TOGGLE_COMMENT":
            return{
                ...state,
                showComments: !state.showComments,
                clickedPost : action.clickedPost,
            };
        case "CLEAR_COMMENTS":
            return{
                ...state,
                comments : [],
                clickedPost: null,
            };

        case "COMMENT_FOCUS":
            if (action.prevFocus === ''){
                return{
                    ...state,
                    focusPostId: action.id,
                    showComments: !state.showComments
                };
            }
            return{
                ...state,
                focusPostId: action.id,
                // clickedPost : action.clickedPost,
            };

        case "POST_COMMENTS":
            return{
                ...state,
                clickedPost : action.clickedPost,
                comments : action.comments,
                // profilePics : action.covers
            };

        case "REFRESH_COMMENTS":
            return{
                ...state,
                clickedPost : action.clickedPost,
                comments : action.comments,
                profilePics : action.covers
            };

        case "WRITE_COMMENT":
            return{
                ...state,
                comment : action.value
            };
        case "CLEAN_PREV_COMMENT":
            return{
                ...state,
                comment : '',
                showComments: !state.showComments
            };

        case "COMMENT_POSTED":
            return{
                ...state,
                comment : ''
            };
        case "COMMENT_DOTS":
            return{
                ...state,
                commentDots : !state.commentDots,
                commentToShowOptions: action.value
            };
        case "TOGGLE_COMMENT_OPTIONS":
            // console.log(action.value)
            return{
                ...state,
                commentOptions: !state.commentOptions,
                commentToToggle : action.value
            };
        case "REPLY_COMMENT":
            return{
                ...state,
                reply : !state.reply,
                commentToReplyId:action.value
            };

        case "REPLY_COMMENT_INPUT":
            return{
                ...state,
                repliedComment:action.value
            };

        case "COMMENT_REPLY_POSTED":
            return{
                ...state,
                repliedComment:''
            };
        case "SHOW_REPLIES":
            return{
                ...state,
                showReplies:!state.showReplies,
                commentToShowRepliesId :action.value
            };

        default : return state;
    }
};

export default CommentsReducer;
