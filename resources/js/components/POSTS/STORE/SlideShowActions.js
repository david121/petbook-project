
export const TotalPics = (totalPics) =>{
    return dispatch =>{
        dispatch({
            type : "TOTAL",
            value : totalPics
        })
    }
};

export const currentSlide = (num,postId) =>{
    // console.log("Action", postId)
        return dispatch =>{
        dispatch({
            type : "NEW_SLIDE",
            action : num,
            post : postId
        })
    }
};
