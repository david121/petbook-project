import React, {Component} from 'react';
import {connect} from "react-redux";
import {postInputHandler, addPost, removePic, closePreview} from "./STORE/Actions";
import "./CSS/WritePosts.css";
class Post extends Component {

    render() {

        return (
            <div style={{width:"100%"}}>
                {
                    this.props.invalid_photo ? <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                            Invalid File{/* - Persistence may lead to a permanent deactivation of your account!*/}
                        </b>
                        : this.props.tooLarge ? <b className="mb-2 text-center" style = {{color:"darkred",fontFamily:"Cambria"}}>
                            File too large
                        </b> :null
                }
                <form>
                        <textarea name="caption" rows="5" value={this.props.caption}
                                  className="caption form-control"
                                  placeholder="Write a caption for your post..."
                                  onChange={(e)=>this.props.postInputHandler("caption",e)}/>
                        <div className="text-right">
                            <label className="post-up-picture">
                                <img src={'/img/camera.svg'} width={'25'} height={'25'} style={{cursor:"pointer"}} alt={'camera'}/>
                                <input type='file' accept="image/*,video/*" hidden={true} multiple={true} onChange={(e)=>this.props.postInputHandler("photos",e)}/>
                            </label>
                        </div>
                </form>
            </div>
        );
    }
}
const mapState = (state)=>{
    return {
        caption : state.Posts.caption,//written caption
        photos : state.Posts.photos,// photo to be uploaded
        invalid_photo : state.Posts.invalid_photo, // Boolean
        tooLarge : state.Posts.file_size, // Boolean
    }
};

const MapDispatch = (dispatch) =>{
    return {
        submitPost : (e,id,caption,files) =>{
            e.preventDefault();
            dispatch(addPost(id,caption,files))
        },
        postInputHandler : (name,e) => {dispatch(postInputHandler(name,e)) },
        removePic : () =>{dispatch(removePic())},
        closePreview : ()=>{dispatch(closePreview())}
    }
};

export default connect(mapState,MapDispatch)(Post);
