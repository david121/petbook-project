import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class PostsInfo extends Component {
    componentDidMount() {
        // console.log(this.props.userId)
    }

    render() {
        let link = this.props.id === this.props.userId ? `/profile` : `/pet-owner/${this.props.id}`;
        return (
            <span className="posts-tooltip">
                <span className="posts-tooltip-text">
                    {this.props.message}
                    <NavLink to={link}><b className="text-warning">{this.props.username}</b></NavLink>
                </span>
            </span>
        );
    }
}

export default PostsInfo;