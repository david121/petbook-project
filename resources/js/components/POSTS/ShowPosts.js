import React, {Component} from 'react';
import './CSS/ShowPosts.css';
import Comments from "./Comments";
import {connect} from "react-redux";
import {
    postLikers,
    toggleComments,
    postOptions,
    deletePost,
    switchEdit,
    captionEdit,
    submitEdit, postInfoIcon, hidePost, commentFocus, writeComment, clearComments, likePost, LikesModal
} from "./STORE/Actions";
import {NavLink} from "react-router-dom";
import SlideShow from "./SlideShow";
import PostsInfo from "./PostsInfo";
import Likers from "./Likers";
class ShowPosts extends Component {

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log(this.props.postOnFocus)
    }

    render() {

        let comments;
        let likers;
        let postDropdown;
        let caption = this.props.postCaption;

        let postPics;
        if (this.props.numOfPics === 0 ){
            postPics = ''
        }else{
            postPics = <SlideShow
                numOfPics = {this.props.numOfPics}
                userId = {this.props.userId}
                petId = {this.props.petId}
                post = {this.props.postId}
                postPic = {this.props.postPic}
            />
        }

        let postBy =this.props.user=== 'pet' ? '' : <i className="postBy">post by : <NavLink to={`/pet/${this.props.petId}`} className="pet-link">
            {
                this.props.petPhoto === null ?
                    <img src={"/img/rodent.svg"} alt={"pet photo"} className="rounded-circle mr-2" width="25" height="20"/>
                        :
                    <img src={`/storage/pets/${this.props.petId}/${this.props.petPhoto}`} alt={"pet photo"} className="rounded-circle mr-2" width={"25"} height={"20"}/>
            }

            {this.props.pet} </NavLink>
        </i>;


        if (this.props.clickedPost === this.props.postId){ // removed this.props.clicked &&
            if (this.props.totalComments === 0 && this.props.writtenComment !== ''){
                comments = <div className="post-comment-prev ml-4 mb-3">
                    <span>
                        {
                            this.props.activeUser.photo_path === null
                                ?
                                <img src={'/img/avatar.png'} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                                :
                                <img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                        }
                    </span>
                    <span className="the-comm-prev"><span className="mr-2">{this.props.activeUser.username} </span> {this.props.writtenComment}</span>
                </div>
            } else if (this.props.totalComments === 0 && this.props.writtenComment === ''){
                comments = <span className="text-center first-comment">Write the first comment for this post</span>
            }else
                comments = <Comments
                    postId = {this.props.postId}
                    allComments={this.props.totalComments}// existing comments on post (number)
                    commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,this.props.postId)}
                    page = {this.props.page}
                />;
        }


        if (this.props.archive){
            if(this.props.hiddenPost){
                if(this.props.toggleOptions && this.props.postToToggle === this.props.postId){
                        postDropdown = <ul className="post-option-drop text-left">
                            <li onClick={()=>this.props.edit(this.props.postId, this.props.postCaption)}>Edit Post</li>
                            <li onClick={()=>this.props.delete(this.props.postId,this.props.petId,this.props.page)}>Delete Post</li>
                            <li onClick={this.props.restore}>Restore Post</li>
                        </ul>;
                }
            }else{
                if(this.props.toggleOptions && this.props.postToToggle === this.props.postId){
                    postDropdown = <ul className="post-option-drop text-left">
                        <li onClick={this.props.remove}>Remove from archive</li>
                        <li>Share Post</li>
                    </ul>;
                }
            }
        }else{
            if(this.props.toggleOptions && this.props.postToToggle === this.props.postId){
                postDropdown=<ul className="post-option-drop text-left">
                    <li onClick={()=>this.props.edit(this.props.postId, this.props.postCaption)}>Edit Post</li>
                    <li onClick={()=>this.props.delete(this.props.postId,this.props.petId,this.props.page)}>Delete Post</li>
                    <li onClick={()=>this.props.hidePost(this.props.postId,this.props.petId,this.props.page)}>Hide Post</li>
                </ul>;
            }
        }

        let likeIcon =<img src={'/img/paw.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.like(this.props.postId,this.props.userId,this.props.petId)}/> ;
        if(this.props.liked !== 0){
            likeIcon = <img src={'/img/paws.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.like(this.props.postId,this.props.userId,this.props.petId)}/>
        }

        if (this.props.likers === "None"){
            likers = <span className="text-center">No likes on this post</span>;
        }else {
            likers = this.props.likers.map((liker,index)=>{
                return <li key={index} className="text-left">
                    {liker.photo_path === null? <img src={'/img/avatar.png'} alt="user" width="20" height="15" className="rounded-circle mr-2" /> :
                        <img src={`/storage/users/${liker.id}/${liker.photo_path}`} className="rounded-circle mr-2" width="20" height="15"/>
                    } {liker.username}
                </li>
            });
        }

        if (this.props.enableEdit && this.props.postToEdit === this.props.postId){
            caption = <div>
                        <input value={this.props.newCaption} autoFocus={true} onChange={this.props.captionEdit}/>
                        <button style={{border:"1px solid",backgroundColor:"darkslategrey", color:'white',padding:"5px"}} className='ml-2 mr-2' onClick={()=>this.props.submitEdit(this.props.newCaption,this.props.postId,this.props.page)}>save</button>
                         <button style={{border:"1px solid",backgroundColor:"darkslategrey", color:'white',padding:"5px"}} onClick={this.props.edit}>cancel</button>
                    </div>
        }

        let ownerImage;
        if (this.props.archive){
            if (this.props.savedPost){
                ownerImage = this.props.userCover === null ? <img src={`/img/avatar.png`} alt={"user"} className="rounded-circle ml-1 mr-1" width='50' height="40"/>
                            : <img src={`/storage/users/${this.props.userId}/${this.props.userCover}`} alt={"user"} className="rounded-circle ml-1 mr-1" width='50' height="40"/>
            }else{
                ownerImage = this.props.userCover === null ? <img src={`/img/avatar.png`} alt={"user"} className="rounded-circle ml-1 mr-1" width='50' height="40"/>
                    : <img src={`/storage/users/${this.props.userId}/${this.props.userCover}`} alt={"user"} className="rounded-circle ml-1 mr-1" width='50' height="40"/>
            }
        }else{
            if (this.props.page === "USER_PROFILE") {
                if (this.props.userCover === null) {
                    ownerImage =
                        <img src={`/img/avatar.png`} alt={"user"} className="rounded-circle ml-1 mr-1" width='50'
                             height="40"/>
                } else {
                    ownerImage = <img src={`/storage/users/${this.props.userId}/${this.props.userCover}`} alt={"user"}
                                      className="rounded-circle ml-1 mr-1" width='50' height="40"/>
                }
            }else{
                if (this.props.userCover === null) {
                    ownerImage =
                        <img src={`/img/rodent.svg`} alt={"pet"} className="rounded-circle ml-1 mr-1" width='50'
                             height="40"/>
                } else {
                    ownerImage = <img src={`/storage/pets/${this.props.petId}/${this.props.userCover}`} alt={"user"}
                                      className="rounded-circle ml-1 mr-1" width='50' height="40"/>
                }
            }
        }


        let postInfo = this.props.userId !== this.props.post_owner ?  <i className="fas fa-info-circle post-info-icon ml-3" onClick={()=>this.props.postMessage(this.props.postId)}/> : '';
        let message = "Post was made by ";
        return (
            <div className="card mb-4" id='post'>
                <div className="card-header post-header">
                    {/*<span className="row">*/}
                        {/*<img src={this.props.userPhoto} alt={"user-cover"} className="rounded-circle" width='50' height="40"/>*/}
                    {ownerImage}
                        <span className="ml-4 username">
                            {this.props.userName}
                        </span>

                        {postInfo}
                        <span className="ml-2">{this.props.showMessage && this.props.postMessageId === this.props.postId ? <PostsInfo id={this.props.post_owner} userId={this.props.activeUser} message={message} username={this.props.post_owner_username}/> : ''}</span>
                    {/*</span>*/}
                    <div className="float-right" onClick={(e)=>{e.stopPropagation();this.props.options(this.props.postId)}} onBlur={()=>this.props.options(this.props.postId)}>
                        <img src={'/img/more.svg'} alt={'options'} height={'20'} width={'20'} className="post-options"/>
                        {postDropdown}
                    </div>
                    <div className="ml-5 post-time">
                        <small className="text-muted col-sm-12" style={{fontFamily:"Cambria"}}> {this.props.time}</small>
                    </div>

                </div>

                <div className="card-body post-body">
                    <span className={"caption"}>
                        {caption}
                    </span>
                    <span className={"post-photo-wrapper mt-4"}>
                        {postPics}
                    </span>
                </div>

                <div className="card-footer post-footer">
                    {postBy}
                    <div className="mt-4 reactions text-center row">

                        <div className="col-sm-2 likes">
                            <span id={"like"}>
                                {likeIcon}
                                <span className="likers ml-2" data-toggle="modal"
                                      data-target="#likesModal"
                                      onClick={()=>this.props.likesModal(this.props.postId)}
                                      style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>
                                    {this.props.totalLikes}
                                </span>
                            </span>
                        </div>

                        <div className="col-sm-8 comment-space">
                            <form onSubmit={e=>this.props.commentSubmit(e,this.props.writtenComment,this.props.postId)}>
                                <label className="row">
                                    {/*<img src={`/img/avatar.png`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>*/}
                                    {/*{ value = this.props.postOnFocus === this.props.postId ? this.props.writtenComment : ''}*/}
                                    <input onFocus={()=>this.props.commentFocus(this.props.postId,this.props.postOnFocus)}
                                           onChange={(e)=>this.props.writeComment(e,this.props.showComments,this.props.postId,this.props.postOnFocus)}
                                           value={this.props.postOnFocus === this.props.postId ? this.props.writtenComment : ''}
                                           className="post-comment-input float-right" placeholder="Click enter to submit comment"/>
                                    {/*<input type="submit"/>*/}
                                </label>
                            </form>
                        </div>

                        <div id={"tags"} className="col-sm-2 comments-div">
                            <span>
                                <img src={'/img/comment.svg'} alt={'comment'} width="27" height="27" className="comment-icon" style={{color:"darkslategrey"}} onClick={()=>this.props.toggleComments(this.props.postId,this.props.showComments)}/>
                                <span className="total-comm ml-2" style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>{this.props.totalComments}</span>
                            </span>
                        </div>

                    </div>
                </div>

                <div className='col-sm-12' id="comment-wrapper">
                    {comments}
                </div>

                {this.props.likersModal ? <Likers postId={this.props.postToShowLikes} activeUser={this.props.activeUser.username}/> : null}
                {/*<div className="modal" id="likesModal">
                    <div className="modal-dialog modal-sm modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6>Likes</h6><button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div className="modal-body d-flex flex-column justify-content-center">
                                {likers}
                            </div>
                        </div>
                    </div>
                </div>*/}

           </div>
        );
    }
}

const MapState = (state)=>{
    return{
        activeUser : state.User.user_info,
        likers : state.Posts.likers,
        likersModal : state.Posts.likerModal,
        toggleOptions : state.Posts.toggleOptions,
        postToToggle : state.Posts.postToToggle,
        //Edit props
        enableEdit : state.Posts.enableEdit,
        postToEdit: state.Posts.postToEdit,
        newCaption : state.Posts.newCaption,
        //comments prop
        postOnFocus : state.Comments.focusPostId,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        writtenComment : state.Comments.comment,

        showMessage : state.Posts.showMessage,
        postMessageId : state.Posts.postId
    }
};

const MapDispatch = (dispatch)=>{
    return{
            comments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
            commentFocus : (postId,prevFocus) =>{dispatch(commentFocus(postId,prevFocus))},
            writeComment : (e,comments,postID,prevToggledPost) => {dispatch(writeComment(e.target.value,comments,postID,prevToggledPost))},
            likesModal : (id) => {dispatch(LikesModal(id))},//{type:"LIKERS_MODAL",post:id});postLikers(id)
            //postLikers : (postId)=>{dispatch(postLikers(postId))},
            postMessage : (post) => {dispatch(postInfoIcon(post))},
            options : (postId) => {dispatch(postOptions(postId))},
            delete : (postId,petId,page) => {dispatch(deletePost(postId,petId,page))},
            edit : (post,caption) => {dispatch(switchEdit(post,caption))},
            captionEdit : (e) => {dispatch(captionEdit(e.target.value))},
            submitEdit : (caption,id,page) => {dispatch(submitEdit(caption,id,page))},
            hidePost : (postId,petId,page) =>{dispatch(hidePost(postId,petId,page))},
            // likePost : (id) => {likePost(id,null)}
    }
};
export default connect(MapState,MapDispatch)(ShowPosts);
