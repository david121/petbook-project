import React, {Component} from 'react';
import './Css/SlideShow.css'
import {currentSlide, TotalPics} from "./STORE/SlideShowActions";
import {connect} from "react-redux";
class SlideShow extends Component {

    componentDidMount() {
        // console.log("slideShow",this.props.postPic)
        // console.log("post",this.props.post)

    }

    render() {
        let slide;
        let slideShow;
        let prev; let next;let image;
        let slideControl = '';
        if(this.props.numOfPics === 1){

            slide = this.props.postPic.map((item,index)=>{
                if(item.path.match(/.(mp4|3gp|ogg|wmv|webm|mov)$/i)){
                    return <div key={index} className="mySlides">
                        <video loop={true} autoPlay muted className="video" width="500" height="281" controls={true} playsInline={true}>
                            <source src={`/storage/pets/${this.props.petId}/${item.path}`} type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
                            <source src={`/storage/pets/${this.props.petId}/${item.path}`} type='video/webm;codecs="vp8, vorbis"'/>
                            <source src={`/storage/pets/${this.props.petId}/${item.path}`} type="video/mov"/>
                            <source src={`/storage/pets/${this.props.petId}/${item.path}`} type="video/3gp"/>
                            Your browser does not support HTML5 video
                        </video>
                    </div>
                }
                return <div key={index} className="mySlides">
                    <img src={`/storage/pets/${this.props.petId}/${item.path}`} alt={"post photo"} className="post-photo"/>
                </div>
            })
        }else{
            this.props.total(this.props.numOfPics);
                slide = this.props.postPic.map((item,index)=>{

                    if (this.props.currentIndex === index+1){
                        if(item.path.match(/.(mp4|3gp|ogg|wmv|webm)$/i)){
                            return <div key={index} className="mySlides">
                                <video loop={true} autoPlay muted className="video" controls={true} playsInline={true}>
                                    <source src={`/storage/pets/${this.props.petId}/${item.path}`} type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
                                    <source src={`/storage/pets/${this.props.petId}/${item.path}`} type='video/webm;codecs="vp8, vorbis"'/>
                                    <source src={`/storage/pets/${this.props.petId}/${item.path}`} type="video/3gp"/>
                                    <source src={`/storage/pets/${this.props.petId}/${item.path}`} type="video/mov"/>
                                    Your browser does not support HTML5 video
                                </video>
                            </div>
                        }
                        return <div key={index} className="mySlides fadeEffect">
                            <div className="numbertext">{this.props.currentIndex} / {this.props.totalPics}</div>
                            <img src={`/storage/pets/${this.props.petId}/${item.path}`} alt={"post photo"} className="post-photo"/>

                        </div>;
                    }

                    prev =<a className="prev" onClick={()=>this.props.newSlide(-1,item.post_id)}>&#10094;</a>;
                    next =<a className="next" onClick={()=>this.props.newSlide(1,item.post_id)}>&#10095;</a>;

                });
        }
        return (
            <div className="slideShowContainer">
                  {slide}
                {prev}{next}
            </div>
        );
    }
}

const mapState = (state)=>{
    return {
        totalPics : state.SlideShow.totalPics,
        currentIndex : state.SlideShow.currentIndex,
        postId : state.SlideShow.postId
    }
};

const mapDispatch = (dispatch)=>{
    return {
        total : (pics) => {dispatch(TotalPics(pics))},
        newSlide : (num,postId) => {dispatch(currentSlide(num,postId))}
    }
};

export default connect(mapState, mapDispatch)(SlideShow);
