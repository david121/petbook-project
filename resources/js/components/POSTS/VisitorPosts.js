import React, {Component} from 'react';
import './CSS/ShowPosts.css';
import {connect} from "react-redux";
import {
    postLikers,
    toggleComments,
    postOptions,
    postInfoIcon,
    commentFocus,
    writeComment,
    archivePost, hidePost, LikesModal,
} from "./STORE/Actions";
import {NavLink} from "react-router-dom";
import Comments from "./Comments";
import SlideShow from "./SlideShow";
import PostsInfo from "./PostsInfo";
import Likers from "./Likers";
class VisitorPosts extends Component {

    render() {
        let comments;
        let likers;
        let postPics;
        if (this.props.numOfPics === 0 ){
            postPics = ''
        }else{
            postPics = <SlideShow
                numOfPics = {this.props.numOfPics}
                userId = {this.props.userId}
                petId = {this.props.petId}
                postPic = {this.props.postPic}
            />
        }
        let postBy =this.props.user=== 'pet' ? '' : <i className="postBy">post by : <NavLink to={`/pet-view/${this.props.petId}`} className="pet-link">
                {
                    this.props.petPhoto === null ? <img src={"/img/rodent.svg"} alt={"pet photo"} className="rounded-circle mr-1 mb-1" width={"25"} height={"20"}/>:
                        <img src={`/storage/pets/${this.props.petId}/${this.props.petPhoto}`} alt={"pet photo"} className="rounded-circle mr-1 mb-1" width={"25"} height={"20"}/>
                }
                {this.props.pet} </NavLink>
        </i>;


        if (this.props.clickedPost === this.props.postId){// removed this.props.clicked &&

            if (this.props.totalComments === 0 && this.props.writtenComment !== ''){
                comments = <div className="post-comment-prev ml-4 mb-3">
                    <span>
                        {
                            this.props.activeUser.photo_path === null
                                ?
                                <img src={'/img/avatar.png'} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                                :
                                <img src={`/storage/users/${this.props.activeUser.id}/${this.props.activeUser.photo_path}`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>
                        }
                    </span>
                    <span className="the-comm-prev"><span className="mr-2">{this.props.activeUser.username} </span> {this.props.writtenComment}</span>
                </div>
            } else if (this.props.totalComments === 0 && this.props.writtenComment === ''){
                comments = <span className="text-center first-comment">Write the first comment for this post</span>
            }else
                comments = <Comments
                    postId = {this.props.postId}
                    allComments={this.props.totalComments}// existing comments on post (number)
                    commentSubmit = {(e)=>this.props.submitComment(e,this.props.writtenComment,this.props.postId)}
                    page={this.props.commentLike}
                    visitId = {this.props.petId}
                />;
          /*  comments = this.props.totalComments === 0 ? <span className="text-center">Be the first to comment</span> : <Comments
                postId = {this.props.postId}
                commentHandler = {this.props.commentHandler}
                comment={this.props.comment}
                allComments={this.props.totalComments}
                commentSubmit = {this.props.commentSubmit}
            />;*/
        }

        let likeIcon = <img src={'/img/paw.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.like(this.props.postId,this.props.userId,this.props.petId)}/> ;
        if(this.props.liked !== 0){
            likeIcon = <img src={'/img/paws.svg'} className="likeIcon" alt="like" width="30" height="30" onClick={()=>this.props.like(this.props.postId,this.props.userId,this.props.petId)}/>
        }

        if (this.props.likers === "None"){
            likers = <span className="text-center">No likes on this post</span>;
        }else {
            likers = this.props.likers.map((liker,index)=>{
                let photo = liker.photo_path === null ?<img src={"/img/avatar.png"} alt={"liker"} className="rounded-circle  mr-2" width="20" height="15"/>:
                    <img src={`/storage/users/${liker.id}/${liker.photo_path}`} alt={"liker"} className="rounded-circle  mr-2" width="20" height="15"/>


                return <li key={index} className="text-left">
                        {photo}
                    {liker.username}
                </li>
            });
        }

        let postDropdown;
        if(this.props.toggleOptions && this.props.postToToggle === this.props.postId){
            postDropdown=<ul className="post-option-drop text-left">
                <li onClick={()=>this.props.addToArchive(this.props.postId)}>Add to archive</li>
                <li>Share Post</li>
            </ul>;
        }
        let src = this.props.userPhoto === "None" ? '/img/avatar.png' : this.props.userPhoto;
        let profilePhoto = this.props.user === 'petOwner' ? <img src={src} alt={"profile photo"} className="rounded-circle" width='50' height="40"/>
        : this.props.petPhoto === null ? <img src={"/img/rodent.svg"} alt={"profile photo"} className="rounded-circle" width='50' height="40"/> :
                <img src={`/storage/pets/${this.props.petId}/${this.props.petPhoto}`} alt={"profile photo"} className="rounded-circle" width='50' height="40"/>


        let postInfo = this.props.userId !== this.props.post_owner ?  <i className="fas fa-info-circle post-info-icon ml-3" onClick={()=>this.props.postMessage(this.props.postId)}/> : '';
        let message = "Post was made by ";

        return (
            <div className="card mb-4" id='post'>

                <div className="card-header post-header">
                    <span>
                        {profilePhoto}
                        <span className="ml-4 username">
                            {this.props.username}
                        </span>
                    </span>

                    {postInfo}
                    <span className="ml-2">{this.props.showMessage && this.props.postMessageId === this.props.postId ? <PostsInfo id={this.props.post_owner} userId={this.props.activeUser} message={message} username={this.props.post_owner_username}/> : ''}</span>

                    <div className="float-right" >{/*onClick={(e)=>{e.stopPropagation();this.props.options(this.props.postId)}}*/}
                        <img src={'/img/more.svg'} alt={'options'} height={'20'} width={'20'}
                             className="post-options"
                             onClick={(e)=>{e.stopPropagation();this.props.options(this.props.postId)}}/>
                        {postDropdown}
                    </div>
                    <div className="ml-5 post-time">
                        <small className="text-muted col-sm-12" style={{fontFamily:"Cambria"}}> {this.props.time}</small>
                    </div>

                </div>

                <div className="card-body post-body">
                    <span className={"caption"}>
                        {this.props.caption}
                    </span>
                    <span className={"post-photo-wrapper mt-4"}>
                        {postPics}
                    </span>
                </div>

                <div className="card-footer post-footer">
                   { postBy}
                    <div className="mt-4 reactions text-center row">

                        <div className="col-sm-2 likes">
                            <span id={"like"}>
                                {likeIcon}
                                <span className="likers ml-2" data-toggle="modal" data-target="#likesModal"
                                      onClick={()=>this.props.likesModal(this.props.postId)}
                                      style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>
                                    {this.props.totalLikes}
                                </span>
                            </span>
                        </div>


                        <div className="col-sm-8 comment-space">
                            <form onSubmit={e=>this.props.submitComment(e,this.props.writtenComment,this.props.postId)}>
                                <label className="row">
                                    {/*<img src={`/img/avatar.png`} alt={"user"} width={"30"} height={"32"} className="rounded-circle mr-2"/>*/}
                                    <input onFocus={()=>this.props.commentFocus(this.props.postId,this.props.postOnFocus)}
                                           onChange={(e)=>this.props.writeComment(e,this.props.showComments,this.props.postId,this.props.postOnFocus)}
                                           value={this.props.postOnFocus === this.props.postId ? this.props.writtenComment : ''}
                                           className="post-comment-input float-right" placeholder="Click enter to submit comment"/>
                                </label>
                            </form>
                        </div>


                        <div id={"tags"} className="col-sm-2 comments-div">
                            <span>
                                <img src={'/img/comment.svg'} width="27" height="27" className="comment-icon" style={{color:"darkslategrey"}} onClick={()=>this.props.toggleComments(this.props.postId,this.props.showComments)}/>
                                <span className="total-comm ml-2" style={{fontWeight:"bolder", color:"darkslategrey",position:"relative", top:"2px", fontSize:"19px"}}>{this.props.totalComments}</span>
                            </span>
                        </div>

                    </div>
                </div>

                <div className='col-sm-12' id="comment-wrapper">
                    {comments}
                </div>
                {this.props.likersModal ? <Likers postId={this.props.postToShowLikes} activeUser={this.props.activeUser.username}/> : null}
            </div>
        );
    }
}

const MapState = (state)=>{
    return{
        likers : state.Posts.likers,
        likersModal : state.Posts.likerModal,
        toggleOptions : state.Posts.toggleOptions,
        postToToggle : state.Posts.postToToggle,
        showMessage : state.Posts.showMessage,
        postMessageId : state.Posts.postId,
        postOnFocus : state.Comments.focusPostId,
        writtenComment : state.Comments.comment,
        clickedPost : state.Comments.clickedPost,
        showComments : state.Comments.showComments,
        activeUser : state.User.user_info,
    }
};

const MapDispatch = (dispatch)=>{
    return{
        // postLikers : (postId)=>{dispatch(postLikers(postId))},
        likesModal : (id) => {dispatch(LikesModal(id))},//{type:"LIKERS_MODAL",post:id});postLikers(id)
        options : (postId) => {dispatch(postOptions(postId))},
        postMessage : (post) => {dispatch(postInfoIcon(post))},
        commentFocus : (postId,prevFocus) =>{dispatch(commentFocus(postId,prevFocus))},
        writeComment : (e,comments,postID,prevToggledPost) => {dispatch(writeComment(e.target.value,comments,postID,prevToggledPost))},
        toggleComments : (index,toggled)=>{dispatch(toggleComments(index,toggled))},
        addToArchive : (postId) =>{dispatch(archivePost(postId))},
    }
};
export default connect(MapState,MapDispatch)(VisitorPosts);
