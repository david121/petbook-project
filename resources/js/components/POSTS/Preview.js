import React, {Component} from 'react';
import {addPost, closePreview, postBy, removePic, selectPet, showAddPet} from "./STORE/Actions";
import {connect} from "react-redux";
import './CSS/Preview.css'
import {NavLink} from "react-router-dom";
import PreviewSlideShow from "./PreviewSlideshow";
import AddPet from "../USER PROFILE/AddPet";
import Add from "./PET/Add";
import {fetchUser} from "../USER PROFILE/STORE/Actions";
import ScrollContainer from "react-indiana-drag-scroll";
class Preview extends Component {

componentDidMount() {
}
componentDidUpdate(prevProps, prevState, snapshot) {
    // console.log(this.props.pets)
}

    render() {
        let user = this.props.user;
        let postPreview = '';
        let photoPrev = "";
        let numberOfPets = this.props.numOfpets;
        let postByToggle;
        let pets ;
        let petId;
        let selectPet;
        let addPetModal;


        if (this.props.poster === 'user'){ //if post is from user page
            if (numberOfPets === 0 ){
                // Ask user to add a pet before their post can be submitted (action dispatched on post submit)
                petId = null;
            }
            else
            if (numberOfPets === 1 ){
                // insert post into that one pet's page
                postByToggle = this.props.pets.map((pet,index)=>{
                    petId = pet.id;
                    return  <i key={index} className="postBy">post by :
                        <span className="pet-select" >
                            {
                                pet.profile_photo === null ?
                                    <img key={index} src={"/img/rodent.svg"} alt={"pet photo"} onClick={()=>this.props.postBy(pet.id)} className="rounded-circle pet-image post-by-pic" width={"25"} height={"20"}/>
                                    :
                                    <img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt="profile photo" className="rounded-circle ml-1" width={"25"} height={"20"}/>
                            }
                                 {/*<img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt="profile photo" className="rounded-circle ml-1" width={"25"} height={"20"}/>*/}
                                  <NavLink className="pet-select pet-link ml-1" to={`/pet/${pet.id}`}>{pet.name}</NavLink>
                                </span>
                    </i>
                })
            }
            else
            if (numberOfPets > 1){
                //ask user to select which pet, the post belongs to
                petId = this.props.postingPetId;
                if(this.props.postingPetId !== ''){ //if user have selected a pet, the selected pet makes the post
                    let postOwner = this.props.pets.filter(pet=>{
                        return pet.id === this.props.postingPetId
                    });
                    // console.log(postOwner)
                    postByToggle = postOwner.map((pet,index)=>{
                            return  <i key={index} className="postBy">post by :
                                <span className="pet-select" onClick={(e) => {e.stopPropagation(); this.props.showSelect();}}>
                                    {
                                        pet.profile_photo === null ?
                                            <img key={index} src={"/img/rodent.svg"} alt={"pet photo"} onClick={()=>this.props.postBy(pet.id)} className="rounded-circle ml-1" width={"25"} height={"20"}/>
                                            :
                                            <img src={`/storage/pets/${pet.id}/${pet.profile_photo}`} alt="profile photo" className="rounded-circle ml-1" width={"25"} height={"20"}/>
                                    }
                                  <u className="pet-select pet-link ml-2">{pet.name}</u>
                                </span>
                            </i>
                    })
                }else{ //if user haven't selected a pet, prompt user to select one of their pets
                    postByToggle = <div>
                        <i className="postBy">post by : <b className="text-danger selectPet" onClick={(e) => {e.stopPropagation(); this.props.showSelect();}} >select pet</b> </i>
                    </div>;
                }

                pets = this.props.pets.map((pet,index)=>{
                    // console.log(pet)
                    return <label key={index} className="mr-5">
                        {
                            pet.profile_photo === null ?
                                <img key={index} src={"/img/rodent.svg"} alt={"pet photo"} onClick={()=>this.props.postBy(pet.id)} className="rounded-circle pet-image post-by-pic" width="130" height="110"/>
                                    :
                                <img key={index} src={this.props.petPhoto[index]} alt={"pet photo"} onClick={()=>this.props.postBy(pet.id)} className="rounded-circle pet-image post-by-pic" width="130" height="110"/>

                        }
                        {/*<img key={index} src={this.props.petPhoto[index]} alt={"pet photo"} onClick={()=>this.props.postBy(pet.id)} className="rounded-circle pet-image post-by-pic" width="130" height="110"/>*/}
                        <div className="text-center" style={{fontWeight:"bold", color: "darkslategrey"}}>{pet.name}</div>
                    </label>
                })
            }
        }else{// else poster ID is posting-pet ID
            petId = this.props.id;
            let photo = this.props.photo === null ? <img src={`/img/avatar.png`} alt={"user"} className="rounded-circle ml-1 mr-1" width={"25"} height={"20"}/>:
                <img src={`/storage/users/${this.props.activeUser}/${this.props.photo}`}  alt={"user"} className="rounded-circle ml-1 mr-1" width={"25"} height={"20"}/>;
            postByToggle = <i className="postBy">pet owner: <NavLink to={`/profile`} className="pet-link">
                {photo}
                {this.props.activeUserName} </NavLink>
            </i>;
        }

        if (this.props.toggleSelect){
            selectPet = <div id="choosePet" className="selectPetModal">
                <div className="selectPet-content" onClick={e => e.stopPropagation()}>
                    <div className="selectPet-header">
                        <span className="close" onClick={(e) => {e.stopPropagation(); this.props.showSelect();}}>&times;</span>
                        <h5>Select Pet</h5>
                    </div>
                    <div className="selectPet-body">
                        <ScrollContainer className="scroll-container">
                            {pets}
                        </ScrollContainer>
                    </div>
                    <div className="selectPet-footer">
                        <h3> </h3>
                    </div>
                </div>
            </div>
        }else {selectPet = ''}

        if (this.props.showAddPetModal){
            addPetModal =
                    <div className="" onClick={e => e.stopPropagation()}>
                      <AddPet fromPostAdd={true} close={this.props.closeAddPet}/>
                    </div>

        } else addPetModal = '';

        //Preview//
        let postOwnerImage;
        if (this.props.poster==="pet"){
            if (this.props.pet.profile_photo === null){
                postOwnerImage = <img src={'/img/rodent.svg'} alt={"pet"} className="rounded-circle" width='40' height="40"/>
            }else{
                postOwnerImage = <img src={`/storage/pets/${this.props.id}/${this.props.pet.profile_photo}`} alt={"pet"} className="rounded-circle" width='70' height="50"/> ;
            }
        }else{
            if (this.props.user_.photo_path === null){
                postOwnerImage = <img src={'/img/avatar.png'} alt={"pet"} className="rounded-circle" width='40' height="40"/>
            }else{
                postOwnerImage = <img src={`/storage/users/${this.props.user_.id}/${this.props.user_.photo_path}`} alt={"pet"} className="rounded-circle" width='70' height="50"/> ;
            }
        }
        if (this.props.preview_files.length > 0){
            photoPrev = <PreviewSlideShow files ={this.props.preview_files} removePic={this.props.removePic}/>
        }

        if (this.props.caption !== '' || this.props.preview_files.length > 0) {
            postPreview = <div className="card mb-2 mt-2" id='post-preview'>

                <div className="card-header post-header">
                    <span className="float-right close-preview" onClick={this.props.closePreview}><img
                        src={"/img/close.png"} alt={"close"} width={"25"}/></span>
                    <div className="text-center"><small><b id="post-card-preview">POST PREVIEW</b></small></div>
                    <span>
                        {postOwnerImage}
                        <span className="ml-4 username">{user}</span>
                    </span>
                </div>

                <div className="card-body post-body">
                    <span className={"caption"}>
                        {this.props.caption}
                    </span>
                    {photoPrev}
                </div>

                <div className="card-footer post-footer">
                    {postByToggle}
                    <label className="float-right">
                        <img src={'/img/send.png'} id="submitPost" alt={"upload photo"}
                             onClick={e=>this.props.submitPost(e,petId,this.props.caption,this.props.photos,this.props.page==="HOME" ? this.props.page : this.props.poster)} width="50"/>
                    </label>
                </div>
            </div>;
        }

        return (
            <div>
                {postPreview}
                {addPetModal}
                {selectPet}
            </div>
        );
    }
}
const MapState = (state)=>{
    return {
        preview_files : state.Posts.media_preview,
        //Post props
        caption : state.Posts.caption,//written caption
        photos : state.Posts.photos,//photo to be uploaded
        //photo_preview : state.Posts.preview_photos,//preview of photo to be uploaded (Blob Obj)
        toggleSelect : state.Posts.selectPet,// Show/ Hide select pet modal
        postingPetId : state.Posts.postBy,// Pet (id) which post belongs to
        showAddPetModal : state.Posts.showAddPet, // Boolean
    }
};

const MapDispatch = (dispatch) =>{
    return {
        submitPost : (e,id,caption,files,page) =>{
            // console.log(id);
            e.preventDefault();
            e.stopPropagation();
            dispatch(addPost(id,caption,files,page))
        },
        removePic : () =>{dispatch(removePic())},
        closePreview : ()=>{dispatch(closePreview())},
        showSelect : ()=>dispatch(selectPet()),
        closeAddPet : ()=>dispatch(showAddPet()),
        postBy : (petId) => {dispatch(postBy(petId))}
    }
};
export default connect(MapState, MapDispatch)( Preview);
