import React, {Component} from 'react';
import {connect} from "react-redux";
import {currentSlide, TotalPics} from "./STORE/SlideShowActions";
import './CSS/Preview.css'

class PreviewSlideShow extends Component {
    componentDidMount() {
        // console.log("slide show", this.props.files )
    }

    render() {
        let filePreview;
        let next;
        let prev;
        if(this.props.files.length === 1){
            filePreview = this.props.files.map((file,index)=>{
                // console.log("Key test",Object.keys(file)[index])
                if (Object.keys(file)[index] === 'photo'){
                    return <span key={index} className={"post-photo-wrapper mt-4"}>
                            <span className="float-right remove-pic" onClick={this.props.removePic}><img src={"/img/close.png"} alt={"close"} width={"25"}/></span>
                            <img src={file.photo} alt="post photo" className={"post-photo"}/>
                        </span>
                }
                    return <span key={index}>
                           <span className="float-right remove-pic" onClick={this.props.removePic}><img src={"/img/close.png"} alt={"close"} width={"25"}/></span>
                        <video key="index" loop={true} autoPlay muted className="video" controls={true}>
                            <source src={file.video} type="video/mp4"/>
                            Your browser does not support video preview
                        </video>
                    </span>
            })

        }else{
            // console.log(this.props.files.length)
            this.props.total(this.props.files.length);
            filePreview = this.props.files.map((file,index)=>{
                if (this.props.currentIndex === index+1){

                    if (Object.keys(file)[index] === 'photo'){
                        return <span key={index} className={"post-photo-wrapper mt-4"}>
                            <span className="float-right remove-pic" onClick={this.props.removePic}><img src={"/img/close.png"} alt={"close"} width={"25"}/></span>
                            <img src={file.photo} alt="post photo" className={"post-photo"}/>
                        </span>
                    }else if(Object.keys(file)[index] === 'video'){
                    return <span>
                           <span className="float-right remove-pic" onClick={this.props.removePic}><img src={"/img/close.png"} alt={"close"} width={"25"}/></span>
                        <video key="index" loop={true} autoPlay muted className="video" controls={true}>
                            <source src={file.video} type="video/mp4"/>
                            Your browser does not support video preview
                        </video>
                    </span>
                }
                }
            });
            prev = <a className="prev" onClick={()=>this.props.newSlide(-1)}>&#10094;</a>;
            next = <a className="next" onClick={()=>this.props.newSlide(1)}>&#10095;</a>;
        }

        return (
            <div>
                {filePreview}
                {prev}{next}
            </div>
        );
    }
}

const mapState = (state) =>{
    return{
        totalPics : state.SlideShow.totalPics,
        currentIndex : state.SlideShow.currentIndex
    }
};

const mapDispatch = (dispatch) =>{
    return{
        total : (pics) => {dispatch(TotalPics(pics))},
        newSlide : (num) => {dispatch(currentSlide(num))}
    }
};

export default connect(mapState,mapDispatch)(PreviewSlideShow);
