import React, {Component} from 'react';
// import {fileHandler, nextHandler, petQuestions, petSubmitHandler, prevHandler} from "../AUTHENTICATION/STORE/Actions";
import {connect} from "react-redux";
import {fetchPetTypes} from "../../AUTHENTICATION/REGISTER PET/STORE/Actions";
import {fileHandler, nextHandler, petQuestions, prevHandler} from "../../AUTHENTICATION/STORE/Actions";
import {addNewPet} from "../STORE/Actions";

class AddPet extends Component {
    componentDidMount() {
        this.props.fetchTypes();
    }

    render() {
        let ind = this.props.questionIndex;
        let input ;//to be input element to answer question
        let next;// to be button to load next question
        let previous;//to be button to load previous question
        let btnMode ; // next button disabled or not

        // loop through and render questions
        const question = this.props.question.map((item,index)=>{
            return <h6 key={index} className="question">{item.q}</h6>
        });
        let types = this.props.pet_types.map((type,index)=>{
            return <option className="select-options" key={index} value={type.id}>{type.name}</option>
        });
        let breeds = this.props.pet_breeds.map((breed,index)=>{
            return <option className="select-options" key={index} value={breed.id}>{breed.name}</option>
        });
        //inputs for questions
        if (ind === 0){
            input = <select name="petType" className="text-center answerInput" onChange={(e)=>this.props.answer(e,"pet_type")}>
                <option value="">Choose Type</option>
                {types}
            </select>
        }else
        if (ind === 1){
            input = <input type="text" onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 2){
            input = <select name="petBreed" className="text-center answerInput" onChange={(e)=>this.props.answer(e,"pet_breed")}>
                <option value="">Choose Breed</option>
                {breeds}
            </select>
        }else
        if (ind === 3){
            input = <select name="gender" className="text-center answerInput" onChange={this.props.answer}>
                <option value=""> </option>
                <option value="m">Male</option>
                <option value="f">Female</option>
            </select>
        }else
        if (ind === 4){
            input = <input type="date" onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 5){
            input = <input type="text" onChange={this.props.answer} className="answerInput"/>
        }else
        if (ind === 6){
            input = <input type="file" onChange={this.props.handleFile} className=""/>
        }

        //button disabled if not answered
        btnMode = !this.props.answered;

        // Next / Submit Button render
        if (ind + 1 !== this.props.question.length){
            next = <button type="button" disabled={btnMode} onClick={this.props.nextQuestion}>Next</button>
        }else
        if (ind + 1 === this.props.question.length) {
            next = <button type="submit" disabled={btnMode} onClick={null}>complete registration</button>;
        }

        //Previous Btn render
        if (ind === 0){
            previous = <button type="button"  disabled={true} onClick={this.props.prevQuestion}>Previous</button>
        }else previous = <button type="button" onClick={this.props.prevQuestion}>Previous</button>;


        const formData = new FormData();
        formData.append('user_id', sessionStorage.getItem('user'));
        formData.append('name', this.props.name);
        formData.append('gender', this.props.gender);
        formData.append('type', this.props.type);
        formData.append('breed', this.props.breed);
        formData.append('age', this.props.age);
        formData.append('username', this.props.username);
        formData.append('photo', this.props.file);
        return (
            <div>
                <form onSubmit={(e)=>this.props.handleSubmit(e, formData)}>
                    <div className="pet-question-wrapper">{question[this.props.questionIndex]}</div>

                    <div className="questionInput-box">{input}</div>

                    <div className="question-control">{previous} {next}</div>
                </form>
            </div>
        );
    }
}


const mapState = (state) =>{
    return {
        question : state.PetReg.questions,
        questionIndex : state.PetReg.currentQues,
        pet_types :state.PetReg.pet_types,
        pet_breeds : state.PetReg.pet_breeds,
        //Was question answered?
        answered :state.PetReg.answered ,
        //pet details
        type: state.PetReg.type,
        breed: state.PetReg.breed,
        age : state.PetReg.age,
        name : state.PetReg.name,
        gender : state.PetReg.gender,
        file : state.PetReg.file,
        photo: state.PetReg.photo,
        username: state.PetReg.username,
    }
};

const mapDispatch = (dispatch)=>{
    return{
        fetchTypes : ()=> {dispatch(fetchPetTypes())},
        answer : (e,name) => {dispatch(petQuestions(e.target.value,name))},
        nextQuestion : () =>{dispatch(nextHandler())},
        prevQuestion : () => {dispatch(prevHandler())},
        handleFile : (e) => {dispatch(fileHandler(e.target.files[0]))},
        handleSubmit: (e,data) => {
            e.preventDefault();
            dispatch(addNewPet(data))
        }
    }
};
export default connect(mapState,mapDispatch)(AddPet);
