<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Friend Request</title>
    <style>
        body{
            /*background-color: darkslategray;*/
        }
        .container{
            /*height: 90vh;*/
            width: 100%;
            /*display: flex;*/
            /*flex-direction: column;*/
            /*justify-content: center;*/
            /*align-items: center;*/
        }
        .card-header button{
            /*background-color: darkslategray;*/
            /*color: white;*/
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;

        }
        .btn-wrap{
            width: 100%;
            text-align: center;
            padding: 20px;
        }
        .btn-wrap a{
            padding: 10px;
            width: 100px;
            background-color: darkslategray;
            color: white;
            cursor: pointer;
            text-decoration: none;
        }
        .btn-wrap a:hover{
            background-color: darkcyan;
        }
        .copyright{
            width: 100%;
            font-size: 13px;
            /*color: white;*/
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            text-align: center;
            margin-top: 15px;
        }
        .site-logo{
            width: 100%;
            padding: 15px;
        }
    </style>
</head>
<body>
<div class="container text-center">
    <div class="site-logo">
        <img alt="logo" src={{asset('/img/cat-dog.svg')}} alt="pets" width="40" height="40">
        {{--        <span>Pawrry</span>--}}
    </div>
    <div class="card">
        <div class="card-header">
            <h4>Hello {{$user->receiver}},</h4> <br>
            <span><b>{{$user->petName}}</b>  has a new admirer.</span><br>
            <div class="btn-wrap">
                <a href="https://pawrry.com/pet/{{$user->petId}}">View Update</a>
            </div>
            <div>
                <p>
                    Thanks for using Pawrry. <br><br>

                    Regards,<br/>
                    Pawrry
                </p>
            </div>
        </div>
    </div>
    <div class="copyright">
        <span>&copy; 2020 Pawrry. All rights reserved.</span>
    </div>
</div>
</body>
</html>