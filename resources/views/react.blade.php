<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pawrry</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="icon" href="{{asset('./img/cat-dog.svg')}}">
{{--    <script src="https://kit.fontawesome.com/cd69b355ce.js" crossorigin="anonymous"> </script>--}}
</head>
<body>

<div id="root"></div>
<script rel="script" src="{{asset('./js/app.js')}}"></script>
{{--<script src="https://www.paypal.com/sdk/js?client-id=AXx0cptImTP-kci1D7ablBrEYwNzw0WfuB8MLBkBOWreL6Qcq_m_FTI-McNCPGoyl9Rni3-hxMsvzb_U"> </script>--}}
</body>
</html>
